import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';

const APP_ROUTES:Routes =[

    {path:'login',component:LoginComponent},

    { path: '**', component: LoginComponent } 


]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
