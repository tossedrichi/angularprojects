import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NbEvaIconsModule } from '@nebular/eva-icons';

import {
  NbSidebarModule,
  NbLayoutModule,
  NbButtonModule,
  NbInputModule,
  NbMenuModule,
  NbActionsModule,
  NbIconModule

} from "@nebular/theme";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NbSidebarModule,
    NbLayoutModule,
    NbButtonModule,
    NbInputModule,
    NbMenuModule,
    NbActionsModule,
    NbIconModule,
    NbEvaIconsModule
  ],
  exports: [
    NbSidebarModule,
    NbLayoutModule,
    NbButtonModule,
    NbInputModule,
    NbMenuModule,
    NbActionsModule,
    NbIconModule,    
    NbEvaIconsModule

  ]
})
export class NebularModule {}
