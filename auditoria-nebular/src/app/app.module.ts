import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { NebularModule } from './nebular.module';
import { APP_ROUTING } from './app.routes';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarService } from '@nebular/theme';
import { AppRoutingModule } from './app-routing.module';
import { NbMenuInternalService, NbMenuService } from '@nebular/theme/components/menu/menu.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    NebularModule,
    HttpClientModule,
    APP_ROUTING,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    AppRoutingModule
  ],
  providers: [
    NbSidebarService,
    NbMenuInternalService,
    NbMenuService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
