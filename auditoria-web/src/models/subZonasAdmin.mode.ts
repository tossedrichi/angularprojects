export class SubZonasList{
    subZonasAsignadas:Array<SubZona>;
    subZonasDisponibles:Array<SubZona>;
}

export class SubZona{
    agente: number;
    idOrigen: number;
    idSubZona: number;
    idSubZonaZonaSubadmin: number;
    idZonaSubadministrador: number;
    id_Area: number
    nombreSubZona: string;
    nombreZona: string;
    selected:boolean;
}

