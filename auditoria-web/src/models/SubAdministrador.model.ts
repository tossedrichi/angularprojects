export class SubAdministradorModel {
  agente: number;
  idOrigen: number;
  idPersona?: number;
  idSubadministrador?: number;
  correo?: string;
  nombre: string;
  porcentajeAutomaticas:number;

  constructor(
    agente: number,
    idOrigen: number,
    idPersona?: number,
    idSubadministrador?: number,
    correo?: string,
    nombre?: string,
    porcentajeAutomaticas?:number

  ) {}
}
