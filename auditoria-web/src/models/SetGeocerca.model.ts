export class SetGeocercaModel {
  idGeocerca: number;
  coordenadas: string;
  cedis: number;
  color1: string;
  color2: string;
  idOrigen: number;
  agente: number;
  idSucursal: number;
}
