export class Sucursal {
  idSucursal: number;
  nombreSubZona: string;
  nombreSucursal: string;
  nombreZona: string;
  selected:boolean;
  variableSucursal:any;
  edicionHabilitada:boolean;
  constructor() {}
}
