export class AuditoriaCreada {
  estado: string;
  fechaProgramada: string;
  idAsignacion: number;
  idAuditoria: number;
  nombreAuditoria: string;
  frecuencia:string;
}


