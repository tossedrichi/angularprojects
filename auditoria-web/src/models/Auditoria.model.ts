import { Sucursal } from './Sucursal.model';
import { SubZona } from './subZonasAdmin.mode';
import { FileItem } from './file-item.model';

export class Auditoria {
  idAuditoria:number;
  idSubadministrador: number;
  nombreAuditoria: string;
  idFrecuencia: number;
  frecuencia:string;
  idOrigen: number;
  idArea: number;
  area:string
  subZonas: SubZona[];
  variables: Varibale[];

  
  constructor(  nombreAuditoria: string,idFrecuencia: number,idArea: number,
    subZonas: SubZona[],variables: Varibale[]) {

    this.nombreAuditoria= nombreAuditoria;
    this.idFrecuencia= idFrecuencia;
    this.idArea= idArea;
    this.subZonas= subZonas;
    this.variables= variables;
  }

}


export class Varibale {

  idVariable:number;
  idVariableNueva:number;
  nombreVariable: string;
  definicion: string;
  requiereEvidencia: boolean;
  ponderacion: number;
  todasLasSucursales: boolean;
  sucursales:Sucursal[];
  imagenes:FileItem[];
  calificacion: number;
  comentario: string;
  idAsignacion: number;
  idDetalleAsignacion: number;


  constructor(nombreVarable:string,definicio:string,requiereEvidencia:boolean,ponderacion:number,todasSuc:boolean,sucursales:Sucursal[],imagenes:FileItem[],idVariable?:number) {
    this.idVariable=idVariable;
    this.nombreVariable=nombreVarable;
    this.definicion=definicio;
    this.requiereEvidencia=requiereEvidencia;
    this.ponderacion=ponderacion;
    this.todasLasSucursales=todasSuc;
    this.sucursales=sucursales;
    this.imagenes=imagenes;
  }

  
}
