export class AsigancionAuditoria {
  idAsignacion: number;
  idAuditorSubadministrador: number;
  idAuditoria: number;
  idSucursal: number;
  fechaProgramacion: string;
  agente: number;
  idOrigen: number;
  nombreSucursal: string;
  constructor() {}
}

export class AuditoriaAsignada {
  estado: string;
  fechaProgramacion: string;
  idAsignacion: number;
  nombreAuditor: string;
  nombreAuditoria: string;
  nombreSucursal: string;
  calificacion:number;

  constructor() {}
}
