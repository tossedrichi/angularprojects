export class ZonasSubAdministrador {
    zonasAsignadas:Zona[];
    zonasDisponibles:Zona[];
}

export class Zona {
  agente: number;
  idOrigen: number;
  idSubadministrador: number;
  idZona: number;
  idZonaSubadmin: number;
  nombreZona: string;
}
