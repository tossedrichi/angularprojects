export class FileItem{
    public idImagen:number;
    public archivo:File;
    public nombreArchivo:string;
    public base64:string;
    public tipoDocumento:number;
    public idStream:string;
    public manejar:number;
    
    constructor(archivo:File,base64:string){
        this.archivo=archivo;
        this.nombreArchivo=archivo.name;
        this.base64=base64;
        this.tipoDocumento=1;
        this.manejar=0;
    }

}

