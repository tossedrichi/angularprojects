import { Varibale } from 'src/models/Auditoria.model';

export class DetalleAsignacionModel {
    calificacion: number;
    estado: string;
    fecha: string;
    idAsignacion: number;
    nombreAuditor: string;
    nombreAuditoria: string;
    nombreSubZona: string;
    nombreSucursal: string;
    nombreZona: string;
    variables: Varibale[];

}

