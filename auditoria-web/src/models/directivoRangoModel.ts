export class DirectivosRangoModel {
  directivosAsignados: Array<DirectivoRango>;
  directivosDisponibles: Array<DirectivoRango>;
}

export class DirectivoRango {
  correo: string;
  idDirectivo: string;
  idDirectivoArea: number;
  idPersona: number;
  idRangoDirectivo: number;
  nombre: string;
}
