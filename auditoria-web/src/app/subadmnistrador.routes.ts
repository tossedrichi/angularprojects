import { Routes } from '@angular/router';
import { AuditoresComponent } from './components/auditores/auditores.component';
import { SubadminGuard } from './guards/subadmin.guard';
import { AuditoriasComponent } from './components/auditorias/auditorias.component';
import { AsigAuditoriasComponent } from './components/asig-auditorias/asig-auditorias.component';
import { AuditoriasPeriodoComponent } from './components/auditorias-periodo/auditorias-periodo.component';

export const SUBADMINISTRADOR_ROUTES: Routes = [
    { path: 'auditores', component: AuditoresComponent, canActivate:[SubadminGuard] },
    { path: 'crearAuditoria', component: AuditoriasComponent, canActivate:[SubadminGuard] },
    { path: 'asignacionAuditorias', component: AsigAuditoriasComponent, canActivate:[SubadminGuard] },
    { path: 'auditoriasPeriodo', component: AuditoriasPeriodoComponent, canActivate:[SubadminGuard] },
]