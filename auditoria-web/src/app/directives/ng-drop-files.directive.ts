import {
  Directive,
  EventEmitter,
  HostListener,
  Input,
  Output
} from "@angular/core";
import { FileItem } from "src/models/file-item.model";
import { toast } from "materialize-css";
import * as tools from'../../app/tools' 

@Directive({
  selector: "[appNgDropFiles]" 
})
export class NgDropFilesDirective {
  constructor() { }

  @Input() archivos: FileItem[] = [];
  @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter();

  @HostListener("dragover", ["$event"])
  public onDrang(event: Event) {
    this.mouseSobre.emit(true);
  }

  @HostListener("dragleave", ["$event"])
  public onDragLeave(event: Event) {
    this.mouseSobre.emit(false);
  }

  @HostListener("drop", ["$event"])
  public onDrop(event: Event) {
    const transferencia = this._getTransferencia(event);

    if (!transferencia) {
      return;
    }

    this._extraerArchivos(transferencia.files);
    this._prevenirDetener(event);

    this.mouseSobre.emit(false);
  }

  private _getTransferencia(event: any) {
    return event.dataTransfer
      ? event.dataTransfer
      : event.originalEvent.dataTransfer;
  }

  private _extraerArchivos(archivosLista: any) {
    console.log("file", archivosLista);

    for (const propiedad in Object.getOwnPropertyNames(archivosLista)) {
      const archivoTemporal = archivosLista[propiedad];

      if (this.archivoPuedeSerAgregado(archivoTemporal)) {
        let reader = new FileReader();
        console.log(archivosLista[propiedad]);

        reader.readAsDataURL(archivosLista[propiedad]);

        reader.onload = async (event: any) => {
          console.log('event', event);
          console.log(archivosLista[propiedad]);

          let base64 = (<FileReader>event.target).result;



          let orientacionEXIF = tools.getOrinetation(base64);

          if (orientacionEXIF != undefined) {
            tools.resetOrientation(base64, orientacionEXIF, (callback) => {
              console.log("ResetOrientacion", orientacionEXIF);
              console.log(callback);

              const nuevoArchivo = new FileItem(archivoTemporal, callback);
              this.archivos.unshift(nuevoArchivo);
            })
          } else {
            const nuevoArchivo = new FileItem(archivoTemporal, base64.toString());
            this.archivos.unshift(nuevoArchivo);
          }


          /*           EXIF.getData(archivosLista[propiedad], function () {
                      // `this` is provided image, check with `console.log(this)`
                      console.log('EXIF', EXIF.getAllTags(this));
                      orientation = EXIF.getTag(this, 'Orientation')
                      console.log(orientation);
          
                    }); */


          /*         this.resetOrientation(base64, orientation, (callback) => {
                    console.log(orientation);
        
                    console.log('Final', callback);
                    base64 = callback;
                  }) */

          /* EXIF.getData(archivosLista[propiedad], function () {
            console.log('EXIF', EXIF.getAllTags(this));

            let orientation = EXIF.getTag(this, "Orientation");
            console.log('Orientation', orientation);

            this.resetOrientation(base64, 3, (callback) => {

              console.log('Respuesta', callback);

              const nuevoArchivo = new FileItem(archivoTemporal, callback);
              this.archivos.push(nuevoArchivo);
              console.log("nuevo", nuevoArchivo.base64);
            });

          });

 */


        };



      }
    }
    // console.log(this.archivos);
    /* 
    
    reader.onload=(evernt:any)=>{
      console.log(evernt.target.result);
    }  
   console.log(reader.readAsDataURL(archivosLista[0]));
  */
  }

  //Validaciones

  private _prevenirDetener(event: Event) {
    event.preventDefault();
    event.stopPropagation();

  }

  public _archivoYaFueAgregado(nombreArchivo: string): boolean {
    for (const archivo of this.archivos) {
      if (archivo.nombreArchivo === nombreArchivo) {
        toast({
          html: `El Archivo ${nombreArchivo} ya fue Agregado`,
          classes: "blue rounded"
        });
        return true;
      }
    }
    return false;
  }

  public _esImagen(tipoArchivo: string): boolean {
    if (tipoArchivo.startsWith("image")) {
      return true;
    }

    return false;
  }

  public archivoPuedeSerAgregado(archivo: File) {
    if (
      !this._archivoYaFueAgregado(archivo.name) &&
      this._esImagen(archivo.type)
    ) {
      return true;
    }
    return false;
  }

 
}

