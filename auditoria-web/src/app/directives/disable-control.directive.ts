import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[disableControl]'
})
export class DisableControlDirective {

  @Input() set disableControl(condicion:boolean){
    const action = condicion ? 'disable':'enable';
    this.ngControl.control[action]();
  }

  constructor(private ngControl:NgControl) { }

}
