import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { NotFoundComponent } from "./components/share/notFound/notfound.component";
import { MenuAdminComponent } from "./components/menu-admin.component/menu-admin.component";
import { BodyComponent } from "./components/body/body.component";
import { AuthGuardService } from "./guards/auth.guard";
import { ADMINISTRADOR_ROUTES } from "./administrador.routes";
import { SUBADMINISTRADOR_ROUTES } from './subadmnistrador.routes';
import { SubadminGuard } from './guards/subadmin.guard';
import { AdminGuard } from './guards/admin.guard';

const app_routes: Routes = [
  { path: "login", component: LoginComponent },

  {
    path: "administrador/menu",
    component: MenuAdminComponent,

    canActivate: [AuthGuardService,AdminGuard]
  },
  {
    path: "administrador/menu/seccion",
    component: BodyComponent,
    children: ADMINISTRADOR_ROUTES,
    canActivate: [AuthGuardService,AdminGuard]
  },

  {
    path: "subadministrador/menu",
    component: MenuAdminComponent,
    canActivate: [AuthGuardService,SubadminGuard]
  },

  {
    path: "subadministrador/menu/seccion",
    component: BodyComponent,
    children: SUBADMINISTRADOR_ROUTES,
    canActivate: [AuthGuardService,SubadminGuard]
  },

  { path: "**", pathMatch: "full", redirectTo: "login" }
];

export const APP_ROUTING = RouterModule.forRoot(app_routes, { useHash: true });
