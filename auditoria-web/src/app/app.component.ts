import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.services';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{

  title = 'Auditoria Web';

  constructor(private route: Router,
              private _AuthService:AuthService) {
  }
  navBarVisible() {

    //console.log(this.route.url)
/* 
    const url = this.route.url;
    if (url === "/") {
      return false;
    } else {
      return true;
    } */

  }

  
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    //this._AuthService.logout();
  }
}
