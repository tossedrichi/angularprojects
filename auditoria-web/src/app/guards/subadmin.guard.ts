import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import * as crypto from 'crypto-js';
import { keyCryp } from '../services/auth.services';

@Injectable({
  providedIn: 'root'
})

export class SubadminGuard implements CanActivate {


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     
      let typeUser = crypto.AES.decrypt(sessionStorage.getItem('typeUser'),keyCryp).toString(crypto.enc.Utf8);
      console.log(typeUser);
      
      if(typeUser==="subadmin"){
        //console.log("Paso Gard SubAdmin")
        return true;
      }else{
        console.log("Bloqueo Guard SubAdmin");
        return false;
      }
  }
}
