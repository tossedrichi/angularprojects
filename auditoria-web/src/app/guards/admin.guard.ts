import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as crypto from 'crypto-js';
import { keyCryp } from '../services/auth.services';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  
  constructor(private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let typeUser = crypto.AES.decrypt(sessionStorage.getItem('typeUser'),keyCryp).toString(crypto.enc.Utf8);

      if(typeUser==="admin"){
        //console.log("Paso Gard Admin")
        return true;
      }else{
        console.log("Bloqueo Guard Admin");
        return false;
      }
  }
}
