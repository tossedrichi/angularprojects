import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate
} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor() { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    console.log(next);
    

    if (sessionStorage.getItem('sessionActive') == 'true') {
    //  console.info("Paso el Guard")
      return true;
    } else {
      console.error("Bloqueado por el Guard")
      return false;
    }


  }
}
