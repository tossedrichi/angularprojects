import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// Components

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/share/notFound/notfound.component';
import { APP_ROUTING } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './components/share/footer/footer.component';
import { MenuAdminComponent } from './components/menu-admin.component/menu-admin.component';
import { BodyComponent } from './components/body/body.component';
import { BarraLateralComponent } from './components/barra-lateral/barra-lateral.component';
import { AreasComponent } from './components/areas/areas.component';
import { DirectivosComponent } from './components/directivos/directivos.component';
import { ConfigComponent } from './components/config/config.component';
import { SubadminComponent } from './components/subadmin/subadmin.component';
import { ZonasComponent } from './components/zonas/zonas.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { AreaComponent } from './components/areas/area/area.component';
import { AsigDirectivosComponent } from './components/directivos/asig-directivos/asig-directivos.component';
import { LoadingComponent } from './components/share/loading/loading.component';
import { NavComponent } from './components/share/nav/nav.component';
import { RangoComponent } from './components/config/rango/rango.component';

//GUARDS
import { AuthGuardService } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { SubadminGuard } from './guards/subadmin.guard';

//Servicios
import { AuthService } from './services/auth.services';
import { AuditoriaService } from './services/auditoria.services';
import { EmailComponent } from './components/config/rango/email/email.component';
import { AdminItemComponent } from './components/subadmin/admin-item/admin-item.component';
import { AuditoresComponent } from './components/auditores/auditores.component';
import { AuditoriasComponent } from './components/auditorias/auditorias.component';
import { AsigAuditoriasComponent } from './components/asig-auditorias/asig-auditorias.component';
import { AuditoriasPeriodoComponent } from './components/auditorias-periodo/auditorias-periodo.component';
import { VariableComponent } from './components/auditorias/variable/variable.component';
import { AuditorItemComponent } from './components/auditores/auditor-item/auditor-item.component';
import { AsigItemComponent } from './components/asig-auditorias/asig-item/asig-item.component';
import { AuditoperItemComponent } from './components/auditorias-periodo/auditoper-item/auditoper-item.component';

import { ReportesItemComponent } from './components/reportes/reportes-item/reportes-item.component';

import { ZonaAsignadaComponent } from './components/subadmin/admin-item/zona-asignada/zona-asignada.component';
import { NgDropFilesDirective } from './directives/ng-drop-files.directive';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { VariableItemComponent } from './components/asig-auditorias/asig-item/variable-item/variable-item.component';
import { DisableControlDirective } from './directives/disable-control.directive';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { ModalTipoAsignacionComponent } from './components/asig-auditorias/modal-tipo-asignacion/modal-tipo-asignacion.component';
import { RevisionAuditoriaModalComponent } from './components/auditorias-periodo/revision-auditoria-modal/revision-auditoria-modal.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import { CalificarAuditoriaModalComponent } from './components/auditorias-periodo/calificar-auditoria-modal/calificar-auditoria-modal.component';
import { ComentariosAuditoriaModalComponent } from './components/auditorias-periodo/comentarios-auditoria-modal/comentarios-auditoria-modal.component';
import { PrevImageModalComponent } from './components/auditorias-periodo/prev-image-modal/prev-image-modal.component';

import { DragScrollModule } from "cdk-drag-scroll";
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    FooterComponent,
    MenuAdminComponent,
    BodyComponent,
    BarraLateralComponent,
    AreasComponent,
    DirectivosComponent,
    ConfigComponent,
    SubadminComponent,
    ZonasComponent,
    ReportesComponent,
    AreaComponent,
    AsigDirectivosComponent,
    LoadingComponent,
    NavComponent,
    RangoComponent,
    EmailComponent,
    AdminItemComponent,
    AuditoresComponent,
    AuditoriasComponent,
    AuditoperItemComponent,
    AsigAuditoriasComponent,
    AuditoriasPeriodoComponent,
    VariableComponent,
    AuditorItemComponent,
    AsigItemComponent,
    ReportesItemComponent,
    AuditoperItemComponent,
    ZonaAsignadaComponent,
    NgDropFilesDirective,
    VariableItemComponent,
    DisableControlDirective,
    ModalTipoAsignacionComponent,
    RevisionAuditoriaModalComponent,
    CalificarAuditoriaModalComponent,
    ComentariosAuditoriaModalComponent,
    PrevImageModalComponent],

  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    DragDropModule,
    ScrollDispatchModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatSidenavModule,
    MatToolbarModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    DragScrollModule

  ],
  providers: [
    MenuAdminComponent,
    AuditoriaService,
    AuthService,
    AuthGuardService,
    AdminGuard,
    SubadminGuard,
    BarraLateralComponent,
    BodyComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
