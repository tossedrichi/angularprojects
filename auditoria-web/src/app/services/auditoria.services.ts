import { Injectable } from "@angular/core";

import { HttpClient, HttpHeaders } from "@angular/common/http";

import { map, timeout } from "rxjs/operators";
import { AreasModel } from "../../models/areas.model";
import { RangoModel } from "../../models/rango.model";
import { Directivos } from "src/interfaces/Directivos";
import { DirectivosRangoModel } from "src/models/directivoRangoModel";
import { MatrizCalificacionesIterface } from "../../interfaces/matrizCalificaciones.interface";
import { SubAdministradorModel } from "../../models/SubAdministrador.model";
import { ZonasSubAdministrador } from "src/models/ZonasSubAdministrador.mode";
import { Zona } from "../../models/ZonasSubAdministrador.mode";
import { Persona } from "src/models/persona.mode.";
import { SubZonasList, SubZona } from "../../models/subZonasAdmin.mode";
import { Frecuecia } from "src/models/Frecuencia.model";
import { Sucursal } from "src/models/Sucursal.model";
import { Auditoria } from "src/models/Auditoria.model";
import { AuditoriaCreada } from "src/models/AuditoriaCreada.model";
import { AsigancionAuditoria, AuditoriaAsignada } from "src/models/AsigancionAuditoria.model";
import { FileItem } from 'src/models/file-item.model';
import { DetalleAsignacionModel } from 'src/models/DetalleAsignacionModel';
import { ProcesadoModel } from '../../models/Procesado.model';
import { SetGeocercaModel } from '../../models/SetGeocerca.model';


const URL_BASE_PRUEBAS =
  "http://microstest.gruporoga.com:2021/rogaservices/auditoriaoperativa/";
//const URL_BASE_PRUEBAS = "http://localhost:3048/"
const headers = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"

  })
};

const TIMEOUT: number = 60000;

@Injectable({
  providedIn: "root"
})
export class AuditoriaService {

  agente: number;
  origen: number = 1;
  asignarOtros: boolean = false;
  constructor(private http: HttpClient) {
    if (sessionStorage.getItem("token")) {
      let data = JSON.parse(atob(sessionStorage.getItem("token").toString()));
      // console.log(data);
      this.agente = data.usuario;
      console.log("Agente", this.agente);
    }

    if (sessionStorage.getItem("asignarOtros")) {
      if (sessionStorage.getItem("asignarOtros").toString() == 'true') {
        this.asignarOtros = true;
      } else {
        this.asignarOtros = false;

      }
    }



    console.log("Auditoria Service Preparado");

  }

  setData(asignarOtros?) {
    sessionStorage.setItem("asignarOtros", asignarOtros);

    if (sessionStorage.getItem("token")) {
      let data = JSON.parse(atob(sessionStorage.getItem("token").toString()));
      // console.log(data);
      this.agente = data.usuario;
      if (sessionStorage.getItem("asignarOtros").toString() == 'true') {
        this.asignarOtros = true;
      } else {
        this.asignarOtros = false;

      }

      console.log(
        this.asignarOtros
      );

    }
  }
  getAreas() {
    const PATH = URL_BASE_PRUEBAS + "getAreas";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setAgregarArea(nombreArea: String) {
    const PATH = URL_BASE_PRUEBAS + "setArea";

    let area: AreasModel = new AreasModel();
    area.nombreArea = nombreArea;
    area.origen = this.origen;
    area.agente = this.agente;

    return this.http.post(PATH, area, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setDeshabilitarArea(area: AreasModel) {
    const PATH = URL_BASE_PRUEBAS + "setEliminarArea";
    area.agente = this.agente;
    area.origen = this.origen
    return this.http.post(PATH, area, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setEditarArea(area: AreasModel) {
    const PATH = URL_BASE_PRUEBAS + "setModificarArea";
    area.agente = this.agente;
    area.origen = this.origen
    return this.http.post(PATH, area, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getAuditorias(idArea: number) {
    const PATH = URL_BASE_PRUEBAS + `getAuditoriasArea/idArea=${idArea}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getMatrizCalificaciones() {
    const PATH = URL_BASE_PRUEBAS + "getRangos";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        console.log(response);

        return <MatrizCalificacionesIterface>response;
      }, timeout(TIMEOUT))
    );
  }

  setModificacionRango(rango: RangoModel) {
    const PATH = URL_BASE_PRUEBAS + "setModificaRango";

    rango.agente = this.agente;
    rango.idOrigen = this.origen

    return this.http.post(PATH, rango, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setNuevoRango(rango: RangoModel) {
    const PATH = URL_BASE_PRUEBAS + "/setRango";

    rango.agente = this.agente;
    rango.idOrigen = this.origen

    return this.http.post(PATH, rango, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setDeshabilitarRango(rango: RangoModel) {
    const PATH = URL_BASE_PRUEBAS + "setEliminarRango";

    rango.agente = this.agente;
    rango.idOrigen = this.origen

    return this.http.post(PATH, rango, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getDirectivosArea(idArea: number) {
    const PATH = URL_BASE_PRUEBAS + `getDirectivosArea/idArea=${idArea}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <Directivos>response;
      }, timeout(TIMEOUT))
    );
  }

  /*  getDirectivosSinArea() {
    const PATH = URL_BASE_PRUEBAS + "";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  } */

  setAsignarDirectivoArea(idArea: number, idDirectivo: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `setDirectivoArea/idArea=${idArea},idDir=${idDirectivo},idOrigen=${this.origen},agente=${this.agente}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setDesAsignarDirectivoArea(idDirectivoArea: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `setEliminarDirectivoArea/idDirArea=${idDirectivoArea}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getDirectivosRango(idRango: number) {
    const PATH = URL_BASE_PRUEBAS + `getDirectivosRangos/idRango=${idRango}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <DirectivosRangoModel>response;
      }, timeout(TIMEOUT))
    );
  }

  setDirectivoRango(idRango: number, idDir: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `setDirectivoRango/idRango=${idRango},idDir=${idDir},idOrigen=${this.origen},agente=${this.agente}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setEliminarDirectivoRango(idDirRango: number) {
    const PATH =
      URL_BASE_PRUEBAS + `setEliminarDirectivoRango/idDirRango=${idDirRango}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getPersonal() {
    const PATH = URL_BASE_PRUEBAS + "getPersonas";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <[Persona]>response;
      }, timeout(TIMEOUT))
    );
  }

  getPersonalDirectivos() {
    const PATH = URL_BASE_PRUEBAS + "getPersonasDirectivos";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <[Persona]>response;
      }, timeout(TIMEOUT))
    );
  }

  setDirectivo(persona: Array<Persona>) {
    const PATH = URL_BASE_PRUEBAS + "setDirectivo";

    persona.forEach(persona => {
      persona.idOrigen = this.origen;
      persona.agente = this.agente;
    })

    return this.http.post(PATH, persona, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  /* setSubadministradores id,agente,origen */
  setSubadministradores(subidministradores: Persona[]) {
    const PATH = URL_BASE_PRUEBAS + "setSubadministradores";

    subidministradores.forEach(subadmin => {
      subadmin.agente = this.agente;
      subadmin.idOrigen = this.origen;
    })

    return this.http.post(PATH, subidministradores, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getSubAdministradores() {
    const PATH = URL_BASE_PRUEBAS + "getSubadministradores";

    return this.http.get(PATH, headers).pipe(
      map(
        response => {
          return <SubAdministradorModel[]>response;
        },
        error => { }
      )
    );
  }

  setDesAsignarSubadministrador(subAdminstrador: SubAdministradorModel) {
    const PATH = URL_BASE_PRUEBAS + "setEliminarSubadministrador";

    subAdminstrador.agente = this.agente;
    subAdminstrador.idOrigen = this.origen;

    return this.http.post(PATH, subAdminstrador, headers).pipe(
      map(response => {
        return <any>response;
      }, timeout(TIMEOUT))
    );
  }

  getZonasSubAdministrador(idSubAdministrador: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getZonasSubadministrador/id=${idSubAdministrador}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <ZonasSubAdministrador>response;
      }, timeout(TIMEOUT))
    );
  }

  setAsignarZonaSubAdministrador(zona: Zona) {
    const PATH = URL_BASE_PRUEBAS + "setAsignarZonaSubadministrador";

    zona.agente = this.agente;
    zona.idOrigen = this.origen;

    return this.http.post(PATH, zona, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setEliminarZonaSubAdmnistrador(idZonaSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS + `setEliminaZonaSubadministrador/id=${idZonaSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <any>response;
      }, timeout(TIMEOUT))
    );
  }

  getZonas() {
    const PATH = URL_BASE_PRUEBAS + "getZonas";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getSubadministradoresZona(idZona: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getSubadministradoresZona/idZona=${idZona}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getSubZonasZonaSubAdmnistrador(idZonSubAdmni: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `getSubZonasZonaSubadministrador/idZonaSubadmin=${idZonSubAdmni}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <SubZonasList>response;
      }, timeout(TIMEOUT))
    );
  }

  setAsignarSubZona(subZona: SubZona) {
    const PATH = URL_BASE_PRUEBAS + `setAsignarSubZona`;

    subZona.agente = this.agente;
    subZona.idOrigen = this.origen;

    return this.http.post(PATH, subZona, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setDesAsignarSubZona(subZona: number) {
    const PATH =
      URL_BASE_PRUEBAS + `setEliminarSubZona/idSubZonaZonaSubadmin=${subZona}`;


    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getAuditores(idUsuario: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getPersonasAuditores/idSubadmin=${idUsuario}`;
    return this.http.get(PATH, headers).pipe(
      map(auditores => {
        return <Array<Persona>>auditores;
      }, timeout(TIMEOUT))
    );
  }

  setAuditores(auditores: Array<Persona>) {
    const PATH = URL_BASE_PRUEBAS + "setAuditores";

    auditores.forEach(auditor => {
      auditor.agente = this.agente;
      auditor.idOrigen = this.origen;
      auditor.idSubadministrador = this.agente;

    })

    return this.http.post(PATH, auditores, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getAuditoriesSubadministrador(idUsuario: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getAuditoresSubadministrador/idSubadmin=${idUsuario}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <Array<Persona>>response;
      }, timeout(TIMEOUT))
    );
  }

  setDesAsignarAuditor(auditor: Persona) {
    const PATH = URL_BASE_PRUEBAS + "setEliminarAuditor";

    auditor.agente = this.agente;
    auditor.idOrigen = this.origen;

    return this.http.post(PATH, auditor, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getDetallesAuditor(idAuditorSubadmin: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `getDetallesAuditor/idAuditorSubadministrador=${idAuditorSubadmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setActualizaCorreoPersonas(personas: Array<Persona>) {
    const PATH = URL_BASE_PRUEBAS + "setActualizaCorreosPersonas";

    personas.forEach(persona => {
      persona.agente = this.agente;
      persona.idOrigen = this.origen;
      persona.idSubadministrador = this.agente;

    })

    return this.http.post(PATH, personas, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getSubZonasSubadministrador(idSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getSubZonasSubadministrador/idSubadmin=${idSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <SubZona[]>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }
  getZonasSubadministrador(idSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS + `/getZonasSubadministradorPersona/idSubadmin=${idSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <Zona[]>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }
  getFrecuencias() {
    const PATH = URL_BASE_PRUEBAS + "getFrecuencias";

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <Frecuecia[]>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }

  getSucursales(idSubAdmin: number, subZonas: SubZona[]) {
    const PATH = URL_BASE_PRUEBAS + "getSucursales";

    let body = {
      idSubadministrador: idSubAdmin,
      subZonas: subZonas
    };

    return this.http.post(PATH, body, headers).pipe(
      map(response => {
        return <Sucursal[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getListaAreasSubadministrador(idSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `getListaAreasSubadministrador/idSubadmin=${idSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <AreasModel[]>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }

  /*   getListaAreasSubadministrador(idSubAdmin: number) {
      const PATH =
        URL_BASE_PRUEBAS +
        `getListaAreasSubadministrador/idSubadmin=${idSubAdmin}`;
  
      return this.http.get(PATH, headers).toPromise(response => {
        return response;
      }, timeout(TIMEOUT))
      )
    } */

  /*   getAreasSubadministrador/idSubadmin={id}
   */

  getAreasSubadministrador(idSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getAreasSubadministrador/idSubadmin=${idSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <AreasModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  /* setAsignarAreaSubadministrador */

  setAsignarAreaSubadministrador(area: AreasModel) {
    const PATH = URL_BASE_PRUEBAS + "setAsignarAreaSubadministrador";

    area.agente = this.agente;
    area.origen = this.origen;

    return this.http.post(PATH, area, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  setAuditoria(auditoria: Auditoria) {
    const PATH = URL_BASE_PRUEBAS + "setAuditoria";

    auditoria.idSubadministrador = this.agente;
    auditoria.idOrigen = this.origen

    return this.http.post(PATH, auditoria, headers).pipe(
      map(response => {
        return <any>response;
      }, timeout(TIMEOUT))
    );
  }

  getAuditoriasSubadministrador(idSubAdministrador: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `/getAuditoriaSubadministrador/idSubadmin=${idSubAdministrador}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <AuditoriaCreada[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getDetallesAuditoria(idAuditoria: number) {
    const PATH =
      URL_BASE_PRUEBAS + `getDetallesAuditoria/idAuditoria=${idAuditoria}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <Auditoria>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }

  setAsignarAuditoria(asignacionAuditoria: AsigancionAuditoria[]) {
    const PATH = URL_BASE_PRUEBAS + "setAsignacionAuditoria";

    asignacionAuditoria.forEach(asignacion => {
      asignacion.agente = this.agente;
      asignacion.idOrigen = this.origen;
    })

    return this.http.post(PATH, asignacionAuditoria, headers).pipe(
      map(response => {
        return <AsigancionAuditoria[]>response;
      }, timeout(TIMEOUT))
    );
  }

  //! NECESITA AGENTE Y ORIGEN ?
  setEliminarAuditoria(auditoria: AuditoriaCreada) {
    const PATH = URL_BASE_PRUEBAS + "setEliminarAuditoria";


    return this.http.post(PATH, auditoria, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  getListaAsignacionesPeriodo(idSubAdmin: number) {
    const PATH =
      URL_BASE_PRUEBAS +
      `getListaAsignacionesPeriodo/idSubadministrador=${idSubAdmin}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <AuditoriaAsignada[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getFotosModeloVariable(idVariable: number) {
    const PATH = URL_BASE_PRUEBAS + `getFotosModeloVariable/idVariable=${idVariable}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <FileItem[]>response;
      }, timeout(TIMEOUT))
    ).toPromise();
  }


  getDetallesAsigancion(idAsignacion: number) {
    const PATH = `${URL_BASE_PRUEBAS}getDetalleAsignacion/idAsignacion=${idAsignacion}`;
    console.log("getDetalle");

    return this.http.get(PATH, headers).pipe(
      timeout(TIMEOUT),
      map(response => {
        console.log("getDetalle");

        return <DetalleAsignacionModel>response;
      })
    )
  }

  getEvidenciasDetalleAsignacion(idDetalleAsignacion) {
    const path =
      `${URL_BASE_PRUEBAS}getFotosEvidenciaDetalleAsignacion/idDetalleAsignacion=${idDetalleAsignacion}`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <FileItem[]>response;
      }, timeout(TIMEOUT))
    )

  }
  //setCambiaEsadoAsignacion/idAsignacion={id},estado={est}

  //! NECESITA AGENTE Y ORIGEN ?

  setCambiaEstadoAsignacion(idAsignacion: number, estado: string, comentario: string) {
    const path = `${URL_BASE_PRUEBAS}setCambiaEsadoAsignacion`;

    let body = {
      "idAsignacion": idAsignacion,
      "estado": estado,
      "comentario": comentario
    }

    return this.http.post(path, body, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    )
  }

  setElimiarAsignacion(idAsignacion: number) {

    const PATH = `${URL_BASE_PRUEBAS}setEliminarAsignacion`;

    let body = {
      "idAsignacion": idAsignacion,
      "Agente": this.agente,
      "idOrigen": this.origen,
    }

    return this.http.post(PATH, body, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    )

  }

  setEliminarVariable(idVariable: number) {
    const PATH = `${URL_BASE_PRUEBAS}setEliminarVariable`;

    let body = {
      "idVariable": idVariable,
      "agente": this.agente,
      "idOrigen": this.origen,
    }
    console.log(body);


    return this.http.post(PATH, body, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    )

  }


  getGeocercasSucurasalesCedis(idCedis: number) {
    const PATH = `${URL_BASE_PRUEBAS}getGeocercasSucursales/idCedis=${idCedis}`;
    let opt = { responseType: 'text' as 'text' };

    return this.http.get(PATH, opt).pipe(
      map(response => {

        return <string>response;
      }, timeout(TIMEOUT))
    )
  }


  setPorcentajeAprobacion(idPersona: number, porcentaje: number) {
    const PATH = URL_BASE_PRUEBAS + `setActualizaPorcentajeAutomaticas/idPersona=${idPersona},porcentaje=${porcentaje},idOrigen=${this.origen},agente=${this.agente}`;

    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    )

    return
  }


  getSucursalesCedis(idCedis: number) {
    const PATH = URL_BASE_PRUEBAS + `getSucursalesCedis/cedis=${idCedis}`;
    //const PATH = `http://micros.gruporoga.com:2020/rogaservices/auditoriaoperativa/getSucursalesCedis/cedis=${idCedis}`;

    return this.http.get(PATH, headers).pipe(map(response => {

      return <Sucursal[]>response;

    }, timeout(TIMEOUT)))
  }
  getSubZonasSubadmiistradorZona(idSubadmin:number,idZona:number){
    const PATH =URL_BASE_PRUEBAS+`getSubZonasSubadministradorZona/idSub=${idSubadmin},idZona=${idZona}`
    return this.http.get(PATH,headers).pipe(
      map(response=>{
        return <SubZona[]>response;
      },timeout(TIMEOUT))
    )
  }
  
  asignarSubZonaAuditoria(idSub:number, idAud:number,idSubZona:number,idOrigen:number){
    const PATH =`${URL_BASE_PRUEBAS}asignarSubZonaAuditoria/idSub=${idSub},idAuditoria=${idAud},idsubZona=${idSubZona},idOrigen=${idOrigen}`
    return this.http.get(PATH,headers).pipe(
      map(response=>{
        return<ProcesadoModel> response;
      },timeout(TIMEOUT))
    )
  }
  desasignarSubZonaAuditoria(idSubZona:number,idAuditoria:number){
    const PATH =`${URL_BASE_PRUEBAS}desasignarSubZonaAuditoria/idAuditoria=${idAuditoria},idsubZona=${idSubZona}`
    return this.http.get(PATH,headers).pipe(
      map(response=>{
        return<ProcesadoModel> response;
      },timeout(TIMEOUT))
    )
  }

  setGeocerca(geocerca:SetGeocercaModel){

    const PATH = `${URL_BASE_PRUEBAS}agregaGeocerca`;

    return this.http.post(PATH,geocerca,headers).pipe(map(response=>{
      return <SetGeocercaModel> response;
    },timeout(TIMEOUT)))

  }

}
