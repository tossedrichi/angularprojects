import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { HttpClient, HttpHeaders } from "@angular/common/http";

import { map } from "rxjs/operators";
import { LoginInterface } from "../../interfaces/login.interface";
import * as crypto from "crypto-js";

const urlLogin =
  "http://microstest.gruporoga.com:2021/rogaservices/security/token/login";

/* const URL_BASE_PRUEBAS =
  "http://microstest.gruporoga.com:2021/rogaservices/auditoriaoperativa/"; */

  const URL_BASE_PRUEBAS =
  "http://microstest.gruporoga.com:2021/rogaservices/auditoriaoperativa/";
  /* const URL_BASE_PRUEBAS =
  "http://192.168.1.65:3048/";
 */
const headers = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

export const keyCryp = "kriptoAce";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {
    console.log("Auditoria Service Preparado");
  }

  setLogin(user: string, password: string): any {
    const bodyParams = `{
                 "userId":"${user}",
                 "userPass":"${password}",
                 "userPorfile":"1",
                 "macAddress":"00-14-22-01-23-45",
                 "appId":"1"
             }`;

    return this.http.post(urlLogin, bodyParams, headers).pipe(
      map(response => {
        return response;
      })
    );
  }

  getPorfileType(idUser: number) {
    const path = URL_BASE_PRUEBAS + `getTipoUsuarioWeb/usuario=${idUser}`;
    return this.http.get(path, headers).pipe(
      map(response => {
        return <any>response;
      })
    );
  }

  cryptoJS(user: string) {
    let cryp = crypto.AES.encrypt(user, "Key");

    sessionStorage.setItem("userC", cryp.toString());
    console.log("cryp", cryp.toString());
    let decrypt = crypto.AES.decrypt(cryp, "Key");
    console.log("decryp", decrypt.toString(crypto.enc.Utf8));
  }

  public setSession(authResult: LoginInterface, typeUser: string): void {
    var iv = crypto.enc.Base64.parse("#base64IV#");

 /*    console.info("Login Session");
    console.info(authResult); */
    // Set the time that the access token will expire at
    //const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    sessionStorage.setItem("token", authResult.token);
    sessionStorage.setItem("sessionActive", "true");
    sessionStorage.setItem("userName", authResult.nombreUsuario);
    // localStorage.setItem('id_token', authResult.idToken);
    sessionStorage.setItem("expireDate", authResult.expireDate);
   
    sessionStorage.setItem(
      "typeUser",
      crypto.AES.encrypt(typeUser, keyCryp, { iv: iv }).toString()
    );


   //console.log("DECODE TOKEN",JSON.parse( atob(authResult.token.toString()))); 
    
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("sessionActive");
    //localStorage.removeItem('id_token');
    sessionStorage.removeItem("expireDate");
    sessionStorage.clear();
    // Go back to the home route
    this.router.navigate(["/"]);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(sessionStorage.getItem("expiresDate"));
    return new Date().getTime() < expiresAt;
  }

  public getProfile(): void {
    const accessToken = sessionStorage.getItem("token");
    if (!accessToken) {
      throw new Error("Access token must exist to fetch profile");
    } else {
    }
  }
}
