import { Routes } from '@angular/router';
import { AreasComponent } from './components/areas/areas.component';
import { DirectivosComponent } from './components/directivos/directivos.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { SubadminComponent } from './components/subadmin/subadmin.component';
import { ZonasComponent } from './components/zonas/zonas.component';
import { ConfigComponent } from './components/config/config.component';


export const ADMINISTRADOR_ROUTES: Routes = [
    { path: 'areas', component: AreasComponent },
    { path: 'directivos', component: DirectivosComponent },
    { path: 'reportes', component: ReportesComponent },
    { path: 'subadministradores', component: SubadminComponent },
    { path: 'zonas', component: ZonasComponent },
    { path: 'config', component: ConfigComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'menu' }

]