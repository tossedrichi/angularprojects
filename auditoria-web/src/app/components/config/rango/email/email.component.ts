import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '../../../../services/auditoria.services';
import { RangoModel } from '../../../../../models/rango.model';
import { RangoComponent } from '../rango.component';
import { DirectivoRango } from '../../../../../models/directivoRangoModel';
import { toast } from 'materialize-css';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  
 @Input() rango:RangoModel;

 listDirectivosAsignados:Array<DirectivoRango>;
 lsitDirectivosDisponibles:Array<DirectivoRango>;
  
  constructor(private rangoComponent:RangoComponent,
              private _AuditoriaService:AuditoriaService) {

                
               }

  ngOnInit() {
    console.log(this.rango);
    this.getDirectivosRango();
  }
  cancelar(){
    this.rangoComponent.mostrarCorreos=false;
  }

  getDirectivosRango(){
    this._AuditoriaService.getDirectivosRango(this.rango.idRango)
      .subscribe(response=>{
        console.log(response);
        this.listDirectivosAsignados=response.directivosAsignados;
        this.lsitDirectivosDisponibles=response.directivosDisponibles;
        
      },error=>{
        toast({html:'Error al Recuperar Directivos',classes:'red rounded'})
      })
  }
  
  asignarCorreoDirectivoArea(idDirectivo:number){
    this._AuditoriaService.setDirectivoRango(
      this.rango.idRango,
      idDirectivo)
      .subscribe(response=>{
        console.log(response);
        this.getDirectivosRango();
        toast({html:'Directivo Asignado',classes:'green rounded'})

      },error=>{
        toast({html:'Error al Asignar Directivo',classes:'red rounded'})

      })
  }

  desAsignarCorreoDirectivoArea(idDirectivoRango){
    this._AuditoriaService.setEliminarDirectivoRango(idDirectivoRango)
    .subscribe(response =>{
      console.log(response);
      this.getDirectivosRango();
      toast({html:'Directivo Des-Asignado',classes:'green rounded'})

    },error=>{
      toast({html:`Error al Des-Asignar Directivo`,classes:'red rounded'})

    })
  }

}
