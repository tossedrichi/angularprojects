import { Component, OnInit, Input } from "@angular/core";
import { AuditoriaService } from "../../../services/auditoria.services";
import { ConfigComponent } from '../config.component';
import { RangoModel } from "../../../../models/rango.model";
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatrizCalificacionesIterface } from '../../../../interfaces/matrizCalificaciones.interface';
import * as cloneDeep from 'lodash/cloneDeep';
import { toast } from 'materialize-css';

@Component({
  selector: "app-rango",
  templateUrl: "./rango.component.html",
  styleUrls: ["./rango.component.css"]
})
export class RangoComponent implements OnInit {
  formModRango: FormGroup;

  mostrarModificar: boolean = false;
  mostrarConfirmar: boolean = false;
  mostrarEliminar: boolean = false;
  mostrarCorreos: boolean = false;

  @Input("rango") rango: RangoModel;
  constructor(
    private config: ConfigComponent,
    private _AuditoriaService: AuditoriaService
  ) {}

  ngOnInit() {
    this.rango.agente = 1;
    this.rango.idOrigen = 1;
  }

  alertConfirmar(){

    if(this.formModRango.valid){

      this.mostrarConfirmar=true
    }
  }

  alertModificar(){
    this.mostrarModificar=true;
    this.validaciones();
  }
  
  setActualizarRango() {
    let rangoMod: RangoModel = new RangoModel();
    let max:number = this.formModRango.get('maxValue').value;
    let min:number = this.formModRango.get('minValue').value;
    let mensaje:string = this.formModRango.get('mensaje').value;

    this.mostrarConfirmar=false;
    this.mostrarModificar=false;

    rangoMod.idOrigen = 1;
    rangoMod.agente = 1;
    rangoMod.idRango = this.rango.idRango;
    rangoMod.limiteInferior = min;
    rangoMod.limiteSuperior = max;
    rangoMod.mensaje = mensaje;

    console.log(rangoMod);
    this._AuditoriaService
      .setModificacionRango(rangoMod)
      .subscribe(response => {
        console.log(response);
        this.config.getRangos();
        toast({html:'Rango Actualizado', classes:'green rounded'})

      },error=>{
        toast({html:'Error al Actualizar Rango', classes:'red rounded'})
      });

  }

  setDeshablitarRango() {
    console.log(this.rango);

    this._AuditoriaService
      .setDeshabilitarRango(this.rango)
      .subscribe(response => {
        console.log(response);
        this.config.getRangos();
        toast({html:'Rango Deshabilitado', classes:'red rounded'})

      },error=>{
        toast({html:'Error al Deshabilitar Rango', classes:'red rounded'})
      });
  }

  validaciones() {
    this.formModRango = new FormGroup({
      minValue: new FormControl(this.rango.limiteInferior, [
        Validators.required,
        Validators.max(100),
        Validators.min(0),
        Validators.pattern("^[0-9]*$"),
        Validators.nullValidator
      ]),

      maxValue: new FormControl(this.rango.limiteSuperior, [
        Validators.required,
        Validators.max(100),
        Validators.min(0),
        Validators.pattern("^[0-9]*$"),
        Validators.nullValidator
      ]),

      mensaje: new FormControl(this.rango.mensaje, [
        Validators.required,
        Validators.minLength(4)
      ])
    });

    this.formModRango
      .get("maxValue")
      .setValidators([
        this.minHigerMax.bind(this.formModRango),
        this.entreRangos.bind(this),
        this.formModRango.get("maxValue").validator
      ]);

    this.formModRango
      .get("minValue")
      .setValidators([
        this.minHigerMax.bind(this.formModRango),
        this.entreRangos.bind(this),
        this.formModRango.get("minValue").validator
      ]);

    this.formModRango.controls["maxValue"].valueChanges.subscribe(data => {
      console.log(
        this.formModRango.controls["minValue"].updateValueAndValidity()
      );
    });
  }

  minHigerMax(): { [s: string]: boolean } {
    let form: any = this;
    console.log(form);

    if (form.controls["minValue"].value >= form.controls["maxValue"].value) {
      return {
        minHigerMax: true
      };
    }
    return null;
  }

  entreRangos():{[s:string]:boolean}{
    let minNew:number = this.formModRango.get('minValue').value;
    let maxNew:number = this.formModRango.get('maxValue').value;
    let rangos :Array<MatrizCalificacionesIterface> = cloneDeep(this.config.listRangos)

    rangos = rangos.filter(x=> x.idRango != this.rango.idRango);
/* getPersonas
   function duplicateArray(arrayOld:Array<any>){
      for(let i=0;i<arrayOld.length;i++){

      }
   } */

    function between(x:number,min:number,max:number):boolean{
      return x >= min && x <= max;
    }

    for(let i=0; i<rangos.length; i++){
      console.log(rangos[i].limiteInferior);
      
      if(between(minNew,rangos[i].limiteInferior,rangos[i].limiteSuperior)||
        between(maxNew,rangos[i].limiteInferior,rangos[i].limiteSuperior)){
          return {
            entreRangos: true
          };
      }
    }
    return null;
   }
}
