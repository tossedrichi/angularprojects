import { Component, OnInit } from "@angular/core";
import { AuditoriaService } from "../../services/auditoria.services";
import { RangoModel } from "src/models/rango.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatrizCalificacionesIterface } from "src/interfaces/matrizCalificaciones.interface";
import { toast } from 'materialize-css';

@Component({
  selector: "app-config",
  templateUrl: "./config.component.html",
  styleUrls: ["./config.component.css"]
})
export class ConfigComponent implements OnInit {
  formNuevoRango: FormGroup;

  maxVal: number;
  minVal: number;
  mensaje: String;

  loadingData: boolean = false;
  mostrarAgregar: boolean = false;
  mostrarConfirmar: boolean = false;
  listRangos: Array<MatrizCalificacionesIterface>;

  constructor(private _AuditoriaService: AuditoriaService) {
    this.getRangos();
  }

  ngOnInit() {}

  getRangos() {
    this._AuditoriaService.getMatrizCalificaciones().subscribe(response => {
      /// console.log(response);
      this.listRangos = <any>response;

      console.log(this.listRangos[0].limiteInferior);

    },error=>{
      toast({ html: 'Error al Recuperar Rangos', classes: 'red rounded '  });

    });
  }

  setNuevoRango(min, max, men) {
    let rango: RangoModel = new RangoModel();
    rango.limiteInferior = min;
    rango.limiteSuperior = max;
    rango.mensaje = men;
    rango.idOrigen = 1;
    rango.agente = 1;
    console.log(rango);

    this._AuditoriaService.setNuevoRango(rango).subscribe(response => {
      console.log(response);
      this.getRangos();
      toast({ html: 'Nuevo Rango Agregado', classes: 'green rounded' });

    },error=>{
      toast({ html: 'Error al Enviar Nuevo Rango', classes: 'red rounded' });
    })
  }

  alertAgrergarRango() {
    this.mostrarAgregar = true;
    this.validaciones();
  }

  alertConfirmar() {
    console.log(this.formNuevoRango);
    if (this.formNuevoRango.valid) {
      this.mostrarConfirmar = true;
    }
  }

  validaciones() {
    this.formNuevoRango = new FormGroup({
      minValue: new FormControl("", [
        Validators.required,
        Validators.max(100),
        Validators.min(0),
        Validators.pattern("^[0-9]*$"),
        Validators.nullValidator
      ]),

      maxValue: new FormControl("", [
        Validators.required,
        Validators.max(100),
        Validators.min(0),
        Validators.pattern("^[0-9]*$"),
        Validators.nullValidator
      ]),

      mensaje: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ])
    });

    this.formNuevoRango
      .get("maxValue")
      .setValidators([
        this.minHigerMax.bind(this.formNuevoRango),
        this.entreRangos.bind(this),
        this.formNuevoRango.get("maxValue").validator
      ]);

    this.formNuevoRango
      .get("minValue")
      .setValidators([
        this.minHigerMax.bind(this.formNuevoRango),
        this.entreRangos.bind(this),
        this.formNuevoRango.get("minValue").validator
      ]);

    this.formNuevoRango.controls["maxValue"].valueChanges.subscribe(data => {
      console.log(
        this.formNuevoRango.controls["minValue"].updateValueAndValidity()
      );
    });
  }

  minHigerMax(): { [s: string]: boolean } {
    let form: any = this;
    console.log(form);
    console.log('min',form.controls["minValue"].value);
    console.log('max',form.controls["maxValue"].value);

    if (form.controls["minValue"].value >= form.controls["maxValue"].value) {
      return {
        minHigerMax: true
      };
    }
    return null;
  }

  entreRangos(): { [s: string]: boolean } {
    let minNew: number = this.formNuevoRango.get("minValue").value;
    let maxNew: number = this.formNuevoRango.get("maxValue").value;

    function between(x: number, min: number, max: number): boolean {
      return x >= min && x <= max;
    }

    for (let i = 0; i < this.listRangos.length; i++) {
      console.log(this.listRangos[i].limiteInferior);

      if (
        between(
          minNew,
          this.listRangos[i].limiteInferior,
          this.listRangos[i].limiteSuperior
        ) ||
        between(
          maxNew,
          this.listRangos[i].limiteInferior,
          this.listRangos[i].limiteSuperior
        )
      ) {
        console.log("Entro");
        return {
          entreRangos: true
        };
      }
    }
    return null;
  }

  rangoNoAceptado(): boolean {
    if (this.formNuevoRango.get("minValue").dirty &&
      this.formNuevoRango.get("maxValue").dirty) {

        if (this.formNuevoRango.get("minValue").errors ||
            this.formNuevoRango.get("maxValue").errors) {

              return true;
        }
      }

    return false;
  }
}
