import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-body",
  templateUrl: "./body.component.html",
  styleUrls: ["./body.component.css", "../share/footer/footer.component.css"]
})
export class BodyComponent implements OnInit {
  menu: String;
  menuAreas: boolean = false;
  menuDirectivos: boolean = false;
  menuConfig: boolean = false;
  menuSubadmin: boolean = false;
  menuZona: boolean = false;
  menuReportes: boolean = false;

  menuAuditores: boolean = false;
  menuAuditorias: boolean = false;
  menuAsigAud: boolean = false;
  menuAudPeriodo = false;

  constructor(private activatedRoute: ActivatedRoute, private route: Router) {
   console.log("Body");
   
   this.activatedRoute.params.subscribe(params => {
      console.log(params);
      this.menu = params["id"];
      this.seleccionarPantalla();
    });
  }

  seleccionarPantalla() {
    this.menuAreas = false;
    this.menuDirectivos = false;
    this.menuConfig = false;
    this.menuSubadmin = false;
    this.menuZona = false;
    this.menuReportes = false;

    this.menuAuditores = false;
    this.menuAuditorias = false;
    this.menuAsigAud = false;
    this.menuAudPeriodo = false;

    if (this.menu == "area") {
      this.menuAreas = true;
      console.log(this.menu);
    }
    if (this.menu == "directivos") {
      this.menuDirectivos = true;
      console.log(this.menu);
    }
    if (this.menu == "config") {
      this.menuConfig = true;
      console.log(this.menu);
    }
    if (this.menu == "subadmins") {
      this.menuSubadmin = true;
      console.log(this.menu);
    }
    if (this.menu == "zonas") {
      this.menuZona = true;
      console.log(this.menu);
    }
    if (this.menu == "reportes") {
      this.menuReportes = true;
      console.log(this.menu);
    }
    if (this.menu == "auditores") {
      this.menuAuditores = true;
      console.log(this.menu);
    }
    if (this.menu == "crear_auditoria") {
      this.menuAuditorias = true;
      console.log(this.menu);
    }
    if (this.menu == "asig_auditoria") {
      this.menuAsigAud = true;
      console.log(this.menu);
    }
    if (this.menu == "auditorias_periodo") {
      this.menuAudPeriodo = true;
      console.log(this.menu);
    }
  }

  ngOnInit() {}
}
