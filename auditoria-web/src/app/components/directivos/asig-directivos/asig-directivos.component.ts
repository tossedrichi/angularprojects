import { Component, OnInit, Input } from "@angular/core";
import { AreasModel } from "src/models/areas.model";
import { AuditoriaService } from "../../../services/auditoria.services";
import { DirectivosAreaModel } from '../../../../interfaces/Directivos';
import { toast } from 'materialize-css';

@Component({
  selector: "app-asig-directivos",
  templateUrl: "./asig-directivos.component.html",
  styleUrls: ["./asig-directivos.component.css"]
})
export class AsigDirectivosComponent implements OnInit {
  @Input() area: AreasModel;

  mostrar: boolean = false;
  loadingData: boolean = false;
  listDirectivosArea: Array<DirectivosAreaModel>;
  listDirectivosSinArea: Array<DirectivosAreaModel>;

  constructor(private _AuditoriaService: AuditoriaService) { }

  cambiarMostrar() {
    this.mostrar = !this.mostrar;
    if (this.mostrar) {
      this.getDirectivosArea();
    }
  }

  getDirectivosArea() {
    this._AuditoriaService
      .getDirectivosArea(this.area.idArea)
      .subscribe(response => {
        console.log(response);

        this.listDirectivosArea = response.directivosAsignados;
        console.log(this.listDirectivosArea)
        this.listDirectivosSinArea = response.directivosDisponibles;
      }, error => {
        toast({ html: 'Error al Cargar Directivos', classes: 'red rounded' })
      });
  }

  desAsignarDirectivo(idDirectivoArea) {
    this._AuditoriaService.setDesAsignarDirectivoArea(idDirectivoArea)
      .subscribe(response => {
        console.log(response);
        this.getDirectivosArea();
      }, error => {
        toast({ html: 'Error al Des-Asignar Directivo', classes: 'red rounded' })

      });
  }

  asignarDirectivo(directivo: DirectivosAreaModel) {
    this._AuditoriaService
      .setAsignarDirectivoArea(this.area.idArea, directivo.idDirectivo)

      .subscribe(response => {
        console.log(response);
        this.getDirectivosArea();
        this.getDirectivosArea();
      }, error => {
        toast({ html: 'Error al Asignar Directivo', classes: 'red rounded' })

      });
  }

  ngOnInit() { }
}
