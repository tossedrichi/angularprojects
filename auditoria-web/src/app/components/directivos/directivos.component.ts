import { Component, OnInit, AfterContentInit } from "@angular/core";
import { AuditoriaService } from "../../services/auditoria.services";
import { toast, Modal } from "materialize-css";
import { Persona } from "src/models/persona.mode.";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";

@Component({
  selector: "app-directivos",
  templateUrl: "./directivos.component.html",
  styleUrls: ["./directivos.component.css", "../login/login.component.css"]
})
export class DirectivosComponent implements OnInit, AfterContentInit {
  formularioCorreos: FormGroup;
  alertCorreos: Modal;

  listAreas: {};
  loadingData: boolean = true;
  mostrarLista: boolean = false;

  listPersonal: Array<Persona> = [];
  listSearchPerson: Array<Persona> = [];
  listPersonalSelected: Array<Persona> = [];
  listPersonaSinCorreo: Array<Persona> = [];

  constructor(private _AuditoriaService: AuditoriaService) {
    this.getAreas();
    this.validacionFormCorreo();
  }
  ngAfterContentInit() {
    var modals = document.querySelector(".modal");
    this.alertCorreos = M.Modal.init(modals);
  }

  getAreas() {
    this._AuditoriaService.getAreas().subscribe(
      response => {
        this.listAreas = response;
        this.loadingData = false;
      },
      error => {
        toast({ html: "Error al Cargar Áreas", classes: "red rounded" });
      }
    );
  }

  ngOnInit() { }

  mostrarListaPersonal() {
    this.mostrarLista = true;
    this.getPersonal();
  }

  getPersonal() {
    this.loadingData = true;
    this._AuditoriaService.getPersonalDirectivos().subscribe(
      response => {
        this.loadingData = false;
        this.listPersonal = response;

        this.listPersonal.forEach(person => {
          person.isSelected = false;
        });

        this.listSearchPerson = response;
        console.log(this.listPersonal);
      },
      error => {
        console.log(error);
        this.loadingData = false;
        toast({ html: "Error al Cargar Personal", classes: "red rounded" });
      }
    );
  }

  addAndRemoveItemsPersona(persona: Persona) {
    if (!this.listPersonalSelected.find(x => x == persona)) {
      persona.isSelected = true;
      this.listPersonalSelected.push(persona);
      console.log(this.listPersonalSelected);
    } else {
      this.listPersonalSelected = this.listPersonalSelected.filter(
        x => x != persona
      );
      console.log(this.listPersonalSelected);
    }

    if (this.mostrarLista == false) {
      this.listPersonalSelected = [];
    }
  }

  searchPerson(params: string) {
    //this.mostrarSearch = true;
    console.log(params);

    this.listSearchPerson = this.listPersonal;

    this.listSearchPerson = this.listSearchPerson.filter(x =>
      x.nombre.toLowerCase().includes(params.toLowerCase())
    );
    console.log(this.listSearchPerson);


  }

  setDirectivos() {
    if (this.listPersonalSelected.length > 0) {
      this._AuditoriaService.setDirectivo(this.listPersonalSelected).subscribe(
        response => {
          this.mostrarLista = false;

          console.log(response);

          toast({ html: "Directivos Agregados", classes: "green rounded" });
        },
        error => {
          toast({
            html: "Error al Agregar Directivos",
            classes: "red rounded"
          });
        }
      );
    } else {
      toast({ html: "Selecciona almenos una Persona", classes: "red rounded" });
    }
  }

  verificarCorreosPersonas() {
    this.listPersonaSinCorreo = [];

    this.listPersonalSelected.forEach(persona => {
      if (persona.correo.length == 0) {
        this.listPersonaSinCorreo.push(persona);
      }
    });

    console.log("Personas Sin Correo", this.listPersonaSinCorreo);

    if (this.listPersonaSinCorreo.length == 0) {
      this.setDirectivos();
    } else {
      this.validacionFormCorreo();
      this.alertCorreos.open();
      this.alertCorreos.options.dismissible = false;

      this.listPersonaSinCorreo.forEach(persona => {
        (<FormArray>this.formularioCorreos.controls["correos"]).push(
          new FormControl("", [Validators.required, Validators.email])
        );
      });
    }
  }

  validacionFormCorreo() {
    this.formularioCorreos = new FormGroup({
      correos: new FormArray([])
    });
  }

  setCorreos() {
    console.log(this.formularioCorreos);

    if (this.formularioCorreos.valid) {
      for (let i = 0; i < this.listPersonaSinCorreo.length; i++) {

        this.listPersonaSinCorreo[i].correo = this.formularioCorreos.controls["correos"].get(i.toString()).value;

      }
      console.log("Correos a Enviar", this.listPersonaSinCorreo);

      this._AuditoriaService.setActualizaCorreoPersonas(this.listPersonaSinCorreo)
        .subscribe(response => {
          this.listPersonaSinCorreo = [];
          this.formularioCorreos.reset();
          this.alertCorreos.close();
          this.setDirectivos()
          this.getPersonal();

        })
    }
  }

  closeDialogCorreos() {
    this.alertCorreos.close()
    this.formularioCorreos.reset()
    this.listPersonaSinCorreo = [];
    this.listPersonalSelected = []
    this.listPersonal = []
    this.listSearchPerson = []
    this.validacionFormCorreo();
  }

  closeDialogoPersonal() {
    this.listPersonalSelected = [];
    this.listPersonal = [];
    this.listSearchPerson = [];
    this.mostrarLista = false;

  }
}
