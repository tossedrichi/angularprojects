import { Component, OnInit, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { toast, Modal } from 'materialize-css';
import { Router } from '@angular/router';
import { LoginInterface } from '../../../interfaces/login.interface';
import { AuthService } from '../../services/auth.services';
import * as crypto from 'crypto-js';
import { AuditoriaService } from '../../services/auditoria.services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    @ViewChild('btnLogin', { static: true }) btnLogin: ElementRef;

    alertTipoSession: Modal;

    datosLogin: LoginInterface;

    constructor(private auth: AuthService,
        private _auditoriaService: AuditoriaService,
        private route: Router) {
        console.log('Login Cargado');
    }

    ngOnInit() {
        let modalTipoSession = document.getElementById("modalTipoSession");
        this.alertTipoSession = Modal.init(modalTipoSession);
    }



    cryptoJS(user: string) {

        let cryp = crypto.AES.encrypt(user, "Key")

        sessionStorage.setItem("userC", cryp.toString());
        console.log('cryp', cryp.toString());
        let decrypt = crypto.AES.decrypt(cryp, "Key")
        console.log('decryp', decrypt.toString(crypto.enc.Utf8));
    }

    /*     isUserLoged() {
            let isActive = sessionStorage.getItem("sessionActive");
            if (isActive == "true") {
    
                this.auth.getPorfileType(4860).subscribe(response => {
                    console.log("Tipo Usuario", response);
    
    
                    if (response.admin || response.subAdmin) {
                       // toast({ html: 'Bienvenido ' + data.nombreUsuario, classes: 'rounded' });
    
                        if (response.admin) {
                            this.route.navigate(['administrador/menu'])
                            console.log('redirect to admin');
                        } else if (response.subAdmin) {
                            console.log('redirect to subadmin');
                            this.route.navigate(['subadministrador/menu'])
                        }
    
            }
        } */

    logIn(user: string, password: string) {
        sessionStorage.clear();
        console.log(`${user} ${password}`);
        // this.cryptoJS(user);
        //!IMPORTANT CAMBIAR USER
        this.auth.setLogin(user, password)
            .subscribe(data => {
                //console.log(data);

                this.datosLogin = data;
                //console.log(datosLogin)

                if (this.datosLogin.token != "TokenError") {
                    this.datosLogin.token = this.limpiarToken(this.datosLogin.token);
                }

                if (this.datosLogin.message === 'Autenticado') {
                    console.log('Estas Autenticado');
                    
                    this.auth.getPorfileType(parseInt(user)).subscribe(response => {
                        console.log("Tipo Usuario", response);

                        if (response.admin && response.subAdmin) {
                            this._auditoriaService.setData(response.asignarOtros)
                            this.alertTipoSession.options.dismissible = false;
                            this.alertTipoSession.open();

                        } else if (response.admin || response.subAdmin) {

                            toast({ html: 'Bienvenido ' + data.nombreUsuario, classes: 'rounded' });
                            if (response.admin) {
                                this.auth.setSession(this.datosLogin, 'admin');
                                this.route.navigate(['administrador/menu'])
                                this._auditoriaService.setData(response.asignarOtros)

                            } else if (response.subAdmin) {
                                console.log('redirect to subadmin');

                                this.auth.setSession(this.datosLogin, 'subadmin');
                                this.route.navigate(['subadministrador/menu']);
                                this._auditoriaService.setData(response.asignarOtros)
                            }

                        } else {
                            toast({ html: 'Usuario no Authorizado', classes: 'red rounded' });
                        }

                    }, error => {
                        toast({ html: 'Error al Identificar Usuario', classes: 'red rounded' });

                    })

                } else {
                    toast({ html: 'Usuario y/o Contraseña Incorrecto(s)', classes: 'red rounded' });
                }
            }, error => {
                console.log(error);

                toast({ html: `Error en el Servicio de Login`, classes: 'red rounded' });
            });
    }

    limpiarToken(token: string): string {
        token = token.split(".")[1].toString();
        return token;
    }

    tipoDeSession(tipo: number) {
        if (tipo == 1) {
            this.auth.setSession(this.datosLogin, 'admin');
            this.route.navigate(['administrador/menu'])
            this._auditoriaService.setData()

        } else if (tipo == 2) {
            console.log('redirect to subadmin');

            this.auth.setSession(this.datosLogin, 'subadmin');
            this.route.navigate(['subadministrador/menu'])
            this._auditoriaService.setData()

        }
    }
}
