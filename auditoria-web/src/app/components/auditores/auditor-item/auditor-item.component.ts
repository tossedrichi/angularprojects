import { Component, OnInit, Input } from "@angular/core";
import { AuditoriaService } from "../../../services/auditoria.services";
import { toast } from "materialize-css";
import { AuditoresComponent } from "../auditores.component";

@Component({
  selector: "app-auditor-item",
  templateUrl: "./auditor-item.component.html",
  styleUrls: ["./auditor-item.component.css"]
})
export class AuditorItemComponent implements OnInit {
  @Input() auditor;

  detallesAuditor:any;

  mostrarEliminar: boolean = false;
  mostrarDetalles: boolean = false;
  mostrarAuditorias: boolean = false;

  constructor(
    private _AuditoriaService: AuditoriaService,
    private auditorComponent: AuditoresComponent
  ) {
  }

  ngOnInit() {}

  mostrarDetallesAuditor(){
    this.getDetallesAuditor()
    this.mostrarDetalles=true;

  }  
  desplegar(): boolean {
    //TODO DETALLES DE AUDITOR
    return this.mostrarDetalles;
  }

  cambiarMostrarAuditorias() {
    //TODO MOSTRAR AUDITORIAS
    this.mostrarAuditorias = !this.mostrarAuditorias;
  }

  getDetallesAuditor() {
    this._AuditoriaService.getDetallesAuditor(this.auditor.idAuditorSubadministrador).subscribe(
      response => {
        console.log("Detalles Auditor",response);
        this.detallesAuditor = response;
      },
      error => {
        toast({ html: "Error Al Cargar Detalles", classes: "red rounded" });
      }
    );
  }

  eliminarAuditor() {
    this._AuditoriaService.setDesAsignarAuditor(this.auditor).subscribe(
      response => {
        console.log(response);
        this.auditorComponent.getAuditoresAdministrador();
        toast({ html: "Auditor Eliminado", classes: "green rounded" });
      },
      error => {
        toast({ html: "Error al Eliminar Auditor ", classes: "red rounded" });
      }
    );
  }
}
