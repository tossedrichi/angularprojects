import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { AuditoriaService } from "../../services/auditoria.services";
import { Persona } from "src/models/persona.mode.";
import { FormGroup, FormArray, Validators, FormControl } from "@angular/forms";
import { toast, Modal } from 'materialize-css';
import { fixName } from 'src/app/tools';

@Component({
  selector: "app-auditores",
  templateUrl: "./auditores.component.html",
  styleUrls: ["./auditores.component.css"]
})
export class AuditoresComponent implements OnInit,AfterContentInit {
  
  formularioCorreos: FormGroup;
  alertCorreos: Modal;
  mostrarLista: boolean = false;
  cargandoPersonal:boolean=false;
  listaAuditores: Array<Persona>;
  listSearchPerson: Array<Persona> = [];
  listPersonalSelected: Array<Persona> = [];
  listPersonaSinCorreo: Array<Persona> = [];
  listAuditoresSubadmin:Array<Persona>=[]

  constructor(
    private _AuditoriaService: AuditoriaService,
              ) {
               // this.auditorItems=new AuditorItemComponent(this._AuditoriaService);
    this.validacionesFormCorreo();
this.getAuditoresAdministrador();
  }

  ngOnInit() {}

  ngAfterContentInit(){
    var modals = document.querySelector(".modal");
    this.alertCorreos = M.Modal.init(modals);
  }

  showAlertAsigAuditores() {
    this.mostrarLista = true;
    this.getAuditores();
  }

  getAuditores() {
    this.cargandoPersonal=true;

    this._AuditoriaService.getAuditores(this._AuditoriaService.agente).subscribe(respon => {
      console.log(`Auditories de ${this._AuditoriaService.agente}`, respon);

      this.listaAuditores = respon;

      this.listaAuditores.forEach(persona => {
        persona.isSelected = false;
      });

      this.listSearchPerson = respon;
      this.cargandoPersonal=false;
    });
  }

  addAndRemoveItemsPersona(persona: Persona) {
    if (!this.listPersonalSelected.find(x => x == persona)) {
      persona.isSelected = true;
      this.listPersonalSelected.push(persona);
      console.log(this.listPersonalSelected);
    } else {
      this.listPersonalSelected = this.listPersonalSelected.filter(
        x => x != persona
      );
      console.log(this.listPersonalSelected);
    }
  }

  searchPerson(params: string) {

    console.log(params);
    console.log(this.listaAuditores);
    
    this.listSearchPerson = this.listaAuditores;

    this.listSearchPerson = this.listSearchPerson.filter(x => 
      x.nombre.toLowerCase().includes(params.toLowerCase())
    );
    console.log(this.listSearchPerson);
  }

  validacionesFormCorreo() {
    this.formularioCorreos = new FormGroup({
      correos: new FormArray([])
    });
  }

  setAuditores() {
    console.log('Send Auditories',this.listPersonalSelected);
    if (this.listPersonalSelected.length > 0) {
      
      this._AuditoriaService
        .setAuditores(this.listPersonalSelected)
        .subscribe(response => {
          console.log("Auditories Agregados",response);
          this.mostrarLista=false;
          this.getAuditoresAdministrador();
          
          toast({html:'Auditores agregados correctamente',classes:'green rounded'})
        },error=>{
          toast({html:'Error al agregar auditores',classes:'red rounded'})

        });
    }else{
      toast({html:'Selecciona almenos una persona',classes:'red rounded'})
    }
  }

  verificarCorreosPersonas() {
    this.listPersonaSinCorreo = [];

    this.listPersonalSelected.forEach(persona => {
      if (persona.correo.length == 0) {
        this.listPersonaSinCorreo.push(persona);
      }
    });

    console.log("Personas Sin Correo", this.listPersonaSinCorreo);

    if (this.listPersonaSinCorreo.length == 0) {
      this.setAuditores();
    } else {
      this.validacionesFormCorreo();
      this.alertCorreos.open();
      this.alertCorreos.options.dismissible=false;

      this.listPersonaSinCorreo.forEach(persona => {
        (<FormArray>this.formularioCorreos.controls["correos"]).push(
          new FormControl("", [Validators.required, Validators.email])
        );
      });
    }
  }

  getAuditoresAdministrador(){

    this._AuditoriaService.getAuditoriesSubadministrador(this._AuditoriaService.agente)
      .subscribe(response=>{
        toast({html:'Actualizando',classes:'green rounded'})
        this.listAuditoresSubadmin=response;

        this.listAuditoresSubadmin.forEach(auditor=>{
          auditor.nombre= fixName(auditor.nombre);
        })
        console.log("Auditores Registrados",this.listAuditoresSubadmin);
        
      })
  }

  setCorreos() {
    console.log(this.formularioCorreos);

    if (this.formularioCorreos.valid) {
      for (let i = 0; i < this.listPersonaSinCorreo.length; i++) {
        
        this.listPersonaSinCorreo[i].correo = this.formularioCorreos.controls["correos"].get(i.toString()).value;
        
      }
      console.log("Correos a Enviar", this.listPersonaSinCorreo);

      this._AuditoriaService.setActualizaCorreoPersonas(this.listPersonaSinCorreo)
        .subscribe(response=>{
          this.listPersonaSinCorreo=[];
          this.formularioCorreos.reset();
          this.alertCorreos.close(); 
          this.setAuditores()
          this.getAuditores();

        })
    }
  }

  closeDialogCorreos(){
    this.alertCorreos.close()
    this.formularioCorreos.reset()
    this.listPersonaSinCorreo=[];
    this.validacionesFormCorreo();
  }
  cerrarDialogoPersonas(){
    this.mostrarLista = false;
    this.listPersonalSelected=[];
    this.listSearchPerson=[]
    this.listaAuditores=[];
  }
}
