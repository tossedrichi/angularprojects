import {
  Component,
  AfterContentInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormSelect, toast } from "materialize-css";
import { AuditoriaService } from "../../services/auditoria.services";
import * as mapboxgl from "mapbox-gl";
import { MapBoxModel } from "../../../models/MapBox.Model";
import { SubZona } from "../../../models/subZonasAdmin.mode";
import { Sucursal } from "../../../models/Sucursal.model";
import { Zona } from "../../../models/ZonasSubAdministrador.mode";
import * as MapboxDraw from "@mapbox/mapbox-gl-draw/";
import { MarkerModel } from "../../../models/Marker.model";
import { Marker } from "mapbox-gl";
import { SetGeocercaModel } from "../../../models/SetGeocerca.model";

@Component({
  selector: "app-zonas",
  templateUrl: "./zonas.component.html",
  styleUrls: ["./zonas.component.css"]
})
export class ZonasComponent implements AfterContentInit {
  mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");
  MapboxGeocoder = require("@mapbox/mapbox-gl-geocoder");
  mapboxDraw = require("@mapbox/mapbox-gl-draw");

  @ViewChild("mapBox", { static: true }) mapBox: ElementRef;
  @ViewChild("geocoder", { static: true }) geocoder: any;
  @ViewChild("collapsibleConf", { static: true }) collapsibleConf: ElementRef;
  @ViewChild("collapsibleSuc", { static: true }) collapsibleSuc: ElementRef;
  @ViewChild("selecCedis", { static: true }) selecCedis: ElementRef;

  elems: any;
  select: FormSelect;
  nombreZona: string;
  mostrarDetalles: boolean = false;
  draw: MapboxDraw;
  listZonas: Zona[] = [];
  listSubZonas: SubZona[] = [];
  listSucursales: Sucursal[] = [];
  listSucursalesSearch: Sucursal[] = [];
  listSubAdministradores: [];
  marcadores: MarkerModel[] = [];
  editando: boolean = false;
  corCedis: { lat: number; lon: number };
  cordenadasNuevaSucursal: Array<any> = [];

  jsonSucursales: JSON;

  mapBoxParse: Array<MapBoxModel>;

  lat: number = 51.678418;
  lng: number = 7.809007;
  collapsible;
  map;

  constructor(private _AuditoriaService: AuditoriaService) {}

  ngAfterContentInit(): void {
    var elems: any = document.querySelectorAll("select");
    this.select = M.FormSelect.init(elems);
    this.initPanleConf();
    this.initConfMapBox();
    //this.localizacionCedis(1);
    this.initGeocoder();
    this.getZonas();
    this.getSubAdministradoresZona(1);
    this.getSucursales(1);

    //this.map.on("load", this.getGeoSucursales(1));
    setTimeout(x => {
      this.getGeoSucursales(1);
    }, 1000);
    //this.getGeoSucursales(1)
  }

  initPanleConf() {
    this.collapsible = M.Collapsible.init(this.collapsibleConf.nativeElement, {
      accordion: false
    });
    this.collapsible.open(0);

    let collapsibleSuc = M.Collapsible.init(this.collapsibleSuc.nativeElement, {
      accordion: true
    });

    //collapsible.open(1);
  }

  initConfMapBox() {
    this.mapboxgl.accessToken =
      "pk.eyJ1IjoidG9zc2VkcmljaGkiLCJhIjoiY2p4bTVtcjI2MDA4azNjbzZsa2x2aTBwdSJ9.36ghDSVOGwQIeXdiHCvaZQ";

    this.map = new this.mapboxgl.Map({
      container: this.mapBox.nativeElement,
      //style: "mapbox://styles/mapbox/streets-v11",
      //style: "mapbox://styles/mapbox/satellite-v9",
      style: "mapbox://styles/mapbox/outdoors-v11",

      center: [-106.08889, 28.63528],
      zoom: 10
    });
  }

  getGeoSucursales(idCedis: number) {
    console.log("Se llamo servicio");

    this._AuditoriaService.getGeocercasSucurasalesCedis(idCedis).subscribe(
      resp => {
        //console.log(resp);

        const regex = /{=/gm;
        const regex2 = /,}]/gm;

        resp = resp.replace(regex, "");
        resp = resp.replace(regex2, "]");

        try {
          this.jsonSucursales = JSON.parse(resp);
          // console.log("GEO RESP", this.jsonSucursales);
          /* if (this.map.isStyleLoaded()) { */
          this.parseData();
          /*  }else{
            console.log('el estilo aun no se carga');
            
          } */
        } catch (e) {
          toast({
            html: "No se puede convertir en un JSON valido",
            classes: "red rounded"
          });
          console.log(e);
        }
      },
      err => console.log(err)
    );
  }

  initGeocoder() {
    let geocoder = new this.MapboxGeocoder({
      accessToken: this.mapboxgl.accessToken,
      mapboxgl: this.mapboxgl,
      marker: false
    });

    document.getElementById("geocoder").appendChild(geocoder.onAdd(this.map));

    //console.log(this.geocoder.nativeElement);
  }

  parseData() {
    console.log("PASRSE DATA MAPBOX");
    this.mapBoxParse = <MapBoxModel[]>(<unknown>this.jsonSucursales);
    console.log(this.mapBoxParse);

    this.dibujarSucursales();
  }

  dibujarNuevaSucursal(sucursal: Sucursal) {
    this.listSucursales.forEach(x => {
      if (x.idSucursal != sucursal.idSucursal) {
        x.edicionHabilitada = false;
      }
    });
    console.log(this.listSucursales);

    this.editando = true;

    this.draw = new this.mapboxDraw({
      displayControlsDefault: false
    });

    this.map.addControl(this.draw);
    this.draw.changeMode("draw_polygon");
  }

  cancelarEdicion() {
    this.draw.deleteAll();
    this.map.removeControl(this.draw);
    this.editando = false;
    this.listSucursales.forEach(x => {
      x.edicionHabilitada = true;
    });
  }

  finalizarEdicion(sucursal: Sucursal) {
    console.log("Parse", this.mapBoxParse);

    this.editando = false;
    console.log(this.draw);
    console.log("Area Seleccionada", this.draw.getSelected());
    console.log("Sucursal", sucursal);
    console.log("MArcadores", this.marcadores);

    let geocercaMod = this.mapBoxParse.filter(
      x => x.idSucursal == sucursal.idSucursal
    );
    console.log("geocerca mod", geocercaMod);

    if (geocercaMod.length > 0) {
      let marcador: MarkerModel = this.marcadores.filter(
        x => x.idSucursal == sucursal.idSucursal
      )[0];

      marcador.marker.remove();

      let coordenadasMarcador: any = this.draw.getSelected();

      coordenadasMarcador =
        coordenadasMarcador.features[0].geometry.coordinates[0][0];

      let popup = new mapboxgl.Popup({ offset: 25 }).setText(
        sucursal.nombreSucursal
      );

      marcador.marker = new Marker()
        .setLngLat(coordenadasMarcador)
        .setPopup(popup)
        .addTo(this.map);

      this.marcadores.filter(
        x => x.idSucursal == sucursal.idSucursal
      )[0].marker = marcador.marker;
      //console.log('Source',this.map.getSource('fuenteSucursales')._data.features);

      let selectedDraw = this.draw.getSelected();
      let data: any = this.map.getSource("fuenteSucursales")._data;
      console.log("Data 1", data);

      this.map.getSource("fuenteSucursales")._data.features.forEach(x => {
        //console.log(x);

        if (x.id == sucursal.idSucursal) {
          selectedDraw.features[0].id = x.id;
          x = selectedDraw.features[0];
          console.log(x);

          data.features = data.features.filter(
            z => parseInt(z.id) != parseInt(x.id)
          );
          data.features.push(x);
        }
      });

      console.log("Data 2", data);
      //console.log("Data 3", JSON.parse(data));

      //console.log(this.map.getSource("fuenteSucursales")._data.features);

      this.map.getSource("fuenteSucursales").setData(data);
      data = [];
    }

    let coordenadasSucursal: any = this.draw.getSelected();
    coordenadasSucursal =
      coordenadasSucursal.features[0].geometry.coordinates[0];

    let coordenandasParse: string = "";

    coordenadasSucursal.forEach(x => {
      let coordenada: string = x.toString();
      coordenandasParse += coordenada.replace(",", " ");
      coordenandasParse += ",";
      console.log(coordenada);
    });
    coordenandasParse =
      "(" + coordenandasParse.slice(0, coordenandasParse.length - 1) + ")";

    console.log(coordenandasParse);

    let geocerca: SetGeocercaModel = new SetGeocercaModel();

    geocerca.idSucursal = sucursal.idSucursal;
    geocerca.cedis = -1;
    geocerca.color1 = "41784600";
    geocerca.color2 = "50784600";
    geocerca.coordenadas = coordenandasParse;

    console.log("Gocerca NUEVA", geocerca);

    this.setGeocercaSucursal(geocerca);
    this.cancelarEdicion();
  }

  dibujarSucursales() {
    console.log("DIBUJAR SUCURSALES");

    let features: string = "";

    this.mapBoxParse.forEach(sucursal => {
      features += JSON.stringify({
        id: sucursal.idSucursal,
        type: "Feature",
        geometry: {
          type: "Polygon",
          coordinates: [sucursal.coordenadas]
        },
        properties: {}
      });

      features += ",";

      let popup = new mapboxgl.Popup({ offset: 25 }).setText(sucursal.nombre);

      let marker: mapboxgl.Marker = new this.mapboxgl.Marker({}).setLngLat([
        sucursal.coordenadas[0][0],
        sucursal.coordenadas[0][1]
      ]);
      marker.setPopup(popup);

      marker.addTo(this.map);

      let marcador: MarkerModel = new MarkerModel();
      marcador.idSucursal = sucursal.idSucursal;
      marcador.marker = marker;
      this.marcadores.push(marcador);
    });

    features = features.slice(0, features.length - 1);
    features = "[" + features + "]";
    //console.log(features);

    this.map.addSource("fuenteSucursales", {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: JSON.parse(features)
      }
    });

    this.map.addLayer({
      id: "capaSucursales",
      type: "fill",
      source: "fuenteSucursales",
      interactive: true,
      paint: {
        "fill-color": "#004678",
        "fill-opacity": 0.8
      },
      filter: ["==", "$type", "Polygon"]
    });
  }

  getZonas() {
    this._AuditoriaService.getZonas().subscribe(
      response => {
        this.listZonas = response;
        console.log("Zonas", this.listZonas);
      },
      error => {
        toast({ html: "Error Al Cargar Zonas", classes: "red rounded" });
      }
    );
  }

  selectZona(idZona: number) {
    if (this.map.getLayer("capaSucursales")) {
      this.map.removeLayer("capaSucursales");
    }

    if (this.map.getSource("fuenteSucursales")) {
      this.map.removeSource("fuenteSucursales");
    }
    //this.localizacionCedis(idZona);
    this.mapBoxParse = [];
    this.marcadores.forEach(x => x.marker.remove());
    this.marcadores = [];

    this.getSubAdministradoresZona(idZona);
    this.getSucursales(idZona);
    this.getGeoSucursales(idZona);
  }

  getSucursales(idZona: number) {
    this.listSucursales = [];
    this._AuditoriaService.getSucursalesCedis(idZona).subscribe(
      resp => {
        console.log(resp);

        resp.forEach(x => (x.edicionHabilitada = true));

        this.listSucursales = resp;
        this.listSucursalesSearch = resp;
        this.collapsible.open(1);
      },
      err => console.log(err)
    );
  }

  searchSucursales(params: string) {
    this.listSucursalesSearch = this.listSucursales.filter(x =>
      x.nombreSucursal.toLowerCase().includes(params.toLowerCase())
    );
  }

  ubicarSucursal(idSucursal: number) {
    let sucursalUbicada: MapBoxModel = this.mapBoxParse.filter(
      x => x.idSucursal == idSucursal
    )[0];

    console.log("Coordenadas Fit", sucursalUbicada.coordenadas);

    //this.map.fitBounds(sucursalUbicada.coordenadas);

    this.map.flyTo({
      center: new mapboxgl.LngLat(
        sucursalUbicada.coordenadas[0][0],
        sucursalUbicada.coordenadas[0][1]
      ),
      zoom: 15
    });

    /* console.log(this.map.getSource(idSucursal));
    console.log(
      "Coordenadas",
      this.map.getSource('fuenteSucursales')._data.features[0].geometry.coordinates[0]
    ); */
  }

  getSubAdministradoresZona(idZona: number) {
    this.listSubAdministradores = [];
    console.log(idZona);

    this._AuditoriaService.getSubadministradoresZona(idZona).subscribe(
      response => {
        this.listSubAdministradores = response;
        console.log(this.listSubAdministradores);
      },
      error => {
        toast({
          html: "Error al Cargar SubAdministradores",
          classes: "red rounded"
        });
      }
    );
  }

  setGeocercaSucursal(geocerca: SetGeocercaModel) {
    this._AuditoriaService.setGeocerca(geocerca).subscribe(
      resp => {
        console.log(resp);

        if (resp.idGeocerca >= 1) {
          toast({ html: "Geocerca agregada", classes: "green rounded" });

          setTimeout(x => {
            this.selectZona(this.selecCedis.nativeElement.value);
          }, 1000);
        }
      },
      err => {
        console.log(err);
        toast({ html: "Error al agregar geocerca", classes: "red rounded" });
      }
    );
  }

  localizacionCedis(id: number) {
    console.log(id);

    switch (id) {
      case 3:
        this.corCedis = { lat: 31.73333, lon: -106.48333 };
        break;
      case 1:
        this.corCedis = { lat: 28.63528, lon: -106.08889 };
        break;
      case 7:
        console.log("CULIACAN");

        this.corCedis = { lat: 24.79032, lon: -107.38782 };
        break;
      case 4:
        this.corCedis = { lat: 29.1026, lon: -106.08889 };
        break;
      case 6:
        this.corCedis = { lat: 21.12908, lon: -101.67374 };
        break;
      case 8:
        this.corCedis = { lat: 32.62781, lon: -115.45446 };
        break;
      case 2:
        this.corCedis = { lat: 25.67507, lon: -100.31847 };
        break;
      case 5:
        this.corCedis = { lat: 25.42321, lon: -101.0053 };
        break;
      case 10:
        this.corCedis = { lat: 32.5027, lon: -117.00371 };
        break;
    }
    if (this.map.isStyleLoaded) {
      this.map.fitBounds([this.corCedis.lon, this.corCedis.lat]);
    }
  }
}
