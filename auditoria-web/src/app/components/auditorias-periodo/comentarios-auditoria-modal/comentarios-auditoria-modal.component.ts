import { Component, OnInit, ViewChild, Output, Input, EventEmitter, ElementRef, SimpleChanges } from '@angular/core';
import { Modal } from 'materialize-css';

@Component({
  selector: 'app-comentarios-auditoria-modal',
  templateUrl: './comentarios-auditoria-modal.component.html',
  styleUrls: ['./comentarios-auditoria-modal.component.css']
})
export class ComentariosAuditoriaModalComponent implements OnInit {

  @ViewChild('modal', { static: true }) modalRef: ElementRef;
  @ViewChild('inputComentarios', { static: true }) inputComentarios: ElementRef;

  @Input() dataInput: {
    open: boolean,
    accion: string,
    tituloVariable: string,
    todoContenido: string
  }


  /*  @Input() open: boolean;
   @Input() tituloVariable: string;
   @Input() accion: string;
   @Input() todoContenido: string;
  */
  @Output() response: EventEmitter<{ open: boolean, resp: boolean,accion:string, comentarios?: string }>;

  modal: Modal;

  constructor() {
    this.response = new EventEmitter();
  }

  ngOnInit() {
    this.modal = Modal.init(this.modalRef.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    console.log(this.inputComentarios);
    this.inputComentarios.nativeElement.height="10rem";
    if (changes) {

      if (changes.dataInput.currentValue.open) {
        this.inputComentarios.nativeElement.value = '';

        if (changes.dataInput.currentValue.accion) {
          if (changes.dataInput.currentValue.accion == "Rechazar") {
            this.inputComentarios.nativeElement.value = changes.dataInput.currentValue.todoContenido.trim();
          }
        }

        this.modal.options.dismissible = false;
        this.modal.open();
      }
    }

  }

  onConfrimClick() {
    console.log(this.inputComentarios.nativeElement.value);

    this.modal.close();
    this.response.emit({
      open: false,
      resp: true,
      accion:this.dataInput.accion,
      comentarios: '\n' + this.dataInput.tituloVariable +
        ': \n' + this.inputComentarios.nativeElement.value
    });
  }

  onDenyClick() {
    this.modal.close();
    this.response.emit({ open: false, resp: false,accion:'' });
  }
}
