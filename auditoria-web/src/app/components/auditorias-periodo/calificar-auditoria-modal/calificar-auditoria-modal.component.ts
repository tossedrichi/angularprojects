import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { Modal } from 'materialize-css';

@Component({
  selector: 'app-calificar-auditoria-modal',
  templateUrl: './calificar-auditoria-modal.component.html',
  styleUrls: ['./calificar-auditoria-modal.component.css']
})
export class CalificarAuditoriaModalComponent implements OnInit, OnChanges {
  
  @ViewChild('modal', { static: true }) modalRef: ElementRef;
  @Input() open: boolean;
  @Output() response: EventEmitter<{ open: boolean, resp: boolean }>;

  modal: Modal;

  constructor() {
    this.response = new EventEmitter();
  }

  ngOnInit() {
    this.modal = Modal.init(this.modalRef.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.open.currentValue) {
      this.modal.options.dismissible = false;
      this.modal.open();
    }

  }

  onConfrimClick() {
    this.modal.close();
    this.response.emit({ open: false, resp: true });
  }

  onDenyClick() {
    this.modal.close();
    this.response.emit({ open: false, resp: false });
  }

}
