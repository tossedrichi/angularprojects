import { Component, OnInit, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { AuditoriaService } from "src/app/services/auditoria.services";
import { toast } from "materialize-css";
import { AuditoriaAsignada } from "src/models/AsigancionAuditoria.model";

@Component({
  selector: "app-auditorias-periodo",
  templateUrl: "./auditorias-periodo.component.html",
  styleUrls: ["./auditorias-periodo.component.css"]
})
export class AuditoriasPeriodoComponent implements OnInit, AfterContentInit {
  
  @ViewChild("refFiltro", { static: true }) filtroSelect:ElementRef
  
  mostrarLista: boolean = false;
  mostrarDetalles: boolean = false;
  cargandoAuditorias:boolean = false;

  listAuditoriasPeriodo: AuditoriaAsignada[] = [];
  listAuditoriasFiltradas: AuditoriaAsignada[] = [];
  filtroActivo:string;
  
  constructor(private _AuditoriaService: AuditoriaService) {}

  ngOnInit() {
    this.getListaAsiganaciones(true);
  }
  ngAfterContentInit(): void {
    document.addEventListener("DOMContentLoaded", function() {
      var elems = document.querySelectorAll("select");
      var instances = M.FormSelect.init(elems);
    });
  }

  filtroAuditorias(estado: string) {
    this.filtroActivo=estado;

    console.log("filtro Actual",estado);
    

    switch (estado) {
      case "Todas":
        this.listAuditoriasFiltradas = this.listAuditoriasPeriodo;
        break;
        
      default:
        this.listAuditoriasFiltradas = this.listAuditoriasPeriodo.filter(
          auditoria => auditoria.estado == estado
        );
        break;
    }
  }

  getListaAsiganaciones(isRefresh?:boolean) {
    this.cargandoAuditorias=true;
    this.listAuditoriasPeriodo = [];
    this.listAuditoriasFiltradas = [];
    this._AuditoriaService.getListaAsignacionesPeriodo(this._AuditoriaService.agente).subscribe(
      response => {
        console.log("Cabezera",response);
        this.listAuditoriasPeriodo = response;
        this.listAuditoriasFiltradas = response;
        console.log(this.filtroActivo);
        console.log("es refresh",isRefresh);
        
        if(isRefresh){
          this.filtroAuditorias("Pendiente")
          this.filtroActivo="Pendiente";
          this.filtroSelect.nativeElement.value=this.filtroActivo

        }else{
        
          this.filtroAuditorias(this.filtroActivo);
        }
        this.cargandoAuditorias=false;

      },
      error => {
        this.cargandoAuditorias=false;

        toast({ html: "Error al cargar auditorias del periodo" });
      }
    );
  }
}
