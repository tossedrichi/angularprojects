import { Component, OnInit, ViewChild, ElementRef, Input, SimpleChanges, OnChanges, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { Modal, toast, Sidenav } from 'materialize-css';
import { DetalleAsignacionModel } from 'src/models/DetalleAsignacionModel';
import { AuditoriaService } from '../../../services/auditoria.services';
import { AuditoriaAsignada } from '../../../../models/AsigancionAuditoria.model';
import { Varibale } from 'src/models/Auditoria.model';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FileItem } from '../../../../models/file-item.model';

@Component({
  selector: 'app-revision-auditoria-modal',
  templateUrl: './revision-auditoria-modal.component.html',
  styleUrls: ['./revision-auditoria-modal.component.css']
})
export class RevisionAuditoriaModalComponent implements OnInit, OnChanges, AfterContentInit {
  @ViewChild('stepper', { static: false }) stepper: any;
  @ViewChild('sidenav', { static: true }) sidenav: any;
  @ViewChild('modal', { static: true }) modal: ElementRef;
  @ViewChild('contenedorVariable', { static: false }) contenedorVariable: ElementRef;

  @Input() auditoria: AuditoriaAsignada;
  @Input() open: boolean;

  @Output() close: EventEmitter<any>;
  @Output() acutualizarListaAsignaciones: EventEmitter<any>;
  modalInstance: Modal;
  openSideNav: boolean = false;
  detalleAsigancion: DetalleAsignacionModel;

  openModalPrevImagen: boolean = false;
  openModalConfirm: boolean = false;
  openModalComentarios: boolean = false;
  accionComentarios: string;
  imageSelected: FileItem;

  variableSeleccionada: Varibale;
  listImagenesEvidencia: FileItem[];
  listImagenesMuestra: FileItem[];



  cargandoMuestras: boolean = false;
  cargandoImagenesEvidencia: boolean = false;

  comentariosAuditoria: string = '';

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 3,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    centeredSlides: false,
    centerInsufficientSlides: true,
  };

  constructor(private _AuditoriaService: AuditoriaService) {
    this.close = new EventEmitter();
    this.acutualizarListaAsignaciones = new EventEmitter();
    this.comentariosAuditoria = '';
  }

  ngOnInit() { }

  ngAfterContentInit() {
    this.modalInstance = Modal.init(this.modal.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges): void {

    console.log(changes);
    if (changes.open.currentValue) {

      console.log(this.sidenav);
      this.modalInstance.options.endingTop = "5%";
      this.modalInstance.options.dismissible = false;
      this.modalInstance.open();
      this.getDetallesAsignacion();

    }

  }

  onChangeVariable(event: any) {

    console.log(event);
    console.log(this.detalleAsigancion.variables[event.selectedIndex]);
    this.contenedorVariable.nativeElement.scrollTop = '0';
    this.variableSeleccionada = this.detalleAsigancion.variables[event.selectedIndex]
    this.getImagenesModelVariable();
    this.listImagenesEvidencia = [];

  }

  getDetallesAsignacion() {
    console.log("Obteniendo detalles Aisgnacion:", this.auditoria.idAsignacion);

    this._AuditoriaService.getDetallesAsigancion(this.auditoria.idAsignacion)
      .subscribe(response => {
        console.log("AFTER", this.stepper);

        console.log("DETALLE", response);
        this.detalleAsigancion = response;
        this.openSideNav = true;

        this.variableSeleccionada = this.detalleAsigancion.variables[0];
        this.getImagenesModelVariable();

      }, error => {
        toast({ html: 'Error al Cargar Detalles' })
      })
  }

  getImagenesModelVariable() {
    console.log("id en ennvidencia", this.variableSeleccionada.idVariable);
    this.listImagenesMuestra = [];
    this.cargandoMuestras = true;
    this._AuditoriaService
      .getFotosModeloVariable(this.variableSeleccionada.idVariable)
      .then(response => {
        console.log(response);
        console.log("Imagenes muestra cargadas");
        this.listImagenesMuestra = response;
        this.cargandoMuestras = false;

        this.listImagenesMuestra.forEach(imagen => {
          imagen.base64 = "data:image/jpeg;base64," + imagen.base64;
        })

        console.log("Iniciando Carga Evidencia");
        this.getEvidencaiVariable();

      }, error => {
        toast({ html: 'Error al cargar imagenes modelo', classes: 'red rounded' })
        // this.cargandoMuestras = false;
      });
  }

  getEvidencaiVariable() {
    //this.cargandoEvidencias = true;
    console.log("id en ennvidencia", this.variableSeleccionada.idDetalleAsignacion);

    this._AuditoriaService.getEvidenciasDetalleAsignacion(this.variableSeleccionada.idDetalleAsignacion)
      .subscribe(response => {
        console.log("Imagenes evidencia cargadas");

        this.listImagenesEvidencia = response;

        this.listImagenesEvidencia.forEach(imagen => {
          imagen.base64 = "data:image/jpeg;base64," + imagen.base64;
        })
        console.log(this.listImagenesEvidencia);
        this.cargandoImagenesEvidencia = false;
      }, error => {
        toast({ html: 'Error al cargar imagenes de evidencia', classes: 'red rounded' })
        this.cargandoImagenesEvidencia = false;
      }
      )
  }

  mostrarComentarioYEvidencias(): boolean {
    if (this.detalleAsigancion.estado === "Pendiente"
      || this.detalleAsigancion.estado === "Aprobada"
      || this.detalleAsigancion.estado === "Rechazada") {
      return true;
    }
    return false;
  }

  cerrar() {
    this.close.emit();
    this.modalInstance.close();
  }


  respDialogiConfirm(event: any) {
    console.log(event);

    this.openModalConfirm = event.open;

    if (event.resp) {
      console.log('APROBADA');
      if (this.comentariosAuditoria.trim() == '') {
        this.comentariosAuditoria = 'NA';
      }
      this.setEstadoAuditoria('Aprobada', this.comentariosAuditoria);
    }

  }

  respDialogiComentarios(event: any) {
    console.log('EVENTO ', event);

    this.openModalComentarios = event.open;

    if (event.resp) {

      if (event.accion == 'Rechazar') {
        this.setEstadoAuditoria('Rechazada', event.comentarios.trim());
      } else {
        this.comentariosAuditoria = this.comentariosAuditoria.concat('\n' + event.comentarios);
        console.log('Comentarios de Auditoria', this.comentariosAuditoria);
      }
    }

  }

  openPrevImagen(img: FileItem) {
    this.imageSelected = img;
    this.openModalPrevImagen = true;
  }

  respDialogPrev(event: any) {
    this.openModalPrevImagen = event.open;
  }

  setEstadoAuditoria(accion: string, comentario: string) {
    console.log(comentario);

    if (comentario.trim() != "") {
      this._AuditoriaService.setCambiaEstadoAsignacion(this.detalleAsigancion.idAsignacion, accion, comentario)
        .subscribe(response => {
          if (accion == 'Aprobada') {
            toast({ 
              html: "Auditoria Aprobada Correctamente",
              classes: 'green rounded' })
          } else {
            toast({ html: "Auditoria Rechazada Correctamente", classes: 'green rounded' })
          }
          this.cerrar();
          this.acutualizarListaAsignaciones.emit()
        }, error => {
          toast({ html: "Error al Rechazar Auditoria", classes: 'red rounded' })
        });
    }
  }


  
  compareSize(index: number): boolean {

    if ((index + 1) == this.detalleAsigancion.variables.length) {
      return true;
    }
    return false;
  }


  esLineal(): boolean {
    if (this.auditoria.estado == 'Pendiente') {
      return true;
    }
    return false;
  }

}
