import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AuditoriaAsignada } from "src/models/AsigancionAuditoria.model";
import { toast } from "materialize-css";
import { AuditoriaService } from 'src/app/services/auditoria.services';
import { DetalleAsignacionModel } from 'src/models/DetalleAsignacionModel';

@Component({
  selector: "app-auditoper-item",
  templateUrl: "./auditoper-item.component.html",
  styleUrls: ["./auditoper-item.component.css"]
})
export class AuditoperItemComponent implements OnInit {

  @Input() auditoria: AuditoriaAsignada;
  @Output() acutualizarListaAsignaciones: EventEmitter<any>

  detalleAsigancion: DetalleAsignacionModel;

  mostrarEliminar: boolean = false;
  mostrarAsignacion: boolean = false;
  mostrarContenedorZonas: boolean = false;
  mostrarRechazarAuditoria: boolean = false;
  puedeCalificarse: boolean = false;

  constructor(private _AuditoriaService: AuditoriaService) {
    this.acutualizarListaAsignaciones = new EventEmitter();
  }

  ngOnInit() {

    console.log("onInit");
    this.getDetallesAsignacion();
  }

  openDetallesAuditoria() {
    console.log("Detalles Abiertos");
    console.log(this.detalleAsigancion);
  
    if (this.detalleAsigancion.estado == "Pendiente") {
      this.puedeCalificarse = true;
    } else {
      this.puedeCalificarse = false;
    }

    this.mostrarAsignacion = true;


  }

  getDetallesAsignacion() {
    console.log("Obteniendo detalles Aisgnacion:",this.auditoria.idAsignacion);
    
    this._AuditoriaService.getDetallesAsigancion(this.auditoria.idAsignacion)
      .subscribe(response => {
        console.log("DETALLE",response);
        this.detalleAsigancion = response;

      },error=>{
        toast({html:'Error al Cargar Detalles'})
      })
  }

  actualizarLista(){
    this.acutualizarListaAsignaciones.emit();
  }

  cambiarMostrarContenedorZonas() {
    this.mostrarContenedorZonas = !this.mostrarContenedorZonas;
  }

  /**
   * Se rechaza auditoria
   * funcion para rechazar una auditorias ya asignada y contestada
   * @param comentario 
   */

  serRechazarAuditoria(comentario: string) {
    console.log(comentario);

    if (comentario.trim() != "") {
      this._AuditoriaService.setCambiaEstadoAsignacion(this.detalleAsigancion.idAsignacion, "Rechazada", comentario)
        .subscribe(response => {
          toast({ html: "Auditoria Rechazada Correctamente", classes: 'green rounded' })
          this.mostrarAsignacion = false;
          this.mostrarRechazarAuditoria = false;
          this.acutualizarListaAsignaciones.emit()
        }, error => {
          toast({ html: "Error al Rechazar Auditoria", classes: 'red rounded' })
        })
    }
  }
  /**
   * Set aprobar auditoira
   */
  setAprobarAuditoira() {
    this._AuditoriaService.setCambiaEstadoAsignacion(this.detalleAsigancion.idAsignacion, "Aprobada", "NA")
      .subscribe(response => {
        console.log(response);

        toast({ html: "Auditoria Aprobada Correctamente", classes: 'green rounded' })
        this.acutualizarListaAsignaciones.emit()

      }, error => {
        toast({ html: "Error al Aprobar Auditoria", classes: 'red rounded' })

      })
  }

  sePuedeEliminar(): boolean {
    if (this.detalleAsigancion != undefined) {
      if (this.detalleAsigancion.estado == "Reasignada" ||
        this.detalleAsigancion.estado == "Asignada") {
        return true;
      }
    }
    return false;
  }

  setEliminarAsignacion() {
    this._AuditoriaService.setElimiarAsignacion(this.detalleAsigancion.idAsignacion)
      .subscribe(response => {
        this.mostrarEliminar = false;
        toast({ html: 'Asignacion Eliminada', classes: 'green rounded' })
        this.acutualizarListaAsignaciones.emit();
      }, error => {
        toast({ html: 'Error al Eliminar Asignacion', classes: 'red rounded' })
      });
  }

}
