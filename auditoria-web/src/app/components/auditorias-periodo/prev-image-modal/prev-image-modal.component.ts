import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FileItem } from '../../../../models/file-item.model';
import { Modal } from 'materialize-css';

@Component({
  selector: 'app-prev-image-modal',
  templateUrl: './prev-image-modal.component.html',
  styleUrls: ['./prev-image-modal.component.css']
})
export class PrevImageModalComponent implements OnInit, OnChanges {

  @ViewChild('modal', { static: true }) modalRef: ElementRef;
  modal: Modal;

  @Input() dataInput: {
    open: boolean,
    img: FileItem
  };

  @Output() resp: EventEmitter<{ open: boolean }>;

  constructor() {
    this.resp = new EventEmitter();
  }

  ngOnInit() {
    this.modal = Modal.init(this.modalRef.nativeElement);

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    this.dataInput = changes.dataInput.currentValue;
    if (changes.dataInput.currentValue.open) {
      this.modal.options.dismissible = false;
      this.modal.open();
    }
  }


  onCloseClick() {
    this.modal.close();
    this.resp.emit({ open: false });
  }

}
