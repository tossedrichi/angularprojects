import { Component, OnInit } from '@angular/core';
import { AuditoriaService } from 'src/app/services/auditoria.services';
import { toast, Modal } from 'materialize-css';
import { AuditoriaCreada } from 'src/models/AuditoriaCreada.model';
import { ModalTipoAsignacionService } from './modal-tipo-asignacion/modal-tipo-asignacion.service';

@Component({
  selector: 'app-asig-auditorias',
  templateUrl: './asig-auditorias.component.html',
  styleUrls: ['./asig-auditorias.component.css']
})
export class AsigAuditoriasComponent implements OnInit {


  alertEditarAuditoria:Modal;

  listAuditoriasCreadas:AuditoriaCreada[]=[];
  listAuditoriasSearch:AuditoriaCreada[]=[];
  lodingAuditoiriasCreadas=false;

  mostrarLista: boolean =false;
  mostrarDetalles: boolean=false;
  mostrarAsignacion: boolean = false;
  modalErrorAsignarAuditorias:Modal;
  constructor(private _AuditoriaService:AuditoriaService,
              ) { }

  ngOnInit() {
    this.getAduitoriasSubAdministrador();
    console.log("AsignarOtros",this._AuditoriaService.asignarOtros);
    
  }

  getAduitoriasSubAdministrador(){
    this.lodingAuditoiriasCreadas=true;
    this._AuditoriaService.getAuditoriasSubadministrador(this._AuditoriaService.agente)
      .subscribe(response=>{
        console.log("Auditorias: ",response);
        this.listAuditoriasCreadas=response;
        this.listAuditoriasSearch=this.listAuditoriasCreadas;
        this.lodingAuditoiriasCreadas=false;
      }, error=>{
        toast({html:'Error al Cargar Auditorias',classes:"red rounded"})
        this.lodingAuditoiriasCreadas=false;

      })
  }

  searchAuditorias(params:string){

    this.listAuditoriasSearch=this.listAuditoriasCreadas;

    this.listAuditoriasSearch=this.listAuditoriasSearch
      .filter(x=> x.nombreAuditoria.toLowerCase().includes(params.toLowerCase()));
  }

}
