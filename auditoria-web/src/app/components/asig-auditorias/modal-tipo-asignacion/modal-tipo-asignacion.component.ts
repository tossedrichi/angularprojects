import { Component, OnInit, ViewChild, ElementRef, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import { toast } from 'materialize-css';
import { ModalTipoAsignacionService } from './modal-tipo-asignacion.service';
import { AuditoriaService } from '../../../services/auditoria.services';
import { SubAdministradorModel } from '../../../../models/SubAdministrador.model';

@Component({
  selector: 'app-modal-tipo-asignacion',
  templateUrl: './modal-tipo-asignacion.component.html',
  styleUrls: ["../asig-item/asig-item.component.css"]


})
export class ModalTipoAsignacionComponent implements OnInit {

  @ViewChild('modalTipoAsignacion', { static: false }) modalStruct: ElementRef;


  listSubadministradores: SubAdministradorModel[] = [];
  listSearch: SubAdministradorModel[] = [];

  constructor(public _modalTipoAsignacionService: ModalTipoAsignacionService,
    private _auditoriaService: AuditoriaService) {


    /* this._modalTipoAsignacionService.abrirModal.subscribe(resp => {
      this._modalTipoAsignacionService.openDialog();
      this.getListSubAdministradores();
    }) */

  }

  ngOnInit(): void {
    this.openAlert();
  }

  getListSubAdministradores() {
    this.listSubadministradores = [];

    this._auditoriaService.getSubAdministradores()
      .subscribe(response => {
        console.log(response);
        
        this.listSubadministradores = response;
        this.listSearch = response;
      }, err => {
        toast({ html: 'Error al Cargar SubAdministradores', classes: 'red rounded' })
      })
  }


  searchAdmin(params: string) {
    if (params != "") {
      this.listSearch = this.listSubadministradores.filter(x => x.nombre.toLowerCase().includes(params.toLowerCase()));
    } else {
      this.listSearch = this.listSubadministradores;
    }
  }

  onClickSubAdmin(subAdmin: SubAdministradorModel) {
    this._modalTipoAsignacionService.notificacion.emit(subAdmin);
    this._modalTipoAsignacionService.closeDialog();
  }

  openAlert() {
   // this.mostrarAlert = true;
    this.getListSubAdministradores();
  }

  closeAlert() {
    this.listSearch = [];
    this.listSubadministradores = [];
    this._modalTipoAsignacionService.closeDialog();
    //this._modalTipoAsignacionService.mostrarDialog = false;
  }





}
