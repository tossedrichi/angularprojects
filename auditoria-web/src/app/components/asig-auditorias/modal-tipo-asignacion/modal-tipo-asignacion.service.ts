import { Injectable, EventEmitter } from '@angular/core';
import { element } from 'protractor';
import { Modal } from 'materialize-css';

@Injectable({
  providedIn: 'root'
})
export class ModalTipoAsignacionService {

  public notificacion = new EventEmitter<any>();
  /*   private modal: Modal; */
  public mostrarDialog: boolean = false;

  constructor() { 
  }


/*   setModalStruct(element: Element) {
    this.modal = M.Modal.init(element);
    this.modal.options.dismissible = false;
    this.modal.options.preventScrolling = true;

  } */

  openDialog() {
    this.mostrarDialog = true;
  
  }

  

  closeDialog() {
    this.notificacion.unsubscribe();
    this.mostrarDialog = false;
  }

}
