import { Component, OnInit, Input, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { Varibale } from "src/models/Auditoria.model";
import { AuditoriaService } from "src/app/services/auditoria.services";
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FileItem } from 'src/models/file-item.model';
import { Modal, toast } from 'materialize-css';

@Component({
  selector: "app-variable-item",
  templateUrl: "./variable-item.component.html",
  styleUrls: [".././asig-item.component.css"]
})
export class VariableItemComponent implements OnInit, AfterContentInit {
  @Input() variable: Varibale;
  @Input() esDetalleAsignacion: boolean;
  @Input() estado:string;
  @ViewChild("imgElement", { static: false }) imgElement: ElementRef;

  mostrarImg: boolean = false;
  mostrarDetalles: boolean = false;
  imagenesModeloCargadas: boolean = false;
  imagenesEvidenciaCargadas: boolean = false;
  cargandoEvidencias: boolean = false;
  cargandoMuestras: boolean = false;

  listImagenesEvidencia: FileItem[] = [];

  modalPrevImagen: Modal;

  imagenPrev: any;




  constructor(private _AuditoriaService: AuditoriaService) { }

  ngOnInit() {

  }

  ngAfterContentInit() {

  }


  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 3,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    centeredSlides: false,
    centerInsufficientSlides: true,
    /* height:150,
    width:150 */
  };

  mostrarCalificacion():boolean {
    if ((this.estado.toString() == 'Pendiente')
      || (this.estado.toString() == "Aprobada")
      || (this.estado.toString() == 'Rechazada')) {

      return true;

    } else {
      return false;
    }
  }

  mostrarDetallesVariable() {
    this.mostrarDetalles = !this.mostrarDetalles;

    if (this.esDetalleAsignacion) {
      if (!this.imagenesModeloCargadas) {
        console.log("Iniciando Carga Muestra");

        this.getImagenesModelVariable();
      }


    } else {
      if (!this.imagenesModeloCargadas) {
        this.getImagenesModelVariable();
      }
    }
  }

  getImagenesModelVariable() {
    console.log("id en ennvidencia", this.variable.idVariable);

    this.cargandoMuestras = true;
    this._AuditoriaService
      .getFotosModeloVariable(this.variable.idVariable)
      .then(response => {
        console.log(response);
        console.log("Imagenes muestra cargadas");

        this.variable.imagenes = response;
        this.imagenesModeloCargadas = true;
        this.cargandoMuestras = false;

        this.variable.imagenes.forEach(imagen => {
          imagen.base64 = "data:image/jpeg;base64," + imagen.base64;
        })
        if (this.esDetalleAsignacion) {
          if (!this.imagenesEvidenciaCargadas) {
            console.log("Iniciando Carga Evidencia");

            this.getEvidencaiVariable();
          }
        }

        console.log(this.variable);

      }, error => {
        toast({ html: 'Error al cargar imagenes modelo', classes: 'red rounded' })
        this.cargandoMuestras = false;

      });
  }

  getEvidencaiVariable() {
    this.cargandoEvidencias = true;
    console.log("id en ennvidencia", this.variable.idDetalleAsignacion);

    this._AuditoriaService.getEvidenciasDetalleAsignacion(this.variable.idDetalleAsignacion)
      .subscribe(response => {
        console.log("Imagenes evidencia cargadas");

        this.imagenesEvidenciaCargadas = true;
        this.listImagenesEvidencia = response;
        this.listImagenesEvidencia.forEach(imagen => {
          imagen.base64 = "data:image/jpeg;base64," + imagen.base64;
        })

        console.log(this.listImagenesEvidencia);
        this.cargandoEvidencias = false;

      }, error => {
        toast({ html: 'Error al cargar imagenes de evidencia', classes: 'red rounded' })
        this.cargandoEvidencias = false;

      })
  }

  mostrarImagen(imagen: FileItem) {
    console.log("imagen cargada", imagen.idImagen);
    if (this.modalPrevImagen != undefined) {
      this.modalPrevImagen.destroy()
      this.modalPrevImagen = undefined;
    }
    console.log("imagen cargada", imagen.base64)
    this.mostrarImg = true;
    this.imagenPrev = imagen.base64;
    let modalPrev = document.getElementById("modalImagenPrev");
    this.modalPrevImagen = Modal.init(modalPrev);
    // this.modalPrevImagen.open();
    //this.imgElement.nativeElement.src=imagen.base64;

  }

  mostrarComentarios() {
    /* console.log("legth",
      this.variable.comentario.toString().trim().length);
    console.log(this); */

    if (this.esDetalleAsignacion) {
      if (this.variable.comentario.trim().length > 1
        || this.variable.comentario != "NA") {
        console.log("mostrar comentarios: true");

        return true;
      }
    }
    return false;
  }

}
