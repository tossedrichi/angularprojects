import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AuditoriaService } from "../../../services/auditoria.services";
import { toast, Modal } from "materialize-css";
import { Persona } from "src/models/persona.mode.";
import { fixName } from "src/app/tools";
import { Auditoria } from "src/models/Auditoria.model";
import { AuditoriaCreada } from "src/models/AuditoriaCreada.model";
import { Sucursal } from "src/models/Sucursal.model";
import { AsigancionAuditoria } from "src/models/AsigancionAuditoria.model";
import { FileItem } from 'src/models/file-item.model';
import { ModalTipoAsignacionService } from '../modal-tipo-asignacion/modal-tipo-asignacion.service';
import { SubAdministradorModel } from '../../../../models/SubAdministrador.model';
import { ModalTipoAsignacionComponent } from '../modal-tipo-asignacion/modal-tipo-asignacion.component';

@Component({
  selector: "app-asig-item",
  templateUrl: "./asig-item.component.html",
  styleUrls: ["./asig-item.component.css"]
})
export class AsigItemComponent implements OnInit {
  @Input() auditoria: AuditoriaCreada;

  @Output() actualizarListaAuditorias: EventEmitter<null>;

  modalErrorAsignarAuditorias: Modal;

  detallesAuditoria: Auditoria;
  auditorSeleccionado: Persona;

  mostrarEditarAuditoria: boolean = false;
  mostrarAsignacion: boolean = false;
  mostrarAuditoria: boolean = false;
  mostrarSeleccionar: boolean = false;
  mostrarSucursales: boolean = false;
  mostrarContenedorZonas: boolean = false;
  mostrarEliminar: boolean = false;
  cargandoAuditores: boolean = false;

  modalSubAdmins: boolean = false;

  selecTodasSucursales: boolean = false;

  listAuditoresSubAdmin: Array<Persona> = [];
  listAuditoresSubAdminSearch: Array<Persona> = [];
  listSucursales: Sucursal[] = [];
  listSucursalesSeleccionadas: Sucursal[] = [];
  listAsignacionesConError: AsigancionAuditoria[] = [];
  listSubadministradores: SubAdministradorModel[] = [];
  listSearchAdmins: SubAdministradorModel[] = [];
  listArchivos: FileItem[] = [];

  fechaProgramada: string = "";
  alertEditarAuditoria: Modal;

  constructor(public _AuditoriaService: AuditoriaService) {
    this.actualizarListaAuditorias = new EventEmitter();
  }

  ngOnInit() {


  }

  editarAuditoria() {
    this.mostrarEditarAuditoria = !this.mostrarEditarAuditoria;
  }

  cambiarMostrarSucursales() {
    this.mostrarSucursales = !this.mostrarSucursales;
  }

  cambiarMostrarContenedorZonas() {
    this.mostrarContenedorZonas = !this.mostrarContenedorZonas;
  }

  mostrarDetallesAuditoria() {
    this.getDetallesAuditoria().then(() => {
      this.mostrarAuditoria = true;

    }).catch(err => {
      toast({ html: 'Error al Cargar Auditoria', classes: 'red rounded' })
      console.error("Error al Cargar Auditoria", err);
    });
  }

  /*   getSubAdmniSelected(subAdmin):SubAdministradorModel{
      console.log("SubAdmin Recibido",subAdmin);
      return subAdmin;
    } */

  mostrarSubAdmins() {
    this.modalSubAdmins = true;
    this.getListSubAdministradores();
  }

  closeSubAdmins() {
    this.modalSubAdmins = false;
    this.listSubadministradores = [];
    this.listSearchAdmins = [];
  }

  getListSubAdministradores() {
    this.listSubadministradores = [];

    this._AuditoriaService.getSubAdministradores()
      .subscribe(response => {
        console.log(response);

        this.listSubadministradores = response;
        this.listSearchAdmins = response;
      }, err => {
        toast({ html: 'Error al Cargar SubAdministradores', classes: 'red rounded' })
      })
  }


  searchAdmin(params: string) {
    if (params != "") {
      this.listSearchAdmins = this.listSubadministradores.filter(x => x.nombre.toLowerCase().includes(params.toLowerCase()));
    } else {
      this.listSearchAdmins = this.listSubadministradores;
    }
  }

  mostrarAuditores(subAdmin: SubAdministradorModel) {

    this.modalSubAdmins = false;
    if (this._AuditoriaService.asignarOtros == true) {
      this.getAuditores(subAdmin.idPersona);
    } else {
      this.getAuditores(this._AuditoriaService.agente);

    }
    this.mostrarAsignacion = true;

    this.getDetallesAuditoria().then(() => {
      this.mostrarAsignacion = true;
    }).catch(err => {
      toast({ html: 'Error al Cargar Auditoria', classes: 'red rounded' })
      console.error("Error al Cargar Auditoria", err);
    });

  }

  mostrarAsignacionAuditoria(auditor: Persona) {
    this.auditorSeleccionado = auditor;
    this.getSucursales();
    this.mostrarAsignacion = false;
    this.mostrarSeleccionar = true;
  }

  searchAuditor(params: string) {
    console.log(params);

    this.listAuditoresSubAdminSearch = this.listAuditoresSubAdmin;

    this.listAuditoresSubAdminSearch = this.listAuditoresSubAdminSearch.filter(
      x => x.nombre.toLowerCase().includes(params.toLowerCase())
    );
  }

  seleccionarTodasSucursales() {
    this.selecTodasSucursales = !this.selecTodasSucursales;

    if (this.selecTodasSucursales) {
      this.listSucursalesSeleccionadas = [];
      this.listSucursales.forEach(x => (x.selected = true));
      this.listSucursalesSeleccionadas = this.listSucursales;
    } else {
      this.listSucursalesSeleccionadas = [];
      this.listSucursales.forEach(x => (x.selected = false));
    }

    console.log(this.listSucursalesSeleccionadas);
  }

  seleccionarSucursal(sucursal: Sucursal) {
    if (
      !this.listSucursalesSeleccionadas.find(
        x => x.idSucursal == sucursal.idSucursal
      )
    ) {
      console.log("Sucursal Seleccionada");
      this.listSucursalesSeleccionadas.push(sucursal);
    } else {
      console.log("Sucursal Des-Seleccionada");
      this.listSucursalesSeleccionadas = this.listSucursalesSeleccionadas.filter(
        x => x.idSucursal != sucursal.idSucursal
      );
    }
    console.log(this.listSucursalesSeleccionadas);
  }

  async getDetallesAuditoria() {
    await this._AuditoriaService
      .getDetallesAuditoria(this.auditoria.idAuditoria)
      .then(
        response => {
          console.log(response);
          this.detallesAuditoria = response;
        },
        error => {
          toast({
            html: "Error al cargar los datos de la auditoria",
            classes: "red rounded"
          });
        }
      );
  }

  getAuditores(idPersona: number) {
    this.cargandoAuditores = true;
    this._AuditoriaService.getAuditoriesSubadministrador(idPersona).subscribe(
      response => {
        console.log(response);
        this.listAuditoresSubAdmin = response;

        this.listAuditoresSubAdmin.forEach(auditor => {
          auditor.nombre = fixName(auditor.nombre);
        });

        this.listAuditoresSubAdminSearch = this.listAuditoresSubAdmin;
        this.cargandoAuditores = false;
      },
      error => {
        this.cargandoAuditores = false;

        toast({ html: "Error al CargarAuditores", classes: "red rounded" });
      }
    );
  }

  getSucursales() {
    this._AuditoriaService
      .getSucursales(this._AuditoriaService.agente, this.detallesAuditoria.subZonas)
      .subscribe(
        response => {
          console.log(response);
          this.listSucursales = response;
        },
        error => {
          toast({ html: "Error al cargar sucursales", classes: "red rounded" });
        }
      );
  }

  setAsignacionAuditoria() {
    let asigancionValida: boolean = true;
    let date: Date = new Date();
    date.setHours(0, 0, 0, 0);

    if (this.listSucursalesSeleccionadas.length <= 0) {
      toast({
        html: "Selecciona al menos una sucursal",
        classes: "red rounded"
      });
      asigancionValida = false;
    }

    if (this.fechaProgramada.toString() == "") {
      toast({ html: "Selecciona una fecha", classes: "red rounded" });
      asigancionValida = false;
    }

    if (date.getTime() > new Date(this.fechaProgramada).setHours(24)) {
      toast({
        html: "La fecha debe ser mayor al dia de hoy",
        classes: "red rounded"
      });
      asigancionValida = false;
    }

    if (asigancionValida) {
      this.mostrarSeleccionar = false;
      let listAuditoriaAsignadas: AsigancionAuditoria[] = [];

      this.listSucursalesSeleccionadas.forEach(sucursal => {
        let auditoriaAsignada = new AsigancionAuditoria();
        /* auditoriaAsignada.Agente = 1;
        auditoriaAsignada.idOrigen = 1; */
        auditoriaAsignada.idAuditorSubadministrador = this.auditorSeleccionado.idAuditorSubadministrador;
        auditoriaAsignada.idAuditoria = this.auditoria.idAuditoria;
        auditoriaAsignada.idSucursal = sucursal.idSucursal;
        auditoriaAsignada.fechaProgramacion = this.fechaProgramada;

        listAuditoriaAsignadas.push(auditoriaAsignada);
      });

      this._AuditoriaService
        .setAsignarAuditoria(listAuditoriaAsignadas)
        .subscribe(
          response => {
            toast({
              html: "Auditoria Asignada",
              classes: "green rounded"
            });
            console.log(response);
            this.listSucursalesSeleccionadas = [];
            this.listAsignacionesConError = [];
            response.forEach(asignacion => {
              if (asignacion.idAsignacion < 1) {
                this.listAsignacionesConError.push(asignacion);
              }
            });

            console.log("error", this.listAsignacionesConError);
            if (this.listAsignacionesConError.length >= 1) {
              let elemModalError = document.getElementById("modalErrorAsignar");
              this.modalErrorAsignarAuditorias = M.Modal.init(elemModalError);
              this.modalErrorAsignarAuditorias.open();
              this.modalErrorAsignarAuditorias.options.dismissible = false;
              this.modalErrorAsignarAuditorias.open();
            }
          },
          error => {
            toast({
              html: "Error al asignar a sucursales",
              classes: "red rounded"
            });
          }
        );
    }
  }

  setEliminarAuditoria() {
    this._AuditoriaService.setEliminarAuditoria(this.auditoria).subscribe(
      response => {
        toast({ html: "Auditoria Eliminada", classes: "green rounded" });
        this.mostrarEliminar = false;
        console.log(response);
        this.actualizarListaAuditorias.emit();
      },
      error => {
        toast({ html: "Error al Eliminar Auditoria", classes: "red rounded" });
      }
    );
  }

  cargarArchivos(idVariable: number) {
    this._AuditoriaService.getFotosModeloVariable(idVariable)
      .then(response => {
        console.log(response);
        this.listArchivos = response;

      }, error => {
        toast({ html: "Error al Cargar Archivos", classes: "red rounded" });

      })
  }
}
