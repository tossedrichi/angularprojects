import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as crypto from 'crypto-js';
import { keyCryp } from '../../services/auth.services';


@Component({
  selector: 'app-menu-admin.component',
  templateUrl: './menu-admin.component.html',
  styleUrls: ['./menu-admin.component.css',
              '../login/login.component.css']
})

export class MenuAdminComponent implements OnInit {

  admin:boolean =false;
  subAdmin :boolean = false;

  constructor(private route:Router) {
    console.log("menu");
    this.regresaTipoUsuario()
  }
  cargarUnMenu(menu:String){
    
          this.route.navigate(['subadministrador/menu/seccion/areas']);
          this.route.navigate(['subadministrador/menu/seccion/areas']);
  }

  regresaTipoUsuario(){
    let typeUser= crypto.AES.decrypt(sessionStorage.getItem('typeUser'),keyCryp).toString(crypto.enc.Utf8)
    if(typeUser==="admin"){
      console.log('admin');
      this.admin=true;
      this.subAdmin=false;
    }else if(typeUser==="subadmin"){
      console.log('menu');
      this.subAdmin=true;
      this.admin=false;
    }
  }


  ngOnInit() {
  }

}
