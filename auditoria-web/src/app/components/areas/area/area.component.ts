import { Component, OnInit, Input, AfterContentInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { toast } from "materialize-css";
import { AreasModel } from "../../../../models/areas.model";
import { AuditoriaService } from "../../../services/auditoria.services";
import { AreasComponent } from "../areas.component";

@Component({
  selector: "app-area",
  templateUrl: "./area.component.html",
  styleUrls: ["./area.component.css", "../../login/login.component.css"]
})
export class AreaComponent implements OnInit, AfterContentInit {
  @Input() area: AreasModel;
  @Output() getAreasEmit: EventEmitter<any>;

  formularioMod: FormGroup;

  loadingData: boolean = false;
  mostrarAuditoria: boolean = false;
  mostrarEliminar: boolean = false;
  mostrarModificar: boolean = false;
  mostrarConfirmar: boolean = false;
  listAuditoriasArea: {};

  constructor(private _AuditoriasService: AuditoriaService,
    private areasComponent: AreasComponent) {

      this.getAreasEmit = new EventEmitter();

  }
  ngOnInit() { }
  ngAfterContentInit() {
    this.area.agente = 1;
    this.area.origen = 1;
  }

  validaciones() {
    this.formularioMod = new FormGroup({
      nombreModArea: new FormControl(this.area.nombreArea, [
        Validators.required,
        Validators.minLength(4)
      ])
    });

    console.log(this.formularioMod);
  }

  getAuditorias(idArea) {
    this.loadingData = true;
    this._AuditoriasService.getAuditorias(idArea).subscribe(response => {
      console.log(response);
      this.listAuditoriasArea = response;
      this.loadingData = false;
    }, error => {
      toast({ html: 'Error al Obtener Auditores', classes: 'red rounded' })
    });
  }

  mostrarAuditorias() {
    this.mostrarAuditoria = !this.mostrarAuditoria;
    if (this.mostrarAuditoria === true) {
      this.getAuditorias(this.area.idArea);
    }
  }

  desplegar(): boolean {
    return this.mostrarAuditoria;
  }

  eliminar() {
    this.mostrarEliminar = false;

    this._AuditoriasService
      .setDeshabilitarArea(this.area)
      .subscribe(response => {

       //this.getAreasEmit.emit();

        this.areasComponent.getAreas();
        toast({ html: "Área Eliminada! ", classes: "green rounded" });

      }, error => {
        toast({ html: "Error al Elminar Área", classes: "red rounded" });

      });
  }

  alertModificar() {
    this.mostrarModificar = true;
    this.validaciones();

  }

  alertConfirmarMod() {
    console.log(this.formularioMod);

    if (this.formularioMod.valid) {
      this.mostrarConfirmar = true
    }

  }

  modificar(nombrenuevo: string) {
    this.mostrarConfirmar = false;
    this.mostrarModificar = false;

    if (nombrenuevo != "") {
      this.area.nombreArea = nombrenuevo;

      this._AuditoriasService.setEditarArea(this.area).subscribe(response => {
/*         this.getAreasEmit.emit();
 */        this.areasComponent.getAreas();
        toast({ html: "Area Modificada  " + nombrenuevo, classes: "green rounded" });
      }, error => {
        toast({ html: "Error al Modificar Area  ", classes: "red rounded" });

      });

    } else {
      toast({ html: "El Campo esta Vacio ", classes: "red rounded" });
    }
  }


}
