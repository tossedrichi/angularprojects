import { Component } from "@angular/core";
import { toast } from "materialize-css";
import { AuditoriaService } from "../../services/auditoria.services";
import { FormGroup, Validators, FormControl } from "@angular/forms";

@Component({
  selector: "app-areas",
  templateUrl: "./areas.component.html",
  styleUrls: ["./areas.component.css" ]
})
export class AreasComponent {
  formularioAdd: FormGroup;
  loadingData: boolean = true;
  mostrarAgregar: boolean = false;
  mostrarConfirmar: boolean = false;
  listaAreasService: any;

  constructor(private _AuditoriaService: AuditoriaService) {
    this.getAreas();
  }

  validaciones() {
    this.formularioAdd = new FormGroup({
      nombreArea: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  getAreas() {
    this._AuditoriaService.getAreas().subscribe(
      response => {
        //console.log(response);
        this.listaAreasService = response;
        console.log(this.listaAreasService);
        this.loadingData = false;
      },
      error => {
        this.loadingData = false;
        toast({ html: "Error al Cargar Áreas", classes: "rounded red" });
      }
    );
  }

  alertAgregar() {
    this.validaciones();
    this.mostrarAgregar = !this.mostrarAgregar;
  }
  alertConfirmar() {
    if (this.formularioAdd.valid) {
      this.mostrarConfirmar = !this.mostrarConfirmar;
    }
  }
  guardarArea(nombre: String) {
    this.mostrarAgregar=false;
    this.mostrarConfirmar=false;
    this._AuditoriaService.setAgregarArea(nombre).subscribe(response => {
      console.log(response);
      toast({ html: "Area Guardada! " + nombre, classes: "rounded" });
      this.getAreas();
    },error=>{
      toast({ html: "Error al Guardar Area" , classes: "red rounded" });

    });
  }
}
