import {
  Component,
  ViewChild,
  ElementRef,
  AfterContentInit
} from "@angular/core";
import { Sidenav } from "materialize-css";
import { Router } from "@angular/router";
import { AuthService, keyCryp } from '../../services/auth.services';
import { BodyComponent } from "../body/body.component";
import { MenuAdminComponent } from "../menu-admin.component/menu-admin.component";
import * as crypto from 'crypto-js';

@Component({
  selector: "app-barra-lateral",
  templateUrl: "./barra-lateral.component.html",
  styleUrls: ["./barra-lateral.component.css"]
})
export class BarraLateralComponent implements AfterContentInit {
  @ViewChild("btn", { static: false }) btnNav: ElementRef;
  elems: any;
  sideBar: Sidenav;
  userName = localStorage.getItem("userName");

  isAdmin: boolean = false;
  isSubAdmin: boolean = false;

  constructor(
    private men: MenuAdminComponent,
    private route: Router,
    private auth: AuthService,
    private body: BodyComponent
  ) {
    this.regresaTipoUsuario();
    this.userData();
  }

  sideNavVisibleButton() {

    //console.log(this.route.url)

    const url = this.route.url;
    if (url === "/subadministrador/menu" ||
      url === "/administrador/menu") {
      return false;
    } else {
      return true;
    }



  }


  regresaTipoUsuario(): boolean {
    let typeUser = crypto.AES.decrypt(sessionStorage.getItem('typeUser'), keyCryp).toString(crypto.enc.Utf8)

    if (typeUser === "admin") {
      this.isAdmin = true;
      this.isSubAdmin = false;
    } else if (typeUser === "subadmin") {
      this.isAdmin = false;
      this.isSubAdmin = true;
    }
    return;
  }

  cargarUnMenu(menu: String) {

  }

  userData() {
    let data = JSON.parse(atob(sessionStorage.getItem("token").toString()))

    this.userName = data.nombreUsuario
  }


  mostrarPantalla(menu: string) {
    console.log("clikeaste");


    this.elems = document.querySelector(".sidenav");
    this.sideBar = M.Sidenav.getInstance(this.elems);

    if (this.isSubAdmin) {
      this.route.navigate(["/subadministrador/menu/seccion", menu]);
    } else if (this.isAdmin) {
      this.route.navigate(["/administrador/menu/seccion", menu]);
    }

    this.sideBar.close();
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    this.elems = document.querySelector(".sidenav");
    this.sideBar = Sidenav.init(this.elems, null);

  }

  closeSession() {
    this.sideBar.close();
    //this.sideBar.destroy();
    this.auth.logout();
  }
}
