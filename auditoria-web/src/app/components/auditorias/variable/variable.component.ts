import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { toast } from "materialize-css";
import { FormGroup, FormControl, FormArray, Validators } from "@angular/forms";
import { Varibale } from "src/models/Auditoria.model";
import { AuditoriaService } from "../../../services/auditoria.services";
import { Sucursal } from "src/models/Sucursal.model";
import { SubZona } from "src/models/subZonasAdmin.mode";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";
import { FileItem } from "src/models/file-item.model";
import * as tools from '../../../tools';
@Component({
  selector: "app-variable",
  templateUrl: "./variable.component.html",
  styleUrls: ["./variable.component.css"]
})

export class VariableComponent implements OnInit {
  @Input() variable: Varibale;
  @Input() subZonas: SubZona[];
  @Input() esEdicionVariable: boolean;

  @Output() variableCargada: EventEmitter<{ pos: number, var?: string }>;

  @Output() variableEditada: EventEmitter<Varibale>;
  @Output() elimiarVariable: EventEmitter<Varibale>;

  @Output() guardarBorrador: EventEmitter<any>;

  formEditarVariable: FormGroup;
  mostrarSucursales: boolean = false;
  mostrarDetalles: boolean = false;
  mostrarEliminar: boolean = false;
  mostrarEditar: boolean = false;
  requiereEvidecia: boolean = false;
  sucursalesCargadas: boolean = false;
  cargandoSucursales: boolean = false;
  listSucursales: Sucursal[] = [];
  selecTodasSucursales: boolean = false;
  listSucursalesSeleccionadas: Sucursal[] = [];
  //listArchivos: FileItem[] = [];

  constructor(private _AuditoriaService: AuditoriaService) {
    this.variableEditada = new EventEmitter();
    this.elimiarVariable = new EventEmitter();
    this.guardarBorrador = new EventEmitter();
    this.variableCargada = new EventEmitter();

  }

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 3,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true
  };

  ngOnInit(): void {
    this.requiereEvidecia = this.variable.requiereEvidencia;

    console.log("Requiere Fotos", this.requiereEvidecia);

    //  this.listArchivos = this.variable.imagenes;
    this.inicializarFormulario();

    if (this.esEdicionVariable) {
      console.log("var nueva", this.variable.idVariableNueva);

      if (!this.variable.idVariableNueva) {
        this.getImagenesVariable().then(() => {
          this.variableCargada.emit({ pos: 1 });
        }).catch(err => {
          this.variableCargada.emit({ pos: -1, var: this.variable.nombreVariable });

        });

      }

    }

    // this.cambiarMostrarSucursales();
  }

  editar() {
    if (this.subZonas.length > 0) {
      this.mostrarEditar = true;
      this.cambiarMostrarSucursales();
    } else {
      toast({ html: "Selecciona al menos una zona" });
    }
  }

  cargar(): boolean {

    return this.mostrarEditar;
  }

  eliminarVariable(variable: Varibale) {
    this.elimiarVariable.emit(variable);
    this.mostrarEliminar = false;

    if (this.esEdicionVariable && variable.idVariableNueva == undefined) {

      this._AuditoriaService.setEliminarVariable(variable.idVariable)
        .subscribe(resp => {
          console.log(resp);

          toast({ html: 'Variable Elminada', classes: 'green rounded' })
        }, err => toast({ html: 'Error al Eliminar Variable', classes: 'red rounded' }))
    }
  }

  inicializarFormulario() {
    this.formEditarVariable = new FormGroup({
      nombreVariable: new FormControl(this.variable.nombreVariable, [
        Validators.required,
        Validators.minLength(5)
      ]),
      definicion: new FormControl(this.variable.definicion, [
        Validators.required,
        Validators.minLength(10)
      ]),
      ponderacion: new FormControl(this.variable.ponderacion, [
        Validators.required,
        /* Validators.max(100), */
        Validators.min(1)
      ]),
      requiereEvidencia: new FormControl(),
      //inputFiles: new FormControl()
      sucursales: new FormArray([])
    });
  }

  seleccionarTodasSucursales() {
    this.selecTodasSucursales = !this.selecTodasSucursales;

    if (this.selecTodasSucursales) {
      this.selecTodasSucursales = true;
      this.listSucursales.forEach(sucursal => (sucursal.selected = true));
      this.listSucursalesSeleccionadas = this.listSucursales;
    } else {
      this.selecTodasSucursales = false;
      this.listSucursales.forEach(sucursal => (sucursal.selected = false));
      this.listSucursalesSeleccionadas = [];
    }
  }

  seleccionarSucursal(sucursal: Sucursal) {

    if (!this.listSucursalesSeleccionadas.find(x => x.idSucursal == sucursal.idSucursal)) {
      sucursal.selected = true;
      this.listSucursalesSeleccionadas.push(sucursal);
    } else {
      sucursal.selected = false;
      this.listSucursalesSeleccionadas = this.listSucursalesSeleccionadas.filter(
        x => x.idSucursal != sucursal.idSucursal
      );
    }
    console.log("Sucursales Seleccionadas:", this.listSucursalesSeleccionadas);


    if (this.listSucursales.length != this.listSucursalesSeleccionadas.length) {
      this.selecTodasSucursales = false;
    } else {
      this.selecTodasSucursales = true;

    }
  }
  cambiarMostrarSucursales() {
    this.mostrarSucursales = true;
    if (!this.sucursalesCargadas) {
      this.getSucursales();
      //FIXME:
      //this.sucursalesCargadas = true;
    }
  }

  getSucursales() {
    this.cargandoSucursales = true;

    this._AuditoriaService.getSucursales(this._AuditoriaService.agente, this.subZonas).subscribe(
      response => {
        this.listSucursales = response;

        this.listSucursalesSeleccionadas = this.variable.sucursales;

        this.listSucursales.forEach(x => (x.selected = false));

        this.listSucursales.forEach(sucursal => {
          this.variable.sucursales.forEach(sucursalVariable => {
            if (sucursal.idSucursal == sucursalVariable.idSucursal) {
              console.log("Entro");

              sucursal.selected = true;
              sucursalVariable.selected = true;
            }


          });
        });
        if (this.listSucursales.length == this.listSucursalesSeleccionadas.length) {
          this.selecTodasSucursales = true;
        }
        console.log(this.listSucursalesSeleccionadas);
        this.cargandoSucursales = false;

        console.log("Sucursales", response);
      },
      error => {
        this.cargandoSucursales = false;
        toast({ html: "Error al cargar Sucursales", classes: "res rounded" });
      }
    );
  }

  guardarEdicionVar() {
    let edicionValid = true;

    if (this.listSucursales.length == this.listSucursalesSeleccionadas.length) {
      this.selecTodasSucursales = true;
    } else {
      this.selecTodasSucursales = false;

    }

    if (this.listSucursalesSeleccionadas.length <= 0) {
      edicionValid = false;
      toast({ html: "Slecciona Sucursales" });
    }
    if (this.formEditarVariable.invalid) {
      edicionValid = false;
      toast({ html: "Formulario Invalido" });
    }

    if (this.requiereEvidecia && this.variable.imagenes.length <= 0) {
      toast({
        html: "Si la variable requiere evidencia, agrega almenos una muestra"
      });
      edicionValid = false;
    }


    if (edicionValid) {
      let editVariable: Varibale = new Varibale(
        this.formEditarVariable.get("nombreVariable").value,
        this.formEditarVariable.get("definicion").value,
        this.requiereEvidecia,
        this.formEditarVariable.get("ponderacion").value,
        this.selecTodasSucursales,
        this.listSucursalesSeleccionadas,
        this.variable.imagenes,
        this.variable.idVariable
      );

      if (this.variable.idVariableNueva) {
        editVariable.idVariableNueva = this.variable.idVariableNueva;
      }

      this.variable = editVariable;
      this.sucursalesCargadas = false;
      this.mostrarEditar = false;
      this.variableEditada.emit(editVariable);
    }
  }

  limiteDeImagenes(): boolean {
    let limiteSuperado: boolean = false;

    let counterImgs = 0;
    console.log("Tamaño", this.variable.imagenes.length);

    if (this.variable.imagenes.length > 0) {

      this.variable.imagenes.forEach(imagen => {
        if (imagen.manejar == 0) {
          counterImgs += 1;
          console.log(counterImgs);

          if (counterImgs >= 5) {

            limiteSuperado = true;
          } else {
            limiteSuperado = false;
          }

        }
      });
    } else {
      limiteSuperado = false;
    }

    return limiteSuperado;
  }

  removeImage(item: FileItem) {
    //this.variable.imagenes = this.variable.imagenes.filter(x => x !== item);
    item.manejar = 1;
    console.log("Imagen Eliminada", item);
  }

  public _archivoYaFueAgregado(nombreArchivo: string): boolean {
    for (const archivo of this.variable.imagenes) {
      if (archivo.nombreArchivo === nombreArchivo) {
        toast({
          html: `El Archivo ${nombreArchivo} ya fue Agregado`,
          classes: "blue rounded"
        });
        return true;
      }
    }
    return false;
  }

  public _esImagen(tipoArchivo: string): boolean {
    if (tipoArchivo.startsWith("image")) {
      return true;
    }

    return false;
  }

  public archivoPuedeSerAgregado(archivo: File) {
    if (
      !this._archivoYaFueAgregado(archivo.name) &&
      this._esImagen(archivo.type)
    ) {
      return true;
    }
    return false;
  }

  onChange(event: any) {

    let limiteSuperado = this.limiteDeImagenes()
    if (!limiteSuperado) {
      console.log("limiteSuperado", limiteSuperado);

      for (const key in Object.getOwnPropertyNames(event.target.files)) {
        console.log(event.target.files[key]);
        const archivoTemporal = event.target.files[key];
        if (this.archivoPuedeSerAgregado) {

          var reader = new FileReader();
          reader.readAsDataURL(archivoTemporal);

          reader.onload = event => {
            let base64 = (<FileReader>event.target).result;

            let orientacion = tools.getOrinetation(base64);

            if (orientacion != undefined) {

              tools.resetOrientation(base64, orientacion, (callback) => {
                const nuevoArchivo = new FileItem(archivoTemporal, callback);
                this.variable.imagenes.unshift(nuevoArchivo);
              })


            } else {
              const nuevoArchivo = new FileItem(archivoTemporal, base64.toString());
              this.variable.imagenes.unshift(nuevoArchivo);

            }


          };

        }
      }
    } else {
      toast({ html: 'Limite de Imagenes Superado', classes: 'red rounded' })
    }
  }


  async getImagenesVariable() {

    await this._AuditoriaService.getFotosModeloVariable(this.variable.idVariable)
      .then(response => {

        console.log("Imagenes Servidor", response);

        if (!this.variable.imagenes) {
          this.variable.imagenes = []
        }


        for (let i = 0; i <= response.length; i++) {

          if (this.variable.imagenes.length <= 0) {
            console.log("Las imagenes de la variables estan vacias");

            this.variable.imagenes = response;
            break;
          }

          if (response[i] != undefined) {

            for (let j = 0; j <= this.variable.imagenes.length; j++) {

              if (this.variable.imagenes[j] != undefined) {
                if (response[j] != undefined) {
                  if (this.variable.imagenes[j].idStream != response[j].idStream) {
                    console.log("Entro a la condicion");

                    this.variable.imagenes.push(response[i]);
                  }
                }

              }
            }

          }

        }


        this.variable.imagenes.forEach(imagen => {
          if (!imagen.base64.toString().includes("base64")) {
            imagen.base64 = "data:image/jpeg;base64," + imagen.base64;
          }
        })

        console.log("Imagenes Totales:", this.variable.imagenes);


      }/* , error => {
        toast({ html: 'Error al cargar fotos modelo', classes: 'rounded red' })
      }*/)
  }
}
