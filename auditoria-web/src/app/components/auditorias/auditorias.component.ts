import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormSelect, toast, Modal } from "materialize-css";
import { FileItem } from "src/models/file-item.model";
import { FormGroup, FormControl, FormArray, Validators } from "@angular/forms";
import { AuditoriaService } from "../../services/auditoria.services";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";
import { SubZona } from "src/models/subZonasAdmin.mode";
import { Frecuecia } from "src/models/Frecuencia.model";
import { Sucursal } from "src/models/Sucursal.model";
import { Varibale, Auditoria } from "src/models/Auditoria.model";
import { AreasModel } from "src/models/areas.model";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import * as cloneDeep from 'lodash/cloneDeep';
import * as tools from '../../tools'
import { Zona } from '../../../models/ZonasSubAdministrador.mode';
import { ProcesadoModel } from '../../../models/Procesado.model';
@Component({
  selector: "app-auditorias",
  templateUrl: "./auditorias.component.html",
  styleUrls: ["./auditorias.component.css"]
})

export class AuditoriasComponent implements OnInit {
  @Input() _idAuditoria: number;
  @Input() esEdicion: boolean;
  @Output() cerrarEdicion: EventEmitter<any>;

  @ViewChild('checkTodasZonas', { static: false }) checkTodasZonas: ElementRef;
  select: FormSelect;
  formNuevaAuditoria: FormGroup;
  formNuevaVariable: FormGroup;

  alertDesseleccionarZona: Modal;
  alertDesseleccionarTodasZonas: Modal;
  alertEnviandoAuditoria: Modal;
  alertCargandoAuditoria: Modal;
  alertBorradorAuditoria: Modal;

  auditoriaBorrador = "AuditoriaBorrador";

  auditoriaEdicion: Auditoria = new Auditoria(null, null, null, null, null);

  estaSobreDrop: boolean = false;
  mostrarAgregar: boolean = false;
  cargado: boolean = false;
  mostrarZonas: boolean = false;
  mostrarSubZonas: boolean = false;
  mostrarSucursales: boolean = false;
  evidecia: boolean = false;
  selecTodasSubZonas: boolean = false;
  selecTodasSucursales: boolean = false;
  sucursalesCargadas: boolean = false;
  auditoriaCargadaCorrectamente = false;
  cargandoSubZonas: boolean = false;
  cargandoSucursales: boolean = false;
  mostrarSubzonasAgregar:boolean = false;

  listFrecuencias: Frecuecia[] = [];
  listAreas: AreasModel[] = [];
  archivos: FileItem[] = [];
  listSubZonas: SubZona[] = [];
  listZonas:Zona[]=[];
  listSubZonasAgregar:SubZona[]=[];
  listsubZonasSeleccionadas: SubZona[] = [];
  listSucursales: Sucursal[] = [];
  listSucursalesSeleccionadas: Sucursal[] = [];
  listVarables: Varibale[] = [];

  idVariable: number = 0;
  variablesCargadas: number = 0;

  subZonaDeseleccionada: SubZona;
  idCheckBoxDeseleccionado: number;

  constructor(private _AuditoriaService: AuditoriaService) {
    this.cerrarEdicion = new EventEmitter();
  }

  ngOnInit() {

    let modalDeselecZona = document.getElementById("modalDeselecZona");
    let modalEnviandoAuditoria = document.getElementById("modalEnviandoAuditoria");
    let modalCargandoAuditoria = document.getElementById("modalCargandoAuditoria");
    let modalDeselecTodasZonas = document.getElementById("modalDeselecTodasZona");
    let modalBorradorAuditoria = document.getElementById("modalBorrador");

    this.alertDesseleccionarTodasZonas = M.Modal.init(modalDeselecTodasZonas);
    this.alertDesseleccionarZona = M.Modal.init(modalDeselecZona);
    this.alertEnviandoAuditoria = M.Modal.init(modalEnviandoAuditoria);
    this.alertCargandoAuditoria = M.Modal.init(modalCargandoAuditoria);
    this.alertBorradorAuditoria = M.Modal.init(modalBorradorAuditoria);

    if (this.esEdicion) {
      this.alertCargandoAuditoria.options.endingTop = "40%";
      this.alertCargandoAuditoria.options.dismissible = false;
      this.alertCargandoAuditoria.open();
      this.inicializarFormulario();
      this.getZonasSubadministrador();
      console.log("Estas Editando una Auditoria");
      this.getDetallesAuditoria().then(() => {
        //this.alertCargandoAuditoria.close();

      }).catch(err => {
        console.error("Error al cargar Auditoria", err);
        toast({ html: "Error al Cargar la Auditoria", classes: "red rounded" });
        this.alertCargandoAuditoria.close();
        this.cancelarEdicion();
      });

    } else {
      //this.getFrecuencias();
      //this.getAreasSubAdministrador();
      this.inicializarFormulario();

      this.cargarRecursos().then(resp => {
        console.log("ExisteBorrador?", localStorage.getItem(this.auditoriaBorrador));

        if (localStorage.getItem(this.auditoriaBorrador) != undefined) {
          this.alertBorradorAuditoria.options.dismissible = false;
          this.alertBorradorAuditoria.open();
          console.log("Abriendo Dialogo");
        }

      });
      console.log("Estas Creando una Nueva Auditoria");
    }

    this.mostrarZonas = true;
    this.mostrarSubZonas=true;

  }

  cargarBorrador() {
    this.reanudarAuditoriaDeBorrador();
    this.alertBorradorAuditoria.close();
  }

  eliminarBorrador() {
    this.alertBorradorAuditoria.close();
    localStorage.removeItem(this.auditoriaBorrador);
  }

  inicializarFormulario() {
    if (this.esEdicion) {
      console.log("FORMULARIO EDICION AUDITORIA");

      this.formNuevaAuditoria = new FormGroup({
        nombreAuditoira: new FormControl({}),
        frecuencia: new FormControl({}),
        area: new FormControl({}),
        subZonas: new FormArray([])
      });

      this.formNuevaVariable = new FormGroup({
        nombreVariable: new FormControl("", [
          Validators.required,
          Validators.minLength(5)
        ]),
        definicion: new FormControl("", [
          Validators.required,
          Validators.minLength(10)
        ]),
        ponderacion: new FormControl("", [
          Validators.required,
          /* Validators.max(100), */
          Validators.min(1)
        ]),
        requiereEvidencia: new FormControl(),
        //inputFiles: new FormControl()
        sucursales: new FormArray([])
      });
    } else {
      console.log("FORMULARIO NUEVA AUDITORIA");

      this.formNuevaAuditoria = new FormGroup({
        nombreAuditoira: new FormControl("", [
          Validators.required,
          Validators.minLength(5)
        ]),
        frecuencia: new FormControl("", Validators.required),
        area: new FormControl("", Validators.required),
        subZonas: new FormArray([])
        //variables:new FormArray([])
      });

      this.formNuevaVariable = new FormGroup({
        nombreVariable: new FormControl("", [
          Validators.required,
          Validators.minLength(5)
        ]),
        definicion: new FormControl("", [
          Validators.required,
          Validators.minLength(10)
        ]),
        ponderacion: new FormControl("", [
          Validators.required,
          /* Validators.max(100), */
          Validators.min(1)
        ]),
        requiereEvidencia: new FormControl(),
        //inputFiles: new FormControl()
        sucursales: new FormArray([])
      });
    }
  }

  imprimir() {
    console.log(this.formNuevaAuditoria);
  }

  async cargarRecursos() {
    await this.getFrecuencias();
    await this.getAreasSubAdministrador();
    await this.getSubZonasSubadministrador();
   

  }

  dragDropListVariables(evento: CdkDragDrop<any>) {
    moveItemInArray(
      this.listVarables,
      evento.previousIndex,
      evento.currentIndex
    );

    if (this.esEdicion) {
      this.auditoriaEdicion.variables = this.listVarables
    }
  }

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 3,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true
  };

  async  getFrecuencias() {
    console.log("LLamando Frecuencias");

    await this._AuditoriaService.getFrecuencias().then(
      response => {
        console.log("Frecuencias Repsonse", response);
        this.listFrecuencias = response;
        // this.inicializarFormulario()
        this.formNuevaAuditoria.controls["frecuencia"].setValue("1", {
          onlySelf: true
        });
      },
      error => {
        toast({ html: "Error Al Cargar Frecuencias", classes: "red rounded" });
      }
    );
  }

  async getAreasSubAdministrador() {
    console.log("Llamando Areas");

    await this._AuditoriaService.getListaAreasSubadministrador(this._AuditoriaService.agente)
      .then(
        response => {
          console.log("Areas");
          console.log("Agente  Response", this._AuditoriaService.agente);

          console.log("Areas", response);
          this.listAreas = response;
          this.formNuevaAuditoria.controls["area"].setValue("10", {
            onlySelf: true
          });
          //this.reanudarAuditoriaDeBorrador();
        },
        error => {
          toast({ html: "Error al cargar Areas", classes: "red rounded" });
        }
      );
  }

  getSucursales() {
    this.cargandoSucursales = true;

    this._AuditoriaService
      .getSucursales(this._AuditoriaService.agente, this.listsubZonasSeleccionadas)
      .subscribe(
        response => {
          console.log("Sucursales", response);
          this.listSucursales = response;
          this.listSucursales.forEach(sucursal => (sucursal.selected = false));
          this.cargandoSucursales = false;

        },
        error => {
          this.cargandoSucursales = false;

          toast({ html: "Error al cargar Sucursales", classes: "res rounded" });
        }
      );
  }

  async getZonasSubadministrador() {
    this.cargandoSubZonas = true;
    await this._AuditoriaService.getZonasSubadministrador(this._AuditoriaService.agente).then(
      response => {
        console.log("Zonas", response);
        this.listZonas = response;
     

      },
      error => {
        toast({ html: "Error al Cargar Zonas" });
        this.cargandoSubZonas = false;

      }
    );
  }

  async getSubZonasSubadministrador() {
    this.cargandoSubZonas = true;
    await this._AuditoriaService.getSubZonasSubadministrador(this._AuditoriaService.agente).then(
      response => {
        console.log("Zonas", response);
        this.listSubZonas = response;
        console.log("check", this.checkTodasZonas);

        this.listSubZonas.forEach(subZona => {
          subZona.selected = false;
          (<FormArray>this.formNuevaAuditoria.controls["subZonas"])
            .push(new FormControl());


          if (this.esEdicion) {
            this.auditoriaEdicion.subZonas.forEach(subZonaAuditoria => {
              if (subZona.idSubZona == subZonaAuditoria.idSubZona) {
                this.seleccionarSubzona(subZona);
              }
            });
          }
        });
        console.log("FORM AUDITORIA", this.formNuevaAuditoria);
        this.cargandoSubZonas = false;

      },
      error => {
        toast({ html: "Error al Cargar Zonas" });
        this.cargandoSubZonas = false;

      }
    );
  }

  seleccionarTodasSubZonas() {
    this.selecTodasSubZonas = !this.selecTodasSubZonas;

    if (this.selecTodasSubZonas) {
      this.listSubZonas.forEach(subZona => (subZona.selected = true));
      this.listsubZonasSeleccionadas = this.listSubZonas;
    } else {

      if (this.listVarables.length > 0) {
        console.log("Deseleccion Todas");

        //this.eliminarRelacionSursalesVariable(undefined,true)

        this.alertDesseleccionarTodasZonas.open();
        this.alertDesseleccionarTodasZonas.options.dismissible = false;
        /*  this.alertDesseleccionarZona.open();
        this.alertDesseleccionarZona.options.dismissible = false; */

      } else {
        this.listSubZonas.forEach(subZona => (subZona.selected = false));
        this.listsubZonasSeleccionadas = [];
      }

    }

    console.log("Seleccionadas", this.listsubZonasSeleccionadas);
    console.log("listaOriginal", this.listSubZonas);
  }

  seleccionarSubzona(subZona: SubZona, idCheckBox?: number) {

    if (!this.listsubZonasSeleccionadas.find(x => x == subZona)) {
      subZona.selected = true;
      this.listsubZonasSeleccionadas.push(subZona);
    } else {
      if (this.listVarables.length > 0) {
        this.listVarables.forEach(variable => {
          console.log("Var");

          if (variable.sucursales.find(
            x => x.nombreSubZona == subZona.nombreSubZona)) {

            this.subZonaDeseleccionada = subZona;
            this.idCheckBoxDeseleccionado = idCheckBox;
            this.alertDesseleccionarZona.open();
            this.alertDesseleccionarZona.options.dismissible = false;
          } else {
            console.log("deseleccion Directa 1");

            subZona.selected = false;
            this.listsubZonasSeleccionadas = this.listsubZonasSeleccionadas.filter(
              x => x != subZona
            );
          }
        });
      } else {
        console.log("deseleccion Directa");

        subZona.selected = false;
        this.listsubZonasSeleccionadas = this.listsubZonasSeleccionadas.filter(
          x => x != subZona
        );
      }
    }

    if (this.listSubZonas.length == this.listsubZonasSeleccionadas.length) {
      this.selecTodasSubZonas = true;
    } else {
      this.selecTodasSubZonas = false;
    }

    console.log("selected suc", this.listsubZonasSeleccionadas);
  }

  eliminarRelacionSursalesVariable(subZona?: SubZona, todas?: boolean) {
    let sucursalesSubZona: Sucursal[] = [];
    if (!todas) {
      this._AuditoriaService
        .getSucursales(this._AuditoriaService.agente, [subZona])
        .subscribe(response => {
          sucursalesSubZona = response;

          this.listVarables.forEach(variable => {
            variable.sucursales.forEach(sucursalVariable => {
              sucursalesSubZona.forEach(sucursal => {
                if (sucursalVariable.idSucursal == sucursal.idSucursal) {
                  console.log(
                    "Desasignada Sucrsal: ",
                    sucursalVariable.nombreSucursal
                  );
                  variable.sucursales = variable.sucursales.filter(
                    x => x.idSucursal != sucursalVariable.idSucursal
                  );
                } else {
                  console.log("Variable SUcursal", variable.sucursales);
                }
              });
            });
          });
        });

      subZona.selected = false;
      this.listsubZonasSeleccionadas = this.listsubZonasSeleccionadas.filter(
        x => x != subZona
      );
    } else {
      this.listVarables.forEach(variable => {
        variable.sucursales = [];
      })
      this.listsubZonasSeleccionadas = [];
      this.listSubZonas.forEach(subZona => subZona.selected = false);

      console.log("Zonas Seleccionadas", this.listsubZonasSeleccionadas);

    }
    this.alertDesseleccionarTodasZonas.close();
    this.alertDesseleccionarZona.close();
  }

  cancelarDeseleccionSubZona(subZona: SubZona, idCheck: number) {
    this.formNuevaAuditoria.controls["subZonas"]
      .get(idCheck.toString())
      .setValue(true);
    subZona.selected = true;
  }

  seleccionarTodasSucursales() {
    this.selecTodasSucursales = !this.selecTodasSucursales;

    if (this.selecTodasSucursales) {
      this.selecTodasSucursales = true;
      this.listSucursales.forEach(sucursal => (sucursal.selected = true));
      this.listSucursalesSeleccionadas = this.listSucursales;
    } else {
      this.selecTodasSucursales = false;
      this.listSucursales.forEach(sucursal => (sucursal.selected = false));
      this.listSucursalesSeleccionadas = [];
    }
  }

  seleccionarSucursal(sucursal: Sucursal) {
    if (!this.listSucursalesSeleccionadas.find(x => x == sucursal)) {
      sucursal.selected = true;
      this.listSucursalesSeleccionadas.push(sucursal);
    } else {
      sucursal.selected = false;
      this.listSucursalesSeleccionadas = this.listSucursalesSeleccionadas.filter(
        x => x != sucursal
      );
    }

    console.log("selected suc", this.listSucursalesSeleccionadas);
  }

  onChange(event: any) {

    let limiteSuperado = this.limiteDeImagenes()
    if (!limiteSuperado) {

      for (const key in Object.getOwnPropertyNames(event.target.files)) {
        console.log(event.target.files[key]);
        const archivoTemporal = event.target.files[key];
        if (this.archivoPuedeSerAgregado) {
          var reader = new FileReader();
          reader.readAsDataURL(archivoTemporal);
          reader.onload = event => {
            let base64 = (<FileReader>event.target).result;

            let orientacion = tools.getOrinetation(base64);

            if (orientacion != undefined) {

              tools.resetOrientation(base64, orientacion, (callback) => {
                const nuevoArchivo = new FileItem(archivoTemporal, callback);
                this.archivos.unshift(nuevoArchivo);
              })


            } else {
              const nuevoArchivo = new FileItem(archivoTemporal, base64.toString());
              this.archivos.unshift(nuevoArchivo);

            }
          };
        }
      }
    } else {
      toast({ html: 'Limite de Imagenes Superado', classes: 'red rounded' })

    }
  }

  cambiarMostrarZonas() {
    this.mostrarZonas = !this.mostrarZonas;
  }

  cambiarMostrarSubZonas() {
    this.mostrarSubZonas = !this.mostrarSubZonas;
  }
  cambiarMostrarSucursales() {
    this.mostrarSucursales = !this.mostrarSucursales;
    if (!this.sucursalesCargadas) {
      this.getSucursales();
      this.sucursalesCargadas = true;
    }
  }

  agregar() {
    // this.inicializarFormulario();

    if (this.esEdicion) {
      if (this.listsubZonasSeleccionadas.length > 0) {
        this.mostrarAgregar = true;
        this.cargado = false;
      } else {
        toast({
          html: "Selecciona una SubZona"
        });
      }
    }

    if (!this.esEdicion) {
      if (
        this.formNuevaAuditoria.valid &&
        this.listsubZonasSeleccionadas.length > 0
      ) {
        this.mostrarAgregar = true;
        this.cargado = false;
      } else {
        toast({
          html: "Para Poder Agregar Variables Completa Los Campos Principales"
        });
      }
    }
  }

  cargar(): boolean {
    return this.mostrarAgregar;
  }

  limiteDeImagenes(): boolean {
    let limiteSuperado: boolean = false;

    let counterImgs = 0;
    console.log("Tamaño", this.archivos.length);

    if (this.archivos.length > 0) {

      this.archivos.forEach(imagen => {
        if (imagen.manejar == 0) {
          counterImgs += 1;
          console.log(counterImgs);

          if (counterImgs >= 5) {

            limiteSuperado = true;
          } else {
            limiteSuperado = false;
          }

        }
      });
    } else {
      limiteSuperado = false;
    }

    return limiteSuperado;
  }
  removeImage(item: FileItem) {
    this.archivos = this.archivos.filter(x => x !== item);
    console.log("Imagen Eliminada");
  }

  limpiarArchivos() {
    this.archivos = [];
  }

  public _archivoYaFueAgregado(nombreArchivo: string): boolean {
    for (const archivo of this.archivos) {
      if (archivo.nombreArchivo === nombreArchivo) {
        toast({
          html: `El Archivo ${nombreArchivo} ya fue Agregado`,
          classes: "blue rounded"
        });
        return true;
      }
    }
    return false;
  }

  public _esImagen(tipoArchivo: string): boolean {
    if (tipoArchivo.startsWith("image")) {
      return true;
    }

    return false;
  }

  public archivoPuedeSerAgregado(archivo: File) {
    if (
      !this._archivoYaFueAgregado(archivo.name) &&
      this._esImagen(archivo.type)
    ) {
      return true;
    }
    return false;
  }

  sendVariable() {
    console.log("Formulario", this.formNuevaVariable);
    let variableValida: boolean = true;

    if (this.listSucursalesSeleccionadas.length <= 0) {
      toast({ html: "Selecciona almenos una sucursal" });
      variableValida = false;
    }

    if (this.evidecia && this.archivos.length <= 0) {
      toast({
        html: "Si la variable requiere evidencia, agrega almenos una muestra"
      });
      variableValida = false;
    }

    if (this.formNuevaVariable.invalid) {
      toast({ html: "El formulario es invalido" });
      variableValida = false;
    }

    if (variableValida) {
      this.idVariable = this.idVariable + 1;

      let nuevaVariable: Varibale = new Varibale(
        this.formNuevaVariable.get("nombreVariable").value,
        this.formNuevaVariable.get("definicion").value,
        this.evidecia,
        this.formNuevaVariable.get("ponderacion").value,
        this.selecTodasSucursales,
        this.listSucursalesSeleccionadas,
        this.archivos,
      );

      nuevaVariable.idVariableNueva = this.idVariable

      console.log("NuevaVarables", nuevaVariable);
      this.listVarables.push(nuevaVariable);

      if (this.esEdicion) {
        this.auditoriaEdicion.variables = this.listVarables;
      }

      this.formNuevaVariable.reset();
      this.sucursalesCargadas = false;
      this.limpiarArchivos();
      this.mostrarAgregar = false;
      this.mostrarSucursales = false;
      this.listSucursalesSeleccionadas = [];
      this.evidecia = false;
      if (!this.esEdicion) {
        this.guardaBorradorAuditoria();
      }
    } else {
      toast({ html: "Completa el formulario correctamente" });
    }
  }

  cancelarVariable() {
    this.formNuevaVariable.reset();
    this.sucursalesCargadas = false;
    this.limpiarArchivos();
    this.mostrarSucursales = false;
    this.mostrarAgregar = false;
    this.listSucursalesSeleccionadas = [];
    this.evidecia = false;
  }

  guardarAuditoria() {
    let auditoriaValida: boolean = true;

    this.listVarables.forEach(variable => {
      if (variable.sucursales.length <= 0) {
        toast({
          html: `La Variable: ${
            variable.nombreVariable
            }, no tiene sucursales, Asigna minimo una.`
        });
        auditoriaValida = false;
      }
    });

    this.listVarables.forEach(variable => {
      if (variable.requiereEvidencia) {
        if (variable.imagenes.length <= 0) {
          toast({
            html: `La Variable: ${
              variable.nombreVariable
              }, Require Evidencia y No Cuenta con Fotos Muestra.`
          });
          auditoriaValida = false;
        }
      }
    });
    if (this.formNuevaAuditoria.invalid) {
      auditoriaValida = false;
      toast({ html: "Faltan Campos de Llenar" });
    }

    if (this.listVarables.length <= 0) {
      auditoriaValida = false;
      toast({ html: "Agrega variables para poder guardar la auditoria" });
    }

    if (auditoriaValida) {
      this.alertEnviandoAuditoria.options.dismissible = false;
      this.alertEnviandoAuditoria.options.endingTop = "40%";
      this.alertEnviandoAuditoria.open();

      this.listVarables.forEach(variable => {

        variable.imagenes.forEach(image => {

          if (image.base64.toString().includes(",")) {
            image.base64 = image.base64.toString().split(",")[1];
          }

        });

      });
      let auditoria: Auditoria;
      if (!this.esEdicion) {
        console.log("Auditoria nueva");

        auditoria = new Auditoria(
          this.formNuevaAuditoria.get("nombreAuditoira").value,
          this.formNuevaAuditoria.get("frecuencia").value,
          this.formNuevaAuditoria.get("area").value,
          this.listsubZonasSeleccionadas,
          this.listVarables
        );
      } else {
        console.log("Auditoria de edicion");

        auditoria = new Auditoria(
          this.formNuevaAuditoria.get("nombreAuditoira").value,
          this.formNuevaAuditoria.get("frecuencia").value,
          this.formNuevaAuditoria.get("area").value,
          this.listsubZonasSeleccionadas,
          this.listVarables);
        auditoria = this.auditoriaEdicion;
        auditoria.idFrecuencia


      }


      if (!this.esEdicion) {
        auditoria.idAuditoria = -1;
      } else {
        // nuevaAuditoria.idAuditoria = this.ed
      }

      console.log("Nueva Auditoria", auditoria);
      let i = 1;
      auditoria.variables.forEach(variable=>{
        variable.imagenes.forEach(img=>{
          img.nombreArchivo = i.toString();
          i++;
        })
      })

      this._AuditoriaService.setAuditoria(auditoria).subscribe(
        response => {
          console.log(response);

          if (response.idAuditoria < 1) {
            toast({
              html: "Error al crear auditoria, ya existe una con este nombre",
              classes: "red rounded"
            });
          } else {
            //this.eliminarBorrador();
            toast({
              html: "Auditoria Agregada Correctamente",
              classes: "green rounded"
            });

            this.limpiarAuditoria();
            if (this.esEdicion) {
              //.eliminarBorrador();
              this.cancelarEdicion();
            }
          }
          this.alertEnviandoAuditoria.close();

        },
        error => {
          toast({ html: "Error al Agregar Auditoria", classes: "red rounded" });
          this.alertEnviandoAuditoria.close();
        }
      );
    }
  }

  actualizarVariable(variable: Varibale) {
    console.log("event", variable);

    this.listVarables.forEach(x => {

      if (x.idVariableNueva) {
        if (x.idVariableNueva == variable.idVariableNueva) {
          //this.listVarables = this.listVarables.filter(varx => varx != x);
          let indexVar: number = this.listVarables.findIndex(varx => varx.idVariableNueva == x.idVariableNueva)
          this.listVarables[indexVar] = variable;
          console.log("se modifico variable local");
          console.log(x);
        }
      } else {
        if (x.idVariable == variable.idVariable) {
          //this.listVarables = this.listVarables.filter(varx => varx != x);
          let indexVar: number = this.listVarables.findIndex(varx => varx.idVariable == x.idVariable)
          this.listVarables[indexVar] = variable;
          console.log("se modifico exitent de servidor");
          console.log(x);
        }
      }

    });

    if (this.esEdicion) {
      this.auditoriaEdicion.variables = this.listVarables;
    }
    console.log("mod", this.listVarables);

    console.log("log de edicion", this.auditoriaEdicion);



    if (!this.esEdicion) {
      this.guardaBorradorAuditoria();
    }
  }

  eliminarVariable(variable: Varibale): void {
    this.listVarables = this.listVarables.filter(x => x != variable);
    if (!this.esEdicion) {
      this.guardaBorradorAuditoria()
    };

    if (this.esEdicion) {
      this.auditoriaEdicion.variables = this.listVarables
    }
  }

  limpiarAuditoria(): void {
    this.idVariable = 1;
    this.listVarables = [];
    this.listSucursalesSeleccionadas = [];
    this.listsubZonasSeleccionadas = [];
    this.selecTodasSucursales = false;
    this.archivos = [];
    this.formNuevaAuditoria.reset();
    this.inicializarFormulario();
    this.formNuevaVariable.reset();
  }

  cancelarEdicion() {
    if (this.esEdicion) {
      this.cerrarEdicion.emit();
    }
  }

  getSubZonasSubadminZona(idSubadmin:number,idZona:number){
    this._AuditoriaService.getSubZonasSubadmiistradorZona(idSubadmin,idZona).
      subscribe(response=>{
        this.listSubZonasAgregar= response;
      },error=>{
        console.log(error);
      })
  }
  asignarSubZonaAuditoria(idSub:number, idAud:number,idSubZona:number,idOrigen:number,subzona:SubZona){
    this._AuditoriaService.asignarSubZonaAuditoria(idSub,idAud,idSubZona,idOrigen).
      subscribe(response=>{
        let p:ProcesadoModel;
        p=response;
        if(p.procesado){
          this.auditoriaEdicion.subZonas.push(subzona);
          toast({ html: "La subzona se agregó correctamente", classes: "green rounded" });
        }else{
          toast({ html: "La subzona ya esta agregada", classes: "red rounded" });
        }
      },error=>{
        console.log(error);
      })
  }
  desasignarSubZonaAuditoria(subzona:SubZona){
    this._AuditoriaService.desasignarSubZonaAuditoria(subzona.idSubZona,this.auditoriaEdicion.idAuditoria).
      subscribe(response=>{
        if(response.procesado){
          this.auditoriaEdicion.subZonas= this.auditoriaEdicion.subZonas.filter(x=> x.idSubZona!=subzona.idSubZona);
        }
      },error=>{
        console.log(error)
      })
  }

  async getDetallesAuditoria() {
    await this._AuditoriaService.getDetallesAuditoria(this._idAuditoria).then(
      response => {
        console.log("Detalle Auditoria", response);

        this.auditoriaEdicion = response;
        this.listSubZonas = this.auditoriaEdicion.subZonas;
        this.listsubZonasSeleccionadas = this.auditoriaEdicion.subZonas;

        console.log(this.listVarables);

        this.auditoriaEdicion.variables.forEach(variable => {
          if ((variable.todasLasSucursales == true)) {
            this.listVarables.push(variable);

          } else {
            variable.sucursales = variable.sucursales.filter(
              x => x.variableSucursal > 0
            );

            this.listVarables.push(variable);
          }
        });

        console.log("Variables", this.listVarables);


        this.formNuevaAuditoria.controls["nombreAuditoira"].setValue(
          this.auditoriaEdicion.nombreAuditoria
        );
        this.formNuevaAuditoria.controls["frecuencia"].setValue(
          this.auditoriaEdicion.frecuencia
        );
        this.formNuevaAuditoria.controls["area"].setValue(
          this.auditoriaEdicion.area
        );
        //this.getSubZonasSubadministrador();
      }/*,
      /* error => {
        toast({ html: "Error al Cargar la Auditoria", classes: "red rounded" });
      } */
    );
  }


  //LOCALSTORAGE DE AUDITORIAS INCONCLUSAS 

  guardaBorradorAuditoria() {

    let variables: Varibale[] = cloneDeep(this.listVarables);

    variables.forEach(variable => {
      variable.imagenes = <any>[];
    })

    let borradorAuditoria: Auditoria = new Auditoria(
      this.formNuevaAuditoria.get("nombreAuditoira").value,
      this.formNuevaAuditoria.get("frecuencia").value,
      this.formNuevaAuditoria.get("area").value,
      this.listsubZonasSeleccionadas,
      variables
    );



    console.log("borrador", borradorAuditoria);

    borradorAuditoria.idSubadministrador = this._AuditoriaService.agente
    borradorAuditoria.idOrigen = this._AuditoriaService.origen;

    let respaldoBorrador
    if (localStorage.getItem(this.auditoriaBorrador)) {
      respaldoBorrador = localStorage.getItem(this.auditoriaBorrador);
    }

    try {
      if (localStorage.getItem(this.auditoriaBorrador) === null) {
        localStorage.setItem(this.auditoriaBorrador, JSON.stringify(borradorAuditoria));
        toast({ html: 'Guardando Borrador ', classes: 'green rounded' });
      } else {
        localStorage.removeItem(this.auditoriaBorrador);
        localStorage.setItem(this.auditoriaBorrador, JSON.stringify(borradorAuditoria));
        toast({ html: 'Actualizando Borrador ', classes: 'green rounded' });
      }
    } catch (error) {
      toast({ html: 'Advertencia: El borrador esta lleno, es recomendable guardar la auditoria', classes: 'yellow rounded' }).options.displayLength = 30000;
      localStorage.setItem(this.auditoriaBorrador, respaldoBorrador);
      console.log(error);
    }

  }


  reanudarAuditoriaDeBorrador() {
    console.log("Reanudando Borrador");

    let auditoriaDeBorrador: Auditoria = JSON.parse(localStorage.getItem(this.auditoriaBorrador));

    if (auditoriaDeBorrador != undefined) {


      if (auditoriaDeBorrador.idSubadministrador !== this._AuditoriaService.agente) {
        localStorage.clear();
      } else {
        this.formNuevaAuditoria.get("nombreAuditoira").setValue(auditoriaDeBorrador.nombreAuditoria);
        this.formNuevaAuditoria.get("frecuencia").setValue(auditoriaDeBorrador.idFrecuencia);
        this.formNuevaAuditoria.get("area").setValue(auditoriaDeBorrador.idArea);
        this.listsubZonasSeleccionadas = auditoriaDeBorrador.subZonas;


        this.listsubZonasSeleccionadas.forEach(subzonaSelect =>
          this.listSubZonas.forEach(subzona => {
            if (subzonaSelect.idSubZona === subzona.idSubZona) {
              subzona.selected = true;
              subzonaSelect.selected = true;
            }
          })
        );
        this.listVarables = auditoriaDeBorrador.variables;
      }
    }
  }


  variableConFotoCargadas(resp: any) {

    if (resp.pos == -1) {
      this.alertCargandoAuditoria.close();
      this.cancelarEdicion();
      toast({ html: `Error al Cargar la Variable ${resp.var}`, classes: 'red rounded' });
      return;
    }

    this.variablesCargadas += resp.pos;
    console.log(this.variablesCargadas);

    if (this.variablesCargadas == this.auditoriaEdicion.variables.length) {
      this.alertCargandoAuditoria.close();
    }


  }
  getSubZonasAgregar(idZona:number){
    this.listSubZonasAgregar=[];
    this.mostrarSubzonasAgregar=true;
    this.getSubZonasSubadminZona(this._AuditoriaService.agente,idZona);
  }
}
