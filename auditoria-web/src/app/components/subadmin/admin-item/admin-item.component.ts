import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AuditoriaService } from "../../../services/auditoria.services";
import { toast, Toast } from "materialize-css";
import { SubAdministradorModel } from "../../../../models/SubAdministrador.model";
import { SubadminComponent } from "../subadmin.component";
import { Zona } from "../../../../models/ZonasSubAdministrador.mode";
import { SubZonasList, SubZona } from '../../../../models/subZonasAdmin.mode';
import { AreasModel } from '../../../../models/areas.model';
@Component({
  selector: "app-admin-item",
  templateUrl: "./admin-item.component.html",
  styleUrls: ["./admin-item.component.css"]
})
export class AdminItemComponent implements OnInit {

  @ViewChild('edtPorcetaje', { static: false }) edtPorcentaje: ElementRef;
  @Input() subadiministrador: SubAdministradorModel;
  @Output() updateSubAdmins: EventEmitter<any>;

  idZonaSubAdmin: number;
  idZonaEliminar: number;
  listAreasDisponibles: AreasModel[] = [];
  listZonasDisponibles: Zona[];
  listZonasAsignadas: Zona[];

  listSubZonasAdmnistrador: SubZonasList;
  listSubZonaDisponible: Array<SubZona>;
  listSubZonasAsignadas: Array<SubZona>;

  mostrarEliminar: boolean = false;
  mostrarEliminarZona: boolean = false;
  mostrarDetalles: boolean = false;
  mostrarZonas: boolean = false;
  mostrarSubzonas: boolean = false;
  mostrarAreas: boolean = false;
  areasCaegadas: boolean = false;
  porcentajeValido: boolean = true;
  constructor(
    private _AuditoriaService: AuditoriaService,
    private subadminComponent: SubadminComponent
  ) {
    this.updateSubAdmins = new EventEmitter();
  }

  ngOnInit() { }
  desplegar(): boolean {
    return this.mostrarDetalles;
  }
  cambiarMostrarZonas() {
    this.mostrarZonas = !this.mostrarZonas;
    this.getZonasSubAdministrador();
  }
  cambiarMostrarSubzonas() {
    this.mostrarSubzonas = !this.mostrarSubzonas;
  }
  cambiarMostrarAreas() {
    this.mostrarAreas = !this.mostrarAreas;
    //this.getAreasDisponibles();
    this.getAreasSubadministrador();
  }

  /*  getAreasDisponibles() {
     this._AuditoriaService.getAreas().subscribe(
       response => {
         this.listAreasDisponibles = response;
         console.log(response);
       },
       error => {
         toast({ html: "Error Al Cargar Áreas", classes: "red rounded" });
       }
     );
   } */

  setEliminarSubAdministrador() {
    this._AuditoriaService
      .setDesAsignarSubadministrador(this.subadiministrador)
      .subscribe(
        response => {
          console.log(response);
          if (response.procesado == true) {
            toast({
              html: "SubAdministrador Eliminado",
              classes: "green rounded"
            });
            this.subadminComponent.getSubAdministradores();
          } else {
            toast({ html: "Error Al Procesar", classes: "red rounded" });
          }
        },
        error => {
          toast({
            html: "Error Al Eliminar SubAdministrador",
            classes: "red rounded"
          });
        }
      );
  }

  getZonasSubAdministrador() {
    this._AuditoriaService
      .getZonasSubAdministrador(this.subadiministrador.idSubadministrador)
      .subscribe(
        response => {
          console.log(response);

          this.listZonasAsignadas = response.zonasAsignadas;
          this.listZonasDisponibles = response.zonasDisponibles;
        },
        error => {
          toast({ html: "Error al Cargar Zonas", classes: "red rounded" });
        }
      );
  }

  setAsignarZonaSubAdministrador(zona: Zona) {
    zona.idSubadministrador = this.subadiministrador.idSubadministrador;
    zona.agente = 1;
    zona.idOrigen = 1;
    console.log(zona);

    this._AuditoriaService.setAsignarZonaSubAdministrador(zona).subscribe(
      response => {
        console.log(response);

        this.getZonasSubAdministrador();
        toast({ html: "Zona Asignada", classes: "green rounded" });
      },
      error => {
        toast({ html: "Error al Asignar Zona", classes: "red rounded" });
      }
    );
  }

  elimiarZona(idZonaSubaAdmin: number) {
    this.mostrarEliminarZona = true;
    this.idZonaEliminar = idZonaSubaAdmin;
  }

  setDesAsignarZonaSubAdmini(idZonaSubAdmin: number) {
    this._AuditoriaService
      .setEliminarZonaSubAdmnistrador(idZonaSubAdmin)
      .subscribe(
        response => {
          console.log(response);

          toast({ html: "Zona Des-Asignada", classes: "green rounded" });
          this.getZonasSubAdministrador();
          this.mostrarEliminarZona = false;
        },
        error => {
          toast({ html: "Error al Des-Asignar Zona", classes: "red rounded" });
        }
      );
  }


  mostrarSubZonas(idZonSubAdmni: number) {
  console.log(idZonSubAdmni)
    console.log("SubZonas");
    this.idZonaSubAdmin = idZonSubAdmni;
    this.mostrarSubzonas = true;
    this.getSubZonasZonaAdmnistrador(idZonSubAdmni);
  }

  getSubZonasZonaAdmnistrador(idZonSubAdmni: number) {
    this._AuditoriaService.getSubZonasZonaSubAdmnistrador(idZonSubAdmni)
      .subscribe(response => {
        console.log("subzonas", response);
        this.listSubZonasAdmnistrador = response;
        this.listSubZonaDisponible = response.subZonasDisponibles;
        this.listSubZonasAsignadas = response.subZonasAsignadas;
      }, error => {
        toast({ html: 'Error al Cargar SubZonas', classes: 'red rounded' });
      })
  }


  setDesAsignarSubZona(subZona: number) {
    this._AuditoriaService.setDesAsignarSubZona(subZona)
      .subscribe(response => {
        console.log(response);
        toast({ html: 'SubZona Des-Asignada', classes: 'green rounded' })
        this.getSubZonasZonaAdmnistrador(this.idZonaSubAdmin)
      }, error => {
        toast({ html: 'Error al Des-Asignar SubZona', classes: 'red rounded' })
      })

  }

  setAsignarSubZona(subZona: SubZona) {
    subZona.idZonaSubadministrador = this.idZonaSubAdmin;
    this._AuditoriaService.setAsignarSubZona(subZona)
      .subscribe(response => {
        toast({ html: 'SubZona Asignada', classes: 'green rounded' })
        this.getSubZonasZonaAdmnistrador(this.idZonaSubAdmin);

      }, error => {
        toast({ html: 'Error al Asignar SubZona', classes: 'red rounded' })

      })
  }

  getAreasSubadministrador() {
    this._AuditoriaService.getAreasSubadministrador(this.subadiministrador.idSubadministrador)
      .subscribe(response => {
        console.log(response);
        this.listAreasDisponibles = response;

      }, error => {
        toast({ html: 'Error Cargar Area', classes: 'red rounded' })

      })
  }

  setAsignarArea(area: AreasModel) {

    area.idSubadministrador = this.subadiministrador.idSubadministrador;
    /*   area.agente=1;
      area.origen=1; */


    this._AuditoriaService.setAsignarAreaSubadministrador(area)
      .subscribe(response => {
        console.log(response);
        toast({ html: 'Area Asignada', classes: 'green rounded' })
        this.getAreasSubadministrador()
      }, error => {
        toast({ html: 'Error al Asignar Area', classes: 'red rounded' })
      })
  }


  validar(porcentaje) {
    console.log("Validando Porcentaje");
    console.log(porcentaje);

    if ((porcentaje >= 0) && (porcentaje <= 100)) {
      this.porcentajeValido = true;
    } else {
      this.porcentajeValido = false;
    }
  }

  setPorcentaje() {
    let porcentaje = this.edtPorcentaje.nativeElement.value;
    if (this.subadiministrador.porcentajeAutomaticas != porcentaje) {
      if (porcentaje >= 0 && porcentaje <= 100) {

        this._AuditoriaService.setPorcentajeAprobacion(this.subadiministrador.idPersona, porcentaje).subscribe(resp => {
          this.mostrarDetalles = false;
          toast({ html: 'Porcentaje Cambiado', classes: 'green rounded' })
          this.updateSubAdmins.emit();

        }, err => {
          console.log("Error al cambiar porcentaje:", err);
          toast({ html: 'Error al cambiar porcentaje', classes: 'red rounded' })
        });

      }
    } else {
      this.mostrarDetalles = false;
      this.updateSubAdmins.emit();

    }
  }


  onClickClose() {
    this.setPorcentaje();

  }
}
