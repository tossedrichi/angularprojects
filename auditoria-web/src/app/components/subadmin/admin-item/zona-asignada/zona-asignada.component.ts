import { Component, OnInit, Injectable, Input } from '@angular/core';

@Component({
  selector: 'app-zona-asignada',
  templateUrl: './zona-asignada.component.html',
  styleUrls: ['./zona-asignada.component.css']
})
export class ZonaAsignadaComponent implements OnInit {
  @Input() zonasAsignadas;
  mostrarEliminar:boolean=false;
  mostrarSubzonas:boolean=false;

  constructor() { }

  ngOnInit() {

    console.log(this.zonasAsignadas);
    
  }

}
