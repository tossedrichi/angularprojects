import { Component, OnInit, AfterContentInit } from "@angular/core";
import { AuditoriaService } from "../../services/auditoria.services";
import { fixName } from "../../tools";
import { toast, Modal } from "materialize-css";
import { Persona } from "src/models/persona.mode.";
import { FormGroup, FormArray, Validators, FormControl } from "@angular/forms";
@Component({
  selector: "app-subadmin",
  templateUrl: "./subadmin.component.html",
  styleUrls: ["./subadmin.component.css"]
})
export class SubadminComponent implements OnInit, AfterContentInit {
  formularioCorreos: FormGroup;
  alertCorreos: Modal;

  mostrarLista: boolean = false;
  loadingData: boolean = false;
  mostrarSearch: boolean = false;

  listPersonal: Array<Persona> = [];
  listSearchPerson: Array<Persona> = [];
  listPersonalSelected: Array<Persona> = [];
  listPersonaSinCorreo: Array<Persona> = [];

  listSubadministradores: any;
  listSearchAdmin: any;

  constructor(private _AuditoriaService: AuditoriaService) { }

  ngOnInit() {
    this.getSubAdministradores();
    this.validacionesFormCorreo();
  }

  ngAfterContentInit() {
    var modals = document.querySelector(".modal");
    this.alertCorreos = M.Modal.init(modals);
  }
  getPersonal() {
    this.loadingData = true;
    this._AuditoriaService.getPersonal().subscribe(
      response => {
        this.loadingData = false;
        this.listPersonal = response;

        this.listPersonal.forEach(person => {
          person.isSelected = false;
        });

        this.listSearchPerson = response;
        console.log(this.listPersonal);
      },
      error => {
        console.log(error);
        this.loadingData = false;
        toast({ html: "Error al Cargar Personal", classes: "red rounded" });
      }
    );
  }

  searchAdmin(params: string) {

    console.log(params);
    console.log(this.listSubadministradores);

    this.listSearchAdmin = this.listSubadministradores;

    this.listSearchAdmin = this.listSearchAdmin.filter(x =>
      x.nombre.toLowerCase().includes(params.toLowerCase())
    );
    console.log(this.listSearchAdmin);
  }

  alertLista() {
    this.mostrarLista = true;
    this.getPersonal();

    var elems = document.querySelectorAll(".autocomplete");
    var instances: any = M.Autocomplete.init(elems);
    console.log(instances);

    //instances.data(this.listPersonal)
  }

  addAndRemoveItemsPersona(persona: Persona) {
    if (!this.listPersonalSelected.find(x => x == persona)) {
      persona.isSelected = true;
      this.listPersonalSelected.push(persona);
      console.log(this.listPersonalSelected);
    } else {
      this.listPersonalSelected = this.listPersonalSelected.filter(
        x => x != persona
      );
      console.log(this.listPersonalSelected);
    }

    if (this.mostrarLista == false) {
      this.listPersonalSelected = [];
    }
  }

  setSubadministradoresSelected() {
    if (this.listPersonalSelected.length > 0) {
      this._AuditoriaService
        .setSubadministradores(this.listPersonalSelected)
        .subscribe(
          response => {
            console.log(response);
            this.mostrarLista = false;
            this.listPersonal = [];
            this.listPersonalSelected = [];
            this.listSubadministradores = [];
            this.getPersonal();
            this.getSubAdministradores();
            toast({
              html: "SubAdministrador(es) Agregado(s)",
              classes: "green rounded"
            });
          },
          error => {
            toast({
              html: "Error Agregar SubAdministradores",
              classes: "red rounded"
            });
          }
        );
    } else {
      toast({
        html: "Selecciona al menos una Persona",
        classes: "red rounded"
      });
    }
  }

  getSubAdministradores() {
    this.listSubadministradores=[];
    this.listSearchAdmin=[];
    this._AuditoriaService.getSubAdministradores().subscribe(
      response => {
        console.log(response);
        this.listSubadministradores = response;
        for (let i = 0; i < this.listSubadministradores.length; i++) {
          this.listSubadministradores[i].nombre = fixName(
            this.listSubadministradores[i].nombre
          );
        }
        this.listSearchAdmin = response


      },
      error => {
        toast({
          html: "Error al Cargar SubAdministradores",
          classes: "red rounded"
        });
      }
    );
  }

  searchPerson(params: string) {
    this.mostrarSearch = true;
    console.log(params);

    this.listSearchPerson = this.listPersonal;

    this.listSearchPerson = this.listSearchPerson.filter(x =>
      x.nombre.toLowerCase().includes(params.toLowerCase())
    );
    console.log(this.listSearchPerson);
  }

  select(isSelected: boolean): string {
    console.log("isSelected", isSelected);

    if (isSelected == true) {
      return "checked";
    }
  }

  validacionesFormCorreo() {
    this.formularioCorreos = new FormGroup({
      correos: new FormArray([])
    });
  }

  verificarCorreosPersonas() {
    this.listPersonaSinCorreo = [];

    this.listPersonalSelected.forEach(persona => {
      if (persona.correo.length == 0) {
        this.listPersonaSinCorreo.push(persona);
      }
    });

    console.log("Personas Sin Correo", this.listPersonaSinCorreo);

    if (this.listPersonaSinCorreo.length == 0) {
      this.setSubadministradoresSelected();
    } else {
      this.validacionesFormCorreo();
      this.alertCorreos.open();
      this.alertCorreos.options.dismissible = false;

      this.listPersonaSinCorreo.forEach(persona => {
        (<FormArray>this.formularioCorreos.controls["correos"]).push(
          new FormControl("", [Validators.required, Validators.email])
        );
      });
    }
  }



  setCorreos() {
    console.log(this.formularioCorreos);

    if (this.formularioCorreos.valid) {
      for (let i = 0; i < this.listPersonaSinCorreo.length; i++) {

        this.listPersonaSinCorreo[i].correo = this.formularioCorreos.controls["correos"].get(i.toString()).value;

      }
      console.log("Correos a Enviar", this.listPersonaSinCorreo);

      this._AuditoriaService.setActualizaCorreoPersonas(this.listPersonaSinCorreo)
        .subscribe(response => {
          this.listPersonaSinCorreo = [];
          this.formularioCorreos.reset();
          this.alertCorreos.close();
          this.setSubadministradoresSelected();
          this.getSubAdministradores();

        })
    }
  }

  closeDialogCorreos() {
    this.alertCorreos.close()
    this.formularioCorreos.reset()
    this.listPersonaSinCorreo = [];
    this.validacionesFormCorreo();
  }

  closeDialogoPersonal() {
    this.listPersonalSelected = [];
    this.listPersonal = [];
    this.listSearchPerson = [];
    this.mostrarLista = false
  }
}
