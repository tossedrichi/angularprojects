import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
  
  mostrarZonas: boolean = false;
  mostrarSubzonas: boolean = false;
  mostrarSucursales: boolean = false;
  mostrarDescargar: boolean = false;
  mostrarReportes: boolean = false;
  
  constructor() { }

  ngOnInit() {
  }
  cambiarMostrarZonas() {
    this.mostrarZonas = !this.mostrarZonas;

  }

  cambiarMostrarSubzonas() {
    this.mostrarSubzonas = !this.mostrarSubzonas;

  }
  cambiarMostrarSucursales() {
    this.mostrarSucursales = !this.mostrarSucursales;

  }
}
