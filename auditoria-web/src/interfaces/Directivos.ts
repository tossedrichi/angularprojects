export interface Directivos {
  directivosAsignados: Array<DirectivosAreaModel>;

  directivosDisponibles:Array<DirectivosAreaModel>;
}

export class DirectivosAreaModel {
  idDirectivo: number;
  idPersona: number;
  nombre: String;
  idDirectivoArea: number;
}


