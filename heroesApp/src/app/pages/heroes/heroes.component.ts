import { Component, OnInit } from "@angular/core";
import { HeroesService } from "../../services/heroes.service";
import { HeroeModel } from "../../models/heroe.model";
import Swal from "sweetalert2";

@Component({
  selector: "app-heroes",
  templateUrl: "./heroes.component.html",
  styleUrls: ["./heroes.component.css"]
})
export class HeroesComponent implements OnInit {
  heroes: HeroeModel[] = [];
  cargando:boolean = false;
 
  constructor(private heroesService: HeroesService) {}

  ngOnInit() {
    this.cargando=true;
    this.heroesService.getHeroes().subscribe(resp => {
      this.cargando=false;
      console.log(resp);
      this.heroes = resp;
     
    });
  }

  borrarHeroe(heroe: HeroeModel, index: number) {
    Swal.fire({
      title: "¿Esta Seguro?",
      text: `Está seguro que desea borrar a ${heroe.nombre}`,
      icon: "question",
      showCnfirmButton: true,
      showCancelButton: true
    }).then(resp => {
      if (resp.value) {
        this.heroesService.borrarHeroe(heroe.id).subscribe(resp => {
          this.heroes.splice(index, 1);
        });
      }
    });
  }
}
