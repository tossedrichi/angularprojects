import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { SolicitudRedigitalizacionComponent } from '../../pages/solicitud-redigitalizacion.component';
import { SolcitudService } from '../../services/solcitud.service';

const PARENT = "SolicitudRedigitalizacion";
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit, AfterViewInit {
  @ViewChild('stepper') stepper;
  
  public selectedStep: number = 0;

  public URL_ACTUAL: string = "";


/*   requiereDatosConyugeCliente:boolean=false;
  reuqiereDomicilioCliente:boolean=false;
  requiereDatosEmpleoCliente:boolean=false;
  requierDatosEmpleoConyugeCliente:boolean;
  requiereDatosAval:boolean=false;
  requiereDatosConyugeAval:boolean=false; */


  constructor(private route: Router, public _SolicitudService:SolcitudService) {
    this.subscribeRoutesEvents();
  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.selectStepByURL(this.URL_ACTUAL);
  }



  selectionPage(event) {
    this.selectedStep = event.selectedIndex;
    console.log(event.selectedIndex);
    switch (this.selectedStep) {
      case 0:
        this.route.navigate([`${PARENT}/datos-cliente`])
        break;
      case 1:
        this.route.navigate([`${PARENT}/datos-conyuge-cliente`])
        break;
      case 2:
        this.route.navigate([`${PARENT}/domicilio-cliente`])
        break;
      case 3:
        this.route.navigate([`${PARENT}/datos-empleo-cliente`])
        break;
      case 4:
        this.route.navigate([`${PARENT}/datos-empleo-conyuge-cliente`])
        break;
      case 5:
        this.route.navigate([`${PARENT}/datos-aval`])
        break;
      case 6:
        this.route.navigate([`${PARENT}/datos-conyuge-aval`])
        break;
      case 7:
        this.route.navigate([`${PARENT}/datos-contactos`])
        break;
      case 8:
        this.route.navigate([`${PARENT}/documentos`])
        break;
    }


  }


  subscribeRoutesEvents() {
    this.route.events
      .pipe(
        filter(evento => evento instanceof ActivationEnd),
        filter((evento: ActivationEnd) => evento.snapshot.routeConfig.path != PARENT),
        map((evento: ActivationEnd) => evento.snapshot.routeConfig.path))
      .subscribe(path => {
        console.log(path);

        this.URL_ACTUAL = path;

        this.selectStepByURL(path)

      })
  }

  selectStepByURL(url: string) {
    console.log("STEPPER", this.stepper._selectedIndex)

    switch (url) {
      case `datos-cliente`:
        this.stepper.selectedIndex = 0

        break;
      case `datos-conyuge-cliente`:
        this.stepper.selectedIndex = 1

        break;
      case `domicilio-cliente`:
        this.stepper.selectedIndex = 2
        break;
      case `datos-empleo-cliente`:
        this.stepper.selectedIndex = 3

        break;
      case `datos-empleo-conyuge-cliente`:
        this.stepper.selectedIndex = 4

        break;
      case `datos-aval`:
        this.stepper.selectedIndex = 5

        break;
      case `datos-conyuge-aval`:
        this.stepper.selectedIndex = 6
        break;
      case `datos-contactos`:
        this.stepper.selectedIndex = 7
        break;
      case `documentos`:
        this.stepper.selectedIndex = 8
        break;
    }

  }


}
