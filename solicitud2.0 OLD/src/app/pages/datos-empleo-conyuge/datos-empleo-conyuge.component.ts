import { Component, OnInit } from '@angular/core';
import { SolcitudService } from '../../services/solcitud.service';
import { EmpleosModel } from 'src/app/models/Empleos.model';

@Component({
  selector: 'app-datos-empleo-conyuge',
  templateUrl: './datos-empleo-conyuge.component.html',
  styleUrls: ['./datos-empleo-conyuge.component.css']
})
export class DatosEmpleoConyugeComponent implements OnInit {

  listEmpleos: EmpleosModel[] = [];

  openModaldDomicilioEmpleo: boolean = false;

  constructor(private _SolicitudService: SolcitudService) { }

  ngOnInit() {
    this.getEmpleos();
  }

  getEmpleos() {
    this._SolicitudService.getEmpleos().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleos = respuesta;

    })
  }


  respDataModalEmpleoConyuge(event) {
    console.log(event);
    if (event.isCancel) {
      this.openModaldDomicilioEmpleo = event.open;
    }
  }

}
