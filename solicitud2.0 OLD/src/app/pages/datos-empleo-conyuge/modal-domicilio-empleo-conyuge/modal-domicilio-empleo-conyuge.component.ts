import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EstadosModel } from 'src/app/models/Estados.model';
import { CiudadesModel } from 'src/app/models/Ciudades.model';
import { ColoniasModel } from 'src/app/models/Colonias.model';
import { SolcitudService } from 'src/app/services/solcitud.service';

@Component({
  selector: 'app-modal-domicilio-empleo-conyuge',
  templateUrl: './modal-domicilio-empleo-conyuge.component.html',
  styleUrls: ['./modal-domicilio-empleo-conyuge.component.css']
})
export class ModalDomicilioEmpleoConyugeComponent implements OnInit {

  @Input() openModal: boolean;
  @Output() dataDomicilioEmpleo: EventEmitter<{ open: boolean, isCancel: boolean, data?: any }>

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];

  constructor(private _SolicitudService: SolcitudService) {
    this.dataDomicilioEmpleo = new EventEmitter();
  }

  ngOnInit() {
    this.getEstados();
  }


  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado)
  }


  closeModal() {
    this.openModal = false;
    this.dataDomicilioEmpleo.emit({ open: this.openModal, isCancel: true })
  }


  getEstados() {
    this._SolicitudService.getEstados().subscribe(respuesta => {
      console.log(respuesta);
      this.listEstados = respuesta;
      this.listCiudades = []
      this.listColonias = []

    })
  }

  getCiudades(idestado: number) {
    this._SolicitudService.getCiudades(idestado).subscribe(respuesta => {
      console.log(respuesta);
      this.listCiudades = respuesta;
      this.listColonias = []

    })
  }

  getColonias(ciudad) {
    this._SolicitudService.getColonias(ciudad).subscribe(respuesta => {
      console.log(respuesta);
      this.listColonias = respuesta;

    })
  }



}
