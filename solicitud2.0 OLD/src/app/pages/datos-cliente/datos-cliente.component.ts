import { Component, OnInit } from '@angular/core';
import { EdoCivilModel } from 'src/app/models/EdoCivil.model';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { FileItem } from 'src/app/models/file-item';

@Component({
  selector: 'app-datos-cliente',
  templateUrl: './datos-cliente.component.html',
  styleUrls: ['./datos-cliente.component.css']
})
export class DatosClienteComponent implements OnInit {
  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];
  listEstadoCivil:EdoCivilModel[]=[];

  constructor(private _solicitudServices: SolcitudService) { }

  ngOnInit() {
    this.getTiposEstadoCivil();
  }

  getTiposEstadoCivil() {

    this._solicitudServices.getEstadoCivil()
      .subscribe(resp => {
        this.listEstadoCivil=resp;
      }, err => {

      })
  }

}


