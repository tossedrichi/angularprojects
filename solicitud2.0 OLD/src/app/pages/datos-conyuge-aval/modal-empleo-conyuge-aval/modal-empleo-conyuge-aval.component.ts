import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { FileItem } from 'src/app/models/file-item';

@Component({
  selector: 'app-modal-empleo-conyuge-aval',
  templateUrl: './modal-empleo-conyuge-aval.component.html',
  styleUrls: ['./modal-empleo-conyuge-aval.component.css']
})
export class ModalEmpleoConyugeAvalComponent implements OnInit {
  @Input() openModal: boolean
  @Output() dataEmpleoConyugeAval: EventEmitter<{open:boolean,data?:any}>;


  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];

  constructor(private _SolicitudService: SolcitudService) { 
    this.dataEmpleoConyugeAval = new EventEmitter();

  }

  ngOnInit() {

  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }
  closeModal() {
    this.openModal = false;
    this.dataEmpleoConyugeAval.emit({open:false})
  }
}
