import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { FileItem } from 'src/app/models/file-item';

@Component({
  selector: 'app-modal-empleo-aval',
  templateUrl: './modal-empleo-aval.component.html',
  styleUrls: ['./modal-empleo-aval.component.css']
})
export class ModalEmpleoAvalComponent implements OnInit {
  @Input() openModal: boolean
  @Output() dataEmpleoAval: EventEmitter<{open:boolean,data?:any}>;


  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];

  constructor(private _SolicitudService: SolcitudService) { 
    this.dataEmpleoAval = new EventEmitter();

  }

  ngOnInit() {

  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }
  closeModal() {
    this.openModal = false;
    this.dataEmpleoAval.emit({open:false})
  }
}
