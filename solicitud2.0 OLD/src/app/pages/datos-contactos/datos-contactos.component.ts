import { Component, OnInit } from '@angular/core';
import { TiposRelacionesModel } from '../../models/TiposRelaciones';
import { SolcitudService } from '../../services/solcitud.service';

@Component({
  selector: 'app-datos-contactos',
  templateUrl: './datos-contactos.component.html',
  styleUrls: ['./datos-contactos.component.css']
})
export class DatosContactosComponent implements OnInit {


  listTipoRelaciones: TiposRelacionesModel[] = [];

  constructor(private _SolicitudSrevice: SolcitudService) { }

  ngOnInit() {
    this.getTiposRelaciones();
  }


  getTiposRelaciones() {
    this._SolicitudSrevice.getTiposRelaciones().subscribe(response => {
      this.listTipoRelaciones = response;
    })
  }
}
