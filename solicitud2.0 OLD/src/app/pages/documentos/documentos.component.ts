import { Component, OnInit } from '@angular/core';
import { FileItem } from '../../models/file-item';
import { TiposDocumentosModel } from '../../models/TiposDocumentos.model';
import { SolcitudService } from '../../services/solcitud.service';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.css']
})
export class DocumentosComponent implements OnInit {

  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];
  listTiposDocumentos: TiposDocumentosModel[] = [];

  constructor(private _SolcitudService: SolcitudService) { }

  ngOnInit() {
    this.getTiposDocumentos();
  }


  getTiposDocumentos() {
    this._SolcitudService.getTiposDocumentos().subscribe(
      response => {
        this.listTiposDocumentos = response;
      }
    )
  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }

}
