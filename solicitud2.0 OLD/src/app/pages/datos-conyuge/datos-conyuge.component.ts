import { Component, OnInit } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { EdoCivilModel } from '../../models/EdoCivil.model';

@Component({
  selector: 'app-datos-conyuge',
  templateUrl: './datos-conyuge.component.html',
  styleUrls: ['./datos-conyuge.component.css']
})
export class DatosConyugeComponent implements OnInit {

  listEstadoCivil:EdoCivilModel[]=[];

  constructor(private _solicitudServices: SolcitudService) { }


  ngOnInit() {
    this.getTiposEstadoCivil();
  }

  getTiposEstadoCivil() {

    this._solicitudServices.getEstadoCivil()
      .subscribe(resp => {
        this.listEstadoCivil=resp;
      }, err => {

      })
  }
}
