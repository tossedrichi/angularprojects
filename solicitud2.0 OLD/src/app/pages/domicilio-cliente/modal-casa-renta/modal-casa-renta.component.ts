import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-casa-renta',
  templateUrl: './modal-casa-renta.component.html',
  styleUrls: ['./modal-casa-renta.component.css']
})
export class ModalCasaRentaComponent implements OnInit {
  @Input() openModal: boolean;
  @Output() dataCasaRenta: EventEmitter<{ open: boolean, isCancel: boolean, monto?: string }>
  constructor() {
    this.dataCasaRenta = new EventEmitter();
  }

  ngOnInit() {

  }

  sendData() {

  }

  closeModal() {
    this.openModal = false;
    this.dataCasaRenta.emit({ open: false, isCancel: true })

  }

}
