import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { SolcitudService } from '../../../services/solcitud.service';
import { RelacionCasaFamiliarModel } from 'src/app/models/RelacionCasaFamiliar.model';

@Component({
  selector: 'app-modal-casa-familiar',
  templateUrl: './modal-casa-familiar.component.html',
  styleUrls: ['./modal-casa-familiar.component.css']
})
export class ModalCasaFamiliarComponent implements OnInit {

  @Input() openModal: boolean
  @Output() dataCasaFamiliar: EventEmitter<{open:boolean,data?:any}>;

  listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[] = []
  constructor(private _solcitudService: SolcitudService) {

    this.dataCasaFamiliar = new EventEmitter();

   }

  ngOnInit() {
    this.getTiposRelacionesCasaFam()
  }

  getTiposRelacionesCasaFam() {
    this._solcitudService.getTiposRelacionesCasaFam().subscribe(respuesta => {
      console.log(respuesta);
      this.listTipoRelacionesCasaFam = respuesta;

    })
  }

  onSelectRelacionCasaFamiliar(event) {

  }

  closeModal() {
    this.openModal = false;
    this.dataCasaFamiliar.emit({open:false})
  }

}
