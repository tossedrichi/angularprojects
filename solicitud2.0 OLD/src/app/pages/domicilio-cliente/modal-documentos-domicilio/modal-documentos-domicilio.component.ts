import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { FileItem } from 'src/app/models/file-item';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';

@Component({
  selector: 'app-modal-documentos-domicilio',
  templateUrl: './modal-documentos-domicilio.component.html',
  styleUrls: ['./modal-documentos-domicilio.component.css']
})
export class ModalDocumentosDomicilioComponent implements OnInit {
  @Input() openModal: boolean
  @Output() dataDocumentosDomicilio: EventEmitter<{open:boolean,data?:any}>;


  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];
  listTiposDocumentos: TiposDocumentosModel[] = [];
  
  constructor(private _SolicitudService: SolcitudService) {

    
    this.dataDocumentosDomicilio = new EventEmitter();

   }



  ngOnInit() {
    this.getTiposDocumentos();
  }

  getTiposDocumentos() {
    this._SolicitudService.getTiposDocumentos().subscribe(
      response => {
        this.listTiposDocumentos = response;
      }
    )
  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }
  closeModal() {
    this.openModal = false;
    this.dataDocumentosDomicilio.emit({open:false})
  }
}

  


