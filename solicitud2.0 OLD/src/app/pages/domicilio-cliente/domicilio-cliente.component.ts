import { Component, OnInit } from '@angular/core';
import { SolcitudService } from '../../services/solcitud.service';
import { EstadosModel } from 'src/app/models/Estados.model';
import { CiudadesModel } from 'src/app/models/Ciudades.model';
import { ColoniasModel } from 'src/app/models/Colonias.model';
import { TiposDomiciliosModel } from 'src/app/models/TiposDomicilio.model';
import { CondicionesModel } from 'src/app/models/CondicionesDomicilio.model';
import { RelacionCasaFamiliarModel } from 'src/app/models/RelacionCasaFamiliar.model';
import { TiposComprobanteDomicilioModel } from 'src/app/models/TiposComprobanteDomicilio.model';

@Component({
  selector: 'app-domicilio-cliente',
  templateUrl: './domicilio-cliente.component.html',
  styleUrls: ['./domicilio-cliente.component.css']
})
export class DomicilioClienteComponent implements OnInit {

  constructor(private _solicitudService: SolcitudService) { }

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];
  listTiposDomicilio: TiposDomiciliosModel[] = [];
  listCondiciones: CondicionesModel[] = [];
  listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[] = [];
  listComprobanteDomicilio: TiposComprobanteDomicilioModel[] = [];

  openModalCasaFamiliar: boolean = false;
  openModalCasaRenta: boolean = false;

  ngOnInit() {
    this.getEstados();
    this.getTiposDomicilios();
    this.getCondiciones();
  }

  getEstados() {
    this._solicitudService.getEstados()
      .subscribe(resp => {
        this.listEstados = resp;
      }, err => {

      })
  }

  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado)
  }

  onSelectCiudad(idCiudad: number) {
    this.getColonias(idCiudad);
  }

  onSelectTipoDomicilio(idTipoDomiclio: string) {
    console.log(idTipoDomiclio);

    switch (parseInt(idTipoDomiclio)) {
      case 2:
        console.log("Casa Familiar");
        this.openModalCasaFamiliar = true;
        break;
      case 3:
        console.log("Casa Renta");
        this.openModalCasaRenta = true;
        break;

    }
  }

  getCiudades(idEstado) {
    this._solicitudService.getCiudades(idEstado).subscribe(respuesta => {
      console.log(respuesta);
      this.listCiudades = respuesta;
      this.listColonias = []

    })
  }

  on(event) {
    console.log(event);

  }
  getColonias(ciudad) {
    this._solicitudService.getColonias(ciudad).subscribe(respuesta => {
      console.log(respuesta);
      this.listColonias = respuesta;

    })
  }

  getTiposDomicilios() {
    this._solicitudService.getTiposDomicilios().subscribe(respuesta => {
      console.log(respuesta);
      this.listTiposDomicilio = respuesta;

    })
  }


  getTiposRelacionesCasaFam() {
    this._solicitudService.getTiposRelacionesCasaFam().subscribe(respuesta => {
      console.log(respuesta);
      this.listTipoRelacionesCasaFam = respuesta;

    })
  }
  getComprobanteDomicilio() {
    this._solicitudService.getTiposComprobanteDomiclio().subscribe(respuesta => {
      console.log(respuesta);
      this.listComprobanteDomicilio = respuesta;

    })


  }
  getCondiciones() {
    this._solicitudService.getCondiciones().subscribe(respuesta => {
      console.log(respuesta);
      this.listCondiciones = respuesta;

    })

  }


  responseDataCasaFamiliar(event) {
    console.log(event);

    if (!event.open) {
      this.openModalCasaFamiliar = event.open;

      console.log(this.openModalCasaFamiliar);
      
    }
  }

  responseDataCasaRenta(event) {
    console.log(event);

    if(event.isCancel){
      this.openModalCasaRenta=event.open;
    }

  

  }
  respDataModalDocumentosDomicilio(event) {
    console.log(event);
    if (event.isCancel) {
      this.openModalDocumentosDomicilio = event.open;
    }
  }
=======

  }

>>>>>>> 4e21c2dcb6d974a010f5ae6a0d9a461c79ff3ba4
}
