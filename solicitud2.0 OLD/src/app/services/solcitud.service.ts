import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators'
import { EstadosModel } from '../models/Estados.model';
import { CiudadesModel } from '../models/Ciudades.model';
import { ColoniasModel } from '../models/Colonias.model';
import { TiposDomiciliosModel } from '../models/TiposDomicilio.model';
import { TiposDocumentosModel } from '../models/TiposDocumentos.model';
import { RelacionCasaFamiliarModel } from '../models/RelacionCasaFamiliar.model';
import { EmpleosModel } from '../models/Empleos.model';
import { TiposContactoModel } from '../models/TiposContacto.model';
import { TiposClienteModel } from '../models/TiposCliente.model';
import { CondicionesModel } from '../models/CondicionesDomicilio.model';
import { TiposComprobanteDomicilioModel } from '../models/TiposComprobanteDomicilio.model';
import { TiposComprobanteIngresosModel } from '../models/TiposComprobanteIngresos.model';
import { EdoCivilModel } from '../models/EdoCivil.model';
import { TiposRelacionesModel } from '../models/TiposRelaciones';

const BASE_URL = "http://microstest.gruporoga.com:2021/rogaservices/credit/"


const headers = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"

  })
};

const TIMEOUT: number = 60000;


@Injectable({
  providedIn: 'root'
})
export class SolcitudService {


  requiereDatosConyugeCliente:boolean=true;
  reuqiereDomicilioCliente:boolean=true;
  requiereDatosEmpleoCliente:boolean=true;
  requierDatosEmpleoConyugeCliente:boolean=true;
  requiereDatosAval:boolean=true;
  requiereDatosConyugeAval:boolean=true;


  constructor(private http: HttpClient) { }

  getEstadoCivil() {
    const path = `${BASE_URL}catalogos/TiposEstadoCivil`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EdoCivilModel[]>response;
      })
    )
  }

  getEstados() {
    const path = `${BASE_URL}catalogos/ConsultaEstados`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EstadosModel[]>response;
      })
    )
  }


  getCiudades(estado: number) {
    const path = `${BASE_URL}catalogos/ConsultaCiudades/?estado=${estado}`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <CiudadesModel[]>response;
      })
    )
  }


  getColonias(ciudad: number) {
    const path = `${BASE_URL}catalogos/ConsultaColonias/?ciudad=${ciudad}`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <ColoniasModel[]>response;
      })
    )
  }


  getTiposDomicilios() {
    const path = `${BASE_URL}catalogos/TiposDomicilio`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposDomiciliosModel[]>response;
      })
    )
  }

  getTiposDocumentos() {
    const path = `${BASE_URL}catalogos/TiposDocumentos`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposDocumentosModel[]>response;
      })
    )
  }
  getTiposRelacionesCasaFam() {
    const path = `${BASE_URL}catalogos/TiposRelacionesCasaFam`
    return this.http.get(path, headers).pipe(
      map(response => {
        return <RelacionCasaFamiliarModel[]>response;
      })
    )
  }

  getEmpleos() {
    const path = `${BASE_URL}catalogos/TiposEmpleo`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EmpleosModel[]>response;
      })
    )
  }

  getTiposContacto() {
    const path = `${BASE_URL}catalogos/TiposContacto`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposContactoModel[]>response;
      })
    )
  }
  getTiposCliente() {
    const path = `${BASE_URL}catalogos/TiposCliente`
    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposClienteModel[]>response;
      })
    )


  }
  getCondiciones() {
    const path = `${BASE_URL}catalogos/TiposCondicionDomicilio`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <CondicionesModel[]>response;
      })
    )

  }
  getTiposComprobanteDomiclio() {
    const path = `${BASE_URL}catalogos/TiposComprobantesDomicilio`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposComprobanteDomicilioModel[]>response;
      })
    )

  }
  getComprobanteIngresos() {
    const path = `${BASE_URL}catalogos/TiposComprobantesIngresos`

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposComprobanteIngresosModel[]>response;
      })
    )

  }

  getTiposRelaciones(){
    const path =`${BASE_URL}catalogos/TiposRelaciones`;

    return this.http.get(path,headers).pipe(
      map(response=>{
        return <TiposRelacionesModel[]>response;
      })
    )
  }
}
