import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { MaterialModule } from './material.module';
import { DatosClienteComponent } from './pages/datos-cliente/datos-cliente.component';
import { DatosConyugeComponent } from './pages/datos-conyuge/datos-conyuge.component';
import { DomicilioClienteComponent } from './pages/domicilio-cliente/domicilio-cliente.component';
import { DatosEmpleoClienteComponent } from './pages/datos-empleo-cliente/datos-empleo-cliente.component';
import { DatosEmpleoConyugeComponent } from './pages/datos-empleo-conyuge/datos-empleo-conyuge.component';
import { DatosAvalComponent } from './pages/datos-aval/datos-aval.component';
import { DatosConyugeAvalComponent } from './pages/datos-conyuge-aval/datos-conyuge-aval.component';
import { DatosContactosComponent } from './pages/datos-contactos/datos-contactos.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { ModalDomicilioEmpleoComponent } from './pages/datos-empleo-cliente/modal-domicilio-empleo/modal-domicilio-empleo.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalCasaRentaComponent } from './pages/domicilio-cliente/modal-casa-renta/modal-casa-renta.component';
import { ModalCasaFamiliarComponent } from './pages/domicilio-cliente/modal-casa-familiar/modal-casa-familiar.component';
import { ModalDomicilioEmpleoConyugeComponent } from './pages/datos-empleo-conyuge/modal-domicilio-empleo-conyuge/modal-domicilio-empleo-conyuge.component';
import { NgDropFilesDirective } from './directives/ng-drop-files.directive';
import { LoginComponent } from './pages/login/login.component';
import { SolicitudRedigitalizacionComponent } from './pages/solicitud-redigitalizacion.component';
import { BusquedaClienteComponent } from './pages/busqueda-cliente/busqueda-cliente.component';
<<<<<<< HEAD
import { ModalDocumentosEmpleoComponent } from './pages/datos-empleo-cliente/modal-documentos-empleo/modal-documentos-empleo.component';
import { ModalDocumentosDomicilioComponent } from './pages/domicilio-cliente/modal-documentos-domicilio/modal-documentos-domicilio.component';
import { ModalEmpleoAvalComponent } from './pages/datos-aval/modal-empleo-aval/modal-empleo-aval.component';
import { ModalEmpleoConyugeAvalComponent } from './pages/datos-conyuge-aval/modal-empleo-conyuge-aval/modal-empleo-conyuge-aval.component';


=======
>>>>>>> 4e21c2dcb6d974a010f5ae6a0d9a461c79ff3ba4

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    DatosClienteComponent,
    DatosConyugeComponent,
    DomicilioClienteComponent,
    DatosEmpleoClienteComponent,
    DatosEmpleoConyugeComponent,
    DatosAvalComponent,
    DatosConyugeAvalComponent,
    DatosContactosComponent,
    DocumentosComponent,
<<<<<<< HEAD
    ModalCasaRentaComponent,
    ModalCasaFamiliarComponent,
    ModalDomicilioEmpleoConyugeComponent,
    ModalDocumentosEmpleoComponent,
    NgDropFilesDirective,
    LoginComponent,
    ModalDomicilioEmpleoComponent,
    SolicitudRedigitalizacionComponent,
    BusquedaClienteComponent,
    ModalDocumentosDomicilioComponent,
    ModalEmpleoAvalComponent,
    ModalEmpleoConyugeAvalComponent],

=======
    ModalDomicilioEmpleoComponent,
    ModalCasaRentaComponent,
    ModalCasaFamiliarComponent,
    ModalDomicilioEmpleoConyugeComponent,
    NgDropFilesDirective,
    LoginComponent,
    SolicitudRedigitalizacionComponent,
    BusquedaClienteComponent],
>>>>>>> 4e21c2dcb6d974a010f5ae6a0d9a461c79ff3ba4


  imports: [
    BrowserModule,
    MaterialModule,
    APP_ROUTING,
    HttpClientModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
