export class TiposComprobanteIngresosModel{
    
    id_tipo_comprobante_ingreso: number
    tipo_comprobante_ingreso: string
    valida_informacion: boolean
    estado_actual: string

}