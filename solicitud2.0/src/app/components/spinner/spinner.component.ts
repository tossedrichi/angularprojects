import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-spinner",
  templateUrl: "./spinner.component.html"
})
export class SpinnerComponent implements OnInit {
  constructor() {}

  @Input() sizeSpinner: string;
  @Input() isFull: boolean = false;
  
  ngOnInit() {}
}
