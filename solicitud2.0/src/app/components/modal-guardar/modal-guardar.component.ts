import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { DataInputModalGuardarModel } from "./dataInputModa.model";
import { SolcitudService } from "../../services/solcitud.service";
import { SucursalModel } from "../../models/Sucursal.model";
import { ZonasModel } from "../../models/Zonas.model";
import { isUndefined } from "util";
import { ToastrService } from "ngx-toastr";
import { getFechaActual } from "src/app/tools";

@Component({
  selector: "app-modal-guardar",
  templateUrl: "./modal-guardar.component.html",
  styleUrls: ["./modal-guardar.component.css"]
})
export class ModalGuardarComponent implements OnInit, OnChanges {
  @Input() dataInput: DataInputModalGuardarModel;
  @Input() openModal: boolean;
  @Input() esNuevaSolcitud: boolean = false;
  @Input() cedisSeleccionado: number = 0;

  @Output() dataGuardar: EventEmitter<{
    save: boolean;
    date?: string;
    idSucursal?: number;
  }>;

  @ViewChild("date") date: ElementRef;

  fechaVacia: boolean = false;
  fechaInvalida: boolean = false;
  sucursales: SucursalModel[] = [];

  listCedis: ZonasModel[] = [];
  idSucursal: number;
  ZonaSeleccionada: ZonasModel = new ZonasModel();
  cedisSelccionado: number;

  constructor(
    private _solcitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataGuardar = new EventEmitter();
  }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = JSON.stringify(chng.currentValue);
      let prev = JSON.stringify(chng.previousValue);
      if (propName == "cedisSeleccionado" && !isUndefined(cur)) {
        this.getZonas();
      }

      console.log(
        `${propName}: currentValue = ${cur}, previousValue = ${prev}`
      );
    }
  }

  ngOnInit() {}

  closeModal(): void {
    this.openModal = false;
    this.dataGuardar.emit({ save: false });
  }

  okModal(): void {
    if (this.esNuevaSolcitud) {
      if (this.validaciones()) {
        if (this.idSucursal > 0) {
          this.openModal = false;
          this.dataGuardar.emit({
            save: true,
            date: this.date.nativeElement.value,
            idSucursal: this.idSucursal
          });
        } else {
          this.toaster.error("Selecciona una sucursal");
        }
      }
    } else {
      this.openModal = false;
      this.dataGuardar.emit({ save: true });
    }
  }

  validaciones(): boolean {
    if (this.date.nativeElement.value == "") {
      this.fechaVacia = true;
      this.fechaInvalida = false;
      return false;
    } else {
      this.fechaVacia = false;
      let fechaActual = new Date(getFechaActual());
     
      let strInputValue =this.date.nativeElement.value
       strInputValue = strInputValue.replace(/-/, '/')  // replace 1st "-" with "/"
      .replace(/-/, '/');
     
      let fechaComp = new Date(strInputValue);
     
      console.log("FECHA",this.date.nativeElement.value)
      fechaComp.setHours(0, 0, 0, 0);
      fechaActual.setHours(0, 0, 0, 0);
      // fechaComp.setDate(fechaComp.getDay()+1)
      console.log("fecha actual", fechaActual);
      console.log("fecha Selec", fechaComp);

      if (fechaComp.getTime() < fechaActual.getTime()) {
        this.fechaInvalida = false;
        this.fechaVacia = false;
        return true;
      } else {
        this.fechaVacia = false;
        this.fechaInvalida = true;
        return false;
      }
    }
  }

  getSucursalesCedis(cedis: number) {
    console.log(cedis);
    this._solcitudService.getSucursalesCedis(cedis).subscribe(
      response => {
        this.sucursales = response;
        console.log(this.sucursales);
      },
      error => {
        console.log(error);
        this.toaster.error(error.message, "Error al cargar sucursales");
      }
    );
  }
  cambiarZona(id: number) {
    this.ZonaSeleccionada = this.listCedis.filter(x => x.idZona == id)[0];

    this.getSucursalesCedis(this.ZonaSeleccionada.idZona);
  }

  getZonas() {
    this._solcitudService.getZonas().subscribe(
      response => {
        let todos: ZonasModel = new ZonasModel();
        todos.idZona = -1;
        todos.nombreZona = "Todos";
        this.listCedis.push(todos);
        response.forEach(x => {
          this.listCedis.push(x);
        });
        this.listCedis = this.listCedis.filter(
          x => !x.nombreZona.includes("Comercio Electronico")
        );

        this.ZonaSeleccionada = this.listCedis
          .filter(x => x.idZona == this.cedisSeleccionado)
          .filter(x => !x.nombreZona.includes("Comercio Electronico"))[0];

        console.log("cedis " + this.cedisSeleccionado);
        this.getSucursalesCedis(this.ZonaSeleccionada.idZona);
      },
      err => console.log(err)
    );
  }
}
