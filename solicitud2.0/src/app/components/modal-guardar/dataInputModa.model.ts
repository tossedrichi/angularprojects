export class DataInputModalGuardarModel {
    title: string;
    content: string;
    listCamposFaltantes: string[];

    constructor(title:string,contetn:string,listCamposFaltantes:string[]){
        this.title = title;
        this.content = contetn;
        this.listCamposFaltantes = listCamposFaltantes;
    }
}