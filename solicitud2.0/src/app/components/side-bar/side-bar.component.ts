import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  EventEmitter,
  AfterViewChecked
} from "@angular/core";
import { Router, ActivationEnd, ActivatedRoute } from "@angular/router";
import { filter, map } from "rxjs/operators";
import { SolicitudRedigitalizacionComponent } from "../../pages/solicitud-redigitalizacion.component";
import { SolcitudService } from "src/app/services/solcitud.service";
import { DataInputModalGuardarModel } from "../modal-guardar/dataInputModa.model";
import { FinalizarSolicitudComponent } from "../../pages/finalizar-solicitud/finalizar-solicitud.component";
import { DatosEmpleoAvalComponent } from "../../pages/datos-empleo-aval/datos-empleo-aval.component";
import { isNullOrUndefined } from "util";

const PARENT = "SolicitudRedigitalizacion";
@Component({
  selector: "app-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.css"]
})
export class SideBarComponent
  implements OnInit, AfterViewInit, AfterViewChecked {
  @ViewChild("stepper") stepper;
  showForm: boolean = false;
  public selectedStep: number = 0;

  public URL_ACTUAL: string = "";
  menuSelected: string = "";
  dataInput: DataInputModalGuardarModel;
  openModalGuardar: boolean = false;
  isSubscribeRouter: boolean = false;

  constructor(
    private route: Router,
    public _SolicitudService: SolcitudService
  ) {
    // this.subscribeRoutesEvents();
  }

  ngOnInit() {
    this._SolicitudService.notificaSideNav.subscribe(resp => {
      console.log("emit");

      this.selectStepByURL(resp);
      this.selectionPage(resp,false);
    });
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
  }
  ngAfterViewChecked(): void {
    //this.subscribeRoutesEvents();
  }

  selectionPage(event, sourceStepper: boolean) {
    if (sourceStepper) {
      this.selectedStep = event.selectedIndex;
      console.log("Pagina Seleccionada", event.selectedStep.ariaLabel);
      this.menuSelected = event.selectedStep.ariaLabel;
    } else {
      this.menuSelected = event;
    }
    //this.selectStepByURL(this.menuSelected);

    switch (this.menuSelected) {
      case this._SolicitudService.DatosPersonalesCliente:
        this.route.navigate([`${PARENT}/datos-cliente`]);
        break;
      case this._SolicitudService.DatosConyugeCliente:
        this.route.navigate([`${PARENT}/datos-conyuge-cliente`]);
        break;
      case this._SolicitudService.DomicilioCliente:
        this.route.navigate([`${PARENT}/domicilio-cliente`]);
        break;
      case this._SolicitudService.DatosEmpleoCliente:
        this.route.navigate([`${PARENT}/datos-empleo-cliente`]);
        break;
      case this._SolicitudService.DatosEmpleoConyugeCliente:
        this.route.navigate([`${PARENT}/datos-empleo-conyuge-cliente`]);
        break;
      case this._SolicitudService.DatosAval:
        this.route.navigate([`${PARENT}/datos-aval`]);
        break;
      case this._SolicitudService.DomicilioAval:
        this.route.navigate([`${PARENT}/domicilio-aval`]);
        break;
      case this._SolicitudService.DatosEmpleoAval:
        this.route.navigate([`${PARENT}/datos-empleo-aval`]);
        break;
      case this._SolicitudService.DatosConyugeAval:
        this.route.navigate([`${PARENT}/datos-conyuge-aval`]);
        break;
      case this._SolicitudService.Documentos:
        this.route.navigate([`${PARENT}/documentos`]);
        break;
      case this._SolicitudService.Referencias:
        this.route.navigate([`${PARENT}/referencias`]);
        break;
      case this._SolicitudService.IngresoMensual:
        this.route.navigate([`${PARENT}/ingresos-familiares`]);
        break;
      case this._SolicitudService.AgregarPedido:
        this.route.navigate([`${PARENT}/agrega-pedido`]);
        break;
      case this._SolicitudService.FinalizarSolicitud:
        this.route.navigate([`${PARENT}/finalizar-solicitud`]);
        break;
    }
  }

  finalizarSolicitud() {
    this._SolicitudService.finalizaSolicitud().subscribe(
      resp => {
        console.log(resp);
      },
      error => {
        console.log(error);
      }
    );
  }
  onSaveClick() {
    this.dataInput = new DataInputModalGuardarModel(
      "Finalizar solicitud",
      "Si finaliza la solicitud ya no podrá editarla, ¿Desea Continuar?",
      []
    );
    this.openModalGuardar = true;
  }
  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;
    if (event.save) {
      this.finalizarSolicitud();
    }
  }

  subscribeRoutesEvents() {
    console.log("subscribeRoutesEvents");

    this.route.events
      .pipe(
        filter(evento => evento instanceof ActivationEnd),
        filter(
          (evento: ActivationEnd) => evento.snapshot.routeConfig.path != PARENT
        ),
        map((evento: ActivationEnd) => evento.snapshot.routeConfig.path)
      )
      .subscribe(path => {
        console.log(path);

        this.URL_ACTUAL = path;

        if (!isNullOrUndefined(this.stepper)) {
          this.selectStepByURL(this.menuSelected);
        }
      });
  }

  selectStepByURL(url: string) {
    console.log("STEPPER", this.stepper.steps._results);

    let indexSelect = this.stepper.steps._results.findIndex(
      x => x.ariaLabel == url
    );
    this.stepper.selectedIndex = indexSelect;

    console.log("Selectstep", indexSelect);
  }

  onClickCerrarSesion() {
    this._SolicitudService.finaliza();
    this.route.navigate(["/login"]);
  }
}
