import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { FileItem } from "../../models/file-item";
import { renderImage } from "src/app/tools";

@Component({
  selector: "app-modal-prew",
  templateUrl: "./modal-prew.component.html",
  styleUrls: ["./modal-prew.component.css"]
})
export class ModalPrewComponent implements OnInit {
  @Input() archivo: FileItem;
  @Input() show: boolean = false;

  @Output() close: EventEmitter<boolean>;
  constructor() {
    this.close = new EventEmitter();
  }

  ngOnInit() {}

  render(base64: string): string {
    return renderImage(base64);
  }

  closeD() {
    this.show = false;
    this.close.emit(this.show);
  }
}
