import {
  Directive,
  Input,
  Output,
  EventEmitter,
  HostListener
} from "@angular/core";
import { FileItem } from "../models/file-item";

@Directive({
  selector: "[appNgDropFiles]"
})
export class NgDropFilesDirective {
  @Input() archivos: FileItem[] = [];
  @Input() useFormControls: boolean;
  @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter();
  @Output() archivoNuevo: EventEmitter<FileItem> = new EventEmitter();
  constructor() {}

  @HostListener("dragover", ["$event"])
  public onDragEnter(event: any) {
    this.mouseSobre.emit(true);
    this._prevenirDetener(event);
  }

  @HostListener("dragleave", ["$event"])
  public onDrangLeave(event: any) {
    this.mouseSobre.emit(false);
  }

  @HostListener("drop", ["$event"])
  public onDrop(event: any) {
    const transferencia = this._getTransferencia(event);

    if (!transferencia) {
      return;
    }

    this._extraerArchivos(transferencia.files);
    this._prevenirDetener(event);
    this.mouseSobre.emit(false);
  }

  private _getTransferencia(event: any) {
    return event.dataTransfer
      ? event.dataTransfer
      : event.originalEvent.dataTransfer;
  }

  private _extraerArchivos(archivosLista: FileItem) {
    console.log(archivosLista);

    for (const propiedad in Object.getOwnPropertyNames(archivosLista)) {
      const archivoTemporal = archivosLista[propiedad];

      if (this.archivoPuedeSerAgregado(archivoTemporal)) {
        let reader = new FileReader();

        reader.readAsDataURL(archivosLista[propiedad]);

        reader.onload = (event: any) => {
          //23

          let base64 = (<FileReader>event.target).result;

          const nuevoArchivo = new FileItem(archivoTemporal, base64.toString());
          if (!this.useFormControls) {
            this.archivos.unshift(nuevoArchivo);
          }
          this.archivoNuevo.emit(nuevoArchivo);
          console.log("nuevo", nuevoArchivo.base64);
        };

        /*   const nuevoArchivo = new FileItem(archivoTemporal);
        this.archivos.push(nuevoArchivo); */
      }
    }
    console.log(this.archivos);
  }

  private archivoPuedeSerAgregado(archivo: File) {
    if (
      !this._archivoYaFueDropeado(archivo.name) &&
      this._esImagen(archivo.type)
    ) {
      return true;
    } else {
      return false;
    }
  }

  private _prevenirDetener(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  private _archivoYaFueDropeado(nombreArchivo: string): boolean {
    for (const archivo of this.archivos) {
      if (archivo.nombreArchivo === nombreArchivo) {
        console.log("El Archivo " + nombreArchivo + " ya fue agregado");
        return true;
      }
    }
    return false;
  }

  private _esImagen(tipoArchivo: string): boolean {
    return tipoArchivo === "" || tipoArchivo === undefined
      ? false
      : tipoArchivo.startsWith("image");
  }
}
