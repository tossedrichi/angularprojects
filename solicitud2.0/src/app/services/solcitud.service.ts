import { Injectable, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SolicitudCompletaModel } from "../models/SolicitudCompleta.model";
import { isUndefined, isNullOrUndefined } from 'util';
import { map, timeout } from "rxjs/operators";
import { EstadosModel } from "../models/Estados.model";
import { CiudadesModel } from "../models/Ciudades.model";
import { ColoniasModel } from "../models/Colonias.model";
import { TiposDomiciliosModel } from "../models/TiposDomicilio.model";
import { TiposDocumentosModel } from "../models/TiposDocumentos.model";
import { RelacionCasaFamiliarModel } from "../models/RelacionCasaFamiliar.model";
import { EmpleosModel } from "../models/Empleos.model";
import { TiposContactoModel } from "../models/TiposContacto.model";
import { TiposClienteModel } from "../models/TiposCliente.model";
import { CondicionesModel } from "../models/CondicionesDomicilio.model";
import { TiposComprobanteDomicilioModel } from "../models/TiposComprobanteDomicilio.model";
import { TiposComprobanteIngresosModel } from "../models/TiposComprobanteIngresos.model";
import { EdoCivilModel } from "../models/EdoCivil.model";
import { TiposRelacionesModel } from "../models/TiposRelaciones";
import { ClienteModel } from "../models/Cliente.model";
import { OtrosIngresosModel } from "../models/OtrosIngresos.model";
import { ZonasModel } from "../models/Zonas.model";
import { PerfilClienteModel } from "../models/PerfilCliente.model";
import { SolicitudModel } from "../models/Solicitud.model";
import { NuevaSolicitudModel } from "../models/NuevaSolicitud.model";
import { TiposComprobantePropiedadModel } from "../models/TiposComprobantePropiedad.model";
import { INEModel } from "../models/INE.model";
import { INERequestModel } from "../models/models-request/INEReques.model";
import { DomicilioClienteRequestModel } from "../models/models-request/DomicilioClienteRequest.model";
import { stringify } from "querystring";
import { PedidoModel } from "../models/Pedido.model";
import { TipoContactoIngresosModel } from "../models/TiposContactoIngreso.model";
import { DatosContactoRequestModel } from "../models/models-request/DatosContactoRequest.model";
import { ConyugeRequestModel } from "../models/models-request/ConyugeRequest.mosel";
import { EmpleoRequestModel } from "../models/models-request/EmpleoRequest.model";
import { TelefonoRequestModel } from "../models/models-request/TelefonoRequest.model";
import { DocumentoRequestModel } from "../models/models-request/DocumentoRequest.model";
import { SucursalModel } from "../models/Sucursal.model";
import { ClienteModelRequest } from "../models/models-request/ClienteRequest.model";
import { DatosContactoModel } from "../models/DatosContactoModel";
import { PedidoRequestModel } from "../models/models-request/PedidoRequest.model";
import { ProcesadoModel } from '../models/Procesado.model';
import { ContactoModel } from '../models/Contacto.model';
import { TiposEmpleoInformalModel } from '../models/TiposEmpleoInformal.model';
import { RequiereAvalRequestModel } from '../models/models-request/RequiereAvalRequest.model';

const BASE_URL = "http://microstest.gruporoga.com:2021/rogaservices/credit/";
const BASE_URL1 = "http://microstest.gruporoga.com:2021/rogaservices/";

const headers = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

const TIMEOUT: number = 60000;

@Injectable({
  providedIn: "root"
})
export class SolcitudService {
  //progress barr
  cargandSolicitud: boolean = false;
  isFullLoading: boolean = true;

  DatosPersonalesCliente = "Datos Personales Cliente";
  DatosConyugeCliente = "Datos Cónyuge Cliente";
  DomicilioCliente = "Domicilio Cliente";
  DatosEmpleoCliente = "Datos Empleo Cliente";
  DatosEmpleoConyugeCliente = "Datos Empleo Cónyuge Cliente";
  DatosAval = "Datos Aval";
  DatosConyugeAval = "Datos Cónyuge Aval";
  Documentos = "Documentos";
  AgregarPedido = "Agregar Pedido";
  Referencias = "Referencias";
  IngresoMensual = "Ingresos Familiares";
  DomicilioAval = "Domicilio Aval";
  FinalizarSolicitud = "Finalizar Solicitud";
  Finalizar = "Finalizar";
  DatosEmpleoAval = "Datos Empleo Aval"

  cliente: ClienteModel = new ClienteModel();
  solicitud: SolicitudModel = new SolicitudModel();
  solicitudCompleta: SolicitudCompletaModel = new SolicitudCompletaModel();

  reqDatosConyugeCliente: boolean = false;
  //reqDatosEmpleoCliente: boolean = false;
  //reqDatosEmpleoConyugeCliente: boolean = false;
  //reqDatosAval: boolean = false;
  reqDatosConyugeAval: boolean = false;
  //reqDatosEmpleoAval: boolean = false;

  showBusqueda: boolean = true;

  public notificaSideNav = new EventEmitter<any>();
   

  idOrigen = -1;
  static agente:number ;
  constructor(private http: HttpClient) {}
  finaliza(){
    this.showBusqueda= true;
    console.log("entra")
/*     this.cliente= new ClienteModel();
    this.solicitud= null;
    this.solicitudCompleta= new SolicitudCompletaModel();
     */
    console.log("sale")
  }
  requiereEmpleoConyuge(){
    if(this.requiereAval()){
      if(this.getDatosContacto(3).infoEmpleosContacto.length>0 || this.requiereEmpleoAval){
        return true;
      }else{
        return false;
      }
    }else{
      return false
    }
  }
  requiereEmpleoAval(){
    if(this.requiereAval()){
      if(this.getDatosContacto(3).infoEmpleosContacto.length>0 ){
        return true;
      }else{
        return false;
      }
    }else{
      return false
    }
  }
  requiereAval():boolean{
   if( this.solicitudCompleta.requiereAval|| !isNullOrUndefined(this.getDatosContacto(3).contacto)//||this.reqDatosAval
   ){
    return true;
   }else{
     return false;
   }
  }
  reqDatosEmpleoClienteF(){
    //if(this.reqDatosEmpleoCliente|| this.solicitudCompleta.infoCliente.empleosCliente.length>0){
    if(this.solicitudCompleta.infoCliente.empleosCliente.length>0){
      return true;
    }else{
      return false;
    }
  }

  reqDatosEmpleoConyugeCliente():boolean{
    if(!isNullOrUndefined(this.solicitudCompleta.infoCliente.conyugeCliente)){
      if(this.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.length>0){
        return true;
      }else{
        return false;
      }

    }else{
      return false;
    }
    
  }

  requiereDatosConyugeAval() {
    if(this.reqDatosConyugeAval){
      return true;
    }
    if (!isNullOrUndefined(this.getDatosContacto(3).contacto)) {
    
      if((!isNullOrUndefined(this.getDatosContacto(3).conyugeContacto))||this.getDatosContacto(3).contacto.idTipoEstadoCivil==1||this.getDatosContacto(3).contacto.idTipoEstadoCivil==3){
        return true;
      }else{
        return false;
      }
     
    } else {
      return false;
    }
  }
  getEstadoCivil() {
    const path = `${BASE_URL}catalogos/TiposEstadoCivil`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EdoCivilModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getEstados() {
    const path = `${BASE_URL}catalogos/ConsultaEstados`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EstadosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getCiudades(estado: number) {
    const path = `${BASE_URL}catalogos/ConsultaCiudades/?estado=${estado}`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <CiudadesModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getColonias(ciudad: number) {
    const path = `${BASE_URL}catalogos/ConsultaColonias/?ciudad=${ciudad}`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <ColoniasModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getTiposDomicilios() {
    const path = `${BASE_URL}catalogos/TiposDomicilio`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposDomiciliosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getTiposDocumentos() {
    const path = `${BASE_URL}catalogos/TiposDocumentos`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposDocumentosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getTiposRelacionesCasaFam() {
    const path = `${BASE_URL}catalogos/TiposRelacionesCasaFam`;
    return this.http.get(path, headers).pipe(
      map(response => {
        return <RelacionCasaFamiliarModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getEmpleos() {
    const path = `${BASE_URL}catalogos/TiposEmpleo`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <EmpleosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getTiposContacto() {
    const path = `${BASE_URL}catalogos/TiposContacto`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposContactoModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getTiposCliente() {
    const path = `${BASE_URL}catalogos/TiposCliente`;
    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposClienteModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getCondiciones() {
    const path = `${BASE_URL}catalogos/TiposCondicionDomicilio`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <CondicionesModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getTiposComprobanteDomiclio() {
    const path = `${BASE_URL}catalogos/TiposComprobantesDomicilio`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposComprobanteDomicilioModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getComprobanteIngresos() {
    const path = `${BASE_URL}catalogos/TiposComprobantesIngresos`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposComprobanteIngresosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getComprobantePropiedad() {
    const path = `${BASE_URL}catalogos/TiposComprobantesPropiedad`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposComprobantePropiedadModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getTiposRelaciones() {
    const path = `${BASE_URL}catalogos/TiposRelaciones`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposRelacionesModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getTiposContactoIngresos() {
    const path = `${BASE_URL}catalogos/TiposContactoIngresos`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TipoContactoIngresosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getListaClientes(nom: string, ap1: string, ap2: string, cedis: number) {
    const path = `${BASE_URL1}customers/buscaClientes?nom=${nom}&ap1=${ap1}&ap2=${ap2}&cedis=${cedis}`;

    return this.http.post(path, headers).pipe(
      map(response => {
        return <ClienteModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  setCliente(cliente: ClienteModel) {
    this.cliente = cliente;
  }

  getOtrosIngresos() {
    const path = `${BASE_URL}catalogos/TiposIngresos`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <OtrosIngresosModel[]>response;
      }, timeout(TIMEOUT))
    );
  }
  getTipoEmpleoInformal() {
    const path = `${BASE_URL}catalogos/TiposEmpleoInformal`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <TiposEmpleoInformalModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getPerfilCreditoCliente(idCliente: string) {
    const path = `${BASE_URL1}creditreport/ConsultaPerfilCreditoCliente?idCliente=${idCliente}`;
    return this.http.get(path).pipe(
      map(response => {
        console.log("Perfil Cliente", response);

        return <PerfilClienteModel>response;
      }, timeout(TIMEOUT))
    );
  }

  getSolicitudCompleta(idSolicitud: number) {
    SolcitudService.agente = parseInt(localStorage.getItem("user"));
    this.solicitudCompleta = new SolicitudCompletaModel();
    const path = `${BASE_URL1}credit/solicitudes/consultaSolicitud?idSolicitud=${idSolicitud}`;
    return this.http.get(path).pipe(
      map(response => {
        this.solicitudCompleta = <SolicitudCompletaModel>response;
        return this.solicitudCompleta;
      }, timeout(TIMEOUT))
    );
  }

  setSolicitud(solicitud: SolicitudModel) {
    this.solicitud = solicitud;
  }
  getZonas() {
    const path = `${BASE_URL1}auditoriaoperativa/getZonas`;

    return this.http.get(path, headers).pipe(
      map(response => {
        return <ZonasModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  getTelefonoClienteFijo(): string {
    let telefono = "";
    this.solicitudCompleta.infoCliente.telefonosCliente.forEach(tel => {
      if (tel.estadoActual.includes("Activo") && tel.idTipoTelefono == 1) {
        telefono = tel.numeroTelefonico;
      }
    });
    return telefono;
  }

  getTelefonoClienteCelular(): string {
    let telefono = "";
    this.solicitudCompleta.infoCliente.telefonosCliente.forEach(tel => {
      if (tel.estadoActual.includes("Activo") && tel.idTipoTelefono == 2) {
        telefono = tel.numeroTelefonico;
      }
    });
    return telefono;
  }

  getINEFrenteCliente(): string {
    if (this.solicitudCompleta.infoCliente.ine != undefined) {
      let base64 = this.solicitudCompleta.infoCliente.ine.streamFrente;

      if (!isUndefined(base64)) {
        if (!isUndefined(base64.includes(""))) {
          if (
            base64.includes("data:image/jpeg;base64,") ||
            base64.includes("data:image/png;base64,")
          ) {
            return base64;
          } else {
            return `data:image/jpeg;base64,${base64}`;
          }
        }
      }
    }
  }

  getINEReversoCliente(): string {
    if (this.solicitudCompleta.infoCliente.ine != undefined) {
      let base64 = this.solicitudCompleta.infoCliente.ine.streamReverso;
      if (!isUndefined(base64)) {
        if (!isUndefined(base64.includes(""))) {
          if (
            base64.includes("data:image/jpeg;base64,") ||
            base64.includes("data:image/png;base64,")
          ) {
            return base64;
          } else {
            return `data:image/jpeg;base64,${base64}`;
          }
        }
      }
    }
  }
  getINEFrenteConyuge(): string {
    let base64 = this.solicitudCompleta.infoCliente.conyugeCliente.infoINE
      .streamFrente;
    if (isUndefined(base64)) {
      return base64;
    } else {
      return `data:image/jpeg;base64,${base64}`;
    }
  }
  getINEReversoConyuge(): string {
    let base64 = this.solicitudCompleta.infoCliente.conyugeCliente.infoINE
      .streamReverso;
    if (isUndefined(base64)) {
      return base64;
    } else {
      return `data:image/jpeg;base64,${base64}`;
    }
  }

  getTelefonoConyugeCelular(): string {
    let telefono = "";
    this.solicitudCompleta.infoCliente.conyugeCliente.telefonosContacto.forEach(
      tel => {
        if (tel.estadoActual.includes("Activo") && tel.idTipoTelefono == 2) {
          telefono = tel.numeroTelefonico;
        }
      }
    );
    return telefono;
  }

  setNuevaSolicitud(
    fecharegistro: string,
    idCliente: number,
    idSucursal: number
  ) {
    const PATH = `${BASE_URL1}redigitalizacion/agregaSolicitudRedigitalizacion`;
    let nuevaSolcitud: NuevaSolicitudModel = new NuevaSolicitudModel();
    let date: Date = new Date();
    let fechaActual: string =
      date.getFullYear().toString() +
      "-" +
      (date.getMonth() + 1).toString() +
      "-" +
      date.getDate().toString();
    nuevaSolcitud.comentariosVentas = "";
    nuevaSolcitud.ingresoFamiliar = 0;
    nuevaSolcitud.numeroDependientes = 0;
    nuevaSolcitud.numeroPersonasDomicilio = 0;
    nuevaSolcitud.fechaActualizacion = fechaActual;
    nuevaSolcitud.fechaCierre = fechaActual;

    nuevaSolcitud.idCliente = idCliente;
    nuevaSolcitud.fechaRegistro = fecharegistro;
    nuevaSolcitud.idSucursal = idSucursal;
    
    SolcitudService.agente=parseInt(localStorage.getItem("user"));
    nuevaSolcitud.agente = SolcitudService.agente;
    nuevaSolcitud.idOrigen = this.idOrigen;
    console.log(nuevaSolcitud);

    return this.http.post(PATH, JSON.stringify(nuevaSolcitud), headers).pipe(
      map(response => {
        return <NuevaSolicitudModel>response;
      }, timeout(TIMEOUT))
    );
  }

  guardarDatosCliente(cliente: ClienteModelRequest) {
    console.log(cliente);
    cliente.idOrigen = this.idOrigen;

    const path = `${BASE_URL1}redigitalizacion/actualizaDatosClientes`;

    console.log("enviado");
    console.log(cliente);
    return this.http.post(path, JSON.stringify(cliente), headers).pipe(
      map(response => {
        return <ClienteModel>response;
      }, timeout(TIMEOUT))
    );
  }
  /*   guardarDatosConyuge(conyugeCl: ConyugeRequestModel) {
    console.log(conyugeCl);
    conyugeCl.idOrigen=this.idOrigen;

    const path = `${BASE_URL1}redigitalizacion/actualizaDatosContacto`;

    console.log("enviado");
    console.log(conyugeCl);
    return this.http.post(path,JSON.stringify(conyugeCl), headers).pipe(
      map(response => {
        return <ClienteModel>response;
      }, timeout(TIMEOUT))
    );
  } */

  setIneCliente(ineCliente: INERequestModel) {
    const PATH = `${BASE_URL1}redigitalizacion/agregaCredencialClienteSolicitud`;
    ineCliente.idOrigen = this.idOrigen;
    ineCliente.agente = SolcitudService.agente;
    console.log(ineCliente);

    return this.http.post(PATH, JSON.stringify(ineCliente), headers).pipe(
      map(response => {
        return <INERequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  setAgregaDomicilioClienteSolicitud(
    domicilioRequest: DomicilioClienteRequestModel
  ) {
    domicilioRequest.idOrigen = this.idOrigen;
    domicilioRequest.agente = SolcitudService.agente;

    const PATH = `${BASE_URL1}/redigitalizacion/agregaDomicilioClienteSolicitud`;

    return this.http.post(PATH, JSON.stringify(domicilioRequest), headers).pipe(
      map(response => {
        return <DomicilioClienteRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  getPedidosAnteriores(idCliente: number) {
    const PATH = `${BASE_URL1}redigitalizacion/consultaPedidosClienteSolicitudAnterior/idCliente=${idCliente}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <PedidoModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaContactoClientSolicitud(contacto: DatosContactoRequestModel) {
    const PATH = `${BASE_URL1}redigitalizacion/agregaContactoClienteSolicitud`;

    contacto.agente = SolcitudService.agente;
    contacto.idOrigen = this.idOrigen;
    return this.http.post(PATH, JSON.stringify(contacto), headers).pipe(
      map(response => {
        return <DatosContactoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaConyugeSolicitud(conyuge: ConyugeRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaConyugeSolicitud`;
    return this.http.post(PATH, JSON.stringify(conyuge), headers).pipe(
      map(response => {
        return <ContactoModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaCredencialContactoSolicitud(ine: INERequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaCredencialContactoSolicitud`;
    ine.idOrigen = this.idOrigen;
    ine.agente = SolcitudService.agente;

    return this.http.post(PATH, JSON.stringify(ine), headers).pipe(
      map(response => {
        return <INERequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaDomicilioContactoSolicitud(
    domicilioRequest: DomicilioClienteRequestModel
  ) {
    domicilioRequest.idOrigen = this.idOrigen;
    domicilioRequest.agente = SolcitudService.agente;

    const PATH = `${BASE_URL1}/redigitalizacion/agregaDomicilioContactoSolicitud`;

    return this.http.post(PATH, JSON.stringify(domicilioRequest), headers).pipe(
      map(response => {
        return <DomicilioClienteRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaDomicilioEmpleoClienteSolicitud(
    domicilioRequest: DomicilioClienteRequestModel
  ) {
    domicilioRequest.idOrigen = this.idOrigen;
    domicilioRequest.agente = SolcitudService.agente;

    const PATH = `${BASE_URL1}/redigitalizacion/agregaDomicilioEmpleoClienteSolicitud`;

    return this.http.post(PATH, JSON.stringify(domicilioRequest), headers).pipe(
      map(response => {
        return <DomicilioClienteRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaDomicilioEmpleoContactoSolicitud(
    domicilioRequest: DomicilioClienteRequestModel
  ) {
    domicilioRequest.idOrigen = this.idOrigen;
    domicilioRequest.agente =SolcitudService.agente;

    const PATH = `${BASE_URL1}/redigitalizacion/agregaDomicilioEmpleoContactoSolicitud`;

    return this.http.post(PATH, JSON.stringify(domicilioRequest), headers).pipe(
      map(response => {
        return <DomicilioClienteRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaEmpleoClienteSolicitud(empleo: EmpleoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaEmpleoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(empleo), headers).pipe(
      map(response => {
        return <EmpleoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }
    actualizaEmpleoClienteSolicitud(empleo: EmpleoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/actualizaEmpleoCliente`;
    return this.http.post(PATH, JSON.stringify(empleo), headers).pipe(
      map(response => {
        return <ProcesadoModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaEmpleoContactoClienteSolicitud(empleo: EmpleoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaEmpleoContactoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(empleo), headers).pipe(
      map(response => {
        return <EmpleoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  actualizaEmpleoContactoClienteSolicitud(empleo: EmpleoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/actualizaEmpleoContacto`;
    return this.http.post(PATH, JSON.stringify(empleo), headers).pipe(
      map(response => {
        return <ProcesadoModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaDocumentoSolicitud(documento: DocumentoRequestModel) {
    const PATH = `${BASE_URL1}redigitalizacion/agregaDocumentoSolicitud`;

    documento.idOrigen = this.idOrigen;
    documento.agente = SolcitudService.agente;

    console.log("Documeto Enviado", documento);

    return this.http.post(PATH, JSON.stringify(documento), headers).pipe(
      map(response => {
        return <DocumentoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }
  agregaPagareClienteSolicitud(documento: DocumentoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaPagareClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(documento), headers).pipe(
      map(response => {
        return <DocumentoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }
  agregaPagareAvalClienteSolicitud(documento: DocumentoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaPagareAvalClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(documento), headers).pipe(
      map(response => {
        return <DocumentoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaPedidoSolicitud(idSolicitud, idPedido) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaPedidoSolicitud/idSolicitud=${idSolicitud},idPedido=${idPedido},idOrigen=${this.idOrigen},agente=${SolcitudService.agente}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  agregaTelefonoClienteSolicitud(telefono: TelefonoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaTelefonoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(telefono), headers).pipe(
      map(response => {
        return <TelefonoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaTelefonoContactoClienteSolicitud(telefono: TelefonoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaTelefonoContactoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(telefono), headers).pipe(
      map(response => {
        return <TelefonoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaTelefonoEmpleoClienteSolicitud(telefono: TelefonoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaTelefonoEmpleoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(telefono), headers).pipe(
      map(response => {
        return <TelefonoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  agregaTelefonoEmpleoContactoClienteSolicitud(telefono: TelefonoRequestModel) {
    const PATH = `${BASE_URL1}/redigitalizacion/agregaTelefonoEmpleoContactoClienteSolicitud`;
    return this.http.post(PATH, JSON.stringify(telefono), headers).pipe(
      map(response => {
        return <TelefonoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  getSucursalesCedis(cedis: number) {
    const PATH = `${BASE_URL1}/redigitalizacion/getSucursalesCedis/cedis=${cedis}`;
    return this.http.get(PATH, headers).pipe(
      map(response => {
        return <SucursalModel[]>response;
      }, timeout(TIMEOUT))
    );
  }

  actualizaContactoClientSolicitud(contacto: DatosContactoRequestModel) {
    const PATH = `${BASE_URL1}redigitalizacion/actualizaContactoClienteSolicitud`;

    contacto.agente = SolcitudService.agente;
    contacto.idOrigen = this.idOrigen;
    return this.http.post(PATH, JSON.stringify(contacto), headers).pipe(
      map(response => {
        return <DatosContactoRequestModel>response;
      }, timeout(TIMEOUT))
    );
  }

  getDatosContacto(tipoContacto: number): DatosContactoModel {
    let conyugeAval: DatosContactoModel = new DatosContactoModel();
    if (this.solicitudCompleta.infoCliente.contactosCliente != undefined) {
      this.solicitudCompleta.infoCliente.contactosCliente.forEach(c => {
        if (c.contacto.idTipoContacto == tipoContacto) {
          conyugeAval = c;
        }
      });
    }
    return conyugeAval;
  }
  getContactos(idTipo: number) {
    return this.solicitudCompleta.infoCliente.contactosCliente.filter(
      x => x.contacto.idTipoContacto == idTipo
    );
  }

  eliminaRelacionPedidoSolicitud(idPedido: number) {
    const PATH = `${BASE_URL1}redigitalizacion/eliminaRelacionPedidoSolicitud`;
    let p: PedidoRequestModel = new PedidoRequestModel();
    p.idSolicitud = this.solicitudCompleta.idSolicitud;
    p.idPedido = idPedido;
    p.idOrigen = this.idOrigen;
    p.agente = SolcitudService.agente;
    return this.http.post(PATH, JSON.stringify(p), headers).pipe(
      map(response => {
        return response;
      }, timeout(TIMEOUT))
    );
  }

  eliminarContacto(idContacto: number) {
    const PATH = `${BASE_URL1}redigitalizacion/eliminaContactoSolicitud`;

    let body = `{
      "idContacto":${idContacto},
      "idSolicitud":${this.solicitud.idSolicitud},
      "idOrigen":${this.idOrigen},
      "agente":${SolcitudService.agente},
      "idInvestigacion":-1
    }`;

    return this.http.post(PATH, body, headers).pipe(
      map(resp => {
        return resp;
      }, timeout(TIMEOUT))
    );
  }

  setEmliminarEmpleoSolcitud(
    idEmpleo: number,
    idCliente: number,
    idContacto: number
  ) {
    const PATH = `${BASE_URL1}redigitalizacion/eliminaEmpleoSolicitud`;

    let empleo: EmpleoRequestModel = new EmpleoRequestModel();
    empleo.idEmpleo = idEmpleo;
    empleo.idSolicitud = this.solicitud.idSolicitud;
    empleo.idCliente = idCliente;
    empleo.idContacto = idContacto;
    empleo.idOrigen = this.idOrigen;
    empleo.agente = SolcitudService.agente;

    return this.http.post(PATH, JSON.stringify(empleo), headers).pipe(
      map(resp => {
        return resp;
      }),
      timeout(TIMEOUT)
    );
  }

  setEliminarDocumentoSolicitud(idDocumento: number) {
    const PATH = `${BASE_URL1}redigitalizacion/eliminaDocumentoSolicitud`;

    let documento: DocumentoRequestModel = new DocumentoRequestModel();
    documento.idDocumento = idDocumento;
    documento.idSolicitud = this.solicitud.idSolicitud;
    documento.idOrigen = this.idOrigen;
    documento.agente = SolcitudService.agente;
    console.log("Documento a eliminar", documento);
    return this.http.post(PATH, JSON.stringify(documento), headers).pipe(
      map(resp => {
        return resp;
      }),
      timeout(TIMEOUT)
    );
  }

  finalizaSolicitud() {
    const PATH = `${BASE_URL1}redigitalizacion/finalizaSolicitudRedigitalizacion`;
    let nuevaSolcitud: NuevaSolicitudModel = new NuevaSolicitudModel();

    nuevaSolcitud.idSolicitud = this.solicitudCompleta.idSolicitud;
    nuevaSolcitud.ingresoFamiliar = this.getIngresosFamiliares();
    nuevaSolcitud.numeroDependientes = 0;
    try {
       nuevaSolcitud.numeroPersonasDomicilio = this.solicitudCompleta.infoCliente.domicilioCliente.domicilio.habitantesDomicilio;

    } catch (error) {
      nuevaSolcitud.numeroPersonasDomicilio =0;
    }
   


    nuevaSolcitud.agente = SolcitudService.agente;
    nuevaSolcitud.idOrigen = this.idOrigen;
    console.log(nuevaSolcitud);

    return this.http.post(PATH, JSON.stringify(nuevaSolcitud), headers).pipe(
      map(response => {
        return <ProcesadoModel>response;
      }, timeout(TIMEOUT))
    );
  }

  getIngresosFamiliares() {
    let ingresos: number = 0;
    //suma ingrsosFamiliares
    this.solicitudCompleta.infoCliente.contactosCliente.forEach(x => {
      if (
        x.contacto.idTipoContacto == 2 &&
        x.contacto.idTipoRelacionContacto == 5
      )
        ingresos += x.contacto.importeIngresos;
    });
    //suma empleosCliente
    this.solicitudCompleta.infoCliente.empleosCliente.forEach(x => {
      ingresos += x.empleo.ingresoMensual;
    });
    //ingreso COnyuge
    if(!isUndefined(this.solicitudCompleta.infoCliente.conyugeCliente)&& this.solicitudCompleta.infoCliente.conyugeCliente!=null){
      
          this.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.forEach(x=>{
        ingresos+=x.empleo.ingresoMensual;
      })
      
    
    }

    return ingresos;
  }

  cambiaRequierAvalSolicitud(reqAval:boolean){

    let req:RequiereAvalRequestModel= new RequiereAvalRequestModel();
    req.idSolicitud=this.solicitudCompleta.idSolicitud;
    req.idOrigen=this.idOrigen;
    req.agente=SolcitudService.agente;
    req.requiereAval= reqAval;
    const PATH = `${BASE_URL1}redigitalizacion/cambiaRequiereAvalSolicitud`;

    return this.http.post(PATH, JSON.stringify(req), headers).pipe(
      map(response => {
        return <ProcesadoModel>response;
      }, timeout(TIMEOUT))
    );

  }

  getEstadoCiudad(idCiudad:number){
    const PATH = `${BASE_URL1}redigitalizacion/regresaEstadoPorCiudad/idCiudad=${idCiudad}`
    
    return this.http.get(PATH,headers).pipe(map(resp=>{
      return <ProcesadoModel>resp
    },timeout(TIMEOUT)))
  }

}
