import { Injectable } from '@angular/core';
import { LoginInterface } from '../../interfaces/login.interface';
import * as crypto from "crypto-js";
import {Router}from"@angular/router"
import {map} from "rxjs/operators";
import { HttpHeaders } from '@angular/common/http';
import { HttpClient} from '@angular/common/http'
import { SolcitudService } from './solcitud.service';


const urlLogin =
  "http://microstest.gruporoga.com:2021/rogaservices/security/token/login";

const URL_BASE_PRUEBAS =
  "http://microstest.gruporoga.com:2021/rogaservices/redigitalizacion/";

const headers = {
  headers: new HttpHeaders({
     "Content-Type": "application/json"
  })
};
export const keyCryp = "kriptoAce";


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) {
    console.log("login")
  }
  setLogin(user:string,password:string):any{
    const bodyParams=`{
                "userId":"${user}",
                "userPass":"${password}",
                "userProfile":"1",
                "macAddress":"00-14-22-01-23-45",
                "appId":"1"
    }`;
    return this.http.post(urlLogin,bodyParams,headers).pipe(
      map(response =>{
        return response;
      })
    );

  }
  cryptoJS(user:string){
    let cryp= crypto.AES.encrypt(user,"Key");
    sessionStorage.setItem("userC", cryp.toString());
    console.log("cryp",cryp.toString());
    let decryp= crypto.AES.decrypt(cryp,"Key");
    console.log("decrypt",decryp.toString(crypto.enc.utf8));
  }

  public setSession(autResult:LoginInterface):void{
    var iv = crypto.enc.Base64.parse("#base64IV#");
    sessionStorage.setItem("token",autResult.token);
    sessionStorage.setItem("sessionActive","true");
    sessionStorage.setItem("userName",autResult.nombreUsuario);
    sessionStorage.setItem("expireDate",autResult.expireDate);
    localStorage.setItem("user",autResult.userid.toString());
    SolcitudService.agente = autResult.userid;
  }

  public logout():void{
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("sessionActive");
    sessionStorage.removeItem("expireDate");
    sessionStorage.clear
    localStorage.clear
    this.router.navigate(["/"])
  }

  public isAuthenticated():boolean{
    const expiresAt =JSON.parse(sessionStorage.getItem("expireDate"));
    return new Date().getTime()<expiresAt;
  }

  public getProfile(){
    const accessToken = sessionStorage.getItem("token");
    if(!accessToken){
      throw new Error("Access token must exist to fetch profile");
    }else{
    }

  }
}
