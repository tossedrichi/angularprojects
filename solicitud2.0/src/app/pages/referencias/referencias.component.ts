import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked
} from "@angular/core";
import { TiposRelacionesModel } from "src/app/models/TiposRelaciones";
import { SolcitudService } from "src/app/services/solcitud.service";
import { DatosContactoRequestModel } from "../../models/models-request/DatosContactoRequest.model";
import { TelefonoRequestModel } from "../../models/models-request/TelefonoRequest.model";
import { DataInputModalGuardarModel } from "src/app/components/modal-guardar/dataInputModa.model";
import { ContactoModel } from "../../models/Contacto.model";
import { DatosContactoModel } from "../../models/DatosContactoModel";
import { ToastrService } from "ngx-toastr";
import { DomicilioClienteModel } from "src/app/models/DomicilioClienteModel";
import { DomicilioModel } from "../../models/Domicilio.Model";

@Component({
  selector: "app-referencias",
  templateUrl: "./referencias.component.html",
  styleUrls: ["./referencias.component.css"]
})
export class ReferenciasComponent implements OnInit, AfterViewChecked {
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("edtTelefonoFijo") edtTelefonoFijo: ElementRef;
  @ViewChild("edtTelefonoCel") edtTelefonoCel: ElementRef;
  @ViewChild("selecParentesco") selecParentesco: ElementRef;

  strBotonGuardar: string = "Guardar";
  contacto: DatosContactoModel;
  openModalGuardar: boolean = false;
  openModalDomicilioRef: boolean = false;
  ingresoMensual: boolean = false;
  botones: boolean = true;
  dataInputSave: DataInputModalGuardarModel;
  contactoSelec: ContactoModel;
  listTipoRelaciones: TiposRelacionesModel[] = [];
  listContactos: DatosContactoModel[] = [];
  esEdicion: boolean = false;
  i = 0;
  referenciaSeleccionada: DatosContactoModel;
  dataInputModalDomicilio: {
    idReferencia: number;
    domicilioRef: DomicilioModel;
  };
  //openModal: ModalDomicilioRefComponent ;

  instance: number;

  constructor(
    private _SolicitudSrevice: SolcitudService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.getTiposRelaciones();
    this.listarReferencias();
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
  }

  listarReferencias() {
    let contactosCliente: DatosContactoModel[] = this._SolicitudSrevice
      .solicitudCompleta.infoCliente.contactosCliente;

    this.listContactos = contactosCliente
      .filter(x => x.contacto.idTipoContacto == 1)
      .filter(
        x =>
          x.contacto.idContacto !=
          this._SolicitudSrevice.solicitudCompleta.infoCliente.domicilioCliente
            .domicilio.propietarioDomicilio
      );
  }

  ingreso(tipo: string) {
    console.log(tipo);

    if (tipo == "Ingreso Familiar") {
      this.ingresoMensual = true;
      this.botones = false;
    } else {
      this.ingresoMensual = false;
      this.botones = true;
    }
  }

  getTiposRelaciones() {
    this._SolicitudSrevice.getTiposRelaciones().subscribe(response => {
      this.listTipoRelaciones = response;
    });
  }
  validarDatos(): boolean {
    let pasa: boolean = true;
    let c: ContactoModel = new ContactoModel();
    c.nombres = this.edtNombre.nativeElement.value;
    c.primerApellido = this.edtApellidoP.nativeElement.value;
    c.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    if (
      !(c.nombres.length > 0) ||
      !(c.primerApellido.length > 0) ||
      c.idTipoRelacionContacto < 1
    ) {
      console.log("nombres mal");
      pasa = false;
    }
    let fijo: string = this.edtTelefonoFijo.nativeElement.value;
    let cel: string = this.edtTelefonoCel.nativeElement.value;
    if (cel.localeCompare("") == 0 && fijo.localeCompare("") == 0) {
      pasa = false;
      console.log("tlefonos mal");
    }
    console.log(c);
    console.log(cel + " " + fijo);
    return pasa;
  }
  resposeDataDomiclioRef(event) {
    this.referenciaSeleccionada = null;
    console.log(event);
    this.openModalDomicilioRef = event.open;
    this.listarReferencias();
    if (event.isCancel) {
    }
  }

  onSaveClick() {
    if (this.validarDatos()) {
      this.instance = 1;
      this.dataInputSave = new DataInputModalGuardarModel(
        "Guardar",
        "¿Desea guardar los cambios",
        []
      );
      this.openModalGuardar = true;
    } else {
      this.toaster.error(
        "Verifica que los datos esten completos",
        "error al guardar"
      );
    }
  }

  onClickEliminarContacto(contacto: ContactoModel) {
    this.instance = 2;

    this.dataInputSave = new DataInputModalGuardarModel(
      "Eliminar referencia",
      "¿Desea eliminar la referencia?",
      []
    );
    this.contactoSelec = contacto;
    this.openModalGuardar = true;
  }

  onSaveConfirm(event) {
    this.openModalGuardar = false;

    if (event.save) {
      switch (this.instance) {
        case 1:
          this.setReferencias();
          break;
        case 2:
          this.eliminarContacto(this.contactoSelec.idContacto);
          break;
      }
    }
  }

  eliminarContacto(idContacto: number) {
    console.log("eliminar ", idContacto);
    this._SolicitudSrevice.cargandSolicitud = true;
    this._SolicitudSrevice.eliminarContacto(idContacto).subscribe(
      resp => {
        this._SolicitudSrevice.cargandSolicitud = false;
        console.log(resp);
        this.toaster.success("Referencia eliminada", "Referencias");
        this._SolicitudSrevice.cargandSolicitud = true;
        this._SolicitudSrevice
          .getSolicitudCompleta(this._SolicitudSrevice.solicitud.idSolicitud)
          .subscribe(
            resp => {
              console.log(resp);
              this._SolicitudSrevice.cargandSolicitud = false;
              this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                resp.infoCliente.contactosCliente;
              this.listarReferencias();
            },
            err => {
              this._SolicitudSrevice.cargandSolicitud = false;
              this.toaster.error(
                err.message,
                "Error al traer datos: Referencia"
              );
            }
          );
      },
      err => {
        this._SolicitudSrevice.cargandSolicitud = false;
        console.log(err);
        this.toaster.error(err.message, "Error: Referencias");
      }
    );
  }
  setReferencias() {
    let referencia: DatosContactoRequestModel = new DatosContactoRequestModel();
    this._SolicitudSrevice.cargandSolicitud = false;
    if (this.esEdicion) {
      referencia.idContacto = this.contactoSelec.idContacto;
    }
    this._SolicitudSrevice.cargandSolicitud = true;
    referencia.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    referencia.idTipoContacto = 1;
    referencia.nombres = this.edtNombre.nativeElement.value;
    referencia.primerApellido = this.edtApellidoP.nativeElement.value;
    referencia.segundoApellido = this.edtApellidoM.nativeElement.value;
    referencia.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    referencia.fechaNacimiento = "1900-01-01";
    referencia.genero = "F";
    referencia.aportaIngresos = false;

    if (!this.esEdicion) {
      this._SolicitudSrevice
        .agregaContactoClientSolicitud(referencia)
        .subscribe(
          resp => {
            this._SolicitudSrevice.cargandSolicitud = false;
            console.log(resp);
            this.toaster.success("Guardada correctamente", "Refereniacias");
            if (
              this.getFijo().localeCompare(
                this.edtTelefonoFijo.nativeElement.value
              ) != 0
            ) {
              this.setTelefonoReferencia(resp.idContacto);
            } else {
              if (
                this.getCelular().localeCompare(
                  this.edtTelefonoCel.nativeElement.value
                ) != 0
              ) {
                this.setTelefonoCelularReferencia(resp.idContacto);
              }
            }
          },
          err => {
            this._SolicitudSrevice.cargandSolicitud = false;
            console.log(err);
            this.toaster.error(err.message, "Error al guardar Referecia");
          }
        );
    } else {
      this.setEdicionContacto();
    }
  }
  getCelular(): string {
    let cel: string = "";
    try {
      if (
        this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 2)
          .length > 0
      ) {
        cel = this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 2
        )[
          this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 2)
            .length - 1
        ].numeroTelefonico;
      }
    } catch (error) {}

    return cel;
  }
  getFijo(): string {
    let cel: string = "";
    try {
      if (
        this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 1)
          .length > 0
      ) {
        cel = this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 1
        )[
          this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 1)
            .length - 1
        ].numeroTelefonico;
      }
    } catch (error) {}

    return cel;
  }

  editarContacto(contacto: DatosContactoModel) {
    this.limpiarForm();
    this.contacto = contacto;
    this.contactoSelec = contacto.contacto;
    this.esEdicion = true;
    console.log(contacto);
    this.edtNombre.nativeElement.value = this.contactoSelec.nombres;
    this.edtApellidoP.nativeElement.value = this.contactoSelec.primerApellido;
    this.edtApellidoM.nativeElement.value = this.contactoSelec.segundoApellido;
    this.selecParentesco.nativeElement.value = this.contactoSelec.idTipoRelacionContacto;

    this.edtTelefonoFijo.nativeElement.value = this.getFijo();

    this.edtTelefonoCel.nativeElement.value = this.getCelular();
  }

  limpiarForm() {
    this.esEdicion = false;
    this.edtNombre.nativeElement.value = "";
    this.edtApellidoP.nativeElement.value = "";
    this.edtApellidoM.nativeElement.value = "";
    this.edtTelefonoCel.nativeElement.value = "";
    this.edtTelefonoFijo.nativeElement.value = "";
    this.selecParentesco.nativeElement.value = "-1";
  }

  setEdicionContacto() {
    let datosEdicion: DatosContactoRequestModel = new DatosContactoRequestModel();
    this._SolicitudSrevice.cargandSolicitud = true;
    datosEdicion.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    datosEdicion.idContacto = this.contactoSelec.idContacto;
    datosEdicion.idTipoContacto = 1;
    datosEdicion.nombres = this.edtNombre.nativeElement.value;
    datosEdicion.primerApellido = this.edtApellidoP.nativeElement.value;
    datosEdicion.segundoApellido = this.edtApellidoM.nativeElement.value;
    datosEdicion.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    datosEdicion.fechaNacimiento = "1900-01-01";
    datosEdicion.genero = "F";
    datosEdicion.aportaIngresos = false;
    datosEdicion.importeIngresos = 0;

    this._SolicitudSrevice
      .actualizaContactoClientSolicitud(datosEdicion)
      .subscribe(
        resp => {
          this._SolicitudSrevice.cargandSolicitud = false;
          console.log("Contacto Editado", resp);
          this.toaster.success("Contacto Editado", "Referencia");
          if (
            this.getFijo().localeCompare(
              this.edtTelefonoFijo.nativeElement.value
            ) != 0
          ) {
            this.setTelefonoReferencia(resp.idContacto);
          } else {
            if (
              this.getCelular().localeCompare(
                this.edtTelefonoCel.nativeElement.value
              ) != 0
            ) {
              this.setTelefonoCelularReferencia(resp.idContacto);
            } else {
              this._SolicitudSrevice.cargandSolicitud = true;
              this._SolicitudSrevice
                .getSolicitudCompleta(
                  this._SolicitudSrevice.solicitud.idSolicitud
                )
                .subscribe(
                  resp => {
                    console.log(resp);
                    this._SolicitudSrevice.cargandSolicitud = false;
                    this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                      resp.infoCliente.contactosCliente;
                    this.listarReferencias();
                    this.limpiarForm();
                  },
                  err => {
                    this._SolicitudSrevice.cargandSolicitud = false;
                    this.toaster.error(
                      err.message,
                      "Error al traer datos: Referencia"
                    );
                  }
                );
            }
          }
        },
        err => {
          console.log("Error Edicion Contacto", err);
          this.toaster.error(err.message, "Error al editar: Referencia");
        }
      );
  }

  setTelefonoReferencia(idContacto) {
    let telefonos: TelefonoRequestModel[] = new Array<TelefonoRequestModel>();

    let telefonoFijo: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoFijo.idContacto = idContacto;
    telefonoFijo.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    telefonoFijo.idSucursal = this._SolicitudSrevice.solicitud.idSucursal;
    telefonoFijo.idTipoTelefono = 1;
    telefonoFijo.telefono = this.edtTelefonoFijo.nativeElement.value;
    this._SolicitudSrevice.cargandSolicitud = true;

    this._SolicitudSrevice
      .agregaTelefonoContactoClienteSolicitud(telefonoFijo)
      .subscribe(
        resp => {
          this._SolicitudSrevice.cargandSolicitud = false;
          if (
            this.getCelular().localeCompare(
              this.edtTelefonoCel.nativeElement.value
            ) != 0
          ) {
            this.setTelefonoCelularReferencia(resp.idContacto);
          } else {
            this._SolicitudSrevice.cargandSolicitud = true;
            this._SolicitudSrevice
              .getSolicitudCompleta(
                this._SolicitudSrevice.solicitud.idSolicitud
              )
              .subscribe(
                resp => {
                  console.log(resp);
                  this._SolicitudSrevice.cargandSolicitud = false;
                  this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                    resp.infoCliente.contactosCliente;
                  this.listarReferencias();
                  this.limpiarForm();
                },
                err => {
                  this._SolicitudSrevice.cargandSolicitud = false;
                  this.toaster.error(
                    err.message,
                    "Error al traer datos: Referencia"
                  );
                }
              );
          }
          console.log(resp);
          this.toaster.success("Telefono Guardado", "Referencia");
        },
        err => {
          this.toaster.error(err.message, "Error al guardar telefono");
          console.log(err);
        }
      );
  }

  setTelefonoCelularReferencia(idContacto) {
    let telefonoCel: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoCel.idContacto = idContacto;
    telefonoCel.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    telefonoCel.idSucursal = this._SolicitudSrevice.solicitud.idSucursal;
    telefonoCel.idTipoTelefono = 2;
    telefonoCel.telefono = this.edtTelefonoCel.nativeElement.value;

    this._SolicitudSrevice.cargandSolicitud = true;
    this._SolicitudSrevice
      .agregaTelefonoContactoClienteSolicitud(telefonoCel)
      .subscribe(
        resp => {
          console.log(resp);
          this.toaster.success("Telefono Guardado", "Referencia");
          this._SolicitudSrevice
            .getSolicitudCompleta(this._SolicitudSrevice.solicitud.idSolicitud)
            .subscribe(
              resp => {
                console.log(resp);
                this._SolicitudSrevice.cargandSolicitud = false;
                this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                  resp.infoCliente.contactosCliente;
                this.listarReferencias();
                this.limpiarForm();
              },
              err => {
                this._SolicitudSrevice.cargandSolicitud = false;
                this.toaster.error(
                  err.message,
                  "Error al traer datos: Referencia"
                );
              }
            );
        },
        err => {
          this._SolicitudSrevice.cargandSolicitud = false;
          this.toaster.error(err.message, "Error al guardar telefono");
          console.log(err);
        }
      );
  }

  onClickDomicilioReferencia(referencia: DatosContactoModel) {
    console.log("Referencia Seleccionada", referencia);
    this.referenciaSeleccionada = referencia;
    this.dataInputModalDomicilio = {
      idReferencia: referencia.contacto.idContacto,
      domicilioRef: referencia.domicilioContacto.domicilio
    };
    this.openModalDomicilioRef = true;
  }
}
