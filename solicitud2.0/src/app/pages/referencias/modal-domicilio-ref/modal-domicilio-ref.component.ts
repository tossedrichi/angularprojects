import {
  Component,
  OnInit,
  OnChanges,
  AfterViewChecked,
  Input,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
  SimpleChanges
} from "@angular/core";
import { DomicilioModel } from "src/app/models/Domicilio.Model";
import { EstadosModel } from "src/app/models/Estados.model";
import { CiudadesModel } from "src/app/models/Ciudades.model";
import { ColoniasModel } from "src/app/models/Colonias.model";
import { SolcitudService } from "src/app/services/solcitud.service";
import { ToastrService } from "ngx-toastr";
import { DomicilioClienteRequestModel } from "src/app/models/models-request/DomicilioClienteRequest.model";
import { getFechaActual } from "src/app/tools";
import { log, isNullOrUndefined } from "util";

@Component({
  selector: "app-modal-domicilio-ref",
  templateUrl: "./modal-domicilio-ref.component.html",
  styleUrls: ["./modal-domicilio-ref.component.css"]
})
export class ModalDomicilioRefComponent
  implements OnInit, OnChanges, AfterViewChecked {
  @Input() openModal: boolean;
  @Input() dataInput: {
    idReferencia: number;
    domicilioRef: DomicilioModel;
  };
  @Output() dataDomicilioRef: EventEmitter<{
    open: boolean;
    isCancel: boolean;
    data?: any;
  }>;

  @ViewChild("selectEstado") selectEstado: ElementRef;
  @ViewChild("selectCiudad") selectCiudad: ElementRef;
  // @ViewChild("selectColonia") selectColonia1: ElementRef;
  @ViewChild("edtCalle") edtCalle: ElementRef;
  @ViewChild("edtCp") edtCp: ElementRef;
  @ViewChild("edtNoExt") edtNoExt: ElementRef;
  @ViewChild("edtNoInt") edtNoInt: ElementRef;
  @ViewChild("auto") edtAuto: any;

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];
  coloniaSeleccionada: ColoniasModel = new ColoniasModel();
  modalCargado: boolean = false;
  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataDomicilioRef = new EventEmitter();
  }

  ngOnInit() {
    console.log("ngOnInit");

    this.getEstados();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes");

    console.log("Changes Domicilio", changes);
    if (!isNullOrUndefined(changes.dataInput)) {
      if (!isNullOrUndefined(changes.dataInput.currentValue)) {
        this.dataInput.idReferencia =
          changes.dataInput.currentValue.idReferencia;
        this.dataInput.domicilioRef =
          changes.dataInput.currentValue.domicilioRef;

        console.log("Empleo Seleccionado", this.dataInput);
      }
    }
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked");
    if (!this.modalCargado) {
      if (this.openModal) {
        this.getEmpleo();
      }
    }
  }

  getEmpleo() {
    if (this.dataInput != undefined) {
      if (this.dataInput.domicilioRef != undefined) {
        this.edtCalle.nativeElement.value = this.dataInput.domicilioRef.calle;
        this.edtCp.nativeElement.value = this.dataInput.domicilioRef.codigoPostal;
        this.edtNoExt.nativeElement.value = this.dataInput.domicilioRef.numeroExterior;
        this.edtNoInt.nativeElement.value = this.dataInput.domicilioRef.numeroInterior;
        
        this.getEstadoCiudad(this.dataInput.domicilioRef.idCiudad);
        this.getColonias(this.dataInput.domicilioRef.idCiudad);
        //this.coloniaSeleccionada.codigo_postal = this.dataInput.domicilioRef.codigoPostal

        setTimeout(x => {
          this.coloniaSeleccionada.nombre = this.dataInput.domicilioRef.colonia;

          this.coloniaSeleccionada.sepomex = this.dataInput.domicilioRef.codigoSepomex;
          console.log("Colonia", this.coloniaSeleccionada);
          this.edtAuto.searchInput.nativeElement.value = this.coloniaSeleccionada.nombre;
        }, 0);

        // this.edtAuto.nativeElement.initialValue = this.coloniaSeleccionada;
        this.modalCargado = true;
      }
    }
  }

  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado);
  }
  onSelectCiudad(idCiudad: number) {
    this.getColonias(idCiudad);
  }

  closeModal() {
    this.coloniaSeleccionada = new ColoniasModel(); 

      this.edtCp.nativeElement.value = "";
      this.edtAuto.searchInput.nativeElement.value ="";
  //  this.coloniaSeleccionada = null;
    this.dataInput = null;
 
    this.modalCargado = false;
    this.openModal = false;
    this.dataDomicilioRef.emit({ open: this.openModal, isCancel: true });
  }

  getEstadoCiudad(idCiudad: number) {
    this._SolicitudService.getEstadoCiudad(idCiudad).subscribe(
      resp => {
        console.log("Estado de  ciudad", resp);
        setTimeout(() => {
          this.selectEstado.nativeElement.value = resp.mensaje;
          this.getCiudades(parseInt(resp.mensaje));
        });
      },
      err => {
        this.toaster.error("Error al recuperar estado");
      }
    );
  }
  getEstados() {
    this._SolicitudService.getEstados().subscribe(respuesta => {
      console.log(respuesta);
      this.listEstados = respuesta;
      this.listCiudades = [];
      this.listColonias = [];
      //this.coloniaSeleccionada.nombre="HOLA"
    });
  }

  getCiudades(idestado: number) {
    this._SolicitudService.getCiudades(idestado).subscribe(respuesta => {
      console.log(respuesta);
      // console.log("Ciudad",this.dataInput.domicilioRef.idCiudad);

      this.listCiudades = respuesta;
      /* this.selectCiudad.nativeElement.value =199 */
      setTimeout(() => {
        this.selectCiudad.nativeElement.value =
          this.dataInput.domicilioRef.idCiudad || -1;
      }, 0);
      console.log("selectCiduades", this.selectCiudad);

      this.listColonias = [];
    });
  }

  getColonias(ciudad) {
    this._SolicitudService.getColonias(ciudad).subscribe(respuesta => {
      console.log(respuesta);
      this.listColonias = respuesta;
    });
  }

  setDomicilioEmpleoReferencia() {
    let domicilio: DomicilioClienteRequestModel = new DomicilioClienteRequestModel();

    domicilio.idContacto = this.dataInput.idReferencia;
    domicilio.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    domicilio.idSucursal = this._SolicitudService.solicitud.idSucursal;

    domicilio.idCiudad = this.selectCiudad.nativeElement.value;

    domicilio.colonia = this.coloniaSeleccionada.nombre;

    domicilio.codigoPostal = this.edtCp.nativeElement.value;
    domicilio.calle = this.edtCalle.nativeElement.value;
    domicilio.numeroExterior = this.edtNoExt.nativeElement.value;
    domicilio.numeroInterior = this.edtNoInt.nativeElement.value;
    domicilio.codigoSepomex = this.coloniaSeleccionada.sepomex;

    domicilio.geolocalizacion = "NA";
    domicilio.referencias = "NA";
    domicilio.idTipoDomicilio = 1;
    domicilio.idTipoRelacionCasaFamiliar = -1;
    domicilio.tiempoResidencia = 0;
    domicilio.idTipoCondicionDomicilio = -1;
    domicilio.importeRenta = 0;
    domicilio.habitantesDomicilio = 0;
    domicilio.propieterioDomicilio = this.dataInput.idReferencia;
    domicilio.fechaRegistro = getFechaActual();

    console.log("Domicilio", domicilio);
    this._SolicitudService.cargandSolicitud = true;
    this._SolicitudService
      .agregaDomicilioContactoSolicitud(domicilio)
      .subscribe(
        resp => {
          this._SolicitudService.cargandSolicitud = false;
          console.log("Respuesta", resp);
          this.toaster.success("Domicilio guardado", "Domicilio Referencia");
          this._SolicitudService
            .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
            .subscribe(resp => {
              this.closeModal();
            });
          // this._SolicitudService.solicitudCompleta.infoCliente.contactosCliente.find(x => x.contacto.idContacto==this.dataInput.idReferencia).domicilioContacto.domicilio =
        },
        err => {
          this._SolicitudService.cargandSolicitud = false;
          console.log(err);
          this.toaster.error(err.message, "Error: Domicilio Empleo");
        }
      );
  }

  selectColonia(event: any) {
    console.log("selecClonia", event);
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();
    this.coloniaSeleccionada = event;

/*     if (this.coloniaSeleccionada.codigo_postal > 0) {
      console.log(this.coloniaSeleccionada.codigo_postal);

      // this.edtCp.nativeElement.value=this.coloniaSeleccionada.codigo_postal
      console.log(this.edtCp.nativeElement.value);
    } */
    console.log(this.coloniaSeleccionada);
  }
  onChangeColonia(event: any) {

    console.log("onChangeColinia", event);
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();

    this.coloniaSeleccionada.nombre = event;
    this.coloniaSeleccionada.sepomex = -1;
    console.log(this.coloniaSeleccionada);
    console.log("changeColonia", event);
  }
  onFocusedColonia(event: any) {}
}
