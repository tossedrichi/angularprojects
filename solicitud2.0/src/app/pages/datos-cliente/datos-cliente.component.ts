import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { EdoCivilModel } from "src/app/models/EdoCivil.model";
import { SolcitudService } from "src/app/services/solcitud.service";
import { ClienteModel } from "src/app/models/Cliente.model";
import { FileItem } from "src/app/models/file-item";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SolicitudCompletaModel } from "../../models/SolicitudCompleta.model";
import { generarRFC, INEFix } from "../../tools";
import { INERequestModel } from "src/app/models/models-request/INEReques.model";
import { DataInputModalGuardarModel } from "../../components/modal-guardar/dataInputModa.model";
import { INEModel } from "src/app/models/INE.model";
import { TelefonoRequestModel } from "src/app/models/models-request/TelefonoRequest.model";
import { isUndefined, isNullOrUndefined } from 'util';
import { ToastrService } from "ngx-toastr";
import { ClienteModelRequest } from "../../models/models-request/ClienteRequest.model";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { EmpleoRequestModel } from 'src/app/models/models-request/EmpleoRequest.model';

@Component({
  selector: "app-datos-cliente",
  templateUrl: "./datos-cliente.component.html",
  styleUrls: ["./datos-cliente.component.css"]
})
export class DatosClienteComponent implements OnInit {
  formularioDatosCliente: FormGroup;
   openModalGuardar: boolean = false;
   openModalEliminarEmpelo: boolean = false;
  cargandoDatos: boolean = false;
  @ViewChild("edtNombreCliente") edtNombreCliente: ElementRef;
  @ViewChild("edtApellidoPaterno") edtApellidoPaterno: ElementRef;
  @ViewChild("edtApellidoMaterno") edtApellidoMaterno: ElementRef;
  @ViewChild("edtFechaNacimiento") edtFechaNacimiento: ElementRef;
  @ViewChild("selectEdo") selectEdo: ElementRef;
  @ViewChild("telefonoMovil") telefonoMovil: ElementRef;
  @ViewChild("telefonoFijo") telefonoFijo: ElementRef;
  @ViewChild("edtRfc") edtRfc: ElementRef;

  guardoDatos:boolean= false;
  guardoDatosExitoso:boolean= false;

  guardoIne:boolean= false;
  guardoIneExitoso:boolean= false;

  guardoCel:boolean= false;
  guardoCelExitoso:boolean= false;

  guardoFijo:boolean= false;
  guardoFijoExitoso:boolean= false;

  RFC: string;
  //cargandoDatos: boolean = true;
  estaSobreDrop: boolean = false;
  generoF: boolean = false;
  generoM: boolean = false;
  cuentaEmpleoSi: boolean = false;
  cuentaEmpleoNo: boolean = false;
  requiereAvalSi: boolean = false;
  requiereAvalNo: boolean = false;
  ineFrente: FileItem ;
  ineReverso: FileItem;

  solicitudCompleta: SolicitudCompletaModel;
  listEstadoCivil: EdoCivilModel[] = [];
  cliente: ClienteModel;
  alertaGuardar: boolean = false;

  dataInput: DataInputModalGuardarModel;
  image: string;

  constructor(
    public _solicitudServices: SolcitudService,
    private toaster: ToastrService
  ) {
    //console.log(this._solicitudServices.cliente);
    //if ( this._solicitudServices.cliente. )
  }

  ngOnInit() {
    console.log("OnInit");
    console.log(this._solicitudServices.solicitudCompleta);
    this.cliente = this._solicitudServices.solicitudCompleta.infoCliente.cliente;

    if (isUndefined(this.cliente)) {
      this.cliente = new ClienteModel();
    }
    this.initForm();
    this.getTiposEstadoCivil();
    this.autoSelect();
  } 


  initForm() {
    console.log("Estado Civil", this.cliente.idTipoEstadoCivil);

    this.formularioDatosCliente = new FormGroup({
      fechaNacimiento: new FormControl(this.getFechaNacimiento()),
      rfc: new FormControl(this.cliente.rfc),

      estadoCivil: new FormControl(this.cliente.idTipoEstadoCivil),
      genero: new FormControl(this.cliente.genero),
      telefonoCelular: new FormControl(""),
      telefonoFijo: new FormControl(),
      correo: new FormControl(this.cliente.correo),
      cuentaEmpleo: new FormControl(),
      requiereAval: new FormControl(),
      empleo: new FormControl(this._solicitudServices.reqDatosEmpleoClienteF())
    });
    console.log(this.formularioDatosCliente);
    //this.getSolicitudCompleta();
  }



 // cuentaEmpleo() {
 //   if (
 //     this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente
 //       .length > 0
 //   ) {
 //     this._solicitudServices.reqDatosEmpleoCliente = true;
 //   } else {
 //     this._solicitudServices.reqDatosEmpleoCliente = false;
 //   }
 // }

  getTiposEstadoCivil() {
    this._solicitudServices.getEstadoCivil().subscribe(
      resp => {
        console.log("Tipos Edo Civil", resp);

        this.listEstadoCivil = resp;

        this.selectEdo.nativeElement.value = this.cliente.idTipoEstadoCivil;

        //this.initForm()
        //this.autoSelect()
      },
      err => {
        console.log(err);
      }
    );
  }

  getFechaNacimiento(): string {
    let fecha: string = "";
    try {
      let fechas: Array<
        String
      > = this._solicitudServices.cliente.nacimiento.split("/");

      fecha = `${fechas[2]}-${fechas[1]}-${fechas[0]}`;
    } catch (error) {}
    console.log("Fecha Cliente", fecha);

    return fecha;
  }

  //reqDatAval(req: boolean) {
  //  this._solicitudServices.solicitudCompleta.requiereAval = req;
  //  this._solicitudServices.reqDatosAval=req;
  //}

  onClickEmpleo(req: boolean) {
    if(req){
      if(this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente.length==0){
        this.setEmpleoCliente()
      }
    }else{
      if(this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length>0){
        console.log("Borrar empleos Preguntando");
        this.dataInput = new DataInputModalGuardarModel(
          "Se van a eliminar los empleos que ya has agregado",
          "¿Deseas continuar?",
          []
          
        );
        this.openModalEliminarEmpelo = true;

      }else if(this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente.length>0 &&
        this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length==0){
          console.log("Borrar empleo NA");
          this.setEliminarEmpleo(0);
        }
    }
    //this._solicitudServices.reqDatosEmpleoCliente = req;
    //this.cuentaEmpleoCliente = req;
  }
 /* 
  requiereAval() {
    if (
      this._solicitudServices.solicitudCompleta.requiereAval
    ) {
      this._solicitudServices.reqDatosAval = true;
    } else {
      this._solicitudServices.reqDatosAval = false;
    }
  }*/

  reqDatConyuge(selec: number) {
    console.log(selec);

    if (selec == 1 || selec == 3) {
      this._solicitudServices.reqDatosConyugeCliente = true;
    } else {
      this._solicitudServices.reqDatosConyugeCliente = false;
    }
  }

  autoSelect() {
    if (this.cliente.genero.includes("M")) {
      this.generoM = true;
      this.generoF = false;
    } else if (this.cliente.genero.includes("F")) {
      this.generoF = true;
      this.generoM = false;
    }


 
    /*   
    if (this.requiereAvalSi = true) {
      this._solicitudServices.reqDatosEmpleoCliente = true;
    } else {
      this._solicitudServices.reqDatosEmpleoCliente = false;
    } */

    if (this.cliente.idTipoEstadoCivil != -1) {
      console.log("Estado Civil 2: ", this.cliente.idTipoEstadoCivil);

      this.selectEdo.nativeElement.value = this.cliente.idTipoEstadoCivil;
      if (this.cliente.idTipoEstadoCivil == 1) {
        this.reqDatConyuge(1);
      }
      if (this.cliente.idTipoEstadoCivil == 3) {
        this.reqDatConyuge(3);
      }
    }

    //setTimeout(() => {
    //  //this.cuentaEmpleo();
    //  this.cuentaEmpleoConyuge();
    //}, 0);

    console.log(this.selectEdo.nativeElement);
  }

  selectImage(evento: any, isFrente) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let ine: INEModel = new INEModel();
      if (isFrente) {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamFrente = file.base64;
        this._solicitudServices.solicitudCompleta.infoCliente.ine = ine;
        this.ineFrente = file;
      } else {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamReverso = file.base64;
        this._solicitudServices.solicitudCompleta.infoCliente.ine = ine;
        this.ineReverso = file;
      }
    };
  }

  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      this.guardaDatosCliente();
     

     
      //this.guardarTelefonos();
    }
  }

  responseDataEliminar(event) {
    console.log(event);
    this.openModalEliminarEmpelo = false;

    if (event.save) {
      this.setEliminarEmpleo(0);
     

     
      //this.guardarTelefonos();
    }
  }
 

  generaRfc() {
    this._solicitudServices.cliente.rfc = generarRFC(
      this.edtNombreCliente.nativeElement.value,
      this.edtApellidoPaterno.nativeElement.value,
      this.edtApellidoMaterno.nativeElement.value,
      this.edtFechaNacimiento.nativeElement.value
    );

    this.formularioDatosCliente.get("rfc").setValue(this.cliente.rfc);
  }

  renderIne(isFrente: boolean) {
    //console.log("renderINE");

    if (!isNullOrUndefined(this._solicitudServices.solicitudCompleta.infoCliente.ine)) {
      let ine: INEModel = this._solicitudServices.solicitudCompleta.infoCliente
        .ine;
      if (isFrente) {
        if (ine.streamFrente != null && this.ineFrente == null) {
          // console.log("Existe Archivo Viejo");
          this.ineFrente = new FileItem(new File([], ""), "");

          this.ineFrente.nombreArchivo = ine.fileNameFrente;
          this.ineFrente.base64 = INEFix + ine.streamFrente;
          this.ineFrente.idDocumento = ine.idCredencial;
          return INEFix + ine.streamFrente;
        } else if (this.ineFrente != null) {
          // console.log("Existe Archivo Nuevo");
          return this.ineFrente.base64;
        } else {
          // console.log("No Existe Archivo");
          this.image = "assets/fren.png";
          return "assets/fren.png";
        }
      } else {
        if (ine.streamReverso != null && this.ineReverso == null) {
          this.ineReverso = new FileItem(new File([], ""), "");
          this.ineReverso.nombreArchivo = ine.fileNameReverso;
          this.ineReverso.base64 = INEFix + ine.streamReverso;
          this.ineReverso.idDocumento = ine.idCredencial;
          return INEFix + ine.streamReverso;
        } else if (this.ineReverso != null) {
          return this.ineReverso.base64;
        } else {
          return "assets/rever.png";
        }
      }
    } else if (isFrente) {
      //console.log("es frente");
      // console.log(this.ineFrente);

      if (this.ineFrente != null) {
        // console.log("Existe Archivo Nuevo");
        return this.ineFrente.base64;
      } else {
        //console.log("asset");

        return "assets/fren.png";
      }
    } else if (!isFrente) {
      if (this.ineReverso != null) {
        return this.ineReverso.base64;
      } else {
        return "assets/rever.png";
      }
    }
  }

  guardarINE() {
      
      let ineAnverso;
      if (this.ineFrente.base64.includes("base64")) {
        ineAnverso = this.ineFrente.base64.split("base64,")[1];
      } else {
        ineAnverso = this.ineFrente.base64;
      }

      let ineReverso;
      if (this.ineReverso.base64.includes("base64")) {
        ineReverso = this.ineReverso.base64.split("base64,")[1];
      } else {
        ineReverso = this.ineReverso.base64;
      }

      let ineCliente: INERequestModel = new INERequestModel();

      ineCliente.idSolicitud = this._solicitudServices.solicitudCompleta.idSolicitud;
      ineCliente.streamAnverso = ineAnverso;
      ineCliente.fileNameAnverso = this.ineFrente.nombreArchivo;
      ineCliente.streamReverso = ineReverso;
      ineCliente.fileNameReverso = this.ineReverso.nombreArchivo;
      this.guardoIne= true;
      console.log(ineCliente);
      this._solicitudServices.cargandSolicitud=true;
      this._solicitudServices.setIneCliente(ineCliente).subscribe(
        resp => {
          this.guardoIneExitoso= true;
          console.log("Guardado INE", resp);
          
          this.ineFrente.idDocumento = resp.idCredencial;
          this.ineReverso.idDocumento = resp.idCredencial;
          if (this._solicitudServices.getTelefonoClienteCelular().localeCompare(this.telefonoMovil.nativeElement.value) != 0) {
            let cel: TelefonoRequestModel = new TelefonoRequestModel();
            cel.agente = SolcitudService.agente;
            cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
            cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
            cel.telefono = this.telefonoMovil.nativeElement.value;
            cel.idTipoTelefono = 2;
            cel.idOrigen = 0;
            console.log(` guardar Movil  ${cel}`);
            console.log(cel);
            this.guardarTelefonos(cel);
          }else {
            if ( this._solicitudServices.getTelefonoClienteFijo().localeCompare(this.telefonoFijo.nativeElement.value) != 0) {
              let cel: TelefonoRequestModel = new TelefonoRequestModel();
              cel.agente = SolcitudService.agente
              cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
              cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
              cel.telefono = this.telefonoFijo.nativeElement.value;
              cel.idTipoTelefono = 1;
              cel.idOrigen = 0;
              console.log(` guardar fijo  ${cel}`);
              console.log(cel);
              this.guardarTelefonos(cel);
            }else{
             
              //Cargar Solicitud Completa
              this.cargaSolicitudCompleta();
            }
          }
         
        },
        err => {
          this._solicitudServices.cargandSolicitud=false;
          console.log(err);
          this.guardoIneExitoso=false;
        }
      );
    
  }
  reqAval() {
 
      
      return this._solicitudServices.requiereAval();
    
  }

  guardaDatosCliente() {
    //this.cliente = new ClienteModel();

    let clienteRequest: ClienteModelRequest = new ClienteModelRequest();

    clienteRequest.idCliente = this.cliente.idCliente;
    clienteRequest.idSucursal = this._solicitudServices.cliente.sucursal;
    console.log("Sucursal", this._solicitudServices.cliente.sucursal);

    clienteRequest.nombres = this.edtNombreCliente.nativeElement.value;
    clienteRequest.primerApellido = this.edtApellidoPaterno.nativeElement.value;
    clienteRequest.segundoApellido = this.edtApellidoMaterno.nativeElement.value;

    clienteRequest.fechaNacimiento = this.formularioDatosCliente.get(
      "fechaNacimiento"
    ).value;

    clienteRequest.rfc = this.edtRfc.nativeElement.value;
    clienteRequest.correo = this.formularioDatosCliente.get("correo").value;
    clienteRequest.idTipoEstadoCivil = this.formularioDatosCliente.get(
      "estadoCivil"
    ).value;
    clienteRequest.genero = this.formularioDatosCliente.get("genero").value;
    clienteRequest.idConyuge = this.cliente.idConyuge;
    clienteRequest.agente = this.cliente.agente;
    clienteRequest.idOrigen = this.cliente.idOrigen;

    console.log("Datos enviados", this.formularioDatosCliente);
    this._solicitudServices.cargandSolicitud=true;
    this.guardoDatos=true;
    this._solicitudServices.guardarDatosCliente(clienteRequest).subscribe(
      response => {
        console.log("Guardado", response);
        this.guardoDatosExitoso=true;
        if (!isUndefined(this.ineReverso) ||!isUndefined(this.ineFrente)) {
          if (isUndefined(this.ineReverso.idDocumento) ||isUndefined(this.ineFrente.idDocumento)) {
            this.guardarINE();
          }else{
            if (this._solicitudServices.getTelefonoClienteCelular().localeCompare(this.telefonoMovil.nativeElement.value) != 0) {
              let cel: TelefonoRequestModel = new TelefonoRequestModel();
              cel.agente = SolcitudService.agente
              cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
              cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
              cel.telefono = this.telefonoMovil.nativeElement.value;
              cel.idTipoTelefono = 2;
              cel.idOrigen = 0;
              console.log(` guardar Movil  ${cel}`);
              console.log(cel);
              this.guardarTelefonos(cel);
            }else {
              if ( this._solicitudServices.getTelefonoClienteFijo().localeCompare(this.telefonoFijo.nativeElement.value) !=0 ) {
                let cel: TelefonoRequestModel = new TelefonoRequestModel();
                cel.agente = SolcitudService.agente
                cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
                cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
                cel.telefono = this.telefonoFijo.nativeElement.value;
                cel.idTipoTelefono = 1;
                cel.idOrigen = 0;
                console.log(` guardar fijo  ${cel}`);
                console.log(cel);
                this.guardarTelefonos(cel);
              }else{
             
                //Cargar Solicitud Completa
                this.cargaSolicitudCompleta();
              }
            }
          }
          
        }else{
          //guardarCel??
          if (this._solicitudServices.getTelefonoClienteCelular().localeCompare(this.telefonoMovil.nativeElement.value) != 0) {
            let cel: TelefonoRequestModel = new TelefonoRequestModel();
            cel.agente = SolcitudService.agente
            cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
            cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
            cel.telefono = this.telefonoMovil.nativeElement.value;
            cel.idTipoTelefono = 2;
            cel.idOrigen = 0;
            console.log(` guardar Movil  ${cel}`);
            console.log(cel);
            this.guardarTelefonos(cel);
          }else {
            if ( this._solicitudServices.getTelefonoClienteFijo().localeCompare(this.telefonoFijo.nativeElement.value) != 0) {
              let cel: TelefonoRequestModel = new TelefonoRequestModel();
              cel.agente = SolcitudService.agente
              cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
              cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
              cel.telefono = this.telefonoFijo.nativeElement.value;
              cel.idTipoTelefono = 1;
              cel.idOrigen = 0;
              console.log(` guardar fijo  ${cel}`);
              console.log(cel);
              this.guardarTelefonos(cel);
            }else{
           
              //Cargar Solicitud Completa
              this.cargaSolicitudCompleta();
            }
          }

        }
      
      },
      error => {
        console.log(error);
        this._solicitudServices.cargandSolicitud=false;
        this.guardoDatosExitoso=false;
      }
    );
  }

  guardarTelefonos(telefono: TelefonoRequestModel) {
    if(telefono.idTipoTelefono==2){
      this.guardoCel=true;
    }else{
      this.guardoFijo=true;
    }
    this._solicitudServices.cargandSolicitud=true;
    this._solicitudServices.agregaTelefonoClienteSolicitud(telefono).subscribe(
      response => {

        
        if(telefono.idTipoTelefono==2){
          this.guardoCelExitoso=true;
          if ( this._solicitudServices.getTelefonoClienteFijo().localeCompare(this.telefonoFijo.nativeElement.value) != 0) {
            let cel: TelefonoRequestModel = new TelefonoRequestModel();
            cel.agente = SolcitudService.agente
            cel.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
            cel.idSucursal = this._solicitudServices.solicitud.idSucursal;
            cel.telefono = this.telefonoFijo.nativeElement.value;
            cel.idTipoTelefono = 1;
            cel.idOrigen = 0;
            console.log(` guardar fijo  ${cel}`);
            console.log(cel);
            this.guardarTelefonos(cel);
          }else{
            
            //Cargar Solicitud Completa
            this.cargaSolicitudCompleta();
          }
        }else{
          this.guardoFijoExitoso=true;
          //cargarSolicitud Completa
          this.cargaSolicitudCompleta();
        }
      
      },
      error => {
        if(telefono.idTipoTelefono==2){
          this.guardoCelExitoso=false;
        }else{
          this.guardoFijoExitoso=false;
        }
        this._solicitudServices.cargandSolicitud=false;
       
      }
    );
  }

  setEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();
    
    empleoCl.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
    empleoCl.agente = this._solicitudServices.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = 1;
    empleoCl.empleo = "NA";
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = 0;
    empleoCl.nombreEmpresa = "NA";
    empleoCl.puesto = "NA";
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = -1;;
    //console.log("Select Tipo Cingreso", this.selecTipoIngreso);

  
      empleoCl.idTipoIngreso = -1;
    
 
    empleoCl.antiguedad = 0;
    //if(empleoCl.idTipoEmpleo>0 &&
    //   empleoCl.nombreEmpresa.localeCompare("")!=0 &&
    //   empleoCl.puesto.localeCompare("")!=0 &&
    //   empleoCl.ingresoMensual>0
    //   ){
      this._solicitudServices.cargandSolicitud=true;
      this._solicitudServices.agregaEmpleoClienteSolicitud(empleoCl).subscribe(
        resp => {
          console.log(resp);
          console.log(
            "Empleo guardado correctamente",
            "Datos Empleo Cliente"
          );
          this._solicitudServices.cargandSolicitud=false;
          this.cargaSolicitudCompleta();
        },
        err => {
          this._solicitudServices.cargandSolicitud=false;
          console.log(err);
          this.toaster.error(err.message, "Datos empleo: Error al guardar");
        }
      );
     
   // }else{
   //   this.toaster.error("Error al guardar", "Llena los campos obligatorios ");
   // }
   
  }

  
 // cuentaEmpleoConyuge() {
 //   if (
 //     this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente !=
 //     null
 //   ) {
 //     if (
 //       this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente
 //         .infoEmpleosContacto.length > 0
 //     ) {
 //       this._solicitudServices.reqDatosEmpleoConyugeCliente = true;
 //     } else {
 //       this._solicitudServices.reqDatosEmpleoConyugeCliente = false;
 //     }
 //   } else {
 //     this._solicitudServices.reqDatosEmpleoConyugeCliente = false;
 //   }
 // }

  onSaveClick() {
    if ((!isUndefined(this.ineReverso) && isUndefined(this.ineFrente))||isUndefined(this.ineReverso) && !isUndefined(this.ineFrente)) {
      this.toaster.error("Falta completar llenado de la INE")
    }else{
      this.dataInput = new DataInputModalGuardarModel(
        "Guardar Cambios",
        "¿Desea guardar los datos del cliente?",
        []
        
      );
      this.openModalGuardar = true;
    }
  
  }
  cargaSolicitudCompleta(){
    this._solicitudServices.cargandSolicitud=true;
    this._solicitudServices
    .getSolicitudCompleta(this._solicitudServices.solicitud.idSolicitud)
    .subscribe(resp => {
      if(this.guardoDatos){
        if(this.guardoDatosExitoso){
          this.toaster.success("Datos Guardados", "Datos del cliente");
        }else{
          this.toaster.error("Error", "Datos Cliente: Error al guardar");
        }
      }

      if(this.guardoIne){
        if(this.guardoIneExitoso){
          this.toaster.success("INE Guardada correctamente", "Datos del cliente");
        }else{
          this.toaster.error("Error", "Ine Cliente: Error al guardar");
        }
      }

      if(this.guardoCel){
        if(this.guardoCelExitoso){
          this.toaster.success("Teléfono móvil guardado correctamente", "Datos del cliente");
        }else{
          this.toaster.error("No se guardó el teléfono móvil", "Datos del cliente");
        }
      }

      if(this.guardoFijo){
        if(this.guardoFijoExitoso){
          this.toaster.success("Teléfono fijo guardado correctamente", "Datos del cliente");
        }else{
          this.toaster.error("No se guardó el teléfono fijo", "Datos del cliente");
        }
      }

      this.guardoDatos= false;
      this.guardoDatosExitoso= false;
    
      this.guardoIne= false;
      this.guardoIneExitoso= false;
    
      this.guardoCel= false;
      this.guardoCelExitoso= false;
    
      this.guardoFijo= false;
      this.guardoFijoExitoso= false;

      this._solicitudServices.cargandSolicitud=false;
    },error=>{
      this.toaster.error("Error al cargar la solicitud", "Verifique su conexión");
      this._solicitudServices.cargandSolicitud=false;
    });
  }
  setEliminarEmpleo(index:number) {
    this._solicitudServices.cargandSolicitud=true;
    if(index<this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente.length){
      
      this._solicitudServices
        .setEmliminarEmpleoSolcitud(
          this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente[index].empleo.idEmpleo,
          this._solicitudServices.solicitudCompleta.infoCliente.cliente.idCliente,
          -1
        )
        .subscribe(
          resp => {
            this._solicitudServices.cargandSolicitud=false;
            console.log(resp);
            
  
            console.log("Empleo eliminado", this._solicitudServices.solicitudCompleta.infoCliente.empleosCliente[index].empleo.idEmpleo);
            this.setEliminarEmpleo(index+1);
          },
          err => {
            this._solicitudServices.cargandSolicitud=false;
            //console.log(err);
            console.log(err.message, "Error: Eliminar Empleo");
          }
        );
    }else{
      this.cargaSolicitudCompleta();
    }
   
  }

  cambiaRequierAvalSolicitud(reqAval:boolean){
    this._solicitudServices.cargandSolicitud=true;
    this._solicitudServices.cambiaRequierAvalSolicitud(reqAval).subscribe(
      resp=>{
        this._solicitudServices.cargandSolicitud=false;
        if(resp.procesado){
          
          this.cargaSolicitudCompleta();
        }
      },err=>{
        console.log("Error", err);
        this._solicitudServices.cargandSolicitud=false;
      }
    );
  }
}
