import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { EmpleosModel } from "src/app/models/Empleos.model";
import { TiposComprobanteIngresosModel } from "src/app/models/TiposComprobanteIngresos.model";
import { SolcitudService } from "../../services/solcitud.service";
import { OtrosIngresosModel } from "../../models/OtrosIngresos.model";
import { DatosEmpleoModel } from "../../models/DatosEmpleo.model";
import { getAnos, getMeses, calcularTiempoMeses } from "src/app/tools";
import { EmpleoModel } from "../../models/Empleo.model";
import { EmpleoRequestModel } from "../../models/models-request/EmpleoRequest.model";
import { TelefonoRequestModel } from "src/app/models/models-request/TelefonoRequest.model";
import { ToastrService } from "ngx-toastr";
import { DataInputModalGuardarModel } from "src/app/components/modal-guardar/dataInputModa.model";
import { DatosContactoRequestModel } from "src/app/models/models-request/DatosContactoRequest.model";
import { IfStmt } from "@angular/compiler";
import { ContactoModel } from "../../models/Contacto.model";
import { isNullOrUndefined, log } from 'util';
import { ClienteModel } from "../../models/Cliente.model";
import { TiposEmpleoInformalModel } from "../../models/TiposEmpleoInformal.model";
import { timeout, filter } from 'rxjs/operators';

@Component({
  selector: "app-datos-empleo-cliente",
  templateUrl: "./datos-empleo-cliente.component.html",
  styleUrls: ["./datos-empleo-cliente.component.css"]
})
export class DatosEmpleoClienteComponent implements OnInit {
  @ViewChild("selectTipoEmpleo") selecTipoEmpleo: ElementRef;
  @ViewChild("selecTipoEmpleoInformal") selecTipoEmpleoInformal: ElementRef;

  @ViewChild("selectTipoIngreso") selecTipoIngreso: ElementRef;
  @ViewChild("edtEmpresa") edtEmpresa: ElementRef;
  @ViewChild("edtEmpleo") edtEmpleo: ElementRef;
  @ViewChild("edtPuesto") edtPuesto: ElementRef;
  @ViewChild("edtIngreso") edtIngreso: ElementRef;
  @ViewChild("edtTelefono") edtTelefono: ElementRef;
  @ViewChild("edtAnos") edtAnos: ElementRef;
  @ViewChild("edtMeses") edtMeses: ElementRef;
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("edtTelContacto") edtTelContacto: ElementRef;
  @ViewChild("edtOtros") edtOtros: ElementRef;
  @ViewChild("edtEmpresa2") edtEmpresa2: ElementRef;

  listEmpleos: EmpleosModel[] = [];
  listEmpleoInformal: TiposEmpleoInformalModel[] = [];
  listComprobanteIngresos: TiposComprobanteIngresosModel[] = [];
  listOtrosIngresos: OtrosIngresosModel[] = [];

  listEmpleosCliente: DatosEmpleoModel[];
  empleoSeleccionado: DatosEmpleoModel;
  //empleoSelecc: EmpleoModel;

  openModaldDomicilioEmpleo: boolean = false;
  openModalGuardar: boolean = false;
  generoF: boolean = false;
  generoM: boolean = false;
  dataInput: DataInputModalGuardarModel;

  openModaldDocumentosEmpleo: boolean = false;
  // openModalContactoEmpleo: boolean = false;

  openModalConfirm: boolean = false;
  instanceModalConfim: number;
  dataInputModalConfirm: DataInputModalGuardarModel;

  empresa: boolean = true;
  empresa2: boolean = false;
  otros: boolean = false;

  empleo: boolean = true;
  puesto: boolean = true;
  telefono: boolean = true;
  tipoEmpleo: boolean = true;
  tipoEmpleoInformal: boolean = false;
  tipoIngreso: boolean = false;
  clienteC: ContactoModel;

  esEdicion: boolean = false;

  idContactoEmpleo: number = -1;
  constructor(
    private _solicitudService: SolcitudService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.obtenerEmpleos();
    this.getComprobanteIngresos();
    this.getEmpleos();
    this.getOtrosIngresos();
    this.listarEmpleos();
    if(this._solicitudService.solicitudCompleta.infoCliente.empleosCliente.filter(x=>x.empleo.empleo.localeCompare("NA")==0).length>0){
      this.empleoSeleccionado=this._solicitudService.solicitudCompleta.infoCliente.empleosCliente.filter(x=>x.empleo.empleo.localeCompare("NA")==0)[0];
      this.esEdicion=true;
      console.log("sva a editar NA")
    }
    this.getTipoEmpleoInformal();
  }

  obtenerEmpleos() {
    this.listEmpleosCliente = this._solicitudService.solicitudCompleta.infoCliente.empleosCliente;

    if (this.listEmpleosCliente.length <= 0) {
      this.listEmpleosCliente = [];
    }
  }

  getEmpleos() {
    this._solicitudService.getEmpleos().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleos = respuesta;
    });
  }
  getOtrosIngresos() {
    this._solicitudService.getOtrosIngresos().subscribe(respuesta => {
      console.log(respuesta);
      this.listOtrosIngresos = respuesta;
    });
  }
  getTipoEmpleoInformal() {
    this._solicitudService.getTipoEmpleoInformal().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleoInformal = respuesta;
    });
  }
  getComprobanteIngresos() {
    this._solicitudService.getComprobanteIngresos().subscribe(respuesta => {
      console.log(respuesta);
      this.listComprobanteIngresos = respuesta;
    });
  }

  resposeDataDomiclioEmpleo(event) {
    console.log(event);
    this.openModaldDomicilioEmpleo = event.open;
    this.obtenerEmpleos()
    if (event.isCancel) {

    }
  }
  resposeDataDocumentosEmpleo(event) {
    console.log(event);
    this.openModaldDocumentosEmpleo = false;

    if (event.isCancel) {
    }
    this.empleoSeleccionado = null;
  }
  resposeDataContactoEmpleo(event) {
    console.log("respuesta", event);
    //this.openModalContactoEmpleo = false;
    if (event.isSave) {
      this.idContactoEmpleo = event.idContacto;
      this.setEmpleoCliente();
    } else {
      this.idContactoEmpleo = -1;
    }
  }

  onSelectTipoEmpleo(idTipoEmpleo: string) {
    console.log(idTipoEmpleo);

    switch (parseInt(idTipoEmpleo)) {
      case 1:
        console.log("Formal");
        this.empresa = true;
        this.empleo = true;
        this.puesto = true;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
      case 2:
        console.log("Informal");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = false;


        break;
      case 3:
        console.log("Negocio propio");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = true;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
      case 4:
        console.log("Otros ingresos");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = true;
        this.empresa2 = false;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
    }
  }
  onSelectTipoEmpleoInformal(idTipoEmpleoInformal: string) {
    console.log(idTipoEmpleoInformal);

    switch (parseInt(idTipoEmpleoInformal)) {
      case 1:
        console.log("Empleada doméstica");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = false;

        break;
        case 2:
        console.log("Otros");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = true;

        break;
       

    }
  }

  getTipoEmpelo(idTipo: number): string {
    let empleo: EmpleosModel[] = this.listEmpleos.filter(
      x => x.id_tipo_empleo == idTipo
    );

    if (empleo[0] != undefined) {
      return empleo[0].tipo_empleo;
    }
    return;
  }

  onClickEditarEmpelo(empleo: DatosEmpleoModel) {
    this.limpiarForm();
    this.empleoSeleccionado = empleo;
    this.onSelectTipoEmpleo(empleo.empleo.idTipoEmpleo.toString());
    this.esEdicion = true;
    this.selecTipoEmpleo.nativeElement.value = empleo.empleo.idTipoEmpleo;

    if (empleo.empleo.idTipoEmpleo == 4) {
      setTimeout(()=>{
        this.selecTipoIngreso.nativeElement.value = this.listOtrosIngresos.filter(x=> x.tipo_ingreso.localeCompare(empleo.empleo.empleo)==0)[0].id_tipo_ingreso
      },0)
      this.edtIngreso.nativeElement.value = empleo.empleo.ingresoMensual;

      this.edtAnos.nativeElement.value = getAnos(empleo.empleo.antiguedad);
      this.edtMeses.nativeElement.value = getMeses(empleo.empleo.antiguedad);
    } else {
      switch (empleo.empleo.idTipoEmpleo) {
        case 1:
          console.log("Formal");

          

          this.edtEmpresa.nativeElement.value = empleo.empleo.nombreEmpresa;
          this.edtEmpleo.nativeElement.value = empleo.empleo.empleo;
          this.edtPuesto.nativeElement.value = empleo.empleo.puesto;
          
  
          break;
        case 2:
          console.log("Informal");
         
            this.onSelectTipoEmpleoInformal(empleo.empleo.idTipoEmpleoInformal.toString())
            setTimeout(()=>{ 
              console.log(this.selecTipoEmpleoInformal);
              if(empleo.empleo.idTipoEmpleoInformal==-1){
                if(empleo.empleo.empleo.localeCompare("Empleada domestica")==0){
                  empleo.empleo.idTipoEmpleoInformal=1
                }else{
                  empleo.empleo.idTipoEmpleoInformal=2
                }
              }
              this.selecTipoEmpleoInformal.nativeElement.value =empleo.empleo.idTipoEmpleoInformal
            },0)
           
        
          
          try {
            this.edtOtros.nativeElement.value = empleo.empleo.empleo;
          } catch (error) {
            
          }
          
         
          //this.tipoEmpleo = true;
         // this.tipoIngreso = false;
         // this.empresa2 = false;
         // this.tipoEmpleoInformal = true;
         // this.otros = false;
  
  
          break;
        case 3:
          console.log("Negocio propio");
          setTimeout(()=>{
            this.edtEmpresa2.nativeElement.value = empleo.empleo.nombreEmpresa;
          },0)
          
  
  
          break;
        case 4:
          console.log("Otros ingresos");
          setTimeout(()=>{
            this.selecTipoIngreso.nativeElement.value= empleo.empleo.tipoIngreso;
          },0)
         // this.selecTipoIngreso.nativeElement.value= empleo.empleo.tipoIngreso;
         // this.empresa = false;
         // this.empleo = false;
         // this.puesto = false;
         // this.telefono = false;
         // this.tipoEmpleo = true;
         // this.tipoIngreso = true;
         // this.empresa2 = false;
         // this.tipoEmpleoInformal = false;
         // this.otros = false;
  
  
          break;
      }
  
      this.edtIngreso.nativeElement.value = empleo.empleo.ingresoMensual;

      this.edtAnos.nativeElement.value = getAnos(empleo.empleo.antiguedad);
      this.edtMeses.nativeElement.value = getMeses(empleo.empleo.antiguedad);
    }
    if (empleo.telefonosEmpleo.length > 0) {
      this.edtTelefono.nativeElement.value =
        empleo.telefonosEmpleo[
          empleo.telefonosEmpleo.length - 1
        ].numeroTelefonico;
    }
    if (!isNullOrUndefined(empleo.contactoEmpleo)) {
      this.edtNombre.nativeElement.value = empleo.contactoEmpleo.nombres;
      this.edtApellidoP.nativeElement.value =
        empleo.contactoEmpleo.primerApellido;
      this.edtApellidoM.nativeElement.value =
        empleo.contactoEmpleo.segundoApellido;
        if(empleo.contactoEmpleo.genero.localeCompare("F")==0){
          this.generoF=true;
          this.generoM=false;
        }
        if(empleo.contactoEmpleo.genero.localeCompare("M")==0){
          this.generoF=false;
          this.generoM=true;
        }
    }
  }

  onClickCambiaGenero(masc:boolean){
    this.generoF=!masc;
    this.generoM=masc;
  } 

  onClickOtroEmpleo() {
    this.empresa = true;
    this.empleo = true;
    this.puesto = true;
    this.telefono = true;
    this.tipoEmpleo = true;
    this.tipoIngreso = false;
    this.limpiarForm();
  }

  setEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();

    empleoCl.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    empleoCl.agente = this._solicitudService.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = this.selecTipoEmpleo.nativeElement.value;
    //empleoCl.empleo = this.edtEmpleo.nativeElement.value;
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = this.edtIngreso.nativeElement.value;
    //empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
    //empleoCl.puesto = this.edtPuesto.nativeElement.value;
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = this.idContactoEmpleo;
    console.log("Select Tipo Cingreso", this.selecTipoIngreso);

    if (this.selecTipoEmpleo.nativeElement.value == 4) {
      empleoCl.idTipoIngreso = this.selecTipoIngreso.nativeElement.value;
    } else {
      empleoCl.idTipoIngreso = -1;
    }

    empleoCl.antiguedad = calcularTiempoMeses(
      parseInt(this.edtAnos.nativeElement.value),
      parseInt(this.edtMeses.nativeElement.value)
    ); switch (parseInt(empleoCl.idTipoEmpleo.toString())) {
      case 1:
        empleoCl.empleo = this.edtEmpleo.nativeElement.value;
        empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
        empleoCl.puesto = this.edtPuesto.nativeElement.value;

        break;
      case 2:
        if (this.selecTipoEmpleoInformal.nativeElement.value == 1) {
      

          empleoCl.nombreEmpresa = "Empleada domestica";
          empleoCl.empleo = "Empleada domestica";
          empleoCl.puesto = "Empleada domestica";
        } else if (this.selecTipoEmpleoInformal.nativeElement.value == 2) {
          empleoCl.nombreEmpresa = this.edtOtros.nativeElement.value;
          empleoCl.empleo = this.edtOtros.nativeElement.value;
          empleoCl.puesto = this.edtOtros.nativeElement.value;
        }

        break;
      case 3:
        empleoCl.nombreEmpresa = this.edtEmpresa2.nativeElement.value;
        empleoCl.empleo = this.edtEmpresa2.nativeElement.value;
        empleoCl.puesto = this.edtEmpresa2.nativeElement.value;

        break;
      case 4:
        let tipo = this.listOtrosIngresos.filter(x => x.id_tipo_ingreso == 1)[0]
          .tipo_ingreso;

          empleoCl.nombreEmpresa = tipo;
          empleoCl.empleo = tipo;
          empleoCl.puesto = tipo;
        break;
    }


    if (
      empleoCl.idTipoEmpleo > 0 &&
      empleoCl.nombreEmpresa.localeCompare("") != 0 &&
      empleoCl.puesto.localeCompare("") != 0 &&
      empleoCl.ingresoMensual > 0
    ) {
      this._solicitudService.cargandSolicitud = true;
     
      this._solicitudService.agregaEmpleoClienteSolicitud(empleoCl).subscribe(
        resp => {
          console.log("Empleo guardado",resp);
          this.toaster.success(
            "Empleo guardado correctamente",
            "Datos Empleo Cliente"
          );
          this._solicitudService.cargandSolicitud = false;
          if(empleoCl.idTipoEmpleo==1||empleoCl.idTipoEmpleo==3){
            this.setTelefonoEmpleo(resp.idEmpleo);
          }else{
            this._solicitudService
            .getSolicitudCompleta(this._solicitudService.solicitud.idSolicitud)
            .subscribe(
              resp => {
                console.log(resp);
               // this.toaster.success("Telefono Guardado", "Datos Cónyuge");
                this._solicitudService.cargandSolicitud = false;
                this._solicitudService.solicitudCompleta.infoCliente.empleosCliente =
                  resp.infoCliente.empleosCliente;
                this.listarEmpleos();
                this.limpiarForm();
              },
              err => {
                this._solicitudService.cargandSolicitud = false;
                console.log(err);
                this.toaster.error(err.message, "Error al recuperar empleos");
              }
            );
            }
        },
        err => {
          this._solicitudService.cargandSolicitud = false;
          console.log(err);
          this.toaster.error(err.message, "Datos empleo: Error al guardar");
        }
      );
    } else {
      this.toaster.error("Error al guardar", "Llena los campos obligatorios ");
    }
  }

  listarEmpleos() {
    let empleosCliente: DatosEmpleoModel[] = this._solicitudService
      .solicitudCompleta.infoCliente.empleosCliente;

    this.listEmpleosCliente = empleosCliente.filter(x=>x.empleo.empleo.localeCompare("NA")!=0);
    console.log("Empleos cliente", this.listEmpleosCliente);
  }

  setTelefonoEmpleo(idEmpleo) {
    let telefono: TelefonoRequestModel = new TelefonoRequestModel();

    telefono.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    telefono.idEmpleo = idEmpleo;
    telefono.idSucursal = this._solicitudService.solicitud.idSucursal;
    telefono.idTipoTelefono = 2;
    telefono.telefono = this.edtTelefono.nativeElement.value;
    // if(telefono.telefono.localeCompare(this.empleoSeleccionado.telefonosEmpleo[this.empleoSeleccionado.telefonosEmpleo.length-1].numeroTelefonico)!=0){
    this._solicitudService.cargandSolicitud = true;
    this._solicitudService
      .agregaTelefonoEmpleoClienteSolicitud(telefono)
      .subscribe(
        resp => {
          console.log(resp);

          this._solicitudService
            .getSolicitudCompleta(this._solicitudService.solicitud.idSolicitud)
            .subscribe(
              resp => {
                console.log(resp);
                this.toaster.success("Telefono Guardado", "Datos Cliente");
                this._solicitudService.cargandSolicitud = false;
                this._solicitudService.solicitudCompleta.infoCliente.empleosCliente =
                  resp.infoCliente.empleosCliente;
                this.listarEmpleos();
                this.limpiarForm();
              },
              err => {
                this._solicitudService.cargandSolicitud = false;
                console.log(err);
                this.toaster.error(err.message, "Error al recuperar empleos");
              }
            );
        },
        err => {
          console.log(err);
          this._solicitudService.cargandSolicitud = false;
          this.toaster.error(err.message, "Datos Cliente : Error telefonos");
        }
      );
    //}
  }

  onClickAgregarDocumentos(empleo: DatosEmpleoModel) {
    console.log("Empleo Seleccionado", empleo);

    this.empleoSeleccionado = empleo;
    this.openModaldDocumentosEmpleo = true;
  }

  /*   onClickContactoEmpleo(empleo: DatosEmpleoModel) {
    this.empleoSeleccionado = empleo;

    //this.openModalContactoEmpleo = true;
  } */

  onClickDomicilioEmpleo(empleo: DatosEmpleoModel) {
    this.empleoSeleccionado = empleo;
    this.openModaldDomicilioEmpleo = true;
  }

  setContactoEmpleoCliente() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 6;
    contacto.idTipoRelacionContacto = -1;
    //contacto = this.empleo.empleo.idEmpleo;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.correo = "NA";
    contacto.rfc = "NA";
    contacto.fechaNacimiento = "1900-01-01";
    if(contacto.nombres.localeCompare("")!=0){
      if(this.generoM){
        contacto.genero = "M"
      }
      if(this.generoF){
        contacto.genero = "F"
      }
    
      if(this.validarContacto(contacto)){
        console.log("Envio", contacto);
        this._solicitudService.cargandSolicitud = true;
        this._solicitudService
          .agregaContactoClientSolicitud(contacto)
          .subscribe(
            resp => {
              this.toaster.success("Contacto guardado", "Contacto Empleo");
              console.log("Repsuesta", resp);
              this._solicitudService.cargandSolicitud = false;
              if (resp.idContacto > 0) {
                this.idContactoEmpleo = resp.idContacto;
                //this.guardarTelefonoContacto();
                this.setEmpleoCliente();
                this.toaster.success("Guardado", "Contacto guardado");
              }

              //this.closeModal(true,resp.idContacto);
            },
            err => {
              this._solicitudService.cargandSolicitud = false;
              this.toaster.error(err.message, "Error: Contacto Empleo");
              console.log(err);
            }
          );
      } else {
        this.toaster.error("Error", "Termina de llenar los datos del contacto");
      }
    } else {
      console.log("Envio", "Empleo");
      this.setEmpleoCliente();
    }
  }
  validarContacto(c: DatosContactoRequestModel) {
    if (
      c.nombres.localeCompare("") != 0 &&
      c.primerApellido.localeCompare("") != 0 //&& "".localeCompare(this.edtTelContacto.nativeElement.value)!=0
    ) {
      return true;
    } else {
      return false;
    }
  }

  guardarTelefonoContacto() {
    // if(this.getTelefono().localeCompare(this.telefonoMovil.nativeElement.value)!=0){
    this._solicitudService.cargandSolicitud = true;
    // console.log("guardar telefono: viejo ",this.getTelefono()+" nuevo "+this.telefonoMovil.nativeElement.value)
    let telefono: TelefonoRequestModel = new TelefonoRequestModel();
    telefono.agente = SolcitudService.agente;
    telefono.idContacto = this.idContactoEmpleo;
    telefono.idOrigen = -1;
    telefono.idSucursal = this._solicitudService.solicitud.idSucursal;
    telefono.idTipoTelefono = 2;
    telefono.telefono = this.edtTelContacto.nativeElement.value;
    telefono.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    // console.log("guarda",telefono);
    //this.guardoCel=true;
    this._solicitudService
      .agregaTelefonoContactoClienteSolicitud(telefono)
      .subscribe(
        response => {
          this._solicitudService.cargandSolicitud = false;
          // this.cargaSolicitud();
          this.toaster.success("Guardado", "Teléfono del Contacto guardado");
          this.setEmpleoCliente();
        },
        error => {
          //this._SolicitudService.cargandSolicitud=false;
          //this.guardoCelExitoso=false;
        }
      );

    // }else{
    // this.cargaSolicitud();
    // }
  }

  limpiarForm() {
    this.edtAnos.nativeElement.value = 0;
    try {
      this.edtEmpleo.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtTelefono.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
       this.edtEmpresa.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtPuesto.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtEmpresa2.nativeElement.value = "";
   } catch (error) {
     
   }
   try {
    this.selecTipoEmpleoInformal.nativeElement.value = "";
 } catch (error) {
   
 }
 try {
  this.selecTipoIngreso.nativeElement.value = "";
} catch (error) {
 
}
    this.generoF=false;
    this.generoM=false;
  
    this.edtMeses.nativeElement.value = 0;
    this.edtIngreso.nativeElement.value = 0;
    this.selecTipoEmpleo.nativeElement.value = "-1";
    this.edtNombre.nativeElement.value="";
    this.edtApellidoM.nativeElement.value="";
    this.edtApellidoP.nativeElement.value="";
   // this.edtTelContacto.nativeElement.value="";
    this.esEdicion=false;
  }

  onClickEliminarEmpleo(empleo: DatosEmpleoModel) {
    /**
     * NOTE Esta funcion abre el modal de confirmacion
     * el cual responde en la funcion onConfirmModal
     * @param empleo
     */

    this.empleoSeleccionado = empleo;
    this.instanceModalConfim = 1;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Eliminar Empleo",
      "¿Deseas eliminar el empleo?",
      []
    );
    console.log(empleo);
  }

  onClickGuardarEmpleo() {
    this.instanceModalConfim = 2;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Empleo del Cliente",
      "¿Desea guardar  el empleo?",
      []
    );
  }
  onSaveClick() {
    this.dataInput = new DataInputModalGuardarModel(
      "Guardar Datos",
      "¿Desea guardar los datos del empleo del cliente cliente?",
      []
    );
    this.openModalGuardar = true;
  }
  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      if (this.esEdicion) {
        console.log("es edicion");
        this.editarEmpleo();
      } else {
        console.log("es Nuevo");
        this.setContactoEmpleoCliente();
      }

      //this.guardarTelefonos();
    }
  }

  onConfirmModal(event: any) {
    this.openModalConfirm = false;

    if (event.save) {
      switch (this.instanceModalConfim) {
        case 1:
          this.setEliminarEmpleo();
          break;
        /*   case 2:
          this.openModalContactoEmpleo = true;
          break; */
      }
    } else {
      if (this.instanceModalConfim == 2) {
        this.setEmpleoCliente();
      }
    }
  }
  editarEmpleo() {
    if (isNullOrUndefined(this.empleoSeleccionado.contactoEmpleo)) {
      this.empleoSeleccionado.contactoEmpleo = new ContactoModel();
      this.empleoSeleccionado.contactoEmpleo.nombres = "";
      this.empleoSeleccionado.contactoEmpleo.primerApellido = "";
      this.empleoSeleccionado.contactoEmpleo.segundoApellido = "";
    }
    if (
      this.empleoSeleccionado.contactoEmpleo.nombres.localeCompare(
        this.edtNombre.nativeElement.value
      ) != 0 ||
      this.empleoSeleccionado.contactoEmpleo.primerApellido.localeCompare(
        this.edtApellidoP.nativeElement.value
      ) != 0 ||
      this.empleoSeleccionado.contactoEmpleo.segundoApellido.localeCompare(
        this.edtApellidoM.nativeElement.value
      ) != 0
    ) {
      console.log("empzamos a editar contacto");
      this.setEditaContactoEmpleoCliente();
    } else {
      console.log("sin editar contacto");
      this.setEditarEmpleoCliente();
    }
  }

  setEditaContactoEmpleoCliente() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 6;
    contacto.idTipoRelacionContacto = -1;
    //contacto = this.empleo.empleo.idEmpleo;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.correo = "NA";
    contacto.rfc = "NA";
    contacto.fechaNacimiento = "1900-01-01";

    if(this.generoM){
      contacto.genero = "M"
    }
    if(this.generoF){
      contacto.genero = "F"
    }
    if (contacto.nombres.localeCompare("") != 0) {
      if (this.validarContacto(contacto)) {
        console.log("Envio", contacto);
        this._solicitudService.cargandSolicitud = true;
        this._solicitudService
          .agregaContactoClientSolicitud(contacto)
          .subscribe(
            resp => {
              this.toaster.success("Contacto guardado", "Contacto Empleo");
              console.log("Repsuesta", resp);
              this._solicitudService.cargandSolicitud = false;
              if (resp.idContacto > 0) {
                this.idContactoEmpleo = resp.idContacto;
                //this.editaTelefonoContacto();
                this.setEditarEmpleoCliente();
                this.toaster.success("Guardado", "Contacto guardado");
              }

              //this.closeModal(true,resp.idContacto);
            },
            err => {
              //this._solcitudService.cargandSolicitud=false;
              this.toaster.error(err.message, "Error: Contacto Empleo");
              console.log(err);
            }
          );
      } else {
        this.toaster.error("Error", "Termina de llenar los datos del contacto");
      }
    } else {
      console.log("Envio", "Empleo");
      this.setEditarEmpleoCliente();
    }
  }

  editaTelefonoContacto() {
    // if(this.getTelefono().localeCompare(this.telefonoMovil.nativeElement.value)!=0){
    this._solicitudService.cargandSolicitud = true;
    // console.log("guardar telefono: viejo ",this.getTelefono()+" nuevo "+this.telefonoMovil.nativeElement.value)
    let telefono: TelefonoRequestModel = new TelefonoRequestModel();
    telefono.agente = SolcitudService.agente;
    telefono.idContacto = this.idContactoEmpleo;
    telefono.idOrigen = -1;
    telefono.idSucursal = this._solicitudService.solicitud.idSucursal;
    telefono.idTipoTelefono = 2;
    telefono.telefono = this.edtTelContacto.nativeElement.value;
    telefono.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    // console.log("guarda",telefono);
    //this.guardoCel=true;
    this._solicitudService
      .agregaTelefonoContactoClienteSolicitud(telefono)
      .subscribe(
        response => {
          this._solicitudService.cargandSolicitud = false;
          // this.cargaSolicitud();
          this.toaster.success("Guardado", "Teléfono del Contacto guardado");
          this.setEditarEmpleoCliente();
        },
        error => {
          //this._SolicitudService.cargandSolicitud=false;
          //this.guardoCelExitoso=false;
        }
      );

    // }else{
    // this.cargaSolicitud();
    // }
  }

  setEditarEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();

    empleoCl.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    empleoCl.agente = this._solicitudService.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = this.selecTipoEmpleo.nativeElement.value;
    //empleoCl.empleo = this.edtEmpleo.nativeElement.value;
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = this.edtIngreso.nativeElement.value;
    //empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
    //empleoCl.puesto = this.edtPuesto.nativeElement.value;
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = this.idContactoEmpleo;
    empleoCl.idEmpleo = this.empleoSeleccionado.empleo.idEmpleo;
    console.log("Select Tipo ingreso", this.selecTipoIngreso);

    if (this.selecTipoEmpleo.nativeElement.value == 4) {
      empleoCl.idTipoIngreso = this.selecTipoIngreso.nativeElement.value;
    } else {
      empleoCl.idTipoIngreso = -1;
    }

    empleoCl.antiguedad = calcularTiempoMeses(
      parseInt(this.edtAnos.nativeElement.value),
      parseInt(this.edtMeses.nativeElement.value)
    );

      console.log("editar empleo", empleoCl);
      this._solicitudService.cargandSolicitud = true;

      switch (parseInt(empleoCl.idTipoEmpleo.toString())) {
        case 1:
          empleoCl.empleo = this.edtEmpleo.nativeElement.value;
          empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
          empleoCl.puesto = this.edtPuesto.nativeElement.value;

      
  
          break;
        case 2:
          if (this.selecTipoEmpleoInformal.nativeElement.value == 1) {
            
  
            empleoCl.nombreEmpresa = "Empleada domestica";
            empleoCl.empleo = "Empleada domestica";
            empleoCl.puesto = "Empleada domestica";
          } else if (this.selecTipoEmpleoInformal.nativeElement.value == 2) {
            empleoCl.nombreEmpresa = this.edtOtros.nativeElement.value;
            empleoCl.empleo = this.edtOtros.nativeElement.value;
            empleoCl.puesto = this.edtOtros.nativeElement.value;
          }
  
          break;
        case 3:
          empleoCl.nombreEmpresa = this.edtEmpresa2.nativeElement.value;
          empleoCl.empleo = this.edtEmpresa2.nativeElement.value;
          empleoCl.puesto = this.edtEmpresa2.nativeElement.value;
  
          break;
        case 4:
          let tipo = this.listOtrosIngresos.filter(x => x.id_tipo_ingreso == 1)[0]
            .tipo_ingreso;
  
            empleoCl.nombreEmpresa = tipo;
            empleoCl.empleo = tipo;
            empleoCl.puesto = tipo;
          break;
      }
      if (
        empleoCl.idTipoEmpleo > 0 &&
        empleoCl.nombreEmpresa.localeCompare("") != 0 &&
        empleoCl.puesto.localeCompare("") != 0 &&
        empleoCl.ingresoMensual > 0
      ) {
      
  
      this._solicitudService
        .actualizaEmpleoClienteSolicitud(empleoCl)
        .subscribe(
          resp => {
            console.log(resp);
            this.toaster.success(
              "Empleo guardado correctamente",
              "Datos Empleo Cliente"
            );
            this._solicitudService.cargandSolicitud = false;
            if (resp.procesado) {
              if (empleoCl.idTipoEmpleo == 1 || empleoCl.idTipoEmpleo == 3){
                this.setTelefonoEmpleo(this.empleoSeleccionado.empleo.idEmpleo);
              }else{
                this._solicitudService
                .getSolicitudCompleta(this._solicitudService.solicitud.idSolicitud)
                .subscribe(
                  resp => {
                    console.log(resp);
                   // this.toaster.success("Telefono Guardado", "Datos Cónyuge");
                    this._solicitudService.cargandSolicitud = false;
                    this._solicitudService.solicitudCompleta.infoCliente.empleosCliente =
                      resp.infoCliente.empleosCliente;
                    this.listarEmpleos();
                    this.limpiarForm();
                  },
                  err => {
                    this._solicitudService.cargandSolicitud = false;
                    console.log(err);
                    this.toaster.error(err.message, "Error al recuperar empleos");
                  }
                );  

              }
          
            }
            
          },
          err => {
            this._solicitudService.cargandSolicitud = false;
            console.log(err);
            this.toaster.error(err.message, "Datos empleo: Error al guardar");
          }
        );
      } else {
       
        this.toaster.error(
          "Error al guardar",
          "Llena los campos obligatorios "
        );
      }
   
  }

  setEliminarEmpleo() {
    this._solicitudService.cargandSolicitud = true;
    this._solicitudService
      .setEmliminarEmpleoSolcitud(
        this.empleoSeleccionado.empleo.idEmpleo,
        this._solicitudService.solicitudCompleta.infoCliente.cliente.idCliente,
        -1
      )
      .subscribe(
        resp => {
          this._solicitudService.cargandSolicitud = false;
          console.log(resp);
          let empleos: DatosEmpleoModel[] = this._solicitudService.solicitudCompleta.infoCliente.empleosCliente.filter(
            x => x.empleo.idEmpleo != this.empleoSeleccionado.empleo.idEmpleo
          );
          this._solicitudService.solicitudCompleta.infoCliente.empleosCliente = empleos;
          this.listEmpleosCliente = empleos;

          this.toaster.success("Empleo eliminado", "Eliminar Empleo");
        },
        err => {
          this._solicitudService.cargandSolicitud = false;
          console.log(err);
          this.toaster.error(err.message, "Error: Eliminar Empleo");
        }
      );
  }

}
