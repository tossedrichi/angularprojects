import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  SimpleChanges,
  AfterViewChecked
} from "@angular/core";
import { SolcitudService } from "src/app/services/solcitud.service";
import { DatosEmpleoModel } from "../../../models/DatosEmpleo.model";
import { ContactoModel } from "../../../models/Contacto.model";
import { ToastrService } from "ngx-toastr";
import { DatosContactoRequestModel } from "src/app/models/models-request/DatosContactoRequest.model";
import { OnChanges } from "@angular/core";

@Component({
  selector: "app-modal-contacto-empleo",
  templateUrl: "./modal-contacto-empleo.component.html",
  styleUrls: ["./modal-contacto-empleo.component.css"]
})
export class ModalContactoEmpleoComponent
  implements OnInit, OnChanges, AfterViewChecked {
  @Input() openModal: boolean;
  @Input() empleo: DatosEmpleoModel;
  @Output() dataContactoEmpleo: EventEmitter<{
    isSave: boolean;
    idContacto: number;
  }>;

  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;

  contactoEmpleo: ContactoModel;
  esNuevo: boolean = false;

  generoF: boolean = false;
  generoM: boolean = false;

  constructor(
    private _solcitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataContactoEmpleo = new EventEmitter();
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log("Changes", changes);
    if (changes.openModal != undefined) {
      if (changes.openModal.currentValue) {
        if (this.empleo == undefined || this.empleo == null) {
          console.log("nuevo contacto empleo");
          this.esNuevo = true;
          this.contactoEmpleo = new ContactoModel();
        } else {
          this.esNuevo = false;
          console.log("es edicion contacto empleo", this.edtNombre);
          this.contactoEmpleo = this.empleo.contactoEmpleo;
        }
      }
    }
  }

  ngAfterViewChecked() {
    if (this.contactoEmpleo != undefined && !this.esNuevo) {
      this.autoSelect();
    }
  }

  autoSelect() {
    this.edtNombre.nativeElement.value = this.contactoEmpleo.nombres;
    this.edtApellidoP.nativeElement.value = this.contactoEmpleo.primerApellido;
    this.edtApellidoM.nativeElement.value = this.contactoEmpleo.segundoApellido;
    this.onClickGenero(this.contactoEmpleo.genero);
  }

  onClickGenero(genero: string) {
    if (this.contactoEmpleo.genero.includes("M")) {
      this.generoF = false;
      this.generoM = true;
    } else if (this.contactoEmpleo.genero.includes("F")) {
      this.generoF = true;
      this.generoM = false;
    }
  }

  setContactoEmpleoCliente() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._solcitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 6;
    contacto.idTipoRelacionContacto = -1;
    //contacto = this.empleo.empleo.idEmpleo;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.correo = "NA";
    contacto.rfc = "NA";
    contacto.fechaNacimiento = "1900-01-01";

    if (this.generoF) {
      contacto.genero = "F";
    } else {
      contacto.genero = "M";
    }
    console.log("Envio", contacto);
    this._solcitudService.cargandSolicitud=true;
    this._solcitudService.agregaContactoClientSolicitud(contacto).subscribe(
      resp => {
        this.toaster.success("Contacto guardado", "Contacto Empleo");
        console.log("Repsuesta", resp);
        this._solcitudService.cargandSolicitud=false;
        

        this.closeModal(true,resp.idContacto);
      },
      err => {
        this._solcitudService.cargandSolicitud=false;
        this.toaster.error(err.message, "Error: Contacto Empleo");
        console.log(err);
      }
    );
  }

  closeModal(isSave:boolean,idContacto: number) {
    console.log("onCloseModal");
    console.log(this.empleo);
    this.dataContactoEmpleo.emit({ isSave: isSave, idContacto });
    this.openModal = false;
  }
}
