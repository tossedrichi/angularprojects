import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { FileItem } from "src/app/models/file-item";
import { TiposDocumentosModel } from "src/app/models/TiposDocumentos.model";
import { SolcitudService } from "src/app/services/solcitud.service";
import { DocumentoRequestModel } from "src/app/models/models-request/DocumentoRequest.model";
import { isUndefined, isNullOrUndefined } from "util";
import { renderImage } from "src/app/tools";
import { FormControl, FormGroup, FormArray } from "@angular/forms";
import { DocumetnosDomicilioModel } from "src/app/models/DocumentosDomicilioModel";
import { TiposComprobanteIngresosModel } from "../../../models/TiposComprobanteIngresos.model";
import { ToastrService } from "ngx-toastr";
import { EmpleoModel } from "../../../models/Empleo.model";
import { DocumentoEmpleo } from "../../../models/DocumentoEmpleoModel";
import { DatosEmpleoModel } from "../../../models/DatosEmpleo.model";
import { DataInputModalGuardarModel } from "../../../components/modal-guardar/dataInputModa.model";

@Component({
  selector: "app-modal-documentos-empleo",
  templateUrl: "./modal-documentos-empleo.component.html",
  styleUrls: ["./modal-documentos-empleo.component.css"]
})
export class ModalDocumentosEmpleoComponent implements OnInit, OnChanges {
  @Input() openModal: boolean;
  @Input() empleoSeleccionado: DatosEmpleoModel;
  @Output() dataDocumentosEmpleo: EventEmitter<{
    open: boolean;
    data?: any;
  }>;

  form: FormGroup;
  listArchivosNuevos: FileItem[];
  guardando: boolean = false;
  preView = false;
  archivoSeleccionado: FileItem;
  estaSobreDrop: boolean = false;
  documetosDomicilio: DocumentoEmpleo[];
  documentoSeleccionado: DocumentoEmpleo;
  listArchivos: FileItem[] = [];

  listTiposComprobantesIngresos: TiposComprobanteIngresosModel[] = [];

  openModalConfirm: boolean = false;
  dataInputModalConfirm: DataInputModalGuardarModel;
  instanceModalConfirm: number;

  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataDocumentosEmpleo = new EventEmitter();
  }

  ngOnInit() {
    this.initForm();

    this.getTiposComprobantesDomicilio();
    this.cargarDocumentos();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.listArchivos = [];

    console.log("changes", changes);
    if (!isNullOrUndefined(this.empleoSeleccionado)) {
      console.log("Cargando Documentos");

      //this.empleoSeleccionado = changes.empleoSeleccionado.currentValue;
      this.cargarDocumentos();
    }
  }

  initForm() {
    this.form = new FormGroup({
      selecTipoComprobante: new FormArray([])
    });
  }

  addSelectTipoComprobante(idTipoComprobante: string, isCarga: boolean) {
    if (isCarga) {
      (<FormArray>this.form.controls["selecTipoComprobante"]).push(
        new FormControl(idTipoComprobante)
      );
    } else {
      (<FormArray>this.form.controls["selecTipoComprobante"]).insert(
        0,
        new FormControl(idTipoComprobante)
      );
    }
  }

  cargarDocumentos() {
    this.listArchivos = [];

    let empleos: DatosEmpleoModel[] = this._SolicitudService.solicitudCompleta
      .infoCliente.empleosCliente;
    console.log("Empleo Seleccionado", this.empleoSeleccionado);

    console.log("Empleos", empleos);

    // console.log(empleos.filter(x => x.empleo.idEmpleo == this.empleoSeleccionado.idEmpleo)[0]);
    if (
      empleos.filter(x => x.empleo.idEmpleo == this.empleoSeleccionado.empleo.idEmpleo)
        .length > 0
    ) {
      this.documetosDomicilio = empleos.filter(
        x => x.empleo.idEmpleo == this.empleoSeleccionado.empleo.idEmpleo
      )[0].documentosEmpleo;

      this.documetosDomicilio.forEach(documento => {
        documento.stream = renderImage(documento.stream);

        let archivo = new FileItem(
          new File([], documento.fileName),
          documento.stream
        );
        archivo.idDocumento = documento.idDocumento;
        archivo.idTipoDocumento = documento.idTipoDocumento;
        archivo.idTipoComprobanteIngreso = documento.idTipoComprobanteIngreso;
        this.listArchivos.push(archivo);

        this.addSelectTipoComprobante(
          documento.idTipoComprobanteIngreso.toString(),
          true
        );
      });
      console.log(this.form);
    } else {
      this.listArchivos = [];
    }
  }

  getTiposComprobantesDomicilio() {
    this._SolicitudService.getComprobanteIngresos().subscribe(response => {
      this.listTiposComprobantesIngresos = response;
    });
  }
  validarDocumentos(archivos: FileItem[]): boolean {
    let pasa: boolean = true;
    archivos.forEach(x => {
      if (x.idTipoComprobanteIngreso > 0) {
      } else {
        this.toaster.error(
          "algunos documentos no tienen tipo de Comprobante",
          "No se puede subir el archivo"
        );
        pasa = false;
      }
    });
    return pasa;
  }

  setDocumentos() {
    this.guardando = true;

    this.listArchivosNuevos = [];
    this.listArchivosNuevos = this.listArchivos.filter(
      x => x.idDocumento <= 0 || isUndefined(x.idDocumento)
    );
    if (this.validarDocumentos(this.listArchivosNuevos)) {
      console.log("Todos los archivos", this.listArchivos);

      console.log("Archivos Nuevos", this.listArchivosNuevos);

      this.guardaDocumento(0);
    }
  }

  guardaDocumento(pos: number) {
    if (pos < this.listArchivosNuevos.length) {
      let documento: DocumentoRequestModel = new DocumentoRequestModel();

      let base64 = this.listArchivosNuevos[pos].base64.split("base64,")[1];

      documento.idSolicitud = this._SolicitudService.solicitudCompleta.idSolicitud;
      documento.idCliente = this._SolicitudService.cliente.idCliente;
      documento.streamFile = base64;
      documento.idTipoDocumento = 3;

      documento.name = this.listArchivosNuevos[pos].nombreArchivo;
      documento.idEmpleo = this.empleoSeleccionado.empleo.idEmpleo;
      documento.idTipoComprobanteIngreso = this.listArchivosNuevos[
        pos
      ].idTipoComprobanteIngreso;
      this._SolicitudService.cargandSolicitud = true;
      console.log("enviado", documento);

      this._SolicitudService.agregaDocumentoSolicitud(documento).subscribe(
        response => {
          console.log(response);

          let documentoNuevo: DocumetnosDomicilioModel = new DocumetnosDomicilioModel();
          documentoNuevo.idDocumento = response.idDocumento;
          documentoNuevo.fileName = response.name;
          documentoNuevo.stream = response.streamFile;
          documento.idTipoComprobanteIngreso =
            response.idTipoComprobanteIngreso;
          documento.idEmpleo = response.idEmpleo;

          this._SolicitudService.solicitudCompleta.infoCliente.empleosCliente
            .filter(
              x => x.empleo.idEmpleo == this.empleoSeleccionado.empleo.idEmpleo
            )[0]
            .documentosEmpleo.push(documentoNuevo);
            
          this.guardaDocumento(pos + 1);
          this.toaster.success(
            "Documento guardado",
            "Documento: " + documento.name
          );
        },
        err => {
          this.guardaDocumento(pos + 1);
          this.toaster.error(
            err.message,
            "Error: Documento " + '"' + documento.name + '"'
          );
          console.log("Error al Enviar Documentos", err);
        }
      );
    }else{
      this.cargaSolicitudCompleta();

    }
  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }

  closeModal() {
    this.empleoSeleccionado = undefined;
    this.openModal = false;
    this.dataDocumentosEmpleo.emit({ open: false });
  }

  openPreView(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.preView = true;
  }

  selectImage(evento: any) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let file = new FileItem(archivoTemporal, base64.toString());
      this.listArchivos.unshift(file);
      this.addSelectTipoComprobante("-1", false);
      console.log("form", this.form);
    };
  }

  onDrag(event: any) {
    this.estaSobreDrop = event;
  }

  onDropArchivoNuevo(archivo: FileItem) {
    //this.lis
    this.addSelectTipoComprobante("-1", false);
    this.listArchivos.unshift(archivo);
  }

  tipoComprobante(idComprobante: number): number {
    if (idComprobante > 0) {
      let tipo: number = this.listTiposComprobantesIngresos.filter(
        x => x.id_tipo_comprobante_ingreso == idComprobante
      )[0].id_tipo_comprobante_ingreso;

      return tipo;
    }
    console.log("return", -1);

    return -1;
  }

  onClickEliminarDocumento(documento: DocumentoEmpleo) {
    /**
     * NOTE Esta funcion abre el modal de confirmacion
     * el cual responde en la funcion onConfirmModal
     * @param empleo
     */

    this.documentoSeleccionado = documento;
    this.instanceModalConfirm = 1;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Eliminar Documento",
      "¿Deseas eliminar el Documento?",
      []
    );
    console.log(documento);
    console.log(this.listArchivos);
  }

  onConfirmModal(event: any) {
    this.openModalConfirm = false;

    if (event.save) {
      switch (this.instanceModalConfirm) {
        case 1:
          this.setEliminarDocumento();
          break;
      }
    }
  }

  setEliminarDocumento() {
    this._SolicitudService.cargandSolicitud = true;
    this._SolicitudService
      .setEliminarDocumentoSolicitud(this.documentoSeleccionado.idDocumento)
      .subscribe(
        resp => {
          this.cargaSolicitudCompleta();

          console.log(resp);
          this.toaster.success("Documento eliminado", "Eliminar Documento");
        },
        err => {
          this._SolicitudService.cargandSolicitud = false;
          console.log(err);
          this.toaster.error(err.message, "Error: Eliminar Documento");
        }
      );
  }

  cargaSolicitudCompleta(){
    this._SolicitudService
    .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
    .subscribe(resp => {
      this.cargarDocumentos();
      this._SolicitudService.cargandSolicitud=false;
    },error=>{
      this._SolicitudService.cargandSolicitud=false;
    });
  }
}
