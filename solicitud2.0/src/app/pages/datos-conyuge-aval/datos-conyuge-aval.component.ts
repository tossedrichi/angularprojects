import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SolcitudService } from '../../services/solcitud.service';
import { TiposRelacionesModel } from '../../models/TiposRelaciones';
import { FileItem } from 'src/app/models/file-item';
import { DatosContactoModel } from '../../models/DatosContactoModel';
import { FormGroup, FormControl } from '@angular/forms';
import { ContactoModel } from '../../models/Contacto.model';
import { ConyugeModel } from '../../models/Conyuge.model';
import { INEModel } from 'src/app/models/INE.model';
import { generarRFC, INEFix } from 'src/app/tools';
import { isUndefined, isNull } from 'util';
import { DatosContactoRequestModel } from '../../models/models-request/DatosContactoRequest.model';
import { ConyugeRequestModel } from '../../models/models-request/ConyugeRequest.mosel';
import { DataInputModalGuardarModel } from 'src/app/components/modal-guardar/dataInputModa.model';
import { ToastrService } from 'ngx-toastr';
import { TelefonoRequestModel } from '../../models/models-request/TelefonoRequest.model';
import { INERequestModel } from 'src/app/models/models-request/INEReques.model';

@Component({
  selector: 'app-datos-conyuge-aval',
  templateUrl: './datos-conyuge-aval.component.html',
  styleUrls: ['./datos-conyuge-aval.component.css']
})
export class DatosConyugeAvalComponent implements OnInit {
  formularioDatosConyugeAval:FormGroup;
  dataInput: DataInputModalGuardarModel;
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoPaterno") edtApellidoPaterno: ElementRef;
  @ViewChild("edtApellidoMaterno") edtApellidoMaterno: ElementRef;
  @ViewChild("edtFechaNacimiento") edtFechaNacimiento: ElementRef;
  @ViewChild("selectEdo") selectEdo: ElementRef;
  @ViewChild("telefonoMovil") telefonoMovil: ElementRef;
  openModalGuardar:boolean= false;
  openModalEmpleoConyugeAval: boolean = false;
  empleoConyugeAval: boolean = false;
  estaSobreDrop: boolean = false;
  listArchivos: FileItem[] = [];
  listArchivos2: FileItem[] = [];
  datos:ConyugeModel= new ConyugeModel();
  generoF: boolean = false;
  generoM: boolean = false;
  ineFrente: FileItem;
  ineReverso: FileItem;
  image: string;
  
  guardoDatos:boolean= false;
  guardoDatosExitoso:boolean= false;

  guardoIne:boolean= false;
  guardoIneExitoso:boolean= false;

  guardoCel:boolean= false;
  guardoCelExitoso:boolean= false;

  listTipoRelaciones: TiposRelacionesModel[] = [];
  constructor(private _SolicitudService: SolcitudService, private toaster: ToastrService) { }

  ngOnInit() {
    this.getTiposRelaciones();
    try { 
      this.datos= this.getDatos().conyugeContacto;
      if(isNull(this.datos) || isUndefined(this.datos)){
        this.datos= new ConyugeModel();
        this.datos.contacto= new ContactoModel();
        this.datos.contacto.nombres="";
        this.datos.contacto.primerApellido="";
        this.datos.contacto.segundoApellido="";
        this.datos.contacto.rfc="";
        this.datos.contacto.idContacto=0;
      }
    } catch (error) {
      this.datos.contacto= new ContactoModel();
      this.datos.contacto.nombres="";
      this.datos.contacto.primerApellido="";
      this.datos.contacto.segundoApellido="";
      this.datos.contacto.rfc="";
      this.datos.contacto.idContacto=0;
    }
    
    this.autoSelect();
    console.log("datos",this.datos)
    this.formularioDatosConyugeAval = new FormGroup({
      fechaNacimiento: new FormControl(this.getFecha()),
      rfc: new FormControl(this.datos.contacto.rfc),

      estadoCivil: new FormControl(this.datos.contacto.idTipoEstadoCivil),
      genero: new FormControl(this.datos.contacto.genero),
      telefonoCelular: new FormControl(this.getTelefono()),
     
      correo: new FormControl(this.datos.contacto.correo),
      cuentaEmpleo: new FormControl(this.tieneEmpleo())
      //empleo: new FormControl(this._solicitudServices.reqDatosEmpleoCliente)
    });
    console.log(this.formularioDatosConyugeAval);
  
  }

  getFecha(){
    if(isUndefined(this.datos.contacto.fechaNacimiento)){
      return "1900-01-01"
    }else{
      return this.datos.contacto.fechaNacimiento.split(" ")[0]
    }
  }
  renderIne(isFrente: boolean) {
    //console.log("renderINE");

    if (this.datos.infoINE != null) {
      let ine: INEModel = this.datos.infoINE;
      if (isFrente) {
        if (ine.streamFrente != null && this.ineFrente == null) {
         // console.log("Existe Archivo Viejo");
          this.ineFrente = new FileItem(new File([], ""), "");

          this.ineFrente.nombreArchivo = ine.fileNameFrente;
          this.ineFrente.base64 = INEFix + ine.streamFrente;
          this.ineFrente.idDocumento= ine.idCredencial;
          console.log("inefrente",ine);
          console.log("inefrente",this.ineFrente);
          return INEFix + ine.streamFrente;
        } else if (this.ineFrente != null) {
          
         // console.log("Existe Archivo Nuevo");
         
          return this.ineFrente.base64;
        } else {
         // console.log("No Existe Archivo");
          this.image = "assets/fren.png";
          return "assets/fren.png";
        }
      } else {
        if (ine.streamReverso != null && this.ineReverso == null) {
          this.ineReverso = new FileItem(new File([], ""), "");
          this.ineReverso.nombreArchivo = ine.fileNameReverso;
          this.ineReverso.base64 = INEFix + ine.streamReverso;
          this.ineReverso.idDocumento= ine.idCredencial;
          
          return INEFix + ine.streamReverso;
        } else if (this.ineReverso != null) {
         
          return this.ineReverso.base64;
        } else {
          return "assets/rever.png";
        }
      }
    } else if (isFrente) {
      //console.log("es frente");
     // console.log(this.ineFrente);

      if (this.ineFrente != null) {
       // console.log("Existe Archivo Nuevo");
        return this.ineFrente.base64;
      } else {
        //console.log("asset");

        return "assets/fren.png";
      }
    } else if (!isFrente) {
      if (this.ineReverso != null) {
        return this.ineReverso.base64;
      } else {
        return "assets/rever.png";
      }
    }
  }

  tieneEmpleo():boolean{
   return this.datos.infoEmpleosContacto.length>0;
  }
 
  getTelefono():String{
    let cel:String="";
    this.datos.telefonosContacto.forEach(x=>{
      if(x.estadoActual.includes("Activo")&& x.idTipoTelefono==2){
        cel=x.numeroTelefonico;
      }
    })
    
    return cel;
  }
  autoSelect() {
    try {
      if (this.datos.contacto.genero.includes("M")) {
        this.generoM = true;
        this.generoF = false;
      } else if (this.datos.contacto.genero.includes("F")) {
        this.generoF = true;
        this.generoM = false;
      }
    } catch (error) {
      
    }
  
  }
  getTiposRelaciones() {
    this._SolicitudService.getTiposRelaciones().subscribe(response => {
      this.listTipoRelaciones = response;
    })
  }
  respDataModalEmpleoConyugeAval(event) {
    console.log(event);
    if (event.isCancel) {
      this.openModalEmpleoConyugeAval = event.open;
    }
  }
  requiereEmpleo(req:boolean){
    this.empleoConyugeAval=req;
  }
  selectImage(evento: any, isFrente) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let ine: INEModel = new INEModel();
      if (isFrente) {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamFrente = file.base64;
        //this._SolicitudService.solicitudCompleta.infoCliente.ine = ine;
        this.ineFrente = file;
      } else {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamReverso = file.base64;
        //this._SolicitudService.solicitudCompleta.infoCliente.ine = ine;
        this.ineReverso = file;
      }
    };
  }
  getDatos():DatosContactoModel{
    return this._SolicitudService.getDatosContacto(3);
  }
  generaRfc() {
    
   this.datos.contacto.rfc = generarRFC(
      this.edtNombre.nativeElement.value,
      this.edtApellidoPaterno.nativeElement.value,
      this.edtApellidoMaterno.nativeElement.value,
      this.edtFechaNacimiento.nativeElement.value
    );

   this.formularioDatosConyugeAval.get("rfc").setValue(this.datos.contacto.rfc);
  }

  guardar(){
 

 
    if(this.datos.contacto.idContacto==0){
      let conyugeCl: ConyugeRequestModel = new ConyugeRequestModel();

      conyugeCl.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
      conyugeCl.idTipoContacto = 5;
      conyugeCl.nombres = this.edtNombre.nativeElement.value;
      conyugeCl.primerApellido = this.edtApellidoPaterno.nativeElement.value;
      conyugeCl.segundoApellido = this.edtApellidoMaterno.nativeElement.value;
      conyugeCl.fechaNacimiento = this.edtFechaNacimiento.nativeElement.value;
      conyugeCl.genero = this.formularioDatosConyugeAval.get("genero").value;
      conyugeCl.rfc=this.datos.contacto.rfc;
      conyugeCl.agente = this._SolicitudService.solicitud.idAgente;
      conyugeCl.aportaIngresos = false;
      conyugeCl.correo = "na";
      conyugeCl.idTipoEstadoCivil=-1;
      conyugeCl.importeIngresos = 0;
      conyugeCl.idOrigen=-1;
      conyugeCl.idTipoContactoIngreso = -1;
      conyugeCl.idTipoContacto = 5;
      conyugeCl.idTipoRelacionEmpleo = -1;
      conyugeCl.idContacto =this.getDatos().contacto.idContacto;
      this._SolicitudService.cargandSolicitud=true;
      console.log("Guardar nuevo",conyugeCl)
      this.guardoDatos=true;
      this._SolicitudService.agregaConyugeSolicitud(conyugeCl).subscribe(
        resp => { 
          console.log(resp);
          if(resp.idConyuge>0){

            this.datos.contacto.idContacto=resp.idConyuge
            this.guardoDatosExitoso=true;
            this._SolicitudService.cargandSolicitud=false;
            if(!isUndefined(this.ineFrente)||!isUndefined(this.ineReverso)){
              if(isUndefined(this.ineFrente.idDocumento)||isUndefined(this.ineReverso.idDocumento)){
                this.agregarCredencial();
              }else{
                //guardar cel
                this.guardarTelefonos();
              }
            }else{
              //gguardar cel
              this.guardarTelefonos();
            }
           
          }else{
            this.guardoDatosExitoso=false;
            this._SolicitudService.cargandSolicitud=false;
          }
         
        },
        err => {
          this.guardoDatosExitoso=false;
          console.log(err);
          this._SolicitudService.cargandSolicitud=false;
        }
      )
    }else{
      let conyugeCl: DatosContactoRequestModel = new DatosContactoRequestModel();

      conyugeCl.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
      conyugeCl.idTipoContacto = 5;
      conyugeCl.nombres = this.edtNombre.nativeElement.value;
      conyugeCl.primerApellido = this.edtApellidoPaterno.nativeElement.value;
      conyugeCl.segundoApellido = this.edtApellidoMaterno.nativeElement.value;
      conyugeCl.fechaNacimiento = this.edtFechaNacimiento.nativeElement.value;
      conyugeCl.genero = this.formularioDatosConyugeAval.get("genero").value;
      conyugeCl.rfc=this.datos.contacto.rfc;
      conyugeCl.agente = this._SolicitudService.solicitud.idAgente;
      conyugeCl.aportaIngresos = false;
      conyugeCl.correo = "na";
      conyugeCl.idTipoEstadoCivil=-1;
      conyugeCl.importeIngresos = 0;
      conyugeCl.idOrigen=-1;
      conyugeCl.idTipoContactoIngreso = -1;
      conyugeCl.idTipoContacto = 5;
      conyugeCl.idTipoRelacionEmpleo = -1;
      conyugeCl.idContacto =this.datos.contacto.idContacto;;
      console.log("Editar",conyugeCl)
      this.guardoDatos=true;
      this._SolicitudService.actualizaContactoClientSolicitud(conyugeCl).
        subscribe(response=>{
          this.guardoDatosExitoso=true;
          //this.toaster.success("Guardado exitósamente!!") 
          console.log(response);
          if(!isUndefined(this.ineFrente)||!isUndefined(this.ineReverso)){
            if(isUndefined(this.ineFrente.idDocumento)||isUndefined(this.ineReverso.idDocumento)){
              this.agregarCredencial();
            }else{
              //guardar cel
              this.guardarTelefonos();
            }
          }else{
            //gguardar cel
            this.guardarTelefonos();
          }
        },error=>{
          this.guardoDatosExitoso=false;
        });
    }
  }
  cargaSolicitud(){
    this._SolicitudService.cargandSolicitud=true;
    this._SolicitudService.getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud).subscribe(resp => {
      console.log(resp);

      if(this.guardoDatos){
        if(this.guardoDatosExitoso){
          this.toaster.success("Guardado exitósamente!!")
        }else{
          this.toaster.error("Error guardando...")
        }
      }

      if(this.guardoIne){
        if(this.guardoIneExitoso){
          this.toaster.success(
            "INE guardada correctamente"
          );
        }else{
          this.toaster.error( "Error: Ine Cliente");
        }
      }

      if(this.guardoCel){
        if(this.guardoCelExitoso){
          this.toaster.success("Telefono guardado exitósamente!!") 
        }else{
          this.toaster.error("Telefono no se guardó!!") 
        }
      }


      this.guardoDatos= false;
      this.guardoDatosExitoso= false;
    
      this.guardoIne= false;
      this.guardoIneExitoso= false;
    
      this.guardoCel= false;
      this.guardoCelExitoso= false;

      this._SolicitudService.cargandSolicitud=false;
      this.datos= this.getDatos().conyugeContacto;
      //this.listarContactos();
    },error=>{
      this._SolicitudService.cargandSolicitud=false;
    });
  }

  onSaveClick() {
    if ((!isUndefined(this.ineReverso) && isUndefined(this.ineFrente))||isUndefined(this.ineReverso) && !isUndefined(this.ineFrente)) {
      this.toaster.error("Falta completar llenado de la INE")
    }else{
    this.dataInput = new DataInputModalGuardarModel(
      "Guardar Cambios",
      "¿Desea guardar los cambios del Conyuge del aval?",
      []
    );
    this.openModalGuardar = true;
    }
  }
  guardarTelefonos(){
    if(this.getTelefono().localeCompare(this.telefonoMovil.nativeElement.value)!=0){
      this._SolicitudService.cargandSolicitud=true;
      console.log("guardar telefono: viejo ",this.getTelefono()+" nuevo "+this.telefonoMovil.nativeElement.value)
      let telefono:TelefonoRequestModel= new TelefonoRequestModel();
      telefono.agente= SolcitudService.agente;
      telefono.idContacto= this.datos.contacto.idContacto;
      telefono.idOrigen=-1;
      telefono.idSucursal= this._SolicitudService.solicitud.idSucursal;
      telefono.idTipoTelefono=2;
      telefono.telefono=this.telefonoMovil.nativeElement.value;
      telefono.idSolicitud=this._SolicitudService.solicitud.idSolicitud;
     // console.log("guarda",telefono);
     this.guardoCel=true;
      this._SolicitudService.agregaTelefonoContactoClienteSolicitud(telefono).
      subscribe(response=>{
        this._SolicitudService.cargandSolicitud=false;
        this.cargaSolicitud();
        if(response.idTelefono>0){
          this.guardoCelExitoso=true;
          
        }else{
         this.guardoCelExitoso=false
        }
      },error=>{
        this._SolicitudService.cargandSolicitud=false;
        this.guardoCelExitoso=false;
      });


    }else{
      this.cargaSolicitud();
    }
  }
  agregarCredencial(){
    console.log("TAG",this.ineFrente)
    this._SolicitudService.cargandSolicitud=true;
          let ineAnverso;
    if (this.ineFrente.base64.includes("base64")) {
      ineAnverso = this.ineFrente.base64.split("base64,")[1];
    } else {
      ineAnverso = this.ineFrente.base64;
    }

    let ineReverso;
    if (this.ineReverso.base64.includes("base64")) {
      ineReverso = this.ineReverso.base64.split("base64,")[1];
    } else {
      ineReverso = this.ineReverso.base64;
    }

    let ineCliente: INERequestModel = new INERequestModel();

    ineCliente.idSolicitud = this._SolicitudService.solicitudCompleta.idSolicitud;
    ineCliente.streamAnverso = ineAnverso;
    ineCliente.fileNameAnverso = this.ineFrente.nombreArchivo;
    ineCliente.streamReverso = ineReverso;
    ineCliente.fileNameReverso = this.ineReverso.nombreArchivo;
    ineCliente.idContacto= this.datos.contacto.idContacto;
    console.log("INE ENVIADA",ineCliente);
    this.guardoIne=true;
    this._SolicitudService.agregaCredencialContactoSolicitud(ineCliente).subscribe(
      resp => {
        console.log("Guardado INE", resp); 
          this.ineFrente.idDocumento= resp.idCredencial;
          this.ineReverso.idDocumento= resp.idCredencial;
           this.guardoIneExitoso=true;
        this._SolicitudService.cargandSolicitud=false;
        this.guardarTelefonos();
      },
      err => {
        this._SolicitudService.cargandSolicitud=false;
        console.log(err);
        this.guardoIneExitoso=false
       
      }
    );
    

  }
  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      console.log("GUARDAR", event.save);
      this.guardar();

    }
  }
 
}
