import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';
import { FileItem } from 'src/app/models/file-item';
import { EmpleosModel } from 'src/app/models/Empleos.model';
import { OtrosIngresosModel } from 'src/app/models/OtrosIngresos.model';

@Component({
  selector: 'app-modal-empleo-conyuge-aval',
  templateUrl: './modal-empleo-conyuge-aval.component.html',
  styleUrls: ['./modal-empleo-conyuge-aval.component.css']
})
export class ModalEmpleoConyugeAvalComponent implements OnInit {
  @Input() openModal: boolean
  @Output() dataEmpleoConyugeAval: EventEmitter<{open:boolean,data?:any}>;

  empresa: boolean = true;
  empleo: boolean = true;
  puesto: boolean = true;
  telefono: boolean = true;
  tipoEmpleo: boolean = true;
  tipoIngreso: boolean = false;
  
  listEmpleos: EmpleosModel[] = [];
  listOtrosIngresos: OtrosIngresosModel []=[];
  estaSobreDrop: boolean = false;

  listArchivos: FileItem[] = [];

  constructor(private _solicitudService: SolcitudService) { 
    this.dataEmpleoConyugeAval = new EventEmitter();

  }

  ngOnInit() {
    this.getEmpleos();
    this.getOtrosIngresos();

  }

eliminarArchivo(archivo: FileItem) {
  this.listArchivos = this.listArchivos.filter(x => x != archivo);
}

onClickLimpiar() {
  this.listArchivos = [];
}
closeModal() {
  this.openModal = false;
  this.dataEmpleoConyugeAval.emit({open:false})
}
getEmpleos() {
  this._solicitudService.getEmpleos().subscribe(respuesta => {
    console.log(respuesta);
    this.listEmpleos = respuesta;

  })
}
getOtrosIngresos() {
  this._solicitudService.getOtrosIngresos().subscribe(respuesta => {
    console.log(respuesta);
    this.listOtrosIngresos = respuesta;

  })
}
onSelectTipoEmpleo(idTipoEmpleo: string) {
  console.log(idTipoEmpleo);

  switch (parseInt(idTipoEmpleo)) {
    case 1:
      console.log("Formal");
      this.empresa= true;
      this.empleo = true;
      this.puesto = true;
      this.telefono =  true;
      this.tipoEmpleo = true;
      this.tipoIngreso = false;
      break;
      case 2:
      console.log("Informal");
      this.empresa= true;
      this.empleo = true;
      this.puesto = true;
      this.telefono =  true;
      this.tipoEmpleo = true;
      this.tipoIngreso = false;
      break;
      case 3:
      console.log("Negocio propio");
      this.empresa= true;
      this.empleo = true;
      this.puesto = true;
      this.telefono =  true;
      this.tipoEmpleo = true;
      this.tipoIngreso = false;
      break;
    case 4:
      console.log("Otros ingresos");
      this.empresa= false;
      this.empleo = false;
      this.puesto = false;
      this.telefono =  false;
      this.tipoEmpleo = true;
      this.tipoIngreso = true;
      break;
   

  }
}

onClickOtroEmpleo(){
  this.empresa= true;
  this.empleo = true;
  this.puesto = true;
  this.telefono =  true;
  this.tipoEmpleo = true;
  this.tipoIngreso = false;
}
}