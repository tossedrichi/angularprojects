import { OnInit, Component, ElementRef, ViewChild } from "@angular/core";
import { SolcitudService } from "../../services/solcitud.service";
import { EstadosModel } from "src/app/models/Estados.model";
import { CiudadesModel } from "src/app/models/Ciudades.model";
import { ColoniasModel } from "src/app/models/Colonias.model";
import { TiposDomiciliosModel } from "src/app/models/TiposDomicilio.model";
import { CondicionesModel } from "src/app/models/CondicionesDomicilio.model";
import { RelacionCasaFamiliarModel } from "src/app/models/RelacionCasaFamiliar.model";
import { TiposComprobanteDomicilioModel } from "src/app/models/TiposComprobanteDomicilio.model";
import { DomicilioClienteRequestModel } from "src/app/models/models-request/DomicilioClienteRequest.model";
import { FormGroup, FormControl } from "@angular/forms";
import { DomicilioModel } from "../../models/Domicilio.Model";
import { isNull, log, isNullOrUndefined } from "util";
import { ToastrService } from "ngx-toastr";
import { FileItem } from "src/app/models/file-item";
import { DocumetnosDomicilioModel } from "src/app/models/DocumentosDomicilioModel";
import { renderImage, calcularTiempoMeses } from "src/app/tools";
import { DataInputModalGuardarModel } from "../../components/modal-guardar/dataInputModa.model";

@Component({
  selector: "app-domicilio-cliente",
  templateUrl: "./domicilio-cliente.component.html",
  styleUrls: ["./domicilio-cliente.component.css"]
})
export class DomicilioClienteComponent implements OnInit {
  CTRL_ESTADO = "ctrlEstado";
  CTRL_CIUDAD = "ctrlCiudad";
  //CTRL_COLONIA = "ctrlColonia";
  CTRL_CALLE = "ctrlCalle";
  CTRL_CP = "ctrlCP";
  CTRL_NUM_EXT = "ctrNumExt";
  CTRL_MUM_INT = "ctrlNumInt";
  CTRL_TIPO_DOMCILIO = "ctrlTipoDomicilio";
  CTRL_ANOS = "ctrlAnos";
  CTRL_MESES = "ctrlMeses";
  CTRL_PERSONAS_DOMICILIO = "ctrlPersonasDom";
  CTRL_CONDICIONES_DOM = "ctrlCondicionesDom";
  CTRL_REFERENCIAS = "ctrlReferencias";
  coloniaSeleccionada: ColoniasModel = new ColoniasModel();
  @ViewChild("toast") toast: any;

  formDomicilio: FormGroup;

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];
  listTiposDomicilio: TiposDomiciliosModel[] = [];
  listCondiciones: CondicionesModel[] = [];
  listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[] = [];
  listComprobanteDomicilio: TiposComprobanteDomicilioModel[] = [];
  //listDocumentosDomicilio: [] = [];

  dataInputuModalGuardar: DataInputModalGuardarModel;

  domicilio: DomicilioModel;
  openModalPrew: boolean = false;
  openModalGuardar: boolean = false;
  openModalCasaFamiliar: boolean = false;
  openModalCasaRenta: boolean = false;
  openModalDocumentosDomicilio: boolean = false;
  esCasaFamiliar: boolean = false;
  documentosDom: boolean = false;
  ver: boolean = false;

  archivoSeleccionado: FileItem;
  estaSobreDrop: boolean = false;
  documetosDomicilio: DocumetnosDomicilioModel[];

  listArchivos: FileItem[] = [];
  listTiposComprobantesDomicilio: TiposComprobanteDomicilioModel[] = [];

  constructor(
    private _solicitudService: SolcitudService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.copyModel();
    this.initForm();
    this.initServ();
    this.llenarData();
    this.cargarDocumentos();
  }
  limpiarForm() {
    this.formDomicilio.reset();
  }

  initServ() {
    this.getEstados();
    console.log("Carga Estados");

    this.getTiposDomicilios();
    console.log("Carga Domicilios");

    this.getCondiciones();
    console.log("Carga Condiciones");
  }

  initForm() {
    this.formDomicilio = new FormGroup({
      ctrlEstado: new FormControl("-1"),
      ctrlCiudad: new FormControl("-1"),
      //ctrlColonia: new FormControl("-1"),
      ctrlCalle: new FormControl(),
      ctrlCP: new FormControl(),
      ctrNumExt: new FormControl(),
      ctrlNumInt: new FormControl(),
      ctrlTipoDomicilio: new FormControl(),
      ctrlAnos: new FormControl(),
      ctrlMeses: new FormControl(),
      ctrlPersonasDom: new FormControl(),
      ctrlCondicionesDom: new FormControl(),
      ctrlReferencias: new FormControl()
    });
  }

  llenarData() {
    if (this.domicilio != null && this.domicilio != undefined) {
      if (!isNullOrUndefined(this.domicilio.idCiudad)) {
        this.getEstadoCiudad(this.domicilio.idCiudad);
      }

      if(this.domicilio.idTipoDomicilio == 2 || this.domicilio.idTipoDomicilio == 3){
        this.ver=true;
      }

      this.coloniaSeleccionada.nombre = this.domicilio.colonia;
      this.coloniaSeleccionada.sepomex = this.domicilio.codigoSepomex;

      this.formDomicilio.get(this.CTRL_CALLE).setValue(this.domicilio.calle);
      this.formDomicilio
        .get(this.CTRL_CP)
        .setValue(this.domicilio.codigoPostal);
      this.formDomicilio
        .get(this.CTRL_NUM_EXT)
        .setValue(this.domicilio.numeroExterior);
      this.formDomicilio
        .get(this.CTRL_MUM_INT)
        .setValue(this.domicilio.numeroInterior);
      this.formDomicilio
        .get(this.CTRL_TIPO_DOMCILIO)
        .setValue(this.domicilio.idTipoDomicilio);
      console.log("que esta pasandaaaaa?" + this.domicilio.tiempoResidencia);

      this.formDomicilio.get(this.CTRL_ANOS).setValue(this.getAnos());
      this.formDomicilio.get(this.CTRL_MESES).setValue(this.getMeses());
      this.formDomicilio
        .get(this.CTRL_PERSONAS_DOMICILIO)
        .setValue(this.domicilio.habitantesDomicilio);
      this.formDomicilio
        .get(this.CTRL_CONDICIONES_DOM)
        .setValue(this.domicilio.idTipoCondicionDomicilio);
      this.formDomicilio
        .get(this.CTRL_REFERENCIAS)
        .setValue(this.domicilio.referencias);
    }
  }

  copyModel() {
    this.domicilio = this._solicitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio;
    console.log("DOMCILIO", this.domicilio);

    if (isNull(this.domicilio)) {
      this.domicilio = new DomicilioModel();
    }
  }

  getTiposComprobantesDomicilio() {
    this._solicitudService.getTiposComprobanteDomiclio().subscribe(response => {
      this.listTiposComprobantesDomicilio = response;
    });
  }

  getEstadoCiudad(idCiudad: number) {
    this._solicitudService.getEstadoCiudad(idCiudad).subscribe(
      resp => {
        console.log("Estado de  ciudad", resp);
        this.formDomicilio.get(this.CTRL_ESTADO).setValue(resp.mensaje);
        this.getCiudades(resp.mensaje);
        this.formDomicilio
          .get(this.CTRL_CIUDAD)
          .setValue(this.domicilio.idCiudad);
      },
      err => {
        this.toastr.error("Error al recuperar estado");
      }
    );
  }

  getEstados() {
    this._solicitudService.getEstados().subscribe(
      resp => {
        this.listEstados = resp;

        if (isNullOrUndefined(this.domicilio)) {
          this.formDomicilio.get(this.CTRL_ESTADO).setValue(8);
          this.getCiudades(this.formDomicilio.get(this.CTRL_ESTADO).value);
        }
        //this.formDomicilio.get(this.CTRL_ESTADO).setValue(this._solicitudService.solicitud.idSucursal);
      },
      err => {}
    );
  }

  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado);
  }

  onSelectCiudad(idCiudad: number) {
    this.getColonias(idCiudad);
  }

  onOpenPrew(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.openModalPrew = true;
  }

  onSelectTipoDomicilio(idTipoDomiclio: string) {
    console.log(idTipoDomiclio);
    this.esCasaFamiliar = false;
    switch (parseInt(idTipoDomiclio)) {
      case 1:
        console.log("Casa Propia");
        this.openModalCasaFamiliar = false;
        this.esCasaFamiliar = false;
        this.ver = false;
        break;
      case 2:
        console.log("Casa Familiar");
        this.openModalCasaFamiliar = true;
        this.esCasaFamiliar = true;
        this.ver = true;
        break;
      case 3:
        console.log("Casa Renta");
        this.openModalCasaRenta = true;
        this.esCasaFamiliar = false;
        this.ver = true;

        break;
      case 4:
        console.log("Casa Propia");
        this.openModalCasaFamiliar = false;
        this.esCasaFamiliar = false;
        this.ver = false;
        break;
    }
  }

  getCiudades(idEstado) {
    this.listColonias = [];

    this._solicitudService.getCiudades(idEstado).subscribe(respuesta => {
      console.log(respuesta);
      this.listCiudades = respuesta;
      console.log(this.domicilio.idCiudad);

      if (this.listCiudades.find(x => x.municipio == this.domicilio.idCiudad)) {
        this.formDomicilio
          .get(this.CTRL_CIUDAD)
          .setValue(this.domicilio.idCiudad);
      } else {
        this.formDomicilio.get(this.CTRL_CIUDAD).setValue("-1");
      }

      this.getColonias(this.formDomicilio.get(this.CTRL_CIUDAD).value);
    });
  }

  on(event) {
    console.log(event);
  }

  onClickSave() {
    this.dataInputuModalGuardar = new DataInputModalGuardarModel(
      "Domicilio Cliente",
      "¿Desea guardar los datos?",
      []
    );
    this.openModalGuardar = true;
  }

  getColonias(ciudad) {
    this.listColonias = [];
    this._solicitudService.getColonias(ciudad).subscribe(respuesta => {
      console.log(respuesta);
      this.listColonias = respuesta;
      if (this.listColonias.find(x => x.nombre == this.domicilio.colonia)) {
        /* FIXME  this.formDomicilio
          .get(this.CTRL_COLONIA)
          .setValue(this.domicilio.colonia); */
      } else {
        /* FIXME  this.formDomicilio.get(this.CTRL_COLONIA).setValue("-1"); */
      }
    });
  }

  async getTiposDomicilios() {
    this.listTiposDomicilio = [];
    await this._solicitudService.getTiposDomicilios().subscribe(respuesta => {
      console.log(respuesta);
      this.listTiposDomicilio = respuesta;
      if (
        this.listTiposDomicilio.find(
          x => x.id_tipo_domicilio == this.domicilio.idTipoDomicilio
        )
      ) {
        this.formDomicilio
          .get(this.CTRL_TIPO_DOMCILIO)
          .setValue(this.domicilio.idTipoDomicilio);
      } else {
        this.formDomicilio.get(this.CTRL_TIPO_DOMCILIO).setValue("-1");
      }
    });
  }

  getTiposRelacionesCasaFam() {
    this._solicitudService.getTiposRelacionesCasaFam().subscribe(respuesta => {
      console.log(respuesta);
      this.listTipoRelacionesCasaFam = respuesta;
    });
  }
  getComprobanteDomicilio() {
    this._solicitudService
      .getTiposComprobanteDomiclio()
      .subscribe(respuesta => {
        console.log(respuesta);
        this.listComprobanteDomicilio = respuesta;
      });
  }
  async getCondiciones() {
    this.listCondiciones = [];
    await this._solicitudService.getCondiciones().subscribe(respuesta => {
      console.log(respuesta);
      this.listCondiciones = respuesta;
      if (
        this.listCondiciones.find(
          x =>
            x.id_tipo_condicion_domicilio ==
            this.domicilio.idTipoCondicionDomicilio
        )
      ) {
        this.formDomicilio
          .get(this.CTRL_CONDICIONES_DOM)
          .setValue(this.domicilio.idTipoCondicionDomicilio);
      } else {
        this.formDomicilio.get(this.CTRL_CONDICIONES_DOM).setValue("-1");
      }
    });
  }

  responseDataCasaFamiliar(event) {
    console.log(event);

    if (event.isSave) {
      this.domicilio.propietarioDomicilio = event.idContactoCasa;
      this.esCasaFamiliar = true;
      this.setDomcilioCliente();
    }

    this.openModalCasaFamiliar = false;
  }

  responseDataCasaRenta(event) {
    console.log(event);
    this.domicilio.importeRenta = event.monto;

    if (!event.isCancel) {
      this.domicilio.propietarioDomicilio = -1;
      this.esCasaFamiliar = false;
      this.setDomcilioCliente();
    }
    if (event.isCancel) {
      this.openModalCasaRenta = event.open;
    }
  }
  respDataModalDocumentosDomicilio(event) {
    this.cargarDocumentos();
    console.log(event);
    if (!event.open) {
      this.openModalDocumentosDomicilio = false;
    }
  }

  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      this.setDomcilioCliente();
    }
  }

  setDomcilioCliente() {
    let domicilioRequest: DomicilioClienteRequestModel = new DomicilioClienteRequestModel();

    domicilioRequest.idSolicitud = this._solicitudService.solicitudCompleta.idSolicitud;
    domicilioRequest.idSucursal = this._solicitudService.solicitud.idSucursal;
    domicilioRequest.idCiudad = this.formDomicilio.get(this.CTRL_CIUDAD).value;

    /*  FIXME   domicilioRequest.colonia = this.formDomicilio.get(this.CTRL_COLONIA).value;
     */

    domicilioRequest.colonia = this.coloniaSeleccionada.nombre;
    domicilioRequest.codigoSepomex = this.coloniaSeleccionada.sepomex;

    domicilioRequest.calle = this.formDomicilio.get(this.CTRL_CALLE).value;
    domicilioRequest.codigoPostal = this.formDomicilio.get(this.CTRL_CP).value;
    domicilioRequest.numeroExterior = this.formDomicilio.get(
      this.CTRL_NUM_EXT
    ).value;
    domicilioRequest.numeroInterior = this.formDomicilio.get(
      this.CTRL_MUM_INT
    ).value;
    domicilioRequest.idTipoDomicilio = this.formDomicilio.get(
      this.CTRL_TIPO_DOMCILIO
    ).value;
    domicilioRequest.tiempoResidencia = calcularTiempoMeses(
      this.formDomicilio.get(this.CTRL_ANOS).value,
      this.formDomicilio.get(this.CTRL_MESES).value
    );
    domicilioRequest.habitantesDomicilio = this.formDomicilio.get(
      this.CTRL_PERSONAS_DOMICILIO
    ).value;
    domicilioRequest.idTipoCondicionDomicilio = this.formDomicilio.get(
      this.CTRL_CONDICIONES_DOM
    ).value;
    domicilioRequest.referencias = this.formDomicilio.get(
      this.CTRL_REFERENCIAS
    ).value;
    domicilioRequest.importeRenta = this.domicilio.importeRenta;

    if (this.esCasaFamiliar) {
      domicilioRequest.propieterioDomicilio = this.domicilio.propietarioDomicilio;
    }

    if (
      //domicilioRequest.idTipoDomicilio>0 &&
      domicilioRequest.idCiudad > 0 &&
      domicilioRequest.calle.localeCompare("") != 0 &&
      domicilioRequest.colonia.localeCompare("") != 0
    ) {
      this._solicitudService.cargandSolicitud = true;
      if (domicilioRequest.idTipoDomicilio < 1) {
        domicilioRequest.idTipoDomicilio = 1;
      }
      console.log("domicilio enviado", domicilioRequest);

      this._solicitudService
        .setAgregaDomicilioClienteSolicitud(domicilioRequest)
        .subscribe(
          resp => {
            if (resp.idDomicilio > 0) {
              console.log(resp);
              this._solicitudService
                .getSolicitudCompleta(
                  this._solicitudService.solicitud.idSolicitud
                )
                .subscribe(
                  resp => {
                    this.copyModel();
                    this.toastr.success(
                      "Domicilio Guardado!",
                      "Domicilio Cliente"
                    );
                    this._solicitudService.cargandSolicitud = false;
                  },
                  err => {
                    this._solicitudService.cargandSolicitud = false;

                    console.log(err);
                  }
                );
            } else {
              this._solicitudService.cargandSolicitud = false;
              this.toastr.error(
                "Error al guardar: por favor seleccione un tipo de domicilio",
                "Domicilio Cliente"
              );
            }
          },
          err => {
            this._solicitudService.cargandSolicitud = false;
            this.toastr.error(
              "Error al guardar: Ocurrió un error al guardar",
              "Domicilio Cliente"
            );

            console.log(err);
          }
        );
    } else {
      this.toastr.error(
        "Error al guardar: Complete los tados obligatorios '*'",
        "Domicilio Cliente"
      );
    }
  }

  getAnos(): number {
    let tiempo = this.domicilio.tiempoResidencia;

    let anos: number = tiempo / 12;
    console.log("Años", anos);

    return Math.trunc(anos);
  }

  getMeses(): number {
    let tiempo = this.domicilio.tiempoResidencia;

    let meses = tiempo % 12;
    return Math.trunc(meses);
  }

  getTipoComprobante(idComprobane): string {
    if (this.listComprobanteDomicilio.length > 0) {
      return this.listComprobanteDomicilio.filter(
        x => x.id_tipo_comprobante_domicilio == idComprobane
      )[0].tipo_comprobante_domicilio;
    } else {
      return "";
    }
  }

  render(base64: string): string {
    return renderImage(base64);
  }
  cargarDocumentos() {
    this.listArchivos = [];
    this.documetosDomicilio = this._solicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio;

    this.documetosDomicilio.forEach(documento => {
      documento.stream = renderImage(documento.stream);

      let archivo = new FileItem(
        new File([], documento.fileName),
        documento.stream
      );
      archivo.idDocumento = documento.idDocumento;
      archivo.idTipoDocumento = documento.idTipoDocumento;
      archivo.idTipoComprobanteDomicilio = documento.idTipoComprobanteDomicilio;
      this.listArchivos.push(archivo);
    });
  }

  selectColonia(event: any) {
    this.coloniaSeleccionada = event;

    if (this.coloniaSeleccionada.codigo_postal > 0) {
      this.formDomicilio
        .get(this.CTRL_CP)
        .setValue(this.coloniaSeleccionada.codigo_postal);
    }
    console.log(this.coloniaSeleccionada);
  }
  onChangeColonia(event: any) {
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();
    this.coloniaSeleccionada.nombre = event;
    this.coloniaSeleccionada.sepomex = -1;
    console.log(this.coloniaSeleccionada);
    console.log("changeColonia", event);
  }
  onFocusedColonia(event: any) {
    /*   // this.coloniaSeleccionada.nombre=event;

    console.log(this.coloniaSeleccionada);
    console.log("onFocused", event.value); */
  }
}
