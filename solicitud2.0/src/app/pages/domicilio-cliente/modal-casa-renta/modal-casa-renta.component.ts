import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { SolcitudService } from '../../../services/solcitud.service';

@Component({
  selector: 'app-modal-casa-renta',
  templateUrl: './modal-casa-renta.component.html',
  styleUrls: ['./modal-casa-renta.component.css']
})
export class ModalCasaRentaComponent implements OnInit {

  @Input() openModal: boolean;
  @Output() dataCasaRenta: EventEmitter<{ open: boolean, isCancel: boolean, monto?: string }>

  @ViewChild('edtImporte') edtImporte: ElementRef;

  constructor(public _SoliciudService:SolcitudService) {
    this.dataCasaRenta = new EventEmitter();
  }

  ngOnInit() {

  }

  sendData() {
    this.openModal = false;
    let importe = this.edtImporte.nativeElement.value;
    this.dataCasaRenta.emit({ open: false, isCancel: false, monto: importe })

  }

  closeModal() {
    this.openModal = false;
    this.dataCasaRenta.emit({ open: false, isCancel: true })
  }

}
