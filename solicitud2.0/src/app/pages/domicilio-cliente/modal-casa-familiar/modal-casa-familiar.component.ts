import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnChanges,
  AfterViewChecked,
  SimpleChanges,
  NgZone
} from "@angular/core";
import { SolcitudService } from "../../../services/solcitud.service";
import { RelacionCasaFamiliarModel } from "src/app/models/RelacionCasaFamiliar.model";
import { DatosContactoRequestModel } from "../../../models/models-request/DatosContactoRequest.model";
import { ContactoModel } from "../../../models/Contacto.model";
import { ToastrService } from "ngx-toastr";
import { delay } from "rxjs/operators";

@Component({
  selector: "app-modal-casa-familiar",
  templateUrl: "./modal-casa-familiar.component.html",
  styleUrls: ["./modal-casa-familiar.component.css"]
})
export class ModalCasaFamiliarComponent
  implements OnInit, OnChanges, AfterViewChecked {
  @Input() openModal: boolean;
  @Output() dataCasaFamiliar: EventEmitter<{
    isSave: boolean;
    idContactoCasa?: any;
  }>;

  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("selecParentesco") selecParentesco: ElementRef;
  @ViewChild("radioF") radioF: ElementRef;
  @ViewChild("radioM") radioM: ElementRef;

  esEdicion: boolean = false;
  estaCargado: boolean = false;
  isF: boolean = false;
  isM: boolean = false;
  datosDuenoDomicilio: ContactoModel;
  listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[] = [];
  constructor(
    private _solcitudService: SolcitudService,
    private ngZone: NgZone,
    private toster: ToastrService
  ) {
    this.dataCasaFamiliar = new EventEmitter();
  }

  ngOnInit() {
    this.getTiposRelacionesCasaFam();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("Casa familiar", changes);

    /*  if (changes.openModal.currentValue != undefined) {
      if(changes.datosDuenoDomicilio!=undefined){
      this.openModal = changes.datosDuenoDomicilio.currentValue;
      }
    } */
  }

  ngAfterViewChecked() {

    if (!this.estaCargado) {
      if (this.openModal) {
        this.datosDuenoDomicilio = this._solcitudService.solicitudCompleta.infoCliente.domicilioCliente.contactoDomicilioFamiliar;
        console.log("Datos Dueño Domiclio", this.datosDuenoDomicilio);
        this.estaCargado = true;
        if (
          this.datosDuenoDomicilio != undefined &&
          this.dataCasaFamiliar != null
        ) {
          this.esEdicion = true;
          this.autoSelect();
        } else {
          this.esEdicion = false;
          this.datosDuenoDomicilio = new ContactoModel();
        }
      }
    }
    
  }

  autoSelect() {
    this.edtApellidoP.nativeElement.value = this.datosDuenoDomicilio.primerApellido;
    this.edtNombre.nativeElement.value = this.datosDuenoDomicilio.nombres;

    this.edtApellidoM.nativeElement.value = this.datosDuenoDomicilio.segundoApellido;

    this.selecParentesco.nativeElement.value = this.datosDuenoDomicilio.idTipoContacto;

    if (this.datosDuenoDomicilio.genero.includes("M")) {
      this.radioM.nativeElement.checked = true;
      // this.isM = true;
      //this.isF = false;
    } else if (this.datosDuenoDomicilio.genero.includes("F")) {
      // this.isM = false;
      //this.isF = true;
      this.radioF.nativeElement.checked = true;
    }
  }

  getTiposRelacionesCasaFam() {
    this._solcitudService.getTiposRelacionesCasaFam().subscribe(respuesta => {
      console.log(respuesta);
      this.listTipoRelacionesCasaFam = respuesta;
    });
  }

  onSelectRelacionCasaFamiliar(event) {
    console.log(event);

    //this.datosDuenoDomicilio.idTipoRelacionContacto = event;
  }

  okModal() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._solcitudService.solicitud.idSolicitud;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.idTipoContacto = 1;
    contacto.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    contacto.fechaNacimiento = "1900-01-01";

    if (this.radioM.nativeElement.checked) {
      contacto.genero = "M";
    } else {
      contacto.genero = "F";
    }

    if (this.esEdicion) {
      contacto.idContacto = this.datosDuenoDomicilio.idContacto;
      this._solcitudService
        .actualizaContactoClientSolicitud(contacto)
        .subscribe(
          resp => {
            this.toster.success("Contacto actualizado", "Contacto Domicilio");

            this.dataCasaFamiliar.emit({
              isSave: true,
              idContactoCasa: resp.idContacto
            });

            this.openModal = false;
            this.estaCargado = false;
          },
          err => {
            console.log(err);

            this.toster.error(err.message, "Error: Contacto Domicilio");
          }
        );
    } else {
      this._solcitudService.agregaContactoClientSolicitud(contacto).subscribe(
        resp => {
          console.log(resp);
          this.toster.success("Datos guardados", "Datos Casa Familiar");

          this.dataCasaFamiliar.emit({
            isSave: true,
            idContactoCasa: resp.idContacto
          });

          this.openModal = false;
          this.estaCargado = false;
        },
        err => {
          this.toster.error(err.message, "Error: Datos Casa Familiar");

          console.log(err);
        }
      );
    }
  }

  closeModal() {
    this.openModal = false;
    this.estaCargado= false;
    this.dataCasaFamiliar.emit({ isSave: false });
  }
}
