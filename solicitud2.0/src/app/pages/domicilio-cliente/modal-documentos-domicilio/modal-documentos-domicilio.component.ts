import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit
} from "@angular/core";
import { SolcitudService } from "src/app/services/solcitud.service";
import { FileItem } from "src/app/models/file-item";
import { TiposComprobanteDomicilioModel } from "../../../models/TiposComprobanteDomicilio.model";
import { DocumetnosDomicilioModel } from "src/app/models/DocumentosDomicilioModel";
import { DocumentoRequestModel } from "../../../models/models-request/DocumentoRequest.model";
import { isUndefined, log } from "util";
import { ToastrService } from "ngx-toastr";
import { renderImage } from "../../../tools";
import { FormGroup, FormArray, FormControl, FormBuilder } from "@angular/forms";
import { DocumentoEmpleo } from "src/app/models/DocumentoEmpleoModel";
import { DataInputModalGuardarModel } from "../../../components/modal-guardar/dataInputModa.model";
import { DatosEmpleoModel } from "src/app/models/DatosEmpleo.model";
import { DomicilioClienteModel } from "../../../models/DomicilioClienteModel";
import { TiposComprobantePropiedadModel } from "../../../models/TiposComprobantePropiedad.model";
import { TiposDocumentosModel } from "../../../models/TiposDocumentos.model";

@Component({
  selector: "app-modal-documentos-domicilio",
  templateUrl: "./modal-documentos-domicilio.component.html",
  styleUrls: ["./modal-documentos-domicilio.component.css"]
})
export class ModalDocumentosDomicilioComponent
  implements OnInit, AfterViewInit {
  @Input() openModal: boolean;
  @Output() dataDocumentosDomicilio: EventEmitter<{
    open: boolean;
    data?: any;
  }>;

  form: FormGroup;
  clasDocumento: FormArray;

  listArchivosNuevos: FileItem[];
  guardando: boolean = false;
  preView = false;
  archivoSeleccionado: FileItem;
  estaSobreDrop: boolean = false;
  documetosDomicilio: DocumetnosDomicilioModel[];
  domicilio: DomicilioClienteModel;
  listArchivos: FileItem[] = [];
  // documentoSeleccionado: DocumentoEmpleo;
  indexControlSelected: number;
  instanceModalConfirm: number;
  openModalConfirm: boolean = false;
  dataInputModalConfirm: DataInputModalGuardarModel;

  listTiposDocumento: TiposDocumentosModel[] = [];
  listTiposComprobantesDomicilio: TiposComprobanteDomicilioModel[] = [];
  listTiposComprobantePropiedad: TiposComprobantePropiedadModel[] = [];

  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService,
    private formBuilder: FormBuilder
  ) {
    this.dataDocumentosDomicilio = new EventEmitter();
  }

  ngOnInit() {
    this.initForm();
    this.getTiposComprobantes();
    this.getTiposComprobantePropiedad();
    this.getTiposComprobantesDomicilio();
    this.cargarDocumentos();
  }

  ngAfterViewInit(): void {}

  initForm() {
    /* this.form = new FormGroup({
      selectTipoDocumento: new FormArray([]),
      selecTipoComprobante: new FormArray([])
    }); */

    this.form = this.formBuilder.group({
      clasDocumento: this.formBuilder.array([])
    });
  }

  crearItem(
    tipoDocumento: number,
    tipoCompDom: number,
    tipoCompProp: number
  ): FormGroup {
    return this.formBuilder.group({
      selecTipoDocumento: tipoDocumento,
      selecCompDom: tipoCompDom,
      selecCompProp: tipoCompProp
    });
  }

  addItem(isCarga: boolean, archivo?: FileItem): void {
    this.clasDocumento = this.form.get("clasDocumento") as FormArray;

    if (isCarga) {
      this.clasDocumento.insert(
        0,
        this.crearItem(
          archivo.idTipoDocumento,
          archivo.idTipoComprobanteDomicilio,
          archivo.idTipoComprobantePropiedad
        )
      );
      let formArray: FormArray = <FormArray>(
        this.form.controls["clasDocumento"].get("0")
      );

      let value: number = parseInt(
        formArray.controls["selecTipoDocumento"].value
      );

      switch (value) {
        case 1:
          formArray.controls["selecCompDom"].enable();

          formArray.controls["selecCompProp"].disable();
          formArray.controls["selecCompProp"].setValue("-1");
          break;
        case 2:
          formArray.controls["selecCompDom"].disable();
          formArray.controls["selecCompDom"].setValue("-1");

          formArray.controls["selecCompProp"].enable();
          break;
      }
      console.log("FORM", this.form);
    } else {
      this.clasDocumento.insert(0, this.crearItem(-1, -1, -1));
    }
  }

  onChangeTipoDocumento(index: number) {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    let value: number = parseInt(
      formArray.controls["selecTipoDocumento"].value
    );

    switch (value) {
      case 1:
        formArray.controls["selecCompDom"].enable();
        formArray.controls["selecCompDom"].setValue("-1");

        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        break;
      case 2:
        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");

        formArray.controls["selecCompProp"].enable();
        formArray.controls["selecCompProp"].setValue("-1");
        break;
    }
  }

  onDropArchivoNuevo(archivo: FileItem) {
    this.listArchivos.unshift(archivo);
    this.addItem(false);
    this.hideAllControlsFromGroup(0);
  }

  hideControl(index: number, controlName: string): boolean {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    if (formArray.controls[controlName].disabled) {
      return true;
    } else {
      return false;
    }
  }

  hideAllControlsFromGroup(index: number) {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    formArray.controls["selecCompDom"].disable();
    formArray.controls["selecCompDom"].setValue("-1");

    formArray.controls["selecCompProp"].disable();
    formArray.controls["selecCompProp"].setValue("-1");
  }

  cargarDocumentos() {
    this.listArchivos = [];
    this.documetosDomicilio = this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio;

    this.documetosDomicilio.forEach(documento => {
      documento.stream = renderImage(documento.stream);

      let archivo = new FileItem(
        new File([], documento.fileName),
        documento.stream
      );
      archivo.idDocumento = documento.idDocumento;
      archivo.idTipoDocumento = documento.idTipoDocumento;
      archivo.idTipoComprobanteDomicilio = documento.idTipoComprobanteDomicilio;
      archivo.idTipoComprobantePropiedad = documento.idTipoComprobantePropiedad;
      this.listArchivos.unshift(archivo);
      this.addItem(true, archivo);
    });
    console.log(this.form);
  }

  getTiposComprobantes() {
    this._SolicitudService.getTiposDocumentos().subscribe(response => {
      this.listTiposDocumento = response.filter(
        x => x.id_tipo_documento == 1 || x.id_tipo_documento == 2
      );
    });
  }

  getTiposComprobantesDomicilio() {
    this._SolicitudService.getTiposComprobanteDomiclio().subscribe(response => {
      this.listTiposComprobantesDomicilio = response;
    });
  }

  getTiposComprobantePropiedad() {
    this._SolicitudService.getComprobantePropiedad().subscribe(response => {
      this.listTiposComprobantePropiedad = response;
    });
  }

  validarDocumentos(): boolean {
    let pasa: boolean = true;

    for (let i = 0; i < this.listArchivos.length; i++) {
      let formArray: FormArray = <FormArray>(
        this.form.controls["clasDocumento"].get(i.toString())
      );

      let value: number = parseInt(
        formArray.controls["selecTipoDocumento"].value
      );
      if (value == 1) {
        if (formArray.controls["selecCompDom"].value < 1) {
          this.toaster.error(
            "algunos documentos no tienen tipo de Comprobante",
            "No se puede subir el archivo"
          );
          pasa = false;
          return false;
        }
      } else if (value == 2) {
        if (formArray.controls["selecCompProp"].value < 1) {
          this.toaster.error(
            "algunos documentos no tienen tipo de Comprobante",
            "No se puede subir el archivo"
          );
          pasa = false;
          return false;
        }
      } else {
        this.toaster.error(
          "algunos documentos no tienen tipo de Comprobante",
          "No se puede subir el archivo"
        );
        pasa = false;
        return false;
      }
    }

    return pasa;
  }

  onClickEliminarDocumento(index: number, documento: FileItem) {
    /**
     * NOTE Esta funcion abre el modal de confirmacion
     * el cual responde en la funcion onConfirmModal
     * @param empleo
     */
    this.indexControlSelected = index;
    this.archivoSeleccionado = documento;
    this.instanceModalConfirm = 1;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Eliminar Documento",
      "¿Deseas eliminar el Documento?",
      []
    );
    console.log(documento);
    console.log(this.listArchivos);
  }

  onConfirmModal(event: any) {
    this.openModalConfirm = false;

    if (event.save) {
      switch (this.instanceModalConfirm) {
        case 1:
          this.setEliminarDocumento();
          break;
      }
    }
  }

  eliminarArchivo(index: number, nombre: string) {
    console.log("Archivo a eliminar:", index);
    console.log("Archivo a eliminar:", nombre);
    console.log("Archivo Seleccionado",this.archivoSeleccionado)
    /*     let posicion: number = this.listArchivos.findIndex(x => x == archivo);

    (<FormArray>this.form.controls["selecTipoComprobante"]).removeAt(posicion); */

    let formArray: FormArray = <FormArray>this.form.controls["clasDocumento"];
    console.log("formArray", formArray);
    formArray.removeAt(index);

    this.listArchivos = this.listArchivos.filter(
      x => x.nombreArchivo != nombre
    );

    console.log("listArchivs", this.listArchivos);

    this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio = this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio.filter(
      x => x.idDocumento != this.archivoSeleccionado.idDocumento
    );
    console.log(
      "Lista archivos 2",
      (this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio = this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio)
    );

    console.log("Form despues de eliminar documetnos", this.form);
  }

  setEliminarDocumento() {
    console.log("Documento Eliminado", this.archivoSeleccionado);

    this._SolicitudService.cargandSolicitud = true;
    if (this.archivoSeleccionado.idDocumento >= 1) {
      this._SolicitudService
        .setEliminarDocumentoSolicitud(this.archivoSeleccionado.idDocumento)
        .subscribe(
          resp => {
            console.log(resp);
            this.eliminarArchivo(
              this.indexControlSelected,
              this.archivoSeleccionado.nombreArchivo
            );
            this._SolicitudService.cargandSolicitud = false;

            console.log(
              "Domicilio Cliente",
              this._SolicitudService.solicitudCompleta.infoCliente
                .empleosCliente
            );
            this.dataDocumentosDomicilio.emit({ open: true });
            //this.cargarDocumentos();

            console.log(resp);

            this.toaster.success("Documento eliminado", "Eliminar Documento");
          },
          err => {
            console.log(err);
            this._SolicitudService.cargandSolicitud = false;
            this.toaster.error(err.message, "Error: Eliminar Documento");
          }
        );
    } else {
      this._SolicitudService.cargandSolicitud = false;
      this.eliminarArchivo(
        this.indexControlSelected,
        this.archivoSeleccionado.nombreArchivo
      );
    }
  }

  setDocumentos() {
    if (this.validarDocumentos()) {
      this.guardando = true;
      this._SolicitudService.cargandSolicitud = true;
      this.listArchivosNuevos = [];

      this.listArchivosNuevos = this.listArchivos.filter(
        x => x.idDocumento <= 0 || isUndefined(x.idDocumento)
      );
      console.log("Todos los archivos", this.listArchivos);

      console.log("Archivos Nuevos", this.listArchivosNuevos);

      if (this.listArchivosNuevos.length > 0) {
        this.guardaDocumento(0);
      } else {
        this.guardando = false;
        this._SolicitudService.cargandSolicitud = false;
        this.closeModal();
      }
    }
  }
  guardaDocumento(pos: number) {
    console.log("lista archivos nuevos", this.listArchivosNuevos);
    if (pos < this.listArchivosNuevos.length) {
      let element: FileItem = this.listArchivosNuevos[pos];
      let documento: DocumentoRequestModel = new DocumentoRequestModel();

      let formArray: FormArray = <FormArray>(
        this.form.controls["clasDocumento"].get(pos.toString())
      );

      let tipoDoc: number = parseInt(
        formArray.controls["selecTipoDocumento"].value
      );
      let tipoCompDom: number = parseInt(
        formArray.controls["selecCompDom"].value
      );
      let tipoComProp: number = parseInt(
        formArray.controls["selecCompProp"].value
      );

      let base64 = element.base64.split("base64,")[1];

      documento.idSolicitud = this._SolicitudService.solicitudCompleta.idSolicitud;
      documento.idCliente = this._SolicitudService.cliente.idCliente;
      documento.streamFile = base64;
      documento.idTipoDocumento = tipoDoc;
      documento.idTipoComprobanteDomicilio = tipoCompDom;
      documento.idTipoComprobantePropiedad = tipoComProp;
      documento.name = element.nombreArchivo;
      documento.idDomicilio = this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio.idDomicilio;

      this._SolicitudService.agregaDocumentoSolicitud(documento).subscribe(
        response => {
          console.log(response);
          this.guardaDocumento(pos + 1);
          this.toaster.success(
            "Documento guardado",
            "Documento: " + documento.name
          );
          let documentoNuevo: DocumetnosDomicilioModel = new DocumetnosDomicilioModel();
          documentoNuevo.idDocumento = response.idDocumento;
          documentoNuevo.fileName = response.name;
          documentoNuevo.stream = response.streamFile;

          documentoNuevo.idTipoComprobanteDomicilio =
            response.idTipoComprobanteDomicilio;

          documentoNuevo.idTipoDocumento = response.idTipoDocumento;
          documentoNuevo.idTipoComprobantePropiedad = response.idTipoComprobantePropiedad;

          this._SolicitudService.solicitudCompleta.infoCliente.domicilioCliente.documentosDomicilio.push(
            documentoNuevo
          );
          console.log(
            this._SolicitudService.solicitudCompleta.infoCliente
              .domicilioCliente.documentosDomicilio
          );
          this.dataDocumentosDomicilio.emit({ open: true });
          if (pos >= this.listArchivosNuevos.length) {
            this.listArchivosNuevos = [];

            this.cargarDocumentos();

            console.log("Barrando archivos nuevos", this.listArchivosNuevos);
          }
        },
        err => {
          this.guardaDocumento(pos + 1);
          this.toaster.error(
            err.message,
            "Error: Documento " + '"' + documento.name + '"'
          );
          console.log("Error al Enviar Documentos", err);
        }
      );
    } else {
      this.guardando = false;
      this._SolicitudService.cargandSolicitud = false;
      this.closeModal();
    }
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }

  closeModal() {
    this.listArchivosNuevos = [];
    this.openModal = false;
    this.dataDocumentosDomicilio.emit({ open: false });
  }

  openPreView(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.preView = true;
  }

  selectImage(evento: any) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let file = new FileItem(archivoTemporal, base64.toString());
      this.listArchivos.unshift(file);
      this.addItem(false);
      console.log("form", this.form);
    };
  }

  tipoComprobante(idComprobante: number): number {
    if (idComprobante > 0) {
      let tipo: number = this.listTiposComprobantesDomicilio.filter(
        x => x.id_tipo_comprobante_domicilio == idComprobante
      )[0].id_tipo_comprobante_domicilio;

      return tipo;
    }
    console.log("return", -1);

    return -1;
  }
}
