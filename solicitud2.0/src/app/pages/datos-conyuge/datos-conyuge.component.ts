import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SolcitudService } from "src/app/services/solcitud.service";
import { EdoCivilModel } from "../../models/EdoCivil.model";
import { FileItem } from "src/app/models/file-item";
import { FormGroup, FormControl, FormArray, Validators } from "@angular/forms";
import { generarRFC, INEFix } from "../../tools";
import { InfoClienteModel } from "../../models/InfoCliente.model";
import { ConyugeModel } from "../../models/Conyuge.model";
import { ConyugeRequestModel } from "../../models/models-request/ConyugeRequest.mosel";

import { isUndefined } from "util";
import { INEModel } from "src/app/models/INE.model";
import { INERequestModel } from "src/app/models/models-request/INEReques.model";
import { ToastrService } from "ngx-toastr";
import { ClienteModelRequest } from "src/app/models/models-request/ClienteRequest.model";
import { ClienteModel } from "../../models/Cliente.model";
import { isNull } from "@angular/compiler/src/output/output_ast";
import { TelefonoRequestModel } from "../../models/models-request/TelefonoRequest.model";
import { ContactoModel } from "src/app/models/Contacto.model";
import { DataInputModalGuardarModel } from 'src/app/components/modal-guardar/dataInputModa.model';
import { EmpleoRequestModel } from 'src/app/models/models-request/EmpleoRequest.model';
@Component({
  selector: "app-datos-conyuge",
  templateUrl: "./datos-conyuge.component.html",
  styleUrls: ["./datos-conyuge.component.css"]
})
export class DatosConyugeComponent implements OnInit {
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("edtTelefonoFijo") edtTelefonoFIjo: ElementRef;
  @ViewChild("edtTelefonoCelular") edtTelfonoCel: ElementRef;
  @ViewChild("edtFechaNacimiento") edtFechaNacimiento: ElementRef;
  @ViewChild("edtRFC") edtRFC: ElementRef;
  //@ViewChild("selectEdo") selectEdo: ElementRef;
  openModalGuardar: boolean = false;
  openModalEliminarEmpelo:boolean=false;
  conyugeCliente: ConyugeModel;
  dataInput: DataInputModalGuardarModel;
  formularioDatosConyuge: FormGroup;
  estaSobreDrop: boolean = false;
  listArchivos: FileItem[] = [];
  listArchivos2: FileItem[] = [];
  listEstadoCivil: EdoCivilModel[] = [];
  generoF: boolean = false;
  generoM: boolean = false;
  tieneEmpleo: boolean = false;

  ineFrente: FileItem;
  ineReverso: FileItem;
  image: string;
  esActualizacion = false;

  guardoDatos:boolean= false;
  guardoDatosExitoso:boolean= false;

  guardoIne:boolean= false;
  guardoIneExitoso:boolean= false;

  guardoCel:boolean= false;
  guardoCelExitoso:boolean= false;


  
  constructor(
    public _solicitudServices: SolcitudService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.conyugeCliente = this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente;
    console.log("Conyuge", this.conyugeCliente);

    if (isUndefined(this.conyugeCliente) || this.conyugeCliente == null) {
      console.log("Creando Nuevo Modelo");
      this.esActualizacion = false;
      this.conyugeCliente = new ConyugeModel();
      this.conyugeCliente.contacto = new ContactoModel();
      this.conyugeCliente.contacto.nombres = "";
      this.conyugeCliente.contacto.primerApellido = "";
      this.conyugeCliente.contacto.segundoApellido = "";
      this.conyugeCliente.contacto.rfc = "";
      this.conyugeCliente.contacto.idContacto = 0;
      this.conyugeCliente.infoINE = undefined;
    } else {
      this.esActualizacion = true;
      this.autoSelect();
    }

    this.getTiposEstadoCivil();
  }

  //cuentaEmpleo(): boolean {
  //  if (
  //    this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente
  //      .infoEmpleosContacto.length > 0
  //  ) {
  //    this._solicitudServices.reqDatosEmpleoConyugeCliente = true;
  //    return true;
  //  } else {
  //    this._solicitudServices.reqDatosEmpleoConyugeCliente = true;
  //
  //    return false;
  //  }
  //}

  getFechaNacimiento(): string {
    let fecha: string = "";
    try {
      let fechas: Array<
        String
      > = this.conyugeCliente.contacto.fechaNacimiento.split(" ");
      fecha = `${fechas[0]}`;
    } catch (error) {}

    return fecha;
  }

  getTiposEstadoCivil() {
    this._solicitudServices.getEstadoCivil().subscribe(
      resp => {
        this.listEstadoCivil = resp;
      },
      err => {}
    );
  }

  reqDatEmpleo(req: boolean) {
    if(req){
      if(this.conyugeCliente.infoEmpleosContacto.length==0){
        this.setEmpleoCliente()
      }
    }else{
      if(this.conyugeCliente.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length>0){
        console.log("Borrar empleos Preguntando");
        this.dataInput = new DataInputModalGuardarModel(
          "Se van a eliminar los empleos que ya has agregado",
          "¿Deseas continuar?",
          []
          
        );
        this.openModalEliminarEmpelo = true;

      }else if(this.conyugeCliente.infoEmpleosContacto.length>0 &&
        this.conyugeCliente.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length==0){
          console.log("Borrar empleo NA");
          this.setEliminarEmpleo(0);
        }
    }
  }

  setEliminarEmpleo(index:number) {
    if(index<this.conyugeCliente.infoEmpleosContacto.length){
      this._solicitudServices.cargandSolicitud=true;
      this._solicitudServices.setEmliminarEmpleoSolcitud(
        this.conyugeCliente.infoEmpleosContacto[index].empleo.idEmpleo,
          -1,
          this.conyugeCliente.contacto.idContacto
        )
        .subscribe(
          resp => {
            this._solicitudServices.cargandSolicitud=false;
            console.log(resp);
         
            console.log("Empleo Elimniado", "Eliminar Empleo");
            this.setEliminarEmpleo(index+1)
          },
          err => {
            this._solicitudServices.cargandSolicitud=false;
            console.log(err);
            this.toaster.error(err.message, "Error: Eliminar Empleo");
          }
        );
    }else{
      this.cargaSolicitudCompleta()
    }

  }

  setEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();
    
    empleoCl.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
    empleoCl.agente = this._solicitudServices.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = 1;
    empleoCl.empleo = "NA";
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = 0;
    empleoCl.nombreEmpresa = "NA";
    empleoCl.puesto = "NA";
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = -1;;
    //console.log("Select Tipo Cingreso", this.selecTipoIngreso);

  
      empleoCl.idTipoIngreso = -1;
    
    empleoCl.idContacto = this.conyugeCliente.contacto.idContacto;
    empleoCl.antiguedad = 0;
    //if(empleoCl.idTipoEmpleo>0 &&
    //   empleoCl.nombreEmpresa.localeCompare("")!=0 &&
    //   empleoCl.puesto.localeCompare("")!=0 &&
    //   empleoCl.ingresoMensual>0
    //   ){
      this._solicitudServices.cargandSolicitud=true;
      this._solicitudServices
        .agregaEmpleoContactoClienteSolicitud(empleoCl)
        .subscribe(
        resp => {
          console.log(resp);
          console.log(
            "Empleo guardado correctamente",
            "Datos Empleo Cliente"
          );
          this._solicitudServices.cargandSolicitud=false;
          this.cargaSolicitudCompleta();
        },
        err => {
          this._solicitudServices.cargandSolicitud=false;
          console.log(err);
          this.toaster.error(err.message, "Datos empleo: Error al guardar");
        }
      );
     
   // }else{
   //   this.toaster.error("Error al guardar", "Llena los campos obligatorios ");
   // }
   
  }

  onPasteIne(event: ClipboardEvent) {
    console.log(event);

    console.log(event.clipboardData.items);
    console.log(event.clipboardData.files[0]);
    console.log(event.clipboardData.getData("image/bmp"));
    //console.log(event.originalEvent.clipboardData)
  }

  autoSelect() {
    //this._solicitudServices.reqDatosEmpleoConyugeCliente = this.conyugeCliente.contacto.aportaIngresos;

    this.edtNombre.nativeElement.value = this.conyugeCliente.contacto.nombres;

    this.edtApellidoP.nativeElement.value = this.conyugeCliente.contacto.primerApellido;

    this.edtApellidoM.nativeElement.value = this.conyugeCliente.contacto.segundoApellido;

    this.edtFechaNacimiento.nativeElement.value = this.getFechaNacimiento();

    this.edtRFC.nativeElement.value = this.conyugeCliente.contacto.rfc;

    this.edtTelfonoCel.nativeElement.value = this.getCel();

    if (this.conyugeCliente.contacto.genero.includes("Masculino")) {
      this.generoM = true;
      this.generoF = false;
    } else if (this.conyugeCliente.contacto.genero.includes("Femenino")) {
      this.generoF = true;
      this.generoM = false;
    }
  }
  getCel() {
    let num = "";
    this.conyugeCliente.telefonosContacto.forEach(x => {
      if (x.idTipoTelefono == 2) {
        num = x.numeroTelefonico;
      }
    });
    return num;
  }

  generaRFC() {
    this.conyugeCliente.contacto.rfc = generarRFC(
      this.edtNombre.nativeElement.value,
      this.edtApellidoP.nativeElement.value,
      this.edtApellidoM.nativeElement.value,
      this.edtFechaNacimiento.nativeElement.value
    );

    this.edtRFC.nativeElement.value = this.conyugeCliente.contacto.rfc;
  }

  setConyugeCliente() {
    let conyugeCl: ConyugeRequestModel = new ConyugeRequestModel();
    if (this.esActualizacion) {
      conyugeCl.idContacto = this.conyugeCliente.contacto.idContacto;
    }
    this._solicitudServices.cargandSolicitud=true;
    conyugeCl.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
    conyugeCl.idTipoContacto = 4;
    conyugeCl.nombres = this.edtNombre.nativeElement.value;
    conyugeCl.primerApellido = this.edtApellidoP.nativeElement.value;
    conyugeCl.segundoApellido = this.edtApellidoM.nativeElement.value;
    //conyugeCl.fechaNacimiento = this.getFechaNacimiento();
    conyugeCl.fechaNacimiento = this.edtFechaNacimiento.nativeElement.value;

    conyugeCl.idTipoEstadoCivil = -1;
    conyugeCl.agente = this._solicitudServices.solicitud.idAgente;
    conyugeCl.correo = "NA";

    conyugeCl.idCliente = this._solicitudServices.solicitudCompleta.infoCliente.cliente.idCliente;

    conyugeCl.importeIngresos = 0;
    conyugeCl.idRelacionContacto = 0;

    /*   conyugeCl.idRelacionConyuge = this._solicitudServices.solicitudCompleta.infoCliente.cliente.idCliente;
     */
    conyugeCl.idTipoContactoIngreso = -1;
    conyugeCl.idTipoRelacionContacto = -1;
    conyugeCl.idTipoRelacionEmpleo = -1;

    conyugeCl.rfc = this.edtRFC.nativeElement.value;

    if (this.generoF) {
      conyugeCl.genero = "F";
    } else if (this.generoM) {
      conyugeCl.genero = "M";
    }

    if (this._solicitudServices.reqDatosEmpleoConyugeCliente) {
      conyugeCl.aportaIngresos = true;
    } else {
      conyugeCl.aportaIngresos = false;
    }

    console.log("Datos Conyuge Enviados", conyugeCl);

    if (!this.esActualizacion) {
      this.guardoDatos=true;
      this._solicitudServices.agregaConyugeSolicitud(conyugeCl).subscribe(
        resp => {
          console.log(resp);
          this.guardoDatosExitoso=true;
          //this.guardaDatosCliente(resp.)
          resp.idContacto = resp.idConyuge;
          this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente= new ConyugeModel();
          this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente.contacto= resp;
          
  
                if (!isUndefined(this.ineFrente) || !isUndefined(this.ineReverso)) {
                  if (isUndefined(this.ineFrente.idDocumento) || isUndefined(this.ineReverso.idDocumento)) {
                    this.guardarINE();
                  }else{
                    this.setTelefonoConyuge(resp.idContacto);
                  }
                 
                }else{
                  this.setTelefonoConyuge(resp.idContacto);
                }
              
              
              
          
        },
        err => {                                                              
          console.log(err);
          this.guardoDatosExitoso=false;
          this._solicitudServices.cargandSolicitud=false;
         
        }
      );
    } else {
      this.setActualizaConyuge(conyugeCl);
    }
  }

  getTelefono():String{
    let cel:String="";
    this.conyugeCliente.telefonosContacto.forEach(x=>{
      if(x.estadoActual.includes("Activo")&& x.idTipoTelefono==2){
        cel=x.numeroTelefonico;
      }
    })
    
    return cel;
  }

  onSelectGenero(genero: string) {
    console.log("onSelecGenero", genero);

    if (genero.includes("M")) {
      this.generoF = false;
      this.generoM = true;
    } else if (genero.includes("F")) {
      this.generoF = true;
      this.generoM = false;
    }
  }

  onSelectCuentaEmpleo(cuentaEmpleo: boolean) {}
  selectImage(evento: any, isFrente) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let ine: INEModel = new INEModel();
      if (isFrente) {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamFrente = file.base64;
        //this._solicitudServices.solicitudCompleta.infoCliente.ine = ine;
        this.ineFrente = file;
      } else {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamReverso = file.base64;
       // this._solicitudServices.solicitudCompleta.infoCliente.ine = ine;
        this.ineReverso = file;
      }
    };
  }
  renderIne(isFrente: boolean) {
    //console.log("renderINE");

    if (this.conyugeCliente.infoINE != null) {
      let ine: INEModel = this._solicitudServices.solicitudCompleta.infoCliente
        .conyugeCliente.infoINE;
      if (isFrente) {
        if (ine.streamFrente != null && this.ineFrente == null) {
          // console.log("Existe Archivo Viejo");
          this.ineFrente = new FileItem(new File([], ""), "");

          this.ineFrente.nombreArchivo = ine.fileNameFrente;
          this.ineFrente.base64 = INEFix + ine.streamFrente;
          this.ineFrente.idDocumento = ine.idCredencial;
          return INEFix + ine.streamFrente;
        } else if (this.ineFrente != null) {
          // console.log("Existe Archivo Nuevo");
          return this.ineFrente.base64;
        } else {
          // console.log("No Existe Archivo");
          this.image = "assets/fren.png";
          return "assets/fren.png";
        }
      } else {
        if (ine.streamReverso != null && this.ineReverso == null) {
          this.ineReverso = new FileItem(new File([], ""), "");
          this.ineReverso.nombreArchivo = ine.fileNameReverso;
          this.ineReverso.base64 = INEFix + ine.streamReverso;
          this.ineReverso.idDocumento = ine.idCredencial;
          return INEFix + ine.streamReverso;
        } else if (this.ineReverso != null) {
          return this.ineReverso.base64;
        } else {
          return "assets/rever.png";
        }
      }
    } else if (isFrente) {
      //console.log("es frente");
      // console.log(this.ineFrente);

      if (this.ineFrente != null) {
        // console.log("Existe Archivo Nuevo");
        return this.ineFrente.base64;
      } else {
        //console.log("asset");

        return "assets/fren.png";
      }
    } else if (!isFrente) {
      if (this.ineReverso != null) {
        return this.ineReverso.base64;
      } else {
        return "assets/rever.png";
      }
    }
  }

  setTelefonoConyuge(idContacto:number) {
    let telefono: TelefonoRequestModel = new TelefonoRequestModel();

    telefono.idSolicitud = this._solicitudServices.solicitud.idSolicitud;
    telefono.idOrigen= this._solicitudServices.idOrigen;
    telefono.agente= SolcitudService.agente
    telefono.idContacto = idContacto;

    telefono.idSucursal = this._solicitudServices.solicitud.idSucursal;
    telefono.idTipoTelefono = 2;
    telefono.telefono = this.edtTelfonoCel.nativeElement.value;
    //console.log("comparar telefonos","anterior "+ this.getCel()+" nuevo "+telefono.telefono +" "+this.getCel().localeCompare(telefono.telefono) )
    if (this.getCel().localeCompare(telefono.telefono) != 0) {
      this.guardoCel=true;
      this._solicitudServices
        .agregaTelefonoContactoClienteSolicitud(telefono)
        .subscribe(
          resp => {
            this.guardoCelExitoso=true;
            console.log(resp);
            this.cargaSolicitudCompleta();
           
          },
          err => {
            this.guardoCelExitoso=false
            console.log(err);
            this._solicitudServices.cargandSolicitud=false;
            
          }
        );
    }else{
      this.cargaSolicitudCompleta();
    }
  }

  guardarINE() {
 
      let ineAnverso;
      if (this.ineFrente.base64.includes("base64")) {
        ineAnverso = this.ineFrente.base64.split("base64,")[1];
      } else {
        ineAnverso = this.ineFrente.base64;
      }

      let ineReverso;
      if (this.ineReverso.base64.includes("base64")) {
        ineReverso = this.ineReverso.base64.split("base64,")[1];
      } else {
        ineReverso = this.ineReverso.base64;
      }

      let ine: INERequestModel = new INERequestModel();

      ine.idSolicitud = this._solicitudServices.solicitudCompleta.idSolicitud;
      ine.streamAnverso = ineAnverso;
      ine.fileNameAnverso = this.ineFrente.nombreArchivo;
      ine.streamReverso = ineReverso;
      ine.fileNameReverso = this.ineReverso.nombreArchivo;
      ine.idContacto = this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;
      this.guardoIne=true;
      this._solicitudServices.agregaCredencialContactoSolicitud(ine).subscribe(
        resp => {
          this.guardoIneExitoso=true;
          this.ineReverso.idDocumento = resp.idCredencial;
          this.ineFrente.idDocumento = resp.idCredencial;

          this.setTelefonoConyuge(this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto);

          console.log("Guardado INE", resp);
          
        },
        err => {
          this.guardoIneExitoso=false;
          console.log(err);
          this._solicitudServices.cargandSolicitud=false;
          
        }
      );
    
  }

  setActualizaConyuge(conyugeCl: ConyugeRequestModel) {
    this.guardoDatos=true;
    this._solicitudServices
      .actualizaContactoClientSolicitud(conyugeCl)
      .subscribe(
        resp => {
          console.log(resp);
          this.guardoDatosExitoso=true;
         
          if (!isUndefined(this.ineFrente) || !isUndefined(this.ineReverso)) {
            if (isUndefined(this.ineFrente.idDocumento) || isUndefined(this.ineReverso.idDocumento)) {
              this.guardarINE();
            }else{
              this.setTelefonoConyuge(this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto);
            }
          }else{
            this.setTelefonoConyuge(this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto);
          }
        

        },
        err => {
          console.log(err);
          this._solicitudServices.cargandSolicitud=false;
          this.guardoDatosExitoso=false;
        }
      );
  }

  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      this.setConyugeCliente();
     

     
      //this.guardarTelefonos();
    }
  }

  responseDataEliminar(event) {
    console.log(event);
    this.openModalEliminarEmpelo = false;

    if (event.save) {
      this.setEliminarEmpleo(0);
     

     
      //this.guardarTelefonos();
    }
  }

  onSaveClick() {
    if ((!isUndefined(this.ineReverso) && isUndefined(this.ineFrente))||isUndefined(this.ineReverso) && !isUndefined(this.ineFrente)) {
      this.toaster.error("Falta completar llenado de la INE")
    }else{
    this.dataInput = new DataInputModalGuardarModel(
      "Guardar Cambios",
      "¿Desea guardar los datos del cónyuge del cliente?",
      []
      
    );
    this.openModalGuardar = true;
    }
  }


  cargaSolicitudCompleta(){
    this._solicitudServices.cargandSolicitud=true;
    this._solicitudServices
    .getSolicitudCompleta(this._solicitudServices.solicitud.idSolicitud)
    .subscribe(resp => {
      this.conyugeCliente = this._solicitudServices.solicitudCompleta.infoCliente.conyugeCliente;
      if(this.guardoDatos){
        if(this.guardoDatosExitoso){
          this.toaster.success(
            "Datos guardados correctamnete",
            "Datos Cònyuge Cliente"
          );
          
        }else{
          this.toaster.error("Error", "Error al guardar cónyuge");
        }
      }

      if(this.guardoIne){
        if(this.guardoIneExitoso){
          this.toaster.success("INE guardada correctamente", "Datos Cónyuge");
        }else{
          this.toaster.error("Error", "Error: Ine Cónyuge");
        }
      }

      if(this.guardoCel){
        if(this.guardoCelExitoso){
          this.toaster.success("Telefono Guardado", "Datos Cónyuge");
        }else{
          this.toaster.error("Error", "Datos Cónyuge : Error telefonos");
        }
      }


      this.guardoDatos= false;
      this.guardoDatosExitoso= false;
    
      this.guardoIne= false;
      this.guardoIneExitoso= false;
    
      this.guardoCel= false;
      this.guardoCelExitoso= false;
    
   

      this._solicitudServices.cargandSolicitud=false;
    },error=>{
      this._solicitudServices.cargandSolicitud=false;
    });
  }
}
