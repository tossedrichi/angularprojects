import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SolcitudService } from "src/app/services/solcitud.service";
import { TipoContactoIngresosModel } from "../../models/TiposContactoIngreso.model";
import { DatosContactoModel } from "../../models/DatosContactoModel";
import { DatosContactoRequestModel } from "src/app/models/models-request/DatosContactoRequest.model";
import { ContactoModel } from "../../models/Contacto.model";
import { ToastrService } from "ngx-toastr";
import { TelefonoRequestModel } from "../../models/models-request/TelefonoRequest.model";
import { DataInputModalGuardarModel } from '../../components/modal-guardar/dataInputModa.model';
@Component({
  selector: "app-ingresos-familiares",
  templateUrl: "./ingresos-familiares.component.html",
  styleUrls: ["./ingresos-familiares.component.css"]
})
export class IngresosFamiliaresComponent implements OnInit {
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("edtTelefonoFijo") edtTelefonoFIjo: ElementRef;
  @ViewChild("edtTelefonoCelular") edtTelfonoCel: ElementRef;
  @ViewChild("selecParentesco") selecParentesco: ElementRef;
  @ViewChild("edtIngreso") edtIngreso: ElementRef;
  //@ViewChild('btnGuardar')btnGuardar:ElementRef;
  contacto:DatosContactoModel;
  listTipoContacto: TipoContactoIngresosModel[] = [];
  ingresoMensual: boolean = false;
  botones: boolean = true;
  listContactos: DatosContactoModel[];
  contactoSelec: ContactoModel;
  esEdicion: boolean = false;
  strBotonGuardar: string = "Guardar";
  dataInputSave: DataInputModalGuardarModel;
  openModalGuardar: boolean = false;
  instance: number;
  dataInputConfirmModal:DataInputModalGuardarModel;
  constructor(
    private _SolicitudSrevice: SolcitudService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.getTipoContactoIngreso();
    this.listarContactos();
  }

  listarContactos() {
    let contactosCliente: DatosContactoModel[] = this._SolicitudSrevice
      .solicitudCompleta.infoCliente.contactosCliente;

    if (
      this._SolicitudSrevice.solicitudCompleta.infoCliente.domicilioCliente
        .domicilio != null
    ) {
      this.listContactos = contactosCliente
        .filter(x => x.contacto.idTipoContacto == 2)
        .filter(
          x =>
            x.contacto.idContacto !=
            this._SolicitudSrevice.solicitudCompleta.infoCliente
              .domicilioCliente.domicilio.propietarioDomicilio
        );
    } else {
      this.listContactos = contactosCliente.filter(
        x => x.contacto.idTipoContacto == 2
      );
    }
    console.log("contactos listados",this.listContactos);
  }

  ingreso(tipo: string) {
    console.log(tipo);

    if (tipo == "Ingreso Familiar") {
      this.ingresoMensual = true;
      this.botones = false;
    } else {
      this.ingresoMensual = false;
      this.botones = true;
    }
  }

  getTipoContactoIngreso() {
    this._SolicitudSrevice.getTiposContactoIngresos().subscribe(response => {
      this.listTipoContacto = response;
    });
  }

  setContactoCliente() {
    let referencia: DatosContactoRequestModel = new DatosContactoRequestModel();
    this._SolicitudSrevice.cargandSolicitud=false;
    if (this.esEdicion) {
      referencia.idContacto = this.contactoSelec.idContacto;
    }
    this._SolicitudSrevice.cargandSolicitud=true;
   referencia.importeIngresos= this.edtIngreso.nativeElement.value;
    referencia.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    referencia.idTipoContacto = 2;
    referencia.nombres = this.edtNombre.nativeElement.value;
    referencia.primerApellido = this.edtApellidoP.nativeElement.value;
    referencia.segundoApellido = this.edtApellidoM.nativeElement.value;
    referencia.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    referencia.fechaNacimiento = "1900-01-01";
    referencia.genero = "F";
    referencia.aportaIngresos = false;

    if (!this.esEdicion) {
      this._SolicitudSrevice
        .agregaContactoClientSolicitud(referencia)
        .subscribe(
          resp => {
            this._SolicitudSrevice.cargandSolicitud=false;
            console.log("Ingreso guardado",resp);
            this.toaster.success("Guardada correctamente", "Refereniacias");
            if(this.getFijo().localeCompare(this.edtTelefonoFIjo.nativeElement.value)!=0){
               this.setTelefonoReferencia(resp.idContacto);
            }else{
              if(this.getCelular().localeCompare(this.edtTelfonoCel.nativeElement.value)!=0){
                this.setTelefonoCelularReferencia(resp.idContacto);
              }
            }
           
          },
          err => {
            this._SolicitudSrevice.cargandSolicitud=false;
            console.log(err);
            this.toaster.error(err.message, "Error al guardar Referecia");
          }
        );
    } else {
      this.setEdicionContacto();
    }
  }

  editarContato(contacto: DatosContactoModel) {
    console.log(contacto);
    this.contactoSelec = contacto.contacto;
    this.limpiarForm();
    this.esEdicion = true;
    this.strBotonGuardar = "Actualizar";
    this.contacto = contacto;
    this.edtIngreso.nativeElement.value=this.contactoSelec.importeIngresos
    this.edtNombre.nativeElement.value = this.contactoSelec.nombres;
    this.edtApellidoP.nativeElement.value = this.contactoSelec.primerApellido;
    this.edtApellidoM.nativeElement.value = this.contactoSelec.segundoApellido;
    this.edtTelefonoFIjo.nativeElement.value = this.getFijo();
    
  
    this.edtTelfonoCel.nativeElement.value = this.getCelular();

    this.selecParentesco.nativeElement.value = this.contactoSelec.idTipoRelacionContacto;

    this.edtIngreso.nativeElement.value = this.contactoSelec.importeIngresos;
  }

  onClickAgregarOtro() {
    this.limpiarForm();
    this.esEdicion = false;
    this.contactoSelec = null;
    this.strBotonGuardar = "Guardar";
  }

  onSaveConfirm(event) {
    this.openModalGuardar = false;

    if (event.save) {
      switch (this.instance) {
        case 1:
          this.setContactoCliente();
          break;
        case 2:
          this.eliminarContacto(this.contactoSelec.idContacto);
          break;
      }
    }
  }

  onClickGuardar() {
    if(this.validarDatos()){
      this.instance = 1;
      this.dataInputSave = new DataInputModalGuardarModel(
        "Guardar",
        "¿Desea guardar los cambios",
        []
      );
      this.openModalGuardar = true;
    }else{
      this.toaster.error("Verifica que los datos sten completos","error al guardar")
    }
   
  }

  onClickEliminarContacto(contacto: ContactoModel) {
    this.instance = 2;
    this.dataInputConfirmModal= new DataInputModalGuardarModel(
      'Eliminar Ingreso Familiar',
      '¿Desea eliminar el ingreso?',
      []
    )
    this.contactoSelec = contacto;
    this.openModalGuardar= true;
  }

  onConfirmEliminarContacto(resp:any) {
    this.openModalGuardar=false;

    if(resp.save){
      this.eliminarContacto(this.contactoSelec.idContacto);
    }
  }
  validarDatos():boolean{
    let pasa:boolean= true;
    let c:ContactoModel= new ContactoModel;
    c.nombres = this.edtNombre.nativeElement.value;
    c.primerApellido = this.edtApellidoP.nativeElement.value;
    c.idTipoRelacionContacto = this.selecParentesco.nativeElement.value
    c.importeIngresos= this.edtIngreso.nativeElement.value;
    if(!(c.nombres.length>0)||!(c.primerApellido.length>0)||c.idTipoRelacionContacto<1||c.importeIngresos<1){
      console.log("nombres mal");
      pasa=false;
    }
    let fijo:string=this.edtTelefonoFIjo.nativeElement.value;
    let cel:string=this.edtTelfonoCel.nativeElement.value;
    if(cel.localeCompare("")==0 && fijo.localeCompare("")==0){
      pasa=false;
      console.log("tlefonos mal");
    }
    console.log(c );
    console.log(cel+" "+fijo  );
    return pasa;
  }

  eliminarContacto(idContacto:number){
    console.log("eliminar ",idContacto)
    this._SolicitudSrevice.cargandSolicitud=true;
    this._SolicitudSrevice.eliminarContacto(idContacto).subscribe(
      resp => {
        this._SolicitudSrevice.cargandSolicitud=false;
        console.log(resp);
        this.toaster.success("Referencia eliminada", "Referencias");
        this._SolicitudSrevice.cargandSolicitud=true;
              this._SolicitudSrevice
              .getSolicitudCompleta(
                this._SolicitudSrevice.solicitud.idSolicitud
              )
              .subscribe(
                resp => {
                  console.log(resp);
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                    resp.infoCliente.contactosCliente;
                  this.listarContactos();
                  
                },
                err => {
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this.toaster.error(
                    err.message,
                    "Error al traer datos: Referencia"
                  );
                }
              );
      },
      err => {
        this._SolicitudSrevice.cargandSolicitud=false;
        console.log(err);
        this.toaster.error(err.message, "Error: Referencias");
      }
    );
  }


  setEdicionContacto() {
    let datosEdicion: DatosContactoRequestModel = new DatosContactoRequestModel();
    this._SolicitudSrevice.cargandSolicitud=true;
    datosEdicion.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    datosEdicion.idContacto = this.contactoSelec.idContacto;
    datosEdicion.idTipoContacto = 2;
    datosEdicion.nombres = this.edtNombre.nativeElement.value;
    datosEdicion.primerApellido = this.edtApellidoP.nativeElement.value;
    datosEdicion.segundoApellido = this.edtApellidoM.nativeElement.value;
    datosEdicion.idTipoRelacionContacto = this.selecParentesco.nativeElement.value;
    datosEdicion.fechaNacimiento = "1900-01-01";
    datosEdicion.genero = "F";
    datosEdicion.aportaIngresos = false;
    datosEdicion.importeIngresos =this.edtIngreso.nativeElement.value;;

    this._SolicitudSrevice
      .actualizaContactoClientSolicitud(datosEdicion)
      .subscribe(
        resp => {
          this._SolicitudSrevice.cargandSolicitud=false
          console.log("Contacto Editado", resp);
          
          if(this.getFijo().localeCompare(this.edtTelefonoFIjo.nativeElement.value)!=0){
            this.toaster.success("Contacto Editado", "Referencia");
            this.setTelefonoReferencia(resp.idContacto);
         }else{
           if(this.getCelular().localeCompare(this.edtTelfonoCel.nativeElement.value)!=0){
             this.toaster.success("Contacto Editado", "Referencia");
             this.setTelefonoCelularReferencia(resp.idContacto);
           }else{
            this._SolicitudSrevice.cargandSolicitud=true;
            this._SolicitudSrevice
            .getSolicitudCompleta(
              this._SolicitudSrevice.solicitud.idSolicitud
            )
            .subscribe(
              resp => {
                console.log(resp);
                this.toaster.success("Contacto Editado", "Referencia");
                this._SolicitudSrevice.cargandSolicitud=false;
                this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                  resp.infoCliente.contactosCliente;
                this.listarContactos();
                this.limpiarForm();
              },
              err => {
                this._SolicitudSrevice.cargandSolicitud=false;
                this.toaster.error(
                  err.message,
                  "Error al traer datos: Referencia"
                );
              }
            );

           }
         }
        },
        err => {
          console.log("Error Edicion Contacto", err);
          this.toaster.error(err.message, "Error al editar: Referencia");
        }
      );
  }
  getCelular():string{
    let cel:string="";
    try {
      if (
        this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 2).length > 0
      ) {
        cel = this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 2
        )[this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 2
        ).length-1].numeroTelefonico;
      }
    } catch (error) {
      console.log(error)
    }

    return cel;
  }
  getFijo():string{
    let cel:string="";
    try {
      if (
        this.contacto.telefonosContacto.filter(x => x.idTipoTelefono == 1).length > 0
      ) {
        cel = this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 1
        )[this.contacto.telefonosContacto.filter(
          x => x.idTipoTelefono == 1
        ).length-1].numeroTelefonico;
      }
    } catch (error) {
      console.log(error)
    }
 
    return cel
  }

  setTelefonoReferencia(idContacto) {
    let telefonos: TelefonoRequestModel[] = new Array<TelefonoRequestModel>();

    let telefonoFijo: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoFijo.idContacto = idContacto;
    telefonoFijo.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    telefonoFijo.idSucursal = this._SolicitudSrevice.solicitud.idSucursal;
    telefonoFijo.idTipoTelefono = 1;
    telefonoFijo.telefono = this.edtTelefonoFIjo.nativeElement.value;
    this._SolicitudSrevice.cargandSolicitud=true;
   
    console.log("telefono fijo",telefonoFijo.telefono);
  
      this._SolicitudSrevice
        .agregaTelefonoContactoClienteSolicitud(telefonoFijo)
        .subscribe(
          resp => {
            this._SolicitudSrevice.cargandSolicitud=false;
            if(this.getCelular().localeCompare(this.edtTelfonoCel.nativeElement.value)!=0){
              this.setTelefonoCelularReferencia(resp.idContacto);
            }else{
              this._SolicitudSrevice.cargandSolicitud=true;
              this._SolicitudSrevice
              .getSolicitudCompleta(
                this._SolicitudSrevice.solicitud.idSolicitud
              )
              .subscribe(
                resp => {
                  console.log(resp);
                  this.toaster.success("Telefono Guardado", "Referencia");
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                    resp.infoCliente.contactosCliente;
                  this.listarContactos();
                  this.limpiarForm();
                },
                err => {
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this.toaster.error(
                    err.message,
                    "Error al traer datos: Referencia"
                  );
                }
              );
            }
            console.log(resp);
            
            
          },
          err => {
            this.toaster.error(err.message, "Error al guardar telefono");
            console.log(err);
          }
        );
   
  }

  setTelefonoCelularReferencia(idContacto) {
    

    let telefonoCel: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoCel.idContacto = idContacto;
    telefonoCel.idSolicitud = this._SolicitudSrevice.solicitud.idSolicitud;
    telefonoCel.idSucursal = this._SolicitudSrevice.solicitud.idSucursal;
    telefonoCel.idTipoTelefono = 2;
    telefonoCel.telefono = this.edtTelfonoCel.nativeElement.value;
    console.log("telefono Cel",telefonoCel.telefono);
    this._SolicitudSrevice.cargandSolicitud=true;
      this._SolicitudSrevice
        .agregaTelefonoContactoClienteSolicitud(telefonoCel)
        .subscribe(
          resp => {
            console.log(resp);
           
            this._SolicitudSrevice
              .getSolicitudCompleta(
                this._SolicitudSrevice.solicitud.idSolicitud
              )
              .subscribe(
                resp => {
                  console.log(resp);
                  this.toaster.success("Telefono Guardado", "Referencia");
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this._SolicitudSrevice.solicitudCompleta.infoCliente.contactosCliente =
                    resp.infoCliente.contactosCliente;
                  this.listarContactos();
                  this.limpiarForm();
                },
                err => {
                  
                  this._SolicitudSrevice.cargandSolicitud=false;
                  this.toaster.error(
                    err.message,
                    "Error al traer datos: Referencia"
                  );
                }
              );
          },
          err => {
            this._SolicitudSrevice.cargandSolicitud=false;
            this.toaster.error(err.message, "Error al guardar telefono");
            console.log(err);
          }
        );
   
  }

  limpiarForm() {
    this.esEdicion = false;
    this.edtNombre.nativeElement.value = "";
    this.edtApellidoP.nativeElement.value = "";
    this.edtApellidoM.nativeElement.value = "";
    this.edtIngreso.nativeElement.value = "";
    this.edtTelefonoFIjo.nativeElement.value = "";
    this.edtTelfonoCel.nativeElement.value = "";
    this.selecParentesco.nativeElement.value = "-1";
  }
}
