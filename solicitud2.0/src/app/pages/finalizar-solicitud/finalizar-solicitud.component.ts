import { Component, OnInit } from "@angular/core";
import { SolcitudService } from "../../services/solcitud.service";
import { isUndefined, isNullOrUndefined } from "util";
import { EmpleoModel } from "../../models/Empleo.model";

import { DatosContactoModel } from "src/app/models/DatosContactoModel";
import { DataInputModalGuardarModel } from "src/app/components/modal-guardar/dataInputModa.model";
import { ToastrService } from "ngx-toastr";
import { SolicitudModel } from "../../models/Solicitud.model";
import { InfoClienteModel } from "../../models/InfoCliente.model";
import { DomicilioClienteModel } from "../../models/DomicilioClienteModel";
import { DomicilioModel } from "../../models/Domicilio.Model";
import { DatosEmpleoModel } from "../../models/DatosEmpleo.model";
import { ConyugeModel } from "../../models/Conyuge.model";
import { ContactoModel } from "../../models/Contacto.model";
import { Router } from '@angular/router';

@Component({
  selector: "app-finalizar-solicitud",
  templateUrl: "./finalizar-solicitud.component.html",
  styleUrls: ["./finalizar-solicitud.component.css"]
})
export class FinalizarSolicitudComponent implements OnInit {
  dataInput: DataInputModalGuardarModel;
  openModalGuardar: boolean = false;

  constructor(
    public _solicitudService: SolcitudService,
    private toaster: ToastrService,
    private router:Router
  ) {}

  ngOnInit() {}
  onSaveClick() {
    this.dataInput = new DataInputModalGuardarModel(
      "Finalizar solicitud",
      "Si finaliza la solicitud ya no podrá editarla, ¿Desea Continuar?",
      []
    );
    this.openModalGuardar = true;
  }
  finalizarSolicitud() {
    //this._solicitudService.finaliza();
   this._solicitudService.finalizaSolicitud().subscribe(
      resp=>{
      console.log(resp);
       if(resp.procesado){
           //reiniciar app
           this._solicitudService.finaliza();
        }else{
           this.toaster.error( "Algo falló, intenta de nuevo");
      }
     },error=>{
       console.log(error);
    }
    )
  }
  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      this.finalizarSolicitud();
    }
  }

  okDatosCliente(): number {
    try {
      if (
        !isNullOrUndefined(this._solicitudService.solicitudCompleta.infoCliente)
      ) {
        let infoClient: InfoClienteModel = this._solicitudService
          .solicitudCompleta.infoCliente;

        if (this.validacioneCliente(infoClient)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }

  validacioneCliente(infoCliente: InfoClienteModel): boolean {
    if (
      infoCliente.cliente.rfc == "" ||
      infoCliente.cliente.idTipoEstadoCivil < 1 ||
      infoCliente.telefonosCliente.length < 1 ||
      infoCliente.cliente.nacimiento == ""  ||
      isNullOrUndefined(infoCliente.ine)
    ) {
      return false;
    } else {
      return true;
    }
  }

  okDomicilioCliente(): number {
    try {
      let domicilioCliente: DomicilioClienteModel = this._solicitudService
        .solicitudCompleta.infoCliente.domicilioCliente;
      if (
        !isNullOrUndefined(domicilioCliente) &&
        !isNullOrUndefined(domicilioCliente.domicilio)
      ) {
        if (this.validacionesDomicilio(domicilioCliente.domicilio)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }

  validacionesDomicilio(domicilio: DomicilioModel) {
    if (
      domicilio.idCiudad < 1 ||
      domicilio.colonia.trim() == "" ||
      domicilio.idTipoDomicilio < 1 ||
      domicilio.calle.trim() == "" ||
      domicilio.codigoPostal.trim() == "" ||
      domicilio.numeroExterior.trim() == "" //||
     // domicilio.tiempoResidencia < 1 ||
      //domicilio.habitantesDomicilio < 1 ||
      //domicilio.idTipoCondicionDomicilio < 1
    ) {
      return false;
    } else {
      return true;
    }
  }

  clienteCuentaEmpleo() {
    try {
      if (
        this._solicitudService.solicitudCompleta.infoCliente.empleosCliente
          .length > 0
      ) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
  okEmpleo(e: DatosEmpleoModel): number {
    try {
      if (e.empleo.idTipoEmpleo < 4) {
        if (this.validacionesEmpleos(e)) {
          return 1;
        } else {
          return 2;
        }
      } else if (e.empleo.idTipoEmpleo == 4) {
        if (this.validacionesOtrosEmpleos(e)) {
          return 1;
        } else {
          return 2;
        }
      }
    } catch (error) {
      return 3;
    }
  }

  validacionesOtrosEmpleos(empleo: DatosEmpleoModel): boolean {
    if (
      empleo.empleo.idTipoEmpleo < 1 ||
      empleo.empleo.idTipoIngreso < 1 ||
      empleo.empleo.antiguedad < 1 ||
      empleo.empleo.ingresoMensual < 1
    ) {
      return false;
    } else {
      return true;
    }
  }

  validacionesEmpleos(empleo: DatosEmpleoModel): boolean {
    if (
      empleo.empleo.idTipoEmpleo < 1 ||
      empleo.empleo.nombreEmpresa.trim() == "" ||
      empleo.empleo.empleo.trim() == "" ||
      empleo.empleo.puesto.trim() == "" ||
      empleo.empleo.antiguedad < 1 ||
      empleo.empleo.ingresoMensual < 1 /* ||
      empleo.telefonosEmpleo.length < 1 */
    ) {
      return false;
    } else {
      return true;
    }
  }

  tieneConyuge() {
    try {
      if (
        this._solicitudService.solicitudCompleta.infoCliente.cliente
          .idTipoEstadoCivil == 1 ||
        this._solicitudService.solicitudCompleta.infoCliente.cliente
          .idTipoEstadoCivil == 3 ||
        (this._solicitudService.solicitudCompleta.infoCliente.conyugeCliente !=
          null &&
          !isUndefined(
            this._solicitudService.solicitudCompleta.infoCliente.conyugeCliente
          ))
      ) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  okDatosConyuge(): number {
    let conyuge: ConyugeModel = this._solicitudService.solicitudCompleta
      .infoCliente.conyugeCliente;
    try {
      if (!isNullOrUndefined(conyuge)) {
        if (this.validacionesConyuge(conyuge)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }

  validacionesConyuge(conyuge: ConyugeModel): boolean {
    if (
      conyuge.contacto.nombres == "" ||
      conyuge.contacto.primerApellido == "" ||
      conyuge.contacto.fechaNacimiento == "" ||
      conyuge.contacto.rfc == "" ||
      conyuge.telefonosContacto.length < 1
    ) {
      return false;
    } else {
      return true;
    }
  }

  conyugeTieneEmpleo() {
    try {
      if (
        this._solicitudService.solicitudCompleta.infoCliente.conyugeCliente
          .infoEmpleosContacto.length > 0
      ) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  tieneReferencias() {
    try {
      if (this._solicitudService.getContactos(1).length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
  okReferencia(c: DatosContactoModel): number {
    if (
      c.contacto.nombres == "" ||
      c.contacto.primerApellido == "" ||
      c.telefonosContacto.length < 0
    ) {
      return 2;
    } else {
      return 1;
    }
  }

  tieneIngresosFamiliares() {
    try {
      if (this._solicitudService.getContactos(2).length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  okIngreso(c: DatosContactoModel): number {
    if (
      c.contacto.nombres == "" ||
      c.contacto.primerApellido == "" ||
      c.telefonosContacto.length < 0 ||
      c.contacto.importeIngresos < 1
    ) {
      return 2;
    } else {
      return 1;
    }
  }
  requiereAval() {

    return this._solicitudService.requiereAval() ;

   /*  try {
      if (
        this._solicitudService.solicitudCompleta.infoCliente.cliente.genero.includes(
          "Mas"
        )
      ) {
        return false;
      } else if (
        !isUndefined(this._solicitudService.getDatosContacto(3)) &&
        this._solicitudService.getDatosContacto(3) != null
      ) {
        return true;
      } else {
        return true;
      }
    } catch (error) {
      return true;
    } */
  }

  okDatosAval(): number {
    try {
      let aval: DatosContactoModel = this._solicitudService.getDatosContacto(3);
      if (!isNullOrUndefined(aval)) {
        if (this.validacionesAval(aval)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }

  validacionesAval(aval: DatosContactoModel): boolean {
    if (
      aval.contacto.nombres == "" ||
      aval.contacto.primerApellido == "" ||
      aval.contacto.fechaNacimiento == "" ||
      aval.contacto.rfc == "" ||
      aval.contacto.idTipoEstadoCivil < 1 ||
      aval.telefonosContacto.length < 0 ||
      isNullOrUndefined(aval.infoINE)
    ) {
      return false;
    } else {
      return true;
    }
  }

  okDomicilioAval(): number {
    try {
      let domicilio: DomicilioClienteModel = this._solicitudService.getDatosContacto(
        3
      ).domicilioContacto;
      if (!isNullOrUndefined(domicilio)) {
        if (this.validacionesDomicilio(domicilio.domicilio)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }


  existeConyugeAval() {
    try {
      if (
        !isUndefined(this._solicitudService.getDatosContacto(3).contacto) &&
        this._solicitudService.getDatosContacto(3).contacto != null
      ) {
        if (
          this._solicitudService.getDatosContacto(3).contacto
            .idTipoEstadoCivil == 1 ||
          this._solicitudService.getDatosContacto(3).contacto
            .idTipoEstadoCivil == 3 ||
          (!isUndefined(
            this._solicitudService.getDatosContacto(3).conyugeContacto
          ) &&
            this._solicitudService.getDatosContacto(3).conyugeContacto != null)
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  okConyugeAval(): number {
    try {
      let conyuge: ConyugeModel = this._solicitudService.getDatosContacto(3)
        .conyugeContacto;

      if (!isNullOrUndefined(conyuge)) {
        if (this.validacionesConyuge(conyuge)) {
          return 1;
        } else {
          return 2;
        }
      } else {
        return 3;
      }
    } catch (error) {
      return 3;
    }
  }
  okPedido() {
    try {
      if (this._solicitudService.solicitudCompleta.pedidosAdjuntos.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  onClickTitle(screen:string){

    console.log("screen",screen);
    
    this._solicitudService.notificaSideNav.emit(screen);

  }
}
