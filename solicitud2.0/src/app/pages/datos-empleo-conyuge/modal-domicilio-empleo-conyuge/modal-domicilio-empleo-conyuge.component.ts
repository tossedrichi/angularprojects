import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  AfterViewChecked,
  ViewChild,
  ElementRef,
  SimpleChanges
} from "@angular/core";
import { EstadosModel } from "src/app/models/Estados.model";
import { CiudadesModel } from "src/app/models/Ciudades.model";
import { ColoniasModel } from "src/app/models/Colonias.model";
import { SolcitudService } from "src/app/services/solcitud.service";
import { DomicilioModel } from "src/app/models/Domicilio.Model";
import { ToastrService } from "ngx-toastr";
import { DomicilioClienteRequestModel } from "src/app/models/models-request/DomicilioClienteRequest.model";
import { getFechaActual } from "src/app/tools";

@Component({
  selector: "app-modal-domicilio-empleo-conyuge",
  templateUrl: "./modal-domicilio-empleo-conyuge.component.html",
  styleUrls: ["./modal-domicilio-empleo-conyuge.component.css"]
})
export class ModalDomicilioEmpleoConyugeComponent
  implements OnInit, OnChanges, AfterViewChecked {
  @Input() openModal: boolean;
  @Input() dataInput: { idEmpleo: number; domicilioEmpleo: DomicilioModel };
  @Output() dataDomicilioEmpleo: EventEmitter<{
    open: boolean;
    isCancel: boolean;
    data?: any;
  }>;

  @ViewChild("selectEstado") selectEstado: ElementRef;
  @ViewChild("selectCiudad") selectCiudad: ElementRef;
 // @ViewChild("selectColonia") selectColonia1: ElementRef;
  @ViewChild("edtCalle") edtCalle: ElementRef;
  @ViewChild("edtCp") edtCp: ElementRef;
  @ViewChild("edtNoExt") edtNoExt: ElementRef;
  @ViewChild("edtNoInt") edtNoInt: ElementRef;
  @ViewChild("auto") edtAuto: any;

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];
  coloniaSeleccionada: ColoniasModel = new ColoniasModel();
  modalCargado: boolean = false;
  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataDomicilioEmpleo = new EventEmitter();
  }

  ngOnInit() {
    this.getEstados();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes");

    console.log("Changes Domicilio", changes);

    if (changes.dataInput != undefined) {
      this.dataInput.domicilioEmpleo =
        changes.dataInput.currentValue.domicilioEmpleo;
      this.dataInput.idEmpleo = changes.dataInput.currentValue.idEmpleo;
      console.log("Empleo Seleccionado", this.dataInput);
    }
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked");
    if (!this.modalCargado) {
      if (this.openModal) {
        this.getEmpleo();
      }
    }
  }

  getEmpleo() {
    if (this.dataInput != undefined) {
      if (this.dataInput.domicilioEmpleo != undefined) {
        this.edtCalle.nativeElement.value = this.dataInput.domicilioEmpleo.calle;
        this.edtCp.nativeElement.value = this.dataInput.domicilioEmpleo.codigoPostal;
        this.edtNoExt.nativeElement.value = this.dataInput.domicilioEmpleo.numeroExterior;
        this.edtNoInt.nativeElement.value = this.dataInput.domicilioEmpleo.numeroInterior;
        this.getEstadoCiudad(this.dataInput.domicilioEmpleo.idCiudad);
        this.getColonias(this.dataInput.domicilioEmpleo.idCiudad);
        this.coloniaSeleccionada.nombre = this.dataInput.domicilioEmpleo.colonia;

        this.coloniaSeleccionada.sepomex = this.dataInput.domicilioEmpleo.codigoSepomex;
        console.log("Colonia", this.coloniaSeleccionada);

        this.modalCargado = true;

        //this.selectCiudad.nativeElement.value = this.dataInput.domicilioEmpleo.idCiudad;
      }
    }
  }

  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado);
  }

  closeModal() {
    this.modalCargado=false;
    this.openModal = false;
    this.dataDomicilioEmpleo.emit({ open: this.openModal, isCancel: true });
  }

  getEstadoCiudad(idCiudad: number) {
    this._SolicitudService.getEstadoCiudad(idCiudad).subscribe(
      resp => {
        console.log("Estado de  ciudad", resp);
        setTimeout(() => {
          this.selectEstado.nativeElement.value = resp.mensaje;
          this.getCiudades(parseInt(resp.mensaje),true); 
        });
      },
      err => {
        this.toaster.error("Error al recuperar estado");
      }
    );
  }
  getEstados() {
    this._SolicitudService.getEstados().subscribe(respuesta => {
      console.log(respuesta);
      this.listEstados = respuesta;
      this.listCiudades = [];
      this.listColonias = [];
    });
  }

  getCiudades(idestado: number,edicion?:boolean) {
    this._SolicitudService.getCiudades(idestado).subscribe(respuesta => {
      console.log(respuesta);
      this.listCiudades = respuesta;
      this.listColonias = [];
      if(edicion){
        console.log("idCiudad",this.dataInput.domicilioEmpleo.idCiudad)
        setTimeout(() => {
          this.selectCiudad.nativeElement.value=this.dataInput.domicilioEmpleo.idCiudad
        }, 0);
      }
    });
  }

  getColonias(ciudad) {
    this._SolicitudService.getColonias(ciudad).subscribe(respuesta => {
      console.log(respuesta);
      this.listColonias = respuesta;
      setTimeout(() => {
        this.edtAuto.searchInput.nativeElement.value = this.coloniaSeleccionada.nombre;

      }, 0);
    });
  }

  setDomicilioEmpleoConyuge() {
    let domicilio: DomicilioClienteRequestModel = new DomicilioClienteRequestModel();

    domicilio.idEmpleo = this.dataInput.idEmpleo;
    domicilio.idContacto = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;
    domicilio.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    domicilio.idSucursal = this._SolicitudService.solicitud.idSucursal;

    domicilio.idCiudad = this.selectCiudad.nativeElement.value;

    domicilio.colonia = this.coloniaSeleccionada.nombre;

    domicilio.codigoPostal = this.edtCp.nativeElement.value;
    domicilio.calle = this.edtCalle.nativeElement.value;
    domicilio.numeroExterior = this.edtNoExt.nativeElement.value;
    domicilio.numeroInterior = this.edtNoInt.nativeElement.value;
    domicilio.codigoSepomex = this.coloniaSeleccionada.sepomex;

    domicilio.geolocalizacion = "NA";
    domicilio.referencias = "NA";
    domicilio.idTipoDomicilio = 1;
    domicilio.idTipoRelacionCasaFamiliar = -1;
    domicilio.tiempoResidencia = 0;
    domicilio.idTipoCondicionDomicilio = -1;
    domicilio.importeRenta = 0;
    domicilio.habitantesDomicilio = 0;
    domicilio.propieterioDomicilio = -1;
    domicilio.fechaRegistro = getFechaActual();

    console.log("Domicilio", domicilio);
    this._SolicitudService.cargandSolicitud = true;
    this._SolicitudService
      .agregaDomicilioEmpleoContactoSolicitud(domicilio)
      .subscribe(
        resp => {
          this._SolicitudService.cargandSolicitud = false;
          console.log(resp);
          this.toaster.success("Domicilio guardado", "Domicilio Empleo");
          this._SolicitudService
          .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
          .subscribe(resp => {
            this.closeModal();
          });
        },
        err => {
          this._SolicitudService.cargandSolicitud = false;
          console.log(err);
          this.toaster.error(err.message, "Error: Domicilio Empleo");
        }
      );
  }

  selectColonia(event: any) {
    console.log("selecClonia", event);
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();
    this.coloniaSeleccionada = event;

    if (this.coloniaSeleccionada.codigo_postal > 0) {
      console.log(this.coloniaSeleccionada.codigo_postal);

      // this.edtCp.nativeElement.value=this.coloniaSeleccionada.codigo_postal
      console.log(this.edtCp.nativeElement.value);
    }
    console.log(this.coloniaSeleccionada);
  }

  onChangeColonia(event: any) {
    console.log("onChangeColinia", event);
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();

    this.coloniaSeleccionada.nombre = event;
    this.coloniaSeleccionada.sepomex = -1;
    console.log(this.coloniaSeleccionada);
    console.log("changeColonia", event);
  }
  onFocusedColonia(event: any) {}
}
