import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SolcitudService } from "../../services/solcitud.service";
import { EmpleosModel } from "src/app/models/Empleos.model";
import { OtrosIngresosModel } from "src/app/models/OtrosIngresos.model";
import { InfoEmpleosContactoModel } from "../../models/InfoEmpleosContacto.model";
import { EmpleoModel } from "../../models/Empleo.model";
import { EmpleoRequestModel } from "../../models/models-request/EmpleoRequest.model";
import { DatosEmpleoModel } from "../../models/DatosEmpleo.model";
import { TelefonoRequestModel } from "src/app/models/models-request/TelefonoRequest.model";
import { calcularTiempoMeses, getAnos, getMeses } from "src/app/tools";
import { ToastrService } from "ngx-toastr";
import { DataInputModalGuardarModel } from "../../components/modal-guardar/dataInputModa.model";
import { DatosContactoRequestModel } from 'src/app/models/models-request/DatosContactoRequest.model';
import { isNullOrUndefined, log } from 'util';
import { ContactoModel } from 'src/app/models/Contacto.model';
import { TiposEmpleoInformalModel } from 'src/app/models/TiposEmpleoInformal.model';

@Component({
  selector: "app-datos-empleo-conyuge",
  templateUrl: "./datos-empleo-conyuge.component.html",
  styleUrls: ["./datos-empleo-conyuge.component.css"]
})
export class DatosEmpleoConyugeComponent implements OnInit {
  @ViewChild("selectTipoEmpleo") selecTipoEmpleo: ElementRef;
  @ViewChild("selectTipoIngreso") selecTipoIngreso: ElementRef;
  @ViewChild("edtEmpresa") edtEmpresa: ElementRef;
  @ViewChild("edtEmpleo") edtEmpleo: ElementRef;
  @ViewChild("edtPuesto") edtPuesto: ElementRef;
  @ViewChild("edtIngresoMensual") edtIngreso: ElementRef;
  @ViewChild("edtTelefono") edtTelefono: ElementRef;
  @ViewChild("edtAnos") edtAnos: ElementRef;
  @ViewChild("edtMeses") edtMeses: ElementRef;
  @ViewChild("edtN") edtNombre: ElementRef;
  @ViewChild("edtApPa") edtApellidoP: ElementRef;
  @ViewChild("edtApMa") edtApellidoM: ElementRef;
  @ViewChild("edtT") edtTelContacto: ElementRef;
  @ViewChild("edtOtros") edtOtros: ElementRef;
  @ViewChild("edtEmpresa2") edtEmpresa2: ElementRef;
  @ViewChild("selecTipoEmpleoInformal") selecTipoEmpleoInformal: ElementRef;



  //infoEmpleoConyugeCliente: InfoEmpleosContactoModel[];

  //infoEmpleoSeleccionado: InfoEmpleosContactoModel;

  listEmpleos: EmpleosModel[] = [];
  listOtrosIngresos: OtrosIngresosModel[] = [];
  listEmpleoInformal: TiposEmpleoInformalModel[] = [];

  listEmpleosConyuge: DatosEmpleoModel[] = [];
  empleoSeleccionado: DatosEmpleoModel;

  dataInputModalConfirm: DataInputModalGuardarModel;
  instanceModalConf: number;

  openModaldDomicilioEmpleo: boolean = false;
  openModaldDocumentosEmpleo: boolean = false;
  openModalContactoEmpleo: boolean = false;
  openModalConfirm: boolean = false;
  empleoConyugeCliente: boolean = true;

  empresa: boolean = true;
  empleo: boolean = true;
  puesto: boolean = true;
  telefono: boolean = true;
  tipoEmpleo: boolean = true;
  tipoIngreso: boolean = false;
  tipoEmpleoInformal: boolean = false;
  empresa2: boolean = false;
  otros: boolean = false;


  esEdicion: boolean = false;
  generoF: boolean = false;
  generoM: boolean = false;
  idContactoEmpleo: number = -1;

  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    /* this.infoEmpleoConyugeCliente = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto; */

    //this.infoEmpleoSeleccionado = new InfoEmpleosContactoModel();

    this.getEmpleos();
    this.getOtrosIngresos();
    this.obtenerEmpleos();
    this.getTipoEmpleoInformal();
 
    
    if(this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")==0).length>0){
      this.empleoSeleccionado=this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")==0)[0];
      this.esEdicion=true;
      console.log("sva a editar NA")
    }
  }
  obtenerEmpleos() {
    this.listEmpleosConyuge = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")!=0);

    if (this.listEmpleosConyuge.length <= 0) {
      this.listEmpleosConyuge = [];
    }
  }

  getEmpleos() {
    this._SolicitudService.getEmpleos().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleos = respuesta;
    });
  }

  getOtrosIngresos() {
    this._SolicitudService.getOtrosIngresos().subscribe(respuesta => {
      console.log(respuesta);
      this.listOtrosIngresos = respuesta;
    });
  }
  getTipoEmpleoInformal() {
    this._SolicitudService.getTipoEmpleoInformal().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleoInformal = respuesta;
    });
  }

  respDataModalEmpleoConyuge(event) {
    console.log(event);
    this.obtenerEmpleos();
    if (event.isCancel) {
      this.openModaldDomicilioEmpleo = event.open;
    }
  }

  getTipoNombreEmpleo(empleo: EmpleoModel) {
    for (let emp of this.listEmpleos) {
      if (empleo.idTipoEmpleo == emp.id_tipo_empleo) {
        return emp.tipo_empleo;
      }
    }
  }
  resposeDataContactoEmpleo(event) {
    console.log(event);

    if (event.isSave) {
      this.idContactoEmpleo = event.idContacto;
      this.setEmpleoConyugeCliente();
    } else {
      this.idContactoEmpleo = -1;
    }
    this.openModalContactoEmpleo = false;
  }

  selectedEmpleo(infoEmpleo: DatosEmpleoModel) {
    this.empleoSeleccionado = infoEmpleo;
    this.selecTipoEmpleo.nativeElement.value = this.getTipoEmpleo();
  }

  onClickContactoEmpleo(empleo: DatosEmpleoModel) {
    this.openModalContactoEmpleo = true;
    this.empleoSeleccionado = empleo;
  }

  onSelectTipoEmpleo(idTipoEmpleo: string) {
    console.log(idTipoEmpleo);

    switch (parseInt(idTipoEmpleo)) {
      case 1:
        console.log("Formal");
        this.empresa = true;
        this.empleo = true;
        this.puesto = true;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
      case 2:
        console.log("Informal");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = false;


        break;
      case 3:
        console.log("Negocio propio");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = true;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
      case 4:
        console.log("Otros ingresos");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = true;
        this.empresa2 = false;
        this.tipoEmpleoInformal = false;
        this.otros = false;


        break;
    }
  }
  onSelectTipoEmpleoInformal(idTipoEmpleoInformal: string) {
    console.log(idTipoEmpleoInformal);

    switch (parseInt(idTipoEmpleoInformal)) {
      case 1:
        console.log("Empleada doméstica");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = false;

        break;
        case 2:
        console.log("Otros");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        this.empresa2 = false;
        this.tipoEmpleoInformal = true;
        this.otros = true;

        break;
       

    }
  }
  /*  getTiposEmpleo(idTipo: number): string {
    let empleo: EmpleosModel[] = this.listEmpleos.filter(x => x.id_tipo_empleo == idTipo);

    return empleo[0].tipo_empleo;

  } */
  onClickDomicilioEmpleo(empleo: DatosEmpleoModel) {
    this.empleoSeleccionado = empleo;
    this.openModaldDomicilioEmpleo = true;
  }

  onClickOtroEmpleo() {
    this.empresa = true;
    this.empleo = true;
    this.puesto = true;
    this.telefono = true;
    this.tipoEmpleo = true;
    this.tipoIngreso = false;
    this.limpiarForm();
  }

  getTipoEmpleo() {
    if (this.empleoSeleccionado.empleo.idTipoEmpleo >= 1) {
      return this.empleoSeleccionado.empleo.idTipoEmpleo;
    }
    return "-1";
  }

  resposeDataDocumentosEmpleo(event) {
    console.log(event);
    this.openModaldDocumentosEmpleo = false;
    if (event.isCancel) {
      this.openModaldDocumentosEmpleo = event.open;
    }
  }

  onClickGuardar() {
    console.log("EsEdicion",this.esEdicion);
    if (this.esEdicion) {
      console.log("EsEdicion");
      this.setEditaContactoEmpleoCliente();
    } else {
      this.setContactoEmpleoCony();
    }
  }
  validarContacto(c:DatosContactoRequestModel){
    if (c.nombres.localeCompare("")!=0 && c.primerApellido.localeCompare("")!=0 // && "".localeCompare(this.edtTelContacto.nativeElement.value)!=0
    ){
      return true;
    }else{
      return false;
    }
  }

  setContactoEmpleoCony() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 6;
    contacto.idTipoRelacionContacto = -1;
    //contacto = this.empleo.empleo.idEmpleo;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.correo = "NA";
    contacto.rfc = "NA";
    contacto.fechaNacimiento = "1900-01-01";

    
    if(this.generoM){
      contacto.genero="M"
    }
    if(this.generoF){
      contacto.genero="F"
    }
    if(contacto.nombres.localeCompare("")!=0){
      if(this.validarContacto(contacto)){
        console.log("Envio", contacto);
        this._SolicitudService.cargandSolicitud=true;
        this._SolicitudService.agregaContactoClientSolicitud(contacto).subscribe(
          resp => {
            this.toaster.success("Contacto guardado", "Contacto Empleo");
            console.log("Repsuesta", resp);
            this._SolicitudService.cargandSolicitud=false;
            if(resp.idContacto>0){
              this.idContactoEmpleo=resp.idContacto;
              //this.guardarTelefonoContacto();
              this.setEmpleoConyugeCliente();
              this.toaster.success("Guardado", "Contacto guardado");
            }
  
            //this.closeModal(true,resp.idContacto);
          },
          err => {
            //this._solcitudService.cargandSolicitud=false;
            this.toaster.error(err.message, "Error: Contacto Empleo");
            console.log(err);
          }
        );
      }else{
        this.toaster.error("Error", "Termina de llenar los datos del contacto");
      }
     
    }else{
      console.log("Envio", "Empleo");
      this.setEmpleoConyugeCliente();
    }

  }
  guardarTelefonoContacto(){
    // if(this.getTelefono().localeCompare(this.telefonoMovil.nativeElement.value)!=0){
       this._SolicitudService.cargandSolicitud=true;
      // console.log("guardar telefono: viejo ",this.getTelefono()+" nuevo "+this.telefonoMovil.nativeElement.value)
       let telefono:TelefonoRequestModel= new TelefonoRequestModel();
       telefono.agente= SolcitudService.agente;
       telefono.idContacto=  this.idContactoEmpleo;
       telefono.idOrigen=-1;
       telefono.idSucursal= this._SolicitudService.solicitud.idSucursal;
       telefono.idTipoTelefono=2;
       telefono.telefono=this.edtTelContacto.nativeElement.value;
       telefono.idSolicitud=this._SolicitudService.solicitud.idSolicitud;
      // console.log("guarda",telefono);
      //this.guardoCel=true;
       this._SolicitudService.agregaTelefonoContactoClienteSolicitud(telefono).
       subscribe(response=>{
         this._SolicitudService.cargandSolicitud=false;
        // this.cargaSolicitud();
        this.toaster.success("Guardado", "Teléfono del Contacto guardado");
         this.setEmpleoConyugeCliente()
       },error=>{
         //this._SolicitudService.cargandSolicitud=false;
         //this.guardoCelExitoso=false;
       });
 
 
    // }else{
      // this.cargaSolicitud();
    // }
   }
 

  setEmpleoConyugeCliente() {
    let empleoCnyCl: EmpleoRequestModel = new EmpleoRequestModel();

    empleoCnyCl.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    empleoCnyCl.agente = this._SolicitudService.solicitud.idAgente;
    empleoCnyCl.idOrigen = -1;
    empleoCnyCl.idTipoEmpleo = this.selecTipoEmpleo.nativeElement.value;
 //   empleoCnyCl.empleo = this.edtEmpleo.nativeElement.value;
    empleoCnyCl.estuvoDesempleado = false;
    empleoCnyCl.ingresoMensual = this.edtIngreso.nativeElement.value;
 //   empleoCnyCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
   // empleoCnyCl.puesto = this.edtPuesto.nativeElement.value;

    if (this.selecTipoEmpleo.nativeElement.value == 4) {
      empleoCnyCl.idTipoIngreso = this.selecTipoIngreso.nativeElement.value;
    } else {
      empleoCnyCl.idTipoIngreso = -1;
    }
    empleoCnyCl.periodoDesempleo = 0;
    empleoCnyCl.idContactoEmpleo = -1;
    empleoCnyCl.antiguedadEmpleoAnterior = 0;

    empleoCnyCl.antiguedad = calcularTiempoMeses(
      parseInt(this.edtAnos.nativeElement.value),
      parseInt(this.edtMeses.nativeElement.value)
    );

    empleoCnyCl.idContacto = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;
    console.log("Empleo Enviado", empleoCnyCl);
    empleoCnyCl.idContactoEmpleo = this.idContactoEmpleo;


      switch (parseInt(empleoCnyCl.idTipoEmpleo.toString())) {
        case 1:
          empleoCnyCl.empleo = this.edtEmpleo.nativeElement.value;
          empleoCnyCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
          empleoCnyCl.puesto = this.edtPuesto.nativeElement.value;
  
          break;
        case 2:
          if (this.selecTipoEmpleoInformal.nativeElement.value == 1) {
            
            empleoCnyCl.nombreEmpresa = "Empleada domestica";
            empleoCnyCl.empleo = "Empleada domestica";
            empleoCnyCl.puesto = "Empleada domestica";
          } else if (this.selecTipoEmpleoInformal.nativeElement.value == 2) {
            empleoCnyCl.nombreEmpresa = this.edtOtros.nativeElement.value;
            empleoCnyCl.empleo = this.edtOtros.nativeElement.value;
            empleoCnyCl.puesto = this.edtOtros.nativeElement.value;
          }
  
          break;
        case 3:
          empleoCnyCl.nombreEmpresa = this.edtEmpresa2.nativeElement.value;
          empleoCnyCl.empleo = this.edtEmpresa2.nativeElement.value;
          empleoCnyCl.puesto = this.edtEmpresa2.nativeElement.value;
  
          break;
        case 4:
          let tipo = this.listOtrosIngresos.filter(x => x.id_tipo_ingreso == 1)[0]
            .tipo_ingreso;
  
            empleoCnyCl.nombreEmpresa = tipo;
            empleoCnyCl.empleo = tipo;
            empleoCnyCl.puesto = tipo;
          break;
      }

      if(empleoCnyCl.idTipoEmpleo>0 &&
        empleoCnyCl.nombreEmpresa.localeCompare("")!=0 &&
        empleoCnyCl.puesto.localeCompare("")!=0 &&
        empleoCnyCl.ingresoMensual>0){
       this._SolicitudService.cargandSolicitud=true;
      this._SolicitudService
        .agregaEmpleoContactoClienteSolicitud(empleoCnyCl)
        .subscribe(
          resp => {
           
            console.log(resp);
            this.toaster.success(
              "Datos empleo guardados",
              "Datos Empleo Cónyuge"
            );
            this._SolicitudService.cargandSolicitud=false;
            if(empleoCnyCl.idTipoEmpleo==1||empleoCnyCl.idTipoEmpleo==3){
              this.setTelefonoEmpleo(this.empleoSeleccionado.empleo.idEmpleo);
            }else{
              this._SolicitudService
              .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
              .subscribe(
                resp => {
                 // this.toaster.success("Telefono Guardado", "Datos Cónyuge");
                  this._SolicitudService.cargandSolicitud=false;
                  this.listEmpleosConyuge =
                    resp.infoCliente.conyugeCliente.infoEmpleosContacto;
                  console.log(resp);
                  this.obtenerEmpleos();
                  this.limpiarForm();
                },
                err => {
                  this._SolicitudService.cargandSolicitud=false;
                  console.log(err);
                  this.toaster.error(
                    err.message,
                    "Error: Traer informacion empleo"
                  );
                }
              );
            }
          
          },
          err => {
            this.toaster.error(err.message, "Error: Datos Empleo Cónyuge");
            console.log(err);
          }
        );
        
    }else{
      this.toaster.error("Error", "Llena los campos obligatorios");
    }
    
  }

  onClickAgregarDocumentos(empleo: DatosEmpleoModel) {
    this.empleoSeleccionado = empleo;
    this.openModaldDocumentosEmpleo = true;
  }

  /* setEdicionEmpleoConyugeCliente() {
    let empleoCnyCl: EmpleoRequestModel = new EmpleoRequestModel();

    empleoCnyCl.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    empleoCnyCl.Agente = this._SolicitudService.solicitud.idAgente;
    empleoCnyCl.idOrigen = -1;
    empleoCnyCl.idTipoEmpleo = this.selecTipoEmpleo.nativeElement.value;
    empleoCnyCl.empleo = this.edtEmpleo.nativeElement.value;
    empleoCnyCl.estuvoDesempleado = false;
    empleoCnyCl.ingresoMensual = this.edtIngreso.nativeElement.value;
    empleoCnyCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
    empleoCnyCl.puesto = this.edtPuesto.nativeElement.value;
    empleoCnyCl.Antiguedad = this.edtAnos.nativeElement.value;
    empleoCnyCl.idTipoIngreso = this.edtIngreso.nativeElement.value;

    empleoCnyCl.Antiguedad = calcularTiempoMeses(
      this.edtAnos.nativeElement.value,
      this.edtMeses.nativeElement.value
    );

    empleoCnyCl.idContactoEmpleo = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;

    this._SolicitudService
      .editar(empleoCnyCl)
      .subscribe(
        resp => {
          console.log(resp);
          this.toaster.success(
            "Datos empleo guardados",
            "Datos Empleo Cónyuge"
          );

          this.setTelefonoEmpleo(resp.idEmpleo);
        },
        err => {
          this.toaster.error(err.message, "Error: Datos Empleo Cónyuge");
          console.log(err);
        }
      );
  } */

  onClickEditarEmpelo(empleo: DatosEmpleoModel) {
    this.limpiarForm();
    console.log("editar",empleo);
    this.esEdicion = true;
    this.empleoSeleccionado = empleo;
    this.onSelectTipoEmpleo(empleo.empleo.idTipoEmpleo.toString());


    this.selecTipoEmpleo.nativeElement.value = empleo.empleo.idTipoEmpleo;

    if (empleo.empleo.idTipoEmpleo == 4) {
      setTimeout(()=>{
        this.selecTipoIngreso.nativeElement.value = this.listOtrosIngresos.filter(x=> x.tipo_ingreso.localeCompare(empleo.empleo.empleo)==0)[0].id_tipo_ingreso
      },0)
      this.edtIngreso.nativeElement.value = empleo.empleo.ingresoMensual;

      this.edtAnos.nativeElement.value = getAnos(empleo.empleo.antiguedad);
      this.edtMeses.nativeElement.value = getMeses(empleo.empleo.antiguedad);
    } else {
      switch (empleo.empleo.idTipoEmpleo) {
        case 1:
          console.log("Formal");

          

          this.edtEmpresa.nativeElement.value = empleo.empleo.nombreEmpresa;
          this.edtEmpleo.nativeElement.value = empleo.empleo.empleo;
          this.edtPuesto.nativeElement.value = empleo.empleo.puesto;
          
  
          break;
        case 2:
          console.log("Informal");
         
            this.onSelectTipoEmpleoInformal(empleo.empleo.idTipoEmpleoInformal.toString())
            setTimeout(()=>{ 
              console.log(this.selecTipoEmpleoInformal);
              if(empleo.empleo.idTipoEmpleoInformal==-1){
                if(empleo.empleo.empleo.localeCompare("Empleada domestica")==0){
                  empleo.empleo.idTipoEmpleoInformal=1
                }else{
                  empleo.empleo.idTipoEmpleoInformal=2
                }
              }
              this.selecTipoEmpleoInformal.nativeElement.value =empleo.empleo.idTipoEmpleoInformal
            },0)
           
        
          
          try {
            this.edtOtros.nativeElement.value = empleo.empleo.empleo;
          } catch (error) {
            
          }
          
         
          //this.tipoEmpleo = true;
         // this.tipoIngreso = false;
         // this.empresa2 = false;
         // this.tipoEmpleoInformal = true;
         // this.otros = false;
  
  
          break;
        case 3:
          console.log("Negocio propio");
          setTimeout(()=>{
            this.edtEmpresa2.nativeElement.value = empleo.empleo.nombreEmpresa;
          },0)
          
  
  
          break;
        case 4:
          console.log("Otros ingresos");
          setTimeout(()=>{
            this.selecTipoIngreso.nativeElement.value= empleo.empleo.tipoIngreso;
          },0)
         // this.selecTipoIngreso.nativeElement.value= empleo.empleo.tipoIngreso;
         // this.empresa = false;
         // this.empleo = false;
         // this.puesto = false;
         // this.telefono = false;
         // this.tipoEmpleo = true;
         // this.tipoIngreso = true;
         // this.empresa2 = false;
         // this.tipoEmpleoInformal = false;
         // this.otros = false;
  
  
          break;
      }
  
      this.edtIngreso.nativeElement.value = empleo.empleo.ingresoMensual;

      this.edtAnos.nativeElement.value = getAnos(empleo.empleo.antiguedad);
      this.edtMeses.nativeElement.value = getMeses(empleo.empleo.antiguedad);
    }
    if(empleo.telefonosEmpleo.length>0){
      this.edtTelefono.nativeElement.value = empleo.telefonosEmpleo[empleo.telefonosEmpleo.length-1].numeroTelefonico;
    }
    if(!isNullOrUndefined(empleo.contactoEmpleo)){
      this.edtNombre.nativeElement.value= empleo.contactoEmpleo.nombres;
      this.edtApellidoP.nativeElement.value= empleo.contactoEmpleo.primerApellido;
      this.edtApellidoM.nativeElement.value=empleo.contactoEmpleo.segundoApellido;
      if(empleo.contactoEmpleo.genero.localeCompare("F")==0){
        this.generoF=true;
        this.generoM=false;
      }
      if(empleo.contactoEmpleo.genero.localeCompare("M")==0){
        this.generoF=false;
        this.generoM=true;
      }
    }
    
    /* } */
  }

  onClickCambiaGenero(masc:boolean){
    this.generoF=!masc;
    this.generoM=masc;
  } 

  limpiarForm() {
    //this.empleoSeleccionado = null;
    
    this.edtAnos.nativeElement.value = 0;
    try {
      this.edtEmpleo.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtTelefono.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
       this.edtEmpresa.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtPuesto.nativeElement.value = "";
    } catch (error) {
      
    }
    try {
      this.edtEmpresa2.nativeElement.value = "";
   } catch (error) {
     
   }
   try {
    this.selecTipoEmpleoInformal.nativeElement.value = "";
 } catch (error) {
   
 }
 try {
  this.selecTipoIngreso.nativeElement.value = "";
} catch (error) {
 
}
    this.generoF=false;
    this.generoM=false;
    
    this.edtMeses.nativeElement.value = 0;
    this.edtIngreso.nativeElement.value = 0;
    this.selecTipoEmpleo.nativeElement.value = "-1";
    this.edtNombre.nativeElement.value="";
    this.edtApellidoP.nativeElement.value="";
    this.edtApellidoM.nativeElement.value="";
    this.esEdicion = false;
  //  this.edtTelContacto.nativeElement.value="";
  }

  setTelefonoEmpleo(idEmpleo) {
    let telefono: TelefonoRequestModel = new TelefonoRequestModel();

    telefono.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    telefono.idEmpleo = idEmpleo;
    telefono.idSucursal = this._SolicitudService.solicitud.idSucursal;
    telefono.idTipoTelefono = 2;
    telefono.telefono = this.edtTelefono.nativeElement.value;
    //if(telefono.telefono.localeCompare(this.empleoSeleccionado.telefonosEmpleo[this.empleoSeleccionado.telefonosEmpleo.length-1].numeroTelefonico)!=0){
      this._SolicitudService.cargandSolicitud=true;
      this._SolicitudService
        .agregaTelefonoEmpleoClienteSolicitud(telefono)
        .subscribe(
          resp => {
            console.log(resp);
  
            
            this._SolicitudService
              .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
              .subscribe(
                resp => {
                  this.toaster.success("Telefono Guardado", "Datos Cónyuge");
                  this._SolicitudService.cargandSolicitud=false;
                  this.listEmpleosConyuge =
                    resp.infoCliente.conyugeCliente.infoEmpleosContacto;
                  console.log(resp);
                  this.obtenerEmpleos();
                  this.limpiarForm();
                },
                err => {
                  this._SolicitudService.cargandSolicitud=false;
                  console.log(err);
                  this.toaster.error(
                    err.message,
                    "Error: Traer informacion empleo"
                  );
                }
              );
          },
          err => {
            console.log(err);
            this._SolicitudService.cargandSolicitud=false;
            this.toaster.error(err.message, "Datos Cónyuge : Error telefonos");
          }
        );
    //}
    
  }

  onClickEliminarEmpleo(empleo: DatosEmpleoModel) {
    /**
     * NOTE Esta funcion abre el modal de confirmacion
     * el cual responde en la funcion onConfirmModal
     * @param empleo
     */

    this.empleoSeleccionado = empleo;
    this.instanceModalConf = 1;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Eliminar Empleo",
      "¿Deseas eliminar el empleo?",
      []
    );
    console.log(empleo);
  }

  onClickGuardarEmpleo() {
    this.instanceModalConf = 2;
    this.openModalConfirm = true;
    this.dataInputModalConfirm = new DataInputModalGuardarModel(
      "Empleo Conyuge del Cliente",
      "¿Desea guardar el empleo del conyuge del cliente?",
      []
    );
  }

  onConfirmModal(event: any) {
    this.openModalConfirm = false;

    if (event.save) {
      switch (this.instanceModalConf) {
        case 1:
          this.setEliminarEmpleo();
          break;
        case 2:
          console.log("EsEdicion",this.esEdicion);
          if (this.esEdicion) {
            console.log("EsEdicion");
            this.setEditaContactoEmpleoCliente();
          } else {
            this.setContactoEmpleoCony();
          }
          break; 
      }
    /* } else {
      if (this.instanceModalConf == 2) {
        this.setEmpleoConyugeCliente();
      }*/
    } 
  }

  setEliminarEmpleo() {
    this._SolicitudService.cargandSolicitud=true;
    this._SolicitudService
      .setEmliminarEmpleoSolcitud(
        this.empleoSeleccionado.empleo.idEmpleo,
        -1,
        this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente
          .contacto.idContacto
      )
      .subscribe(
        resp => {
          this._SolicitudService.cargandSolicitud=false;
          console.log(resp);
          let empleos: DatosEmpleoModel[] = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto.filter(
            x => x.empleo.idEmpleo != this.empleoSeleccionado.empleo.idEmpleo
          );

          console.log("empleos", empleos);

          this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.infoEmpleosContacto = empleos;
          this.listEmpleosConyuge = empleos;
          this.toaster.success("Empleo Elimniado", "Eliminar Empleo");
        },
        err => {
          this._SolicitudService.cargandSolicitud=false;
          console.log(err);
          this.toaster.error(err.message, "Error: Eliminar Empleo");
        }
      );
  }

  editarEmpleo(){
    if(isNullOrUndefined(this.empleoSeleccionado.contactoEmpleo)){
      this.empleoSeleccionado.contactoEmpleo= new ContactoModel();
      this.empleoSeleccionado.contactoEmpleo.nombres="";
      this.empleoSeleccionado.contactoEmpleo.primerApellido="";
      this.empleoSeleccionado.contactoEmpleo.segundoApellido="";
    }
    if(this.empleoSeleccionado.contactoEmpleo.nombres.localeCompare(this.edtNombre.nativeElement.value)!=0||
    this.empleoSeleccionado.contactoEmpleo.primerApellido.localeCompare(this.edtApellidoP.nativeElement.value)!=0||
    this.empleoSeleccionado.contactoEmpleo.segundoApellido.localeCompare(this.edtApellidoM.nativeElement.value)!=0){
     console.log("empzamos a editar contacto");
      this.setEditaContactoEmpleoCliente();
    }else{
      console.log("sin editar contacto");
      this.setEditarEmpleoCliente();
    }
  }

  setEditaContactoEmpleoCliente() {
    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 6;
    contacto.idTipoRelacionContacto = -1;
    //contacto = this.empleo.empleo.idEmpleo;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.correo = "NA";
    contacto.rfc = "NA";
    contacto.fechaNacimiento = "1900-01-01";

    
    if(this.generoM){
      contacto.genero="M"
    }
    if(this.generoF){
      contacto.genero="F"
    }
    if(contacto.nombres.localeCompare("")!=0){
      if(this.validarContacto(contacto)){
        console.log("Envio", contacto);
        this._SolicitudService.cargandSolicitud=true;
        this._SolicitudService.agregaContactoClientSolicitud(contacto).subscribe(
          resp => {
            this.toaster.success("Contacto guardado", "Contacto Empleo");
            console.log("Repsuesta", resp);
            this._SolicitudService.cargandSolicitud=false;
            if(resp.idContacto>0){
              this.idContactoEmpleo=resp.idContacto;
              //this.editaTelefonoContacto();
              this.setEditarEmpleoCliente();
              this.toaster.success("Guardado", "Contacto guardado");
            }
  
            //this.closeModal(true,resp.idContacto);
          },
          err => {
            //this._solcitudService.cargandSolicitud=false;
            this.toaster.error(err.message, "Error: Contacto Empleo");
            console.log(err);
          }
        );
      }else{
        this.toaster.error("Error", "Termina de llenar los datos del contacto");
      }
     
    }else{
      console.log("Envio", "Empleo");
      this.setEditarEmpleoCliente();
    }

  }
/*
  editaTelefonoContacto(){
    // if(this.getTelefono().localeCompare(this.telefonoMovil.nativeElement.value)!=0){
       this._solicitudService.cargandSolicitud=true;
      // console.log("guardar telefono: viejo ",this.getTelefono()+" nuevo "+this.telefonoMovil.nativeElement.value)
       let telefono:TelefonoRequestModel= new TelefonoRequestModel();
       telefono.agente= this._solicitudService.agente;
       telefono.idContacto=  this.idContactoEmpleo;
       telefono.idOrigen=-1;
       telefono.idSucursal= this._solicitudService.solicitud.idSucursal;
       telefono.idTipoTelefono=2;
       telefono.telefono=this.edtTelContacto.nativeElement.value;
       telefono.idSolicitud=this._solicitudService.solicitud.idSolicitud;
      // console.log("guarda",telefono);
      //this.guardoCel=true;
       this._solicitudService.agregaTelefonoContactoClienteSolicitud(telefono).
       subscribe(response=>{
         this._solicitudService.cargandSolicitud=false;
        // this.cargaSolicitud();
        this.toaster.success("Guardado", "Teléfono del Contacto guardado");
         this.setEditarEmpleoCliente()
       },error=>{
         //this._SolicitudService.cargandSolicitud=false;
         //this.guardoCelExitoso=false;
       });
 
 
    // }else{
      // this.cargaSolicitud();
    // }
   }
*/
   setEditarEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();
    empleoCl.idContacto = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;
    empleoCl.idSolicitud = this._SolicitudService.solicitud.idSolicitud;
    empleoCl.agente = this._SolicitudService.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = this.selecTipoEmpleo.nativeElement.value;
    //empleoCl.empleo = this.edtEmpleo.nativeElement.value;
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = this.edtIngreso.nativeElement.value;
    //empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
    //empleoCl.puesto = this.edtPuesto.nativeElement.value;
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = this.idContactoEmpleo;
    empleoCl.idEmpleo=this.empleoSeleccionado.empleo.idEmpleo;
    console.log("Select Tipo ingreso", this.selecTipoIngreso);

    if (this.selecTipoEmpleo.nativeElement.value == 4) {
      empleoCl.idTipoIngreso = this.selecTipoIngreso.nativeElement.value;
    } else {
      empleoCl.idTipoIngreso = -1;
    }
 
    empleoCl.antiguedad = calcularTiempoMeses(
      parseInt(this.edtAnos.nativeElement.value),
      parseInt(this.edtMeses.nativeElement.value)
    );
  
      console.log("editar empleo",empleoCl)
      this._SolicitudService.cargandSolicitud=true;
      switch (parseInt(empleoCl.idTipoEmpleo.toString())) {
        case 1:
          empleoCl.empleo = this.edtEmpleo.nativeElement.value;
          empleoCl.nombreEmpresa = this.edtEmpresa.nativeElement.value;
          empleoCl.puesto = this.edtPuesto.nativeElement.value;
          
          break;
        case 2:
          console.log("tipo empl inf",this.selecTipoEmpleoInformal.nativeElement.value)
          if (this.selecTipoEmpleoInformal.nativeElement.value == 1) {
           /* let tipo = this.listEmpleoInformal.filter(
              x => x.id_tipo_empleo_informal == 1
            )[0].tipo_empleo_informal;
  */
            empleoCl.nombreEmpresa = "Empleada domestica";
            empleoCl.empleo = "Empleada domestica";
            empleoCl.puesto = "Empleada domestica";
            console.log(empleoCl)
          } else if (this.selecTipoEmpleoInformal.nativeElement.value == 2) {
            empleoCl.nombreEmpresa = this.edtOtros.nativeElement.value;
            empleoCl.empleo = this.edtOtros.nativeElement.value;
            empleoCl.puesto = this.edtOtros.nativeElement.value;
            console.log(empleoCl)
          }
  
          break;
        case 3:
          empleoCl.nombreEmpresa = this.edtEmpresa2.nativeElement.value;
          empleoCl.empleo = this.edtEmpresa2.nativeElement.value;
          empleoCl.puesto = this.edtEmpresa2.nativeElement.value;
  
          break;
        case 4:
          let tipo = this.listOtrosIngresos.filter(x => x.id_tipo_ingreso == 1)[0]
            .tipo_ingreso;
  
            empleoCl.nombreEmpresa = tipo;
            empleoCl.empleo = tipo;
            empleoCl.puesto = tipo;
          break;
      }
      if (
        empleoCl.idTipoEmpleo > 0 &&
        empleoCl.nombreEmpresa.localeCompare("") != 0 &&
        empleoCl.puesto.localeCompare("") != 0 &&
        empleoCl.ingresoMensual > 0
      ) {
     

      this._SolicitudService.actualizaEmpleoContactoClienteSolicitud(empleoCl).subscribe(
        resp => {
          console.log(resp);
          this.toaster.success(
            "Empleo guardado correctamente",
            "Datos Empleo Conyuge"
          );
          this._SolicitudService.cargandSolicitud=false;
          if(resp.procesado){
            if(empleoCl.idTipoEmpleo==1||empleoCl.idTipoEmpleo==3){
              this.setTelefonoEmpleo(this.empleoSeleccionado.empleo.idEmpleo);
            }else{
              this._SolicitudService
              .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
              .subscribe(
                resp => {
                 // this.toaster.success("Telefono Guardado", "Datos Cónyuge");
                  this._SolicitudService.cargandSolicitud=false;
                  this.listEmpleosConyuge =
                    resp.infoCliente.conyugeCliente.infoEmpleosContacto;
                  console.log(resp);
                  this.obtenerEmpleos();
                  this.limpiarForm();
                },
                err => {
                  this._SolicitudService.cargandSolicitud=false;
                  console.log(err);
                  this.toaster.error(
                    err.message,
                    "Error: Traer informacion empleo"
                  );
                }
              );
            }
          }
         
        },
        err => {
          this._SolicitudService.cargandSolicitud=false;
          console.log(err);
          this.toaster.error(err.message, "Datos empleo: Error al guardar");
        }
      ); } else {
    
        this.toaster.error(
          "Error al guardar",
          "Llena los campos obligatorios "
        );
      }
     
   
  }

  
}
