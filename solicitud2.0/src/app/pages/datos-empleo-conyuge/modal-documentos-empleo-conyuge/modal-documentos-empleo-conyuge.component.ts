import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  SimpleChanges
} from "@angular/core";
import { FileItem } from "src/app/models/file-item";
import { SolcitudService } from "../../../services/solcitud.service";
import { FormGroup, FormArray, FormControl } from "@angular/forms";
import { renderImage } from "src/app/tools";
import { isUndefined } from "util";
import { DocumentoRequestModel } from "src/app/models/models-request/DocumentoRequest.model";
import { ToastrService } from "ngx-toastr";
import { DocumetnosDomicilioModel } from "src/app/models/DocumentosDomicilioModel";
import { TiposComprobanteDomicilioModel } from "src/app/models/TiposComprobanteDomicilio.model";
import { TiposComprobanteIngresosModel } from "src/app/models/TiposComprobanteIngresos.model";
import { DatosEmpleoModel } from "src/app/models/DatosEmpleo.model";
import { EmpleoModel } from "src/app/models/Empleo.model";
import { DocumentoEmpleo } from "src/app/models/DocumentoEmpleoModel";

@Component({
  selector: "app-modal-documentos-empleo-conyuge",
  templateUrl: "./modal-documentos-empleo-conyuge.component.html",
  styleUrls: ["./modal-documentos-empleo-conyuge.component.css"]
})
export class ModalDocumentosEmpleoConyugeComponent implements OnInit {
  @Input() openModal: boolean;
  @Input() empleoSeleccionado: EmpleoModel;
  @Output() dataDocumentosEmpleo: EventEmitter<{
    open: boolean;
    data?: any;
  }>;

  form: FormGroup;
  listArchivosNuevos: FileItem[] = [];
  guardando: boolean = false;
  preView = false;
  archivoSeleccionado: FileItem;
  estaSobreDrop: boolean = false;
  documetosDomicilio: DocumentoEmpleo[];

  listArchivos: FileItem[] = [];

  listTiposComprobantesIngresos: TiposComprobanteIngresosModel[] = [];

  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataDocumentosEmpleo = new EventEmitter();
  }

  ngOnInit() {
    this.initForm();
    this.cargarDocumentos();
    this.getTiposComprobantesDomicilio();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.listArchivos = [];

    console.log(changes);
    if (changes.empleoSeleccionado != undefined) {
      this.empleoSeleccionado = changes.empleoSeleccionado.currentValue;
      this.cargarDocumentos();
      console.log("cargando documentos");
      
    }
  }

  initForm() {
    this.form = new FormGroup({
      selecTipoComprobante: new FormArray([])
    });
  }

  addSelectTipoComprobante(idTipoComprobante: string, isCarga: boolean) {
    if (isCarga) {
      (<FormArray>this.form.controls["selecTipoComprobante"]).push(
        new FormControl(idTipoComprobante)
      );
    } else {
      (<FormArray>this.form.controls["selecTipoComprobante"]).insert(
        0,
        new FormControl(idTipoComprobante)
      );
    }
  }

  onDropArchivoNuevo(archivo: FileItem) {
    this.addSelectTipoComprobante("-1", false);
    this.listArchivos.unshift(archivo);
  }

  cargarDocumentos() {
    this.listArchivos=[];
    let empleos: DatosEmpleoModel[] = this._SolicitudService.solicitudCompleta
      .infoCliente.conyugeCliente.infoEmpleosContacto;
    console.log("Empleo Seleccionado", this.empleoSeleccionado);

    console.log("Empleos", empleos);

    // console.log(empleos.filter(x => x.empleo.idEmpleo == this.empleoSeleccionado.idEmpleo)[0]);
    if (
      this.empleoSeleccionado != undefined &&
      this.empleoSeleccionado != null
    ) {
      if (
        empleos.filter(
          x => x.empleo.idEmpleo == this.empleoSeleccionado.idEmpleo
        ).length > 0
      ) {
        this.documetosDomicilio = empleos.filter(
          x => x.empleo.idEmpleo == this.empleoSeleccionado.idEmpleo
        )[0].documentosEmpleo;

        this.documetosDomicilio.forEach(documento => {
          documento.stream = renderImage(documento.stream);

          let archivo = new FileItem(
            new File([], documento.fileName),
            documento.stream
          );
          archivo.idDocumento = documento.idDocumento;
          archivo.idTipoDocumento = documento.idTipoDocumento;
          archivo.idTipoComprobanteIngreso = documento.idTipoComprobanteIngreso;
          this.listArchivos.push(archivo);

          this.addSelectTipoComprobante(
            documento.idTipoComprobanteIngreso.toString(),
            true
          );
        });
        console.log(this.form);
      } else {
        console.log("no hay documentos");

        this.listArchivos = [];
      }
    } else {
      this.listArchivos = [];
    }
  }

  getTiposComprobantesDomicilio() {
    this._SolicitudService.getComprobanteIngresos().subscribe(response => {
      this.listTiposComprobantesIngresos = response;
    });
  }
  validarDocumentos(archivos: FileItem[]): boolean {
    let pasa: boolean = false;
    archivos.forEach(x => {
      if (x.idTipoComprobanteIngreso > 0) {
        pasa = true;
      } else {
        this.toaster.error(
          "algunos documentos no tienen tipo de Comprobante",
          "No se puede subir el archivo"
        );
      }
    });
    return pasa;
  }

  setDocumentos() {
    this.guardando = true;
    this.listArchivosNuevos = [];

  this.listArchivosNuevos = this.listArchivos.filter(
      x => x.idDocumento <= 0 || isUndefined(x.idDocumento)
    );
    if(this.validarDocumentos(this.listArchivosNuevos)){
      
      console.log("Todos los archivos", this.listArchivos);

      console.log("Archivos Nuevos", this.listArchivosNuevos);
      if(this.listArchivosNuevos.length>0){
        this._SolicitudService.cargandSolicitud=true;
        this.guardaDocumento(0);
      }
     
 
      
  
    
    }

  }

  guardaDocumento(pos:number){
    if(pos<this.listArchivosNuevos.length){
      let documento: DocumentoRequestModel = new DocumentoRequestModel();
  
      let base64 = this.listArchivosNuevos[pos].base64.split("base64,")[1];

      documento.idSolicitud = this._SolicitudService.solicitudCompleta.idSolicitud;
      documento.idContacto = this._SolicitudService.solicitudCompleta.infoCliente.conyugeCliente.contacto.idContacto;
      documento.streamFile = base64;
      documento.idTipoDocumento = 3;

      documento.idTipoComprobanteIngreso = this.listArchivosNuevos[pos].idTipoComprobanteIngreso;

      documento.name = this.listArchivosNuevos[pos].nombreArchivo;
      documento.idEmpleo = this.empleoSeleccionado.idEmpleo;

      console.log("enviado", documento);

      this._SolicitudService.agregaDocumentoSolicitud(documento).subscribe(
        response => {
          console.log(response);
          this.cargaSolicitudCompleta();
          this.toaster.success(
            "Documento guardado",
            "Documento: " + documento.name
          );
        },
        err => {
          this._SolicitudService.cargandSolicitud=false;
          this.toaster.error(
            err.message,
            "Error: Documento " + '"' + documento.name + '"'
          );
          console.log("Error al Enviar Documentos", err);
        }
      );
    }else{
      this.guardando = false;
      this.cargaSolicitudCompleta();
    }
       
  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }

  closeModal() {
    this.openModal = false;
    this.dataDocumentosEmpleo.emit({ open: false });
  }

  openPreView(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.preView = true;
  }

  selectImage(evento: any) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let file = new FileItem(archivoTemporal, base64.toString());
      this.listArchivos.unshift(file);
      this.addSelectTipoComprobante("-1", false);
      console.log("form", this.form);
    };
  }

  tipoComprobante(idComprobante: number): number {
    if (idComprobante > 0) {
      let tipo: number = this.listTiposComprobantesIngresos.filter(
        x => x.id_tipo_comprobante_ingreso == idComprobante
      )[0].id_tipo_comprobante_ingreso;

      return tipo;
    }
    console.log("return", -1);

    return -1;
  }
  cargaSolicitudCompleta(){
    this._SolicitudService
    .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
    .subscribe(resp => {
      this.cargarDocumentos();
     

      
      this._SolicitudService.cargandSolicitud=false;
    },error=>{
      this._SolicitudService.cargandSolicitud=false;
    });
  }
}
