import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { EstadosModel } from "src/app/models/Estados.model";
import { CiudadesModel } from "src/app/models/Ciudades.model";
import { SolcitudService } from "../../services/solcitud.service";
import { ColoniasModel } from "src/app/models/Colonias.model";
import { TiposDomiciliosModel } from "src/app/models/TiposDomicilio.model";
import { CondicionesModel } from "src/app/models/CondicionesDomicilio.model";
import { RelacionCasaFamiliarModel } from "src/app/models/RelacionCasaFamiliar.model";
import { TiposComprobanteDomicilioModel } from "src/app/models/TiposComprobanteDomicilio.model";
import { DomicilioModel } from "src/app/models/Domicilio.Model";
import { DomicilioClienteRequestModel } from "src/app/models/models-request/DomicilioClienteRequest.model";
import {
  getAnos,
  getMeses,
  calcularTiempoMeses,
  getFechaActual
} from "src/app/tools";
import { isUndefined, isNullOrUndefined, log } from "util";
import { DataInputModalGuardarModel } from "../../components/modal-guardar/dataInputModa.model";
import { ToastrService } from "ngx-toastr";
import { ContactoModel } from "../../models/Contacto.model";
import { DatosContactoModel } from "../../models/DatosContactoModel";
import { TiposComprobantePropiedadModel } from "src/app/models/TiposComprobantePropiedad.model";

@Component({
  selector: "app-domiclio-aval",
  templateUrl: "./domiclio-aval.component.html",
  styleUrls: ["./domiclio-aval.component.css"]
})
export class DomiclioAvalComponent implements OnInit {
  CTRL_ESTADO = "ctrlEstado";
  CTRL_CIUDAD = "ctrlCiudad";
  //CTRL_COLONIA = "ctrlColonia";
  CTRL_CALLE = "ctrlCalle";
  CTRL_CP = "ctrlCP";
  CTRL_NUM_EXT = "ctrNumExt";
  CTRL_MUM_INT = "ctrlNumInt";
  CTRL_TIPO_DOMCILIO = "ctrlTipoDomicilio";
  CTRL_ANOS = "ctrlAnos";
  CTRL_MESES = "ctrlMeses";
  CTRL_PERSONAS_DOMICILIO = "ctrlPersonasDom";
  CTRL_CONDICIONES_DOM = "ctrlCondicionesDom";
  CTRL_REFERENCIAS = "ctrlReferencias";

  constructor(
    private _solicitudService: SolcitudService,
    private toaster: ToastrService
  ) {}

  formDomicilio: FormGroup;
  dataInputSaveModalSave: DataInputModalGuardarModel;
  coloniaSeleccionada: ColoniasModel = new ColoniasModel();

  listEstados: EstadosModel[] = [];
  listCiudades: CiudadesModel[] = [];
  listColonias: ColoniasModel[] = [];
  listTiposDomicilio: TiposDomiciliosModel[] = [];
  listCondiciones: CondicionesModel[] = [];
  listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[] = [];
  listComprobanteDomicilio: TiposComprobanteDomicilioModel[] = [];
  listComprobantePropiedad: TiposComprobantePropiedadModel[] = [];
  domicilio: DomicilioModel;

  openModalGuardar: boolean = false;
  openModalCasaFamiliar: boolean = false;
  openModalCasaRenta: boolean = false;
  esCasaFamiliar: boolean = false;
  openModalDocumentosDomicilio: boolean = false;
  aval: DatosContactoModel = new DatosContactoModel();
  ngOnInit() {
    this.copyModel();
    this.initForm();
    this.llenarData();
    this.initServ();
  }
  limpiarForm() {
    this.formDomicilio.reset();
  }
  initServ() {
    this.getEstados();
    console.log("Carga Estados");

    this.getTiposDomicilios();
    console.log("Carga Domicilios");

    this.getCondiciones();
    console.log("Carga Condiciones");
  }
  initForm() {
    this.formDomicilio = new FormGroup({
      ctrlEstado: new FormControl("-1"),
      ctrlCiudad: new FormControl("-1"),
      // ctrlColonia: new FormControl("-1"),
      ctrlCalle: new FormControl(),
      ctrlCP: new FormControl(),
      ctrNumExt: new FormControl(),
      ctrlNumInt: new FormControl(),
      ctrlTipoDomicilio: new FormControl(),
      ctrlAnos: new FormControl(),
      ctrlMeses: new FormControl(),
      ctrlPersonasDom: new FormControl(),
      ctrlCondicionesDom: new FormControl(),
      ctrlReferencias: new FormControl()
    });
  }
  copyModel() {
    this.aval = this._solicitudService.solicitudCompleta.infoCliente.contactosCliente.find(
      x => x.contacto.idTipoContacto == 3
    );

    this.domicilio = this.aval.domicilioContacto.domicilio;

    if (isNullOrUndefined(this.domicilio)) {
      this.domicilio = new DomicilioModel();
    }
    console.log("DOMICILIO AVAL", this.domicilio);
  }
  getEstadoCiudad(idCiudad: number) {
    this._solicitudService.getEstadoCiudad(idCiudad).subscribe(
      resp => {
        console.log("Estado de  ciudad", resp);
        this.formDomicilio.get(this.CTRL_ESTADO).setValue(resp.mensaje);
        this.getCiudades(resp.mensaje);
      },
      err => {
        this.toaster.error("Error al recuperar estado");
      }
    );
  }

  getEstados() {
    this._solicitudService.getEstados().subscribe(
      resp => {
        this.listEstados = resp;
        this.formDomicilio.get(this.CTRL_ESTADO).setValue(8);

        this.getCiudades(this.formDomicilio.get(this.CTRL_ESTADO).value);
        // this.formDomicilio.get(this.CTRL_ESTADO).setValue(this._solicitudService.solicitud.idSucursal);
      },
      err => {
        this.toaster.error(err.message, "Error: cargar estados");
      }
    );
  }

  onSelectEstado(idEstado: number) {
    this.getCiudades(idEstado);
  }

  onSelectCiudad(idCiudad: number) {
    this.getColonias(idCiudad);
  }

  onSelectTipoDomicilio(idTipoDomiclio: string) {
    console.log(idTipoDomiclio);
    this.esCasaFamiliar = false;
    switch (parseInt(idTipoDomiclio)) {
      case 2:
        console.log("Casa Familiar");
        this.esCasaFamiliar = true;
        this.openModalCasaFamiliar = true;
        break;
      case 3:
        console.log("Casa Renta");
        this.openModalCasaRenta = true;
        break;
    }
  }

  getCiudades(idEstado) {
    this.listColonias = [];

    this._solicitudService.getCiudades(idEstado).subscribe(
      respuesta => {
        console.log(respuesta);
        this.listCiudades = respuesta;
        if (!isNullOrUndefined(this.domicilio)) {
          console.log(this.domicilio.idCiudad);

          if (
            this.listCiudades.find(x => x.municipio == this.domicilio.idCiudad)
          ) {
            this.formDomicilio
              .get(this.CTRL_CIUDAD)
              .setValue(this.domicilio.idCiudad);
          } else {
            this.formDomicilio.get(this.CTRL_CIUDAD).setValue("-1");
          }
        } else {
          this.formDomicilio.get(this.CTRL_CIUDAD).setValue("-1");
        }

        this.getColonias(this.formDomicilio.get(this.CTRL_CIUDAD).value);
      },
      err => {
        this.toaster.error(err.message, "Error: cargar ciudades");
      }
    );
  }

  on(event) {
    console.log(event);
  }
  getColonias(ciudad) {
    this.listColonias = [];
    this._solicitudService.getColonias(ciudad).subscribe(
      respuesta => {
        console.log(respuesta);
        this.listColonias = respuesta;
        //if (this.listColonias.find(x => x.nombre == this.domicilio.colonia)) {
        /* FIXME this.formDomicilio
          .get(this.CTRL_COLONIA)
          .setValue(this.domicilio.colonia); */
        //} else {
        /* FIXME        this.formDomicilio.get(this.CTRL_COLONIA).setValue("-1");
         */
        //}
      },
      err => {
        this.toaster.error(err.message, "Error: cargar colonias");
      }
    );
  }

  async getTiposDomicilios() {
    this.listTiposDomicilio = [];
    await this._solicitudService.getTiposDomicilios().subscribe(
      respuesta => {
        console.log(respuesta);

        this.listTiposDomicilio = respuesta
          .filter(x => x.id_tipo_domicilio != 2)
          .filter(x => x.id_tipo_domicilio != 3)
          .filter(x => x.id_tipo_domicilio != 4);

        if (!isNullOrUndefined(this.domicilio)) {
          if (
            this.listTiposDomicilio.find(
              x => x.id_tipo_domicilio == this.domicilio.idTipoDomicilio
            )
          ) {
            this.formDomicilio
              .get(this.CTRL_TIPO_DOMCILIO)
              .setValue(this.domicilio.idTipoDomicilio);
          } else {
            this.formDomicilio.get(this.CTRL_TIPO_DOMCILIO).setValue("-1");
          }
        } else {
          this.formDomicilio.get(this.CTRL_TIPO_DOMCILIO).setValue("-1");
        }
      },
      err => {
        this.toaster.error(err.message, "Error: cargar tipos de domicilo");
      }
    );
  }

  getTiposRelacionesCasaFam() {
    this._solicitudService.getTiposRelacionesCasaFam().subscribe(
      respuesta => {
        console.log(respuesta);
        this.listTipoRelacionesCasaFam = respuesta;
      },
      err => {
        this.toaster.error(
          err.message,
          "Error: cargar relaciones de casa familiar"
        );
      }
    );
  }
  getComprobantePropiedad() {
    this._solicitudService.getComprobantePropiedad().subscribe(respuesta => {
      console.log(respuesta);
      this.listComprobantePropiedad = respuesta;
    });
  }
  async getCondiciones() {
    this.listCondiciones = [];
    await this._solicitudService.getCondiciones().subscribe(respuesta => {
      console.log(respuesta);
      this.listCondiciones = respuesta;

      if (!isNullOrUndefined(this.domicilio)) {
        if (
          this.listCondiciones.find(
            x =>
              x.id_tipo_condicion_domicilio ==
              this.domicilio.idTipoCondicionDomicilio
          )
        ) {
          this.formDomicilio
            .get(this.CTRL_CONDICIONES_DOM)
            .setValue(this.domicilio.idTipoCondicionDomicilio);
        } else {
          this.formDomicilio.get(this.CTRL_CONDICIONES_DOM).setValue("-1");
        }
      } else {
        this.formDomicilio.get(this.CTRL_CONDICIONES_DOM).setValue("-1");
      }
    });
  }

  /*   responseDataCasaFamiliar(event) {
    console.log(event);

    if (!event.open) {
      this.openModalCasaFamiliar = event.open;

      console.log(this.openModalCasaFamiliar);
    }
  } */

  responseDataCasaFamiliar(event) {
    console.log("resp casa failiar aval", event);

    if (event.isSave) {
      this.domicilio.propietarioDomicilio = event.idContactoCasa;
      this.esCasaFamiliar = true;
      this.setDomcilioAval();
    }

    this.openModalCasaFamiliar = false;
  }

  responseDataCasaRenta(event) {
    console.log(event);

    if (event.isCancel) {
      this.openModalCasaRenta = event.open;
    }
  }
  respDataModalDocumentosDomicilio(event) {
    console.log(event);
    this.openModalDocumentosDomicilio = false;
  }

  responseDataGuardar(event) {
    console.log(event);
    this.openModalGuardar = false;

    if (event.save) {
      this.setDomcilioAval();
    }
  }
  onClickSave() {
    this.dataInputSaveModalSave = new DataInputModalGuardarModel(
      "Domicilio Aval",
      "¿Desea guardar el domicilio del aval?",
      []
    );
    this.openModalGuardar = true;
  }

  setDomcilioAval() {
    let domicilioRequest: DomicilioClienteRequestModel = new DomicilioClienteRequestModel();
    domicilioRequest.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    domicilioRequest.idSucursal = this._solicitudService.solicitud.idSucursal;

    /* domicilioRequest.idTipoDomicilio = this.domicilio.idTipoDomicilio;
    domicilioRequest.idTipoCondicionDomicilio = this.domicilio.idTipoCondicionDomicilio;
    domicilioRequest.idTipoRelacionCasaFamiliar = this.domicilio.idTipoRelacionCasaFamiliar; */
    domicilioRequest.idCiudad = this.formDomicilio.get(this.CTRL_CIUDAD).value;
    domicilioRequest.geolocalizacion = "NA";

    if (this.aval.contacto.idContacto < 1) {
      this.toaster.error(
        "No se puede guardar el domcilio, no existe un aval",
        "Domicilio Aval"
      );
      return;
    } else {
      domicilioRequest.idContacto = this.aval.contacto.idContacto;
    }

    domicilioRequest.colonia = this.coloniaSeleccionada.nombre;
    domicilioRequest.calle = this.formDomicilio.get(this.CTRL_CALLE).value;
    domicilioRequest.codigoPostal = this.formDomicilio.get(this.CTRL_CP).value;
    domicilioRequest.codigoSepomex = this.coloniaSeleccionada.sepomex;
    domicilioRequest.fechaRegistro = getFechaActual();

    domicilioRequest.numeroExterior = this.formDomicilio.get(
      this.CTRL_NUM_EXT
    ).value;

    domicilioRequest.numeroInterior = this.formDomicilio.get(
      this.CTRL_MUM_INT
    ).value;

    domicilioRequest.idTipoDomicilio = this.formDomicilio.get(
      this.CTRL_TIPO_DOMCILIO
    ).value;

    domicilioRequest.tiempoResidencia = calcularTiempoMeses(
      this.formDomicilio.get(this.CTRL_ANOS).value,
      this.formDomicilio.get(this.CTRL_MESES).value
    );

    domicilioRequest.habitantesDomicilio = this.formDomicilio.get(
      this.CTRL_PERSONAS_DOMICILIO
    ).value;
    domicilioRequest.idTipoCondicionDomicilio = this.formDomicilio.get(
      this.CTRL_CONDICIONES_DOM
    ).value;
    domicilioRequest.referencias = this.formDomicilio.get(
      this.CTRL_REFERENCIAS
    ).value;

    domicilioRequest.propieterioDomicilio = -1;
    if (
      //domicilioRequest.idTipoDomicilio>0 &&
      domicilioRequest.idCiudad > 0 &&
      domicilioRequest.calle.localeCompare("") != 0 &&
      domicilioRequest.colonia.localeCompare("") != 0
    ) {
      if (domicilioRequest.idTipoDomicilio < 1) {
        domicilioRequest.idTipoDomicilio = 1;
      }
      this._solicitudService.cargandSolicitud = true;
      this._solicitudService
        .agregaDomicilioContactoSolicitud(domicilioRequest)
        .subscribe(
          resp => {
            console.log(resp);
            if (resp.idDomicilio > 0) {
              this._solicitudService
                .getSolicitudCompleta(
                  this._solicitudService.solicitud.idSolicitud
                )
                .subscribe(
                  resp => {
                    this.copyModel();
                    this.toaster.success(
                      "Domicilio guardado.",
                      "Domicilio Aval"
                    );
                    this._solicitudService.cargandSolicitud = false;
                  },
                  err => {
                    this._solicitudService.cargandSolicitud = false;

                    console.log(err);
                  }
                );
            }
          },
          err => {
            console.log(err);
            this._solicitudService.cargandSolicitud = false;
            this.toaster.error(err.message, "Error: Domicilio Aval");
          }
        );
    } else {
      this.toaster.error(
        "Error al guardar: Complete los tados obligatorios '*'",
        "Domicilio Aval"
      );
    }
  }

  /* calcularTiempoResidencia(): number {
    let tiempo =
      this.formDomicilio.get("anos").value * 12 +
      this.formDomicilio.get("meses").value;

    return tiempo;
  } */

  onClickOpenDocumentos() {
    if (isNullOrUndefined(this.domicilio)) {
      this.toaster.info(
        "Para guardar documentos, debes guardar un domicilio antes"
      );
    } else {
      this.openModalDocumentosDomicilio = true;
    }
  }
  llenarData() {
    if (!isNullOrUndefined(this.domicilio)) {
      this.toaster.success("Existe Domicilio");

      // this.formDomicilio.get(this.CTRL_ESTADO).setValue("-1");

      if (!isNullOrUndefined(this.domicilio.idCiudad)) {
        this.getEstadoCiudad(this.domicilio.idCiudad);
      }
      /*  FIXME      this.formDomicilio
        .get(this.CTRL_COLONIA)
        .setValue(this.domicilio.colonia); */

      this.coloniaSeleccionada.nombre = this.domicilio.colonia;
      this.coloniaSeleccionada.sepomex = this.domicilio.codigoSepomex;

      this.formDomicilio.get(this.CTRL_CALLE).setValue(this.domicilio.calle);
      this.formDomicilio
        .get(this.CTRL_CP)
        .setValue(this.domicilio.codigoPostal);
      this.formDomicilio
        .get(this.CTRL_NUM_EXT)
        .setValue(this.domicilio.numeroExterior);
      this.formDomicilio
        .get(this.CTRL_MUM_INT)
        .setValue(this.domicilio.numeroInterior);
      this.formDomicilio
        .get(this.CTRL_TIPO_DOMCILIO)
        .setValue(this.domicilio.idTipoDomicilio);
      this.formDomicilio
        .get(this.CTRL_ANOS)
        .setValue(getAnos(this.domicilio.tiempoResidencia));
      this.formDomicilio
        .get(this.CTRL_MESES)
        .setValue(getMeses(this.domicilio.tiempoResidencia));
      this.formDomicilio
        .get(this.CTRL_PERSONAS_DOMICILIO)
        .setValue(this.domicilio.habitantesDomicilio);
      this.formDomicilio
        .get(this.CTRL_CONDICIONES_DOM)
        .setValue(this.domicilio.idTipoCondicionDomicilio);
      this.formDomicilio
        .get(this.CTRL_REFERENCIAS)
        .setValue(this.domicilio.referencias);
    }
  }

  selectColonia(event: any) {
    this.coloniaSeleccionada = event;
    if (this.coloniaSeleccionada.codigo_postal > 0) {
      this.formDomicilio
        .get(this.CTRL_CP)
        .setValue(this.coloniaSeleccionada.codigo_postal);
    }
    console.log(this.coloniaSeleccionada);
    console.log("selectColonia", event);
  }
  onChangeColonia(event: any) {
    this.coloniaSeleccionada = null;
    this.coloniaSeleccionada = new ColoniasModel();
    this.coloniaSeleccionada.nombre = event;
    this.coloniaSeleccionada.sepomex = -1;
    console.log(this.coloniaSeleccionada);
    console.log("changeColonia", event);
  }
  onFocusedColonia(event: any) {
    /*   // this.coloniaSeleccionada.nombre=event;

    console.log(this.coloniaSeleccionada);
    console.log("onFocused", event.value); */
  }
  getAnosl(tiempo: number) {
    return getAnos(tiempo);
  }
  getMesesl(tiempo: number) {
    return getMeses(tiempo);
  }
}
