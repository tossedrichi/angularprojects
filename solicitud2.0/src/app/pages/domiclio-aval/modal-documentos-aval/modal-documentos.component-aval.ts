import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  AfterViewChecked
} from "@angular/core";
import { FormGroup, FormArray, FormControl } from "@angular/forms";
import { FileItem } from "src/app/models/file-item";
import { DocumetnosDomicilioModel } from "src/app/models/DocumentosDomicilioModel";
import { TiposComprobanteDomicilioModel } from "src/app/models/TiposComprobanteDomicilio.model";
import { ToastrService } from "ngx-toastr";
import { SolcitudService } from "src/app/services/solcitud.service";
import { renderImage } from "src/app/tools";
import { isUndefined, log } from 'util';
import { DocumentoRequestModel } from "src/app/models/models-request/DocumentoRequest.model";
import { DomicilioModel } from "../../../models/Domicilio.Model";
import { DomicilioClienteModel } from "src/app/models/DomicilioClienteModel";
import { TiposComprobantePropiedadModel } from 'src/app/models/TiposComprobantePropiedad.model';
@Component({
  selector: "app-modal-documentos-aval",
  templateUrl: "./modal-documentos.component-aval.html",
  styleUrls: ["./modal-documentos.component-aval.css"]
})
export class ModalDocumentosAvalComponent implements OnInit, AfterViewChecked {
  @Input() openModal: boolean;
  @Output() dataDocumentosDomicilio: EventEmitter<{
    open: boolean;
    data?: any;
  }>;

  datosCargados:boolean = false;

  form: FormGroup;
  listArchivosNuevos: FileItem[];
  guardando: boolean = false;
  preView = false;
  archivoSeleccionado: FileItem;
  estaSobreDrop: boolean = false;
  documetosDomicilio: DocumetnosDomicilioModel[];
  domicilioAval: DomicilioClienteModel;

  listArchivos: FileItem[] = [];

  listTiposComprobantesDomicilio: TiposComprobanteDomicilioModel[] = [];
  listComprobantePropiedad: TiposComprobantePropiedadModel []=[]


  constructor(
    private _SolicitudService: SolcitudService,
    private toaster: ToastrService
  ) {
    this.dataDocumentosDomicilio = new EventEmitter();
  }
  ngOnInit() {
    this.initForm();

    this.getTipoComprobantePropiedad();
    this.cargarDocumentos();
  }

  ngAfterViewChecked(): void {
    console.log("Domicilio Aval", this.domicilioAval);
    if(this.openModal){
    if(!this.datosCargados){
      console.log("Cargando....")
      this.cargarDocumentos();
      this.datosCargados=true;
    }
  }
  }
  initForm() {
    this.form = new FormGroup({
      selecTipoComprobante: new FormArray([])
    });
  }

  addSelectTipoComprobante(idTipoComprobante: string, isCarga: boolean) {
    if (isCarga) {
      (<FormArray>this.form.controls["selecTipoComprobante"]).push(
        new FormControl(idTipoComprobante)
      );
    } else {
      (<FormArray>this.form.controls["selecTipoComprobante"]).insert(
        0,
        new FormControl(idTipoComprobante)
      );
    }
  }

  cargarDocumentos() {
    this.listArchivos=[];
    this.domicilioAval = this._SolicitudService.solicitudCompleta.infoCliente.contactosCliente.filter(
      x => x.contacto.idTipoContacto == 3
    )[0].domicilioContacto;
    console.log("Domicilio Aval", this.domicilioAval);

    this.documetosDomicilio = this.domicilioAval.documentosDomicilio;

    this.documetosDomicilio.forEach(documento => {
      documento.stream = renderImage(documento.stream);

      let archivo = new FileItem(
        new File([], documento.fileName),
        documento.stream
      );

      archivo.idDocumento = documento.idDocumento;
      archivo.idTipoDocumento = documento.idTipoDocumento;
      archivo.idTipoComprobantePropiedad = documento.idTipoComprobantePropiedad;
      this.listArchivos.push(archivo);
      this.addSelectTipoComprobante(
        documento.idTipoComprobantePropiedad.toString(),
        true
      );
    });
    console.log(this.form);
  }

  getTipoComprobantePropiedad() {
    this._SolicitudService.getComprobantePropiedad().subscribe(response => {
      this.listComprobantePropiedad = response;
    });
  }

  validarDocumentos(): boolean {
    let pasa: boolean = true;
    this.listArchivos.forEach(x => {
      if (x.idTipoComprobantePropiedad > 0) {
        
      } else {
        pasa = false;
        this.toaster.error(
          "algunos documentos no tienen tipo de Comprobante",
          "No se puede subir el archivo"
        );
      }
    });
    return pasa;
  }
  
  setDocumentos() {
    if (this.validarDocumentos()) {
      this.guardando = true;
      this.listArchivosNuevos = [];

      this.listArchivosNuevos = this.listArchivos.filter(
        x => x.idDocumento <= 0 || isUndefined(x.idDocumento)
      );
      console.log("Todos los archivos", this.listArchivos);

      console.log("Archivos Nuevos", this.listArchivosNuevos);

      this.guardaDocumento(0);
     

      
    }
  }

  guardaDocumento(pos:number){
    if(pos<this.listArchivosNuevos.length){
      let element:FileItem= this.listArchivosNuevos[pos];
      let documento: DocumentoRequestModel = new DocumentoRequestModel();
  
      let base64 = element.base64.split("base64,")[1];
  
      documento.idSolicitud = this._SolicitudService.solicitudCompleta.idSolicitud;
      documento.idContacto = this._SolicitudService.solicitudCompleta.infoCliente.contactosCliente.filter(
        x => x.contacto.idTipoContacto == 3
      )[0].contacto.idContacto;
      documento.streamFile = base64;
      documento.idTipoDocumento = 2;
      documento.idTipoComprobantePropiedad =
        element.idTipoComprobantePropiedad;
      documento.name = element.nombreArchivo;
      documento.idDomicilio = this.domicilioAval.domicilio.idDomicilio;
        this._SolicitudService.cargandSolicitud=true;
        console.log("Documento enviado",documento);
        
      this._SolicitudService.agregaDocumentoSolicitud(documento).subscribe(
        response => {
          console.log(response);
          this._SolicitudService.cargandSolicitud=false;
          this.toaster.success(
            "Documento guardado",
            "Documento: " + documento.name
          );
                    
          this.guardaDocumento(pos+1);
          this.dataDocumentosDomicilio.emit({ open: true });
        },
        err => {
          this._SolicitudService.cargandSolicitud=false;
          this.toaster.error(
            err.message,
            "Error: Documento " + '"' + documento.name + '"'
          );
          console.log("Error al Enviar Documentos", err);
        }
      );
    }else{
      this.cargaSolicitud();
    }
    
  }

  cargaSolicitud(){
    this._SolicitudService.cargandSolicitud=true;
    this._SolicitudService
            .getSolicitudCompleta(this._SolicitudService.solicitud.idSolicitud)
            .subscribe(
              resp => {
                this._SolicitudService.cargandSolicitud=false;
                this.cargarDocumentos();
                this.guardando = false;
              },
              err => {
                this._SolicitudService.cargandSolicitud=false;

                console.log(err);
              }
            );
  }

  eliminarArchivo(archivo: FileItem) {
    this.listArchivos = this.listArchivos.filter(x => x != archivo);
  }

  onClickLimpiar() {
    this.listArchivos = [];
  }

  closeModal() {
    this.openModal = false;
    this.datosCargados=false;

    this.dataDocumentosDomicilio.emit({ open: false });
  }

  openPreView(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.preView = true;
  }

  selectImage(evento: any) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let file = new FileItem(archivoTemporal, base64.toString());
      this.listArchivos.unshift(file);
      this.addSelectTipoComprobante("-1", false);
      console.log("form", this.form);
    };
  }

  onDropArchivoNuevo(archivo: FileItem) {
    this.addSelectTipoComprobante("-1", false);
    this.listArchivos.unshift(archivo);
  }

  tipoComprobante(idComprobante: number): number {
    if (idComprobante > 0) {
      let tipo: number = this.listComprobantePropiedad .filter(
        x => x.id_tipo_comprobante_propiedad == idComprobante
      )[0].id_tipo_comprobante_propiedad;

      return tipo;
    }
    console.log("return", -1);

    return -1;
  }
}
