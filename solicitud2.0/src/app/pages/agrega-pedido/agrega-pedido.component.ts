import { Component, OnInit } from '@angular/core';
import { SolcitudService } from '../../services/solcitud.service';
import { PedidoModel } from '../../models/Pedido.model';
import { Toast } from 'ngx-toastr';

@Component({
  selector: 'app-agrega-pedido',
  templateUrl: './agrega-pedido.component.html',
  styleUrls: ['./agrega-pedido.component.css']
})
export class AgregaPedidoComponent implements OnInit {
  pedidos:PedidoModel[];
  openModalAgregaPedido: boolean = false;
  pedidoSeleccionado: PedidoModel;

  constructor(public __solicitudService:SolcitudService) { }

  ngOnInit() {
    this.getPedidosAnteriores();
  }


  getPedidosAnteriores(){
    this.__solicitudService.getPedidosAnteriores(this.__solicitudService.cliente.idCliente)
    .subscribe(response=>{
      this.pedidos=response;
    },error=>{
      console.log(error);
    });
  }
  respDataModalPedido(event) {
    console.log(event);
    if (event.isCancel) {
      this.openModalAgregaPedido = event.open;
    }
  }
  seleccionarPedido(pedido: PedidoModel){
    this.pedidoSeleccionado = pedido;
    this.openModalAgregaPedido = true;
  }


  agregarPedido(event){
    this.openModalAgregaPedido=false;

    if(event.save){
      this.__solicitudService.cargandSolicitud=true;
    this.__solicitudService.agregaPedidoSolicitud(this.__solicitudService.solicitudCompleta.idSolicitud,this.pedidoSeleccionado.idPedido).subscribe(resp=>{
      console.log(resp);
     
      this.__solicitudService
      
      .getSolicitudCompleta(this.__solicitudService.solicitud.idSolicitud)
      .subscribe(resp => {
        this.__solicitudService.cargandSolicitud=false;
  
      },err=>{
        this.__solicitudService.cargandSolicitud=false;
      });
      
    },err=> {
      console.log(err);
      this.__solicitudService.cargandSolicitud=false;
    })
  }else{
    console.log("Operacion Cancelada");
    
  }
  }

  estaLigado(idPedido:number):boolean{
    let ligado:boolean=false;
    this.__solicitudService.solicitudCompleta.pedidosAdjuntos.forEach(x=>{
      if(x.pedido==idPedido){
        ligado=true;
      }
    })
    return ligado;
  }
  desligarPedido(idPedido:number){
    console.log("Pedido desligado", idPedido);
    this.__solicitudService.cargandSolicitud=true;
    this.__solicitudService.eliminaRelacionPedidoSolicitud(idPedido).
    subscribe(resp=>{
      
      this.__solicitudService
      .getSolicitudCompleta(this.__solicitudService.solicitud.idSolicitud)
      .subscribe(resp => {
        this.__solicitudService.cargandSolicitud=false;
  
      },err=>{
        this.__solicitudService.cargandSolicitud=false;
      });
    },error=>{
      this.__solicitudService
      .getSolicitudCompleta(this.__solicitudService.solicitud.idSolicitud)
      .subscribe(resp => {
        this.__solicitudService.cargandSolicitud=false;
  
      },err=>{
        this.__solicitudService.cargandSolicitud=false;
      });
    })
  }

}
