import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SolcitudService } from 'src/app/services/solcitud.service';

@Component({
  selector: 'app-modal-pedido',
  templateUrl: './modal-pedido.component.html',
  styleUrls: ['./modal-pedido.component.css']
})
export class ModalPedidoComponent implements OnInit {
  @Input() openModal: boolean;
  @Output() resp: EventEmitter<{save:boolean}>;
  openModalAgregaPedido: boolean = false;


  constructor(private _solicitudService: SolcitudService) { 
    this.resp = new EventEmitter();

  }

  ngOnInit() {
  }

  okModal(){
    this.openModal = false;
    this.resp.emit({save:true})
  }

  closeModal() {
    this.openModal = false;
    this.resp.emit({save:false})
  }

}
