import { Component, OnInit, ViewChild, ElementRef, Directive } from '@angular/core';
import {  Router } from '@angular/router';
import { LoginInterface } from '../../../interfaces/login.interface';
import { LoginService } from '../../services/login.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
  
})
export class LoginComponent implements OnInit {
  loading: boolean=false
  datosLogin: LoginInterface;
  errorLogin: boolean=false;
  @ViewChild('btnLogin') btnLogin: ElementRef;
 
  

  constructor(private route: Router,
    private loginService:LoginService) { }

  ngOnInit() {
  }
  limpiarToken(token: string): string {
    token = token.split(".")[1].toString();
    return token;
  }
 
  login(user:string,password:string) {
    this.loading=true;
    this.errorLogin=false;
    console.log("user: "+ user+" password: "+password);
    this.loginService.setLogin(user,password)
        .subscribe(data=>{
          
          this.loading=false;
          this.datosLogin=data;
          if(this.datosLogin.token !="TokenError"){
            this.errorLogin=true;
            this.datosLogin.token= this.limpiarToken(this.datosLogin.token);
          }
          if(this.datosLogin.message==='Autenticado'){
            
            this.loginService.setSession(this.datosLogin);
            this.route.navigate(['/SolicitudRedigitalizacion']);
          }else{
            this.errorLogin=true;
          }
    },error => {
      this.loading=false;
      console.log(error);

    });
    
    //this.route.navigate(['/SolicitudRedigitalizacion']);
  }

}
