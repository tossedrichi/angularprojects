import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { SolcitudService } from "../../services/solcitud.service";
import { ClienteModel } from "../../models/Cliente.model";
import { Router } from "@angular/router";
import { SolicitudModel } from "../../models/Solicitud.model";
import { ZonasModel } from "src/app/models/Zonas.model";
import { DataInputModalGuardarModel } from "../../components/modal-guardar/dataInputModa.model";

import { ToastrService } from "ngx-toastr";
import { AfterViewChecked } from '@angular/core';

@Component({
  selector: "app-busqueda-cliente",
  templateUrl: "./busqueda-cliente.component.html",
  styleUrls: ["./busqueda-cliente.component.css"]
})
export class BusquedaClienteComponent implements OnInit, OnChanges,AfterViewChecked {
 
  @Output() showForm: EventEmitter<boolean>;


  clienteActual: ClienteModel = new ClienteModel();
  clientes: ClienteModel[];

  mostrarSolicitudes: boolean = false;

  solicitudes: SolicitudModel[];
  listCedis: ZonasModel[] = [];

  load: boolean = false;
  estadoCivil: string;
  loading: boolean = false;
  btnAgregar: boolean = false;
  mostrarPedido: boolean = false;
  openModalGuardar: boolean = false;
  dataInput: DataInputModalGuardarModel;
  cedisSeleccionado: number = -1;

  constructor(
    public _solcitudService: SolcitudService,
    private route: Router,
    private toaster: ToastrService
  ) {
    this.showForm = new EventEmitter();
  }

  ngOnInit() {
    // this.getClientes("luis","","",1);

    this.getZonas();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = JSON.stringify(chng.currentValue);
      let prev = JSON.stringify(chng.previousValue);
      if(propName=="ShowForm" ){
        this.mostrarSolicitudes=false;
        this.clientes=  [];
        this.clienteActual= new ClienteModel;
        this.solicitudes=[];
      }

      
    }
     
  }
  ngAfterViewChecked(): void {
    if (this._solcitudService.showBusqueda) {
      //console.log("entro ng after contnt view")

      setTimeout(() => {
       // 
        this.showForm.emit(!this._solcitudService.showBusqueda);

      }, 0);
    }
  }

  irDatosCliente() {
    this.route.navigate(["/datos-cliente"]);
  }

  pasardatos(cliente: ClienteModel) {
    console.log(cliente);

    this.route.navigate(["SolicitudRedigitalizacion/datos-cliente"]);

    this._solcitudService.setCliente(cliente);

    console.log(cliente);
  }

  setSolicitud(solicitud: SolicitudModel) {
    this._solcitudService.cargandSolicitud= true;

    this._solcitudService.setSolicitud(solicitud);

    this._solcitudService.getSolicitudCompleta(solicitud.idSolicitud).subscribe(
      resp => {
        console.log("SOLICITUD: ", resp);

        this.pasardatos(this.clienteActual);
        this.mostrarSolicitudes=false;
        this.clientes=  [];
        this.clienteActual= new ClienteModel;
        this.solicitudes=[];
        this._solcitudService.showBusqueda = false;
        this._solcitudService.cargandSolicitud = false;
        this.showForm.emit(true);
      },
      err => {
        console.log(err);

        this._solcitudService.cargandSolicitud = false;
        this.toaster.error(err.message, "Error al cargar solicitud");
      }
    );
  }

  selectModoBusqueda(
    modo: string,
    nom: string,
    ap1: string,
    ap2: string,
    cedis: number,
    idCliente: string
  ) {
    console.log(`modo ${modo}`);
    this.toaster.clear();

    if (modo.includes("Nombre")) {
      //BUSCAR POR NOMBRE
      this.mostrarSolicitudes = false;
      if (nom.trim().length > 0) {
        this.getClientes(nom, "", "", 1);
      } else {
        this.toaster.warning("Ingresa un nombre", "Busqueda Cliente");
      }
    } else if (modo.includes("No. Cliente")) {
      //buscar por no cliente
      this.mostrarSolicitudes = true;
      this.clienteActual.idCliente = parseInt(idCliente);
      if (idCliente.trim().length > 0) {
        this.getPerfilCreditoCliente(this.clienteActual);
      } else {
        this.toaster.warning(
          "Ingresa un numero de cliente",
          "Busqueda Cliente"
        );
      }
    } else {
      this.toaster.warning(
        "Selecciona un tipo de busqueda",
        "Busqueda Cliente"
      );
    }
  }

  getClientes(nom: string, ap1: string, ap2: string, cedis: number) {
    this.clientes = [];

    this.load = true;

    this._solcitudService.getListaClientes(nom, ap1, ap2, cedis).subscribe(
      data => {
        this.clientes = data;

        this.load = false;

        console.log(this.clientes);
      },
      error => {
        this.load = false;
        this.toaster.error(error.message, "Error: Busqueda Cliente");
        console.log(error);
      }
    );
  }

  esEditable(estado:string){
    if(estado.localeCompare("Digitalización Finalizada")==0){
      return false;
    }else{
      return true;
    }
  }

  getPerfilCreditoCliente(cliente: ClienteModel) {
    this.clienteActual = cliente;
    this._solcitudService
      .getPerfilCreditoCliente(cliente.idCliente.toString())
      .subscribe(
        data => {
          this.clienteActual = data.infoCliente;

          this.solicitudes = data.solicitudesActuales.filter(x=>x.statusSolicitud.includes("En Redigitalización")||x.statusSolicitud.includes("Digitalización Finalizada") ||x.statusSolicitud.includes("Pendiente Anticipo") );
          for(let x of data.historialCrediticio){
            if(x.statusSolicitud.includes("En Redigitalización")||x.statusSolicitud.includes("Digitalización Finalizada")){
              this.solicitudes.push(x);
             
            }
           
          }
         // this.solicitudes = data.solicitudesActuales;
          console.log(this.solicitudes);
        },
        error => {
          this.toaster.error(error.message, "Error: Busqueda Cliente");

          console.log(error);
        }
      );
  }

  muestraSolicitudes() {
    this.mostrarSolicitudes = !this.mostrarSolicitudes;
  }

  getZonas() {
    this._solcitudService.getZonas().subscribe(
      response => {
        this.listCedis = response;
        this.listCedis = this.listCedis.filter(
          x => !x.nombreZona.includes("Comercio Electronico")
        );
      },
      err => {
        console.log(err);
        this.toaster.error(err.message, "Error al cargar Cedis");
      }
    );
  }

  agregarNuevaSolicitud(cliente: ClienteModel) {
    this.clienteActual = cliente;
    this.openModalGuardar = true;
    this.dataInput = new DataInputModalGuardarModel(
      "Nueva Solicitud",
      "¿Deseas crear una nueva solcitud?",
      []
    );
  }

  crearSolicitud(event) {
    console.log(event);
    if (event.save) {
      console.log("Crear Solicitud");
      this._solcitudService.cargandSolicitud= true;
      this.openModalGuardar = false;

      this._solcitudService
        .setNuevaSolicitud(
          event.date,
          this.clienteActual.idCliente,
          event.idSucursal
        )
        .subscribe(
          resp => {
            console.log(resp);
            this._solcitudService.solicitud.idSolicitud = resp.idSolicitud;
            this._solcitudService.solicitud.idSucursal = resp.idSucursal;

            this.pasardatos(this.clienteActual);
            this._solcitudService
              .getSolicitudCompleta(resp.idSolicitud)
              .subscribe(
                resp => {
                  this.showForm.emit(true);
                  this._solcitudService.showBusqueda = false;
                  this._solcitudService.cargandSolicitud = false;
                  console.log("SOLICITUD: ", resp);
                },
                err => {
                  this._solcitudService.showBusqueda = false;
                  this._solcitudService.cargandSolicitud = false;
                  this.toaster.error(err.message, "Error al cargar solcitud");
                }
              );
          },
          err => {
            console.log(err);
            this._solcitudService.showBusqueda = false;
            this._solcitudService.cargandSolicitud = false;
            this.toaster.error(err.message, "Error al crear solcitud");
          }
        );
    } else {
      this.openModalGuardar = false;
      console.log("operacion Cancelada");
    }
  }
}
