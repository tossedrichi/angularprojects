import { Component, OnInit } from "@angular/core";
import { FileItem } from "../../models/file-item";
import { TiposDocumentosModel } from "../../models/TiposDocumentos.model";
import { SolcitudService } from "../../services/solcitud.service";
import { TiposContactoModel } from "../../models/TiposContacto.model";
import { TiposComprobanteDomicilioModel } from "../../models/TiposComprobanteDomicilio.model";
import { TiposComprobanteIngresosModel } from "../../models/TiposComprobanteIngresos.model";
import { TiposComprobantePropiedadModel } from "../../models/TiposComprobantePropiedad.model";
import { DatosContactoModel } from "src/app/models/DatosContactoModel";
import { ToastrService, Toast } from "ngx-toastr";
import { DocumentoRequestModel } from "src/app/models/models-request/DocumentoRequest.model";

import { isNullOrUndefined } from "util";
import { FormGroup, FormArray, FormBuilder } from "@angular/forms";

import { fixToSendImage } from "src/app/tools";

@Component({
  selector: "app-documentos",
  templateUrl: "./documentos.component.html",
  styleUrls: ["./documentos.component.css"]
})
export class DocumentosComponent implements OnInit {
  preView = false;
  archivoSeleccionado: FileItem;

  form: FormGroup;
  clasDocumento: FormArray;

  estaSobreDrop: boolean = false;
  relacionContacto: boolean = false;
  listaContactos: DatosContactoModel[];

  listArchivos: FileItem[] = [];
  listArchivosSubidos: { index: number; file: FileItem }[] = [];

  listTiposDocumentos: TiposDocumentosModel[] = [];
  listTipoContactoDocumentos: TiposContactoModel[] = [];
  listTipoComprobanteDomicilio: TiposComprobanteDomicilioModel[] = [];
  listTipoComprobanteIngresos: TiposComprobanteIngresosModel[] = [];
  listComprobantePropiedad: TiposComprobantePropiedadModel[] = [];
  constructor(
    private _SolcitudService: SolcitudService,
    private formBuilder: FormBuilder,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initForm();
    this.getTiposDocumentos();
    this.getTipoContactoDocumentos();
    this.getTipoComprobantesDomicilio();
    this.getTipoComprobantesIngresos();
    this.getTipoComprobantePropiedad();
  }

  getDocumentosPrecargados() {}

  initForm() {
    console.log("init form");

    this.form = this.formBuilder.group({
      clasDocumento: this.formBuilder.array([
        /*this.createItem() */
      ])
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      selecTipoDocumento: "-1",
      selecCompDom: "-1",
      selecCompProp: "-1",
      selecCompIng: "-1",
      selecTipoContacto: "-1",
      selecContacto: "-1"
    });
  }

  addItem(): void {
    this.clasDocumento = this.form.get("clasDocumento") as FormArray;
    this.clasDocumento.insert(0, this.createItem());
  }

  onDropArchivoNuevo(archivo: FileItem) {
    this.listArchivos.unshift(archivo);
    this.addItem();
    this.hideAllControlsFromGroup(0);
  }

  hideControl(index: number, controlName: string): boolean {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    if (formArray.controls[controlName].disabled) {
      return true;
    } else {
      return false;
    }
  }

  hideAllControlsFromGroup(index: number) {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    formArray.controls["selecCompDom"].disable();
    formArray.controls["selecCompDom"].setValue("-1");

    formArray.controls["selecCompProp"].disable();
    formArray.controls["selecCompProp"].setValue("-1");

    formArray.controls["selecCompIng"].disable();
    formArray.controls["selecCompIng"].setValue("-1");

    formArray.controls["selecContacto"].disable();
    formArray.controls["selecContacto"].setValue("-1");
  }

  onChangeSelectTipoDocumento(index: number) {
    console.log(index);
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    let value: number = parseInt(
      formArray.controls["selecTipoDocumento"].value
    );

    switch (value) {
      case 1:
        console.log("Comprobante de Domicilio");

        formArray.controls["selecCompDom"].enable();
        formArray.controls["selecCompDom"].setValue("-1");
        formArray.controls["selecTipoContacto"].enable();
        formArray.controls["selecTipoContacto"].setValue("-1");

        formArray.controls["selecContacto"].disable();
        formArray.controls["selecContacto"].setValue("-1");
        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");

        break;

      case 2:
        console.log("Comprobante de propiedad");
        formArray.controls["selecCompProp"].enable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecTipoContacto"].enable();
        formArray.controls["selecTipoContacto"].setValue("-1");

        formArray.controls["selecContacto"].disable();
        formArray.controls["selecContacto"].setValue("-1");
        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");
        break;
      case 3:
        console.log("Comprobante de Ingresos");

        ///this.listaContactos.filter(x => x.contacto.idTipoContacto == 3);

        formArray.controls["selecCompIng"].enable();
        formArray.controls["selecCompIng"].setValue("-1");
        formArray.controls["selecTipoContacto"].enable();
        formArray.controls["selecTipoContacto"].setValue("-1");
        formArray.controls["selecContacto"].enable();
        formArray.controls["selecContacto"].setValue("-1");

        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");

        break;

      case 4:
        console.log("Pagare");

        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");
        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");
        formArray.controls["selecTipoContacto"].disable();
        formArray.controls["selecTipoContacto"].setValue("-1");
        formArray.controls["selecContacto"].disable();
        formArray.controls["selecContacto"].setValue("-1");

        break;

      case 5:
        console.log("Fotografia Casa (Exterior)");

        formArray.controls["selecTipoContacto"].disable();
        formArray.controls["selecTipoContacto"].setValue("-1");
        formArray.controls["selecContacto"].disbale();
        formArray.controls["selecContacto"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");
        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");
        break;

      case 6:
        console.log("Fotografía Casa (Interior)");

        formArray.controls["selecTipoContacto"].disable();
        formArray.controls["selecTipoContacto"].setValue("-1");
        formArray.controls["selecContacto"].disbale();
        formArray.controls["selecContacto"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");
        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");

        break;

      case 7:
        console.log("Pagare Aval");

        formArray.controls["selecCompDom"].disable();
        formArray.controls["selecCompDom"].setValue("-1");
        formArray.controls["selecCompProp"].disable();
        formArray.controls["selecCompProp"].setValue("-1");
        formArray.controls["selecCompIng"].disable();
        formArray.controls["selecCompIng"].setValue("-1");
        formArray.controls["selecTipoContacto"].disable();
        formArray.controls["selecTipoContacto"].setValue("-1");
        formArray.controls["selecContacto"].disable();
        formArray.controls["selecContacto"].setValue("-1");
        break;
    }
  }

  getTiposDocumentos() {
    this._SolcitudService.getTiposDocumentos().subscribe(response => {
      this.listTiposDocumentos = response;
    });
  }

  eliminarArchivo(index: number, archivo: FileItem) {
    console.log("Archivo a eliminar:" ,index+ archivo.nombreArchivo);
    
    /*     let posicion: number = this.listArchivos.findIndex(x => x == archivo);

    (<FormArray>this.form.controls["selecTipoComprobante"]).removeAt(posicion); */

    let formArray: FormArray = <FormArray>this.form.controls["clasDocumento"];
    formArray.removeAt(index);
    this.listArchivos = this.listArchivos.filter(x => x.nombreArchivo != archivo.nombreArchivo);
    console.log("Form despues de eliminar documetnos", this.form);
  }

  onClickLimpiar() {
    this.initForm();
    console.log("Form", this.form);

    this.listArchivos = [];
  }
  getTipoContactoDocumentos() {
    this._SolcitudService.getTiposContacto().subscribe(response => {
      this.listTipoContactoDocumentos = response;
    });
  }
  getTipoComprobantesDomicilio() {
    this._SolcitudService.getTiposComprobanteDomiclio().subscribe(response => {
      this.listTipoComprobanteDomicilio = response;
    });
  }

  getTipoComprobantesIngresos() {
    this._SolcitudService.getComprobanteIngresos().subscribe(response => {
      this.listTipoComprobanteIngresos = response;
    });
  }

  getTipoComprobantePropiedad() {
    this._SolcitudService.getComprobantePropiedad().subscribe(response => {
      this.listComprobantePropiedad = response;
    });
  }

  getListaTiposContacto(index: number): TiposContactoModel[] {
    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );
    let tipoDocumento: number = parseInt(
      formArray.controls["selecTipoDocumento"].value
    );
    let listaTipoContaco: TiposContactoModel[] = this
      .listTipoContactoDocumentos;

    //console.log("tipo", tipoDocumento);

    switch (tipoDocumento) {
      case 1:
        return listaTipoContaco.filter(x => x.id_tipo_contacto == 3);
        break;
      case 2:
        return listaTipoContaco.filter(x => x.id_tipo_contacto == 3);
        break;
      case 3:
        return listaTipoContaco.filter(
          x => x.id_tipo_contacto == 2 || x.id_tipo_contacto == 4
        );

        break;
      default:
        // console.log("Otro Tipo");

        return listaTipoContaco;
        break;
    }
  }
  onSelectContacto(index: number, idTipoContacto: string) {
    console.log(idTipoContacto);

    let formArray: FormArray = <FormArray>(
      this.form.controls["clasDocumento"].get(index.toString())
    );

    if (
      idTipoContacto.includes("cliente") ||
      idTipoContacto.includes("3") ||
      idTipoContacto.includes("4") ||
      idTipoContacto.includes("5")
    ) {
      formArray.controls["selecContacto"].disable();
      formArray.controls["selecContacto"].setValue("-1");
    } else {
      formArray.controls["selecContacto"].enable();
    }

    switch (parseInt(idTipoContacto)) {
      case 0:
        this.listaContactos = this._SolcitudService.getContactos(
          parseInt(idTipoContacto)
        );
        this.relacionContacto = false;
        break;
      case 1:
        console.log("Referencia");
        this.listaContactos = this._SolcitudService.getContactos(
          parseInt(idTipoContacto)
        );
        this.relacionContacto = true;
        break;
      case 2:
        console.log("Familiar");
        this.listaContactos = this._SolcitudService.getContactos(
          parseInt(idTipoContacto)
        );
        this.relacionContacto = true;
        break;
      case 3:
        console.log("Aval");
        this.relacionContacto = false;
        break;
      case 4:
        console.log("Cónyuge Cliente");
        this.relacionContacto = false;
        break;
      case 5:
        console.log("Cónyuge Aval");
        this.relacionContacto = false;
        break;
      case 6:
        console.log("Contacto Empleo");
        this.relacionContacto = true;
        break;
    }
  }

  selectImage(evento: any) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);

    reader.onload = event => {
      console.log(event);
      let base64: string = (<FileReader>event.target).result.toString();

      let archivo: FileItem = new FileItem(new File([], ""), base64);
      archivo.nombreArchivo = archivoTemporal.name;
      archivo.archivo = archivoTemporal;
      this.listArchivos.unshift(archivo);
      this.addItem();
      this.hideAllControlsFromGroup(0);

      console.log("Formulario", this.form);
    };
  }

  openPreView(archivo: FileItem) {
    this.archivoSeleccionado = archivo;
    this.preView = true;
  }

  setDocumentos() {
    console.log("Lista de Archivos", this.listArchivos);
    console.log("form", this.form);

    this.guardarDocs(0);
  }
  guardarDocs(i: number) {
    if (i < this.listArchivos.length) {
      console.log(
        "Documento",
        "posicion: " + i + " " + this.listArchivos[i].nombreArchivo
      );

      let documento: DocumentoRequestModel = new DocumentoRequestModel();
      documento.agente = SolcitudService.agente;
      documento.idOrigen = -1;
      documento.idSolicitud = this._SolcitudService.solicitud.idSolicitud;
      documento.streamFile = fixToSendImage(this.listArchivos[i].base64);
      documento.name = this.listArchivos[i].nombreArchivo;

      let formArray: FormArray = <FormArray>(
        this.form.controls["clasDocumento"].get(i.toString())
      );

      documento.idTipoDocumento = parseInt(
        formArray.controls["selecTipoDocumento"].value
      );

      documento.idTipoComprobanteDomicilio = parseInt(
        formArray.controls["selecCompDom"].value
      );

      documento.idTipoComprobantePropiedad = parseInt(
        formArray.controls["selecCompProp"].value
      );

      documento.idTipoComprobanteIngreso = parseInt(
        formArray.controls["selecCompIng"].value
      );

      documento.idTipoComprobanteIngreso = parseInt(
        formArray.controls["selecCompIng"].value
      );

      if (formArray.controls["selecTipoContacto"].value == "cliente") {
        documento.idCliente = this._SolcitudService.solicitudCompleta.infoCliente.cliente.idCliente;
        documento.idContacto = -1;
      } else {
        documento.idCliente = -1;
        documento.idContacto = parseInt(
          formArray.controls["selecContacto"].value
        );
      }

      // console.log("tipo documento", documento.idTipoDocumento);

      switch (documento.idTipoDocumento) {
        case 1:
          console.log("Comprobante de Domicilio");

          try {
            if (formArray.controls["selecTipoContacto"].value == "cliente") {
              documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio.idDomicilio;
            } else if (formArray.controls["selecTipoContacto"].value == 3) {
              documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.contactosCliente.filter(
                x => x.contacto.idTipoContacto == 3
              )[0].domicilioContacto.domicilio.idDomicilio;
            }
          } catch (e) {
            //this.toaster.error("No existe un domicilio");
          }

          if (documento.idDomicilio > 1) {
            this.setDocumentoGeneral(documento, i,this.listArchivos[i]);
          } else {
            this.toaster.error("No existe un domicilio");
            this.guardarDocs(i+1);
          }

          break;

        case 2:
          console.log("Comprobante de propiedad");
          try {
            if (formArray.controls["selecTipoContacto"].value == "cliente") {
              documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio.idDomicilio;
            } else if (formArray.controls["selecTipoContacto"].value == 3) {
              documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.contactosCliente.filter(
                x => x.contacto.idTipoContacto == 3
              )[0].domicilioContacto.domicilio.idDomicilio;
            }
          } catch (e) {
            //this.toaster.error("No existe un domicilio");
          }

          if (documento.idDomicilio > 1) {
            this.setDocumentoGeneral(documento, i,this.listArchivos[i]);
          } else {
            this.toaster.error("No existe un domicilio");
            this.guardarDocs(i+1);

          }
          break;
        case 3:
          console.log("Comprobante de Ingresos");

          this.setDocumentoGeneral(documento, i,this.listArchivos[i]);

          break;

        case 4:
          console.log("Pagare");

          this.setPagareCliente(documento, i,this.listArchivos[i]);
          break;

        case 5:
          console.log("Fotografia Casa (Exterior)");
          try {
            /* if (formArray.controls["selecTipoContacto"].value == "cliente") { */
            documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio.idDomicilio;
            /* } */
          } catch (e) {
            //this.toaster.error("No existe un domicilio");
          }

          if (documento.idDomicilio > 1) {
            this.setDocumentoGeneral(documento, i,this.listArchivos[i]);
          } else {
            this.toaster.error("No existe un domicilio");
            this.guardarDocs(i+1);

          }

          break;

        case 6:
          console.log("Fotografía Casa (Interior)");
          try {
            /* if (formArray.controls["selecTipoContacto"].value == "cliente") { */
            documento.idDomicilio = this._SolcitudService.solicitudCompleta.infoCliente.domicilioCliente.domicilio.idDomicilio;
            /* } */
          } catch (e) {
            //this.toaster.error("No existe un domicilio");
          }

          if (documento.idDomicilio > 1) {
            this.setDocumentoGeneral(documento, i,this.listArchivos[i]);
          } else {
            this.toaster.error("No existe un domicilio");
            this.guardarDocs(i+1);

          }

          break;

        case 7:
          console.log("Pagare Aval");

          documento.idContacto = this._SolcitudService.getDatosContacto(
            3
          ).contacto.idContacto;

          if (documento.idContacto > 0) {
            this.setPagareAval(documento, i,this.listArchivos[i]);
          } else {
            this.toaster.error("No existe un Aval");
            this.guardarDocs(i+1);

          }

          break;
      }


      
    }else{
      console.log("Eliminando Docuemntos Subidos");
      
      this.listArchivosSubidos.forEach(x=>{
        this.eliminarArchivo(x.index,x.file);
      })
      this.listArchivosSubidos=[];
    }
  }

  setPagareAval(pagare: DocumentoRequestModel, i: number,archivo: FileItem
    ) {
    console.log("Pagare Aval Enviado", pagare);
    this._SolcitudService.cargandSolicitud = true;
    this._SolcitudService.agregaPagareAvalClienteSolicitud(pagare).subscribe(
      resp => {
        // console.log(resp);
        console.log("Pagare aval guardado");
        this.toaster.success("Pagare Aval", "Documentos: Guardado");
        this._SolcitudService.cargandSolicitud = false;
        this.listArchivosSubidos.push({ index: i, file: archivo });

        this.guardarDocs(i + 1);
      },
      err => {
        this.toaster.error(err.message, "Documentos: Pagare Aval");
        this._SolcitudService.cargandSolicitud = false;
        console.log(err);
        this.guardarDocs(i + 1);

      }
    );
  }

  setPagareCliente(
    pagare: DocumentoRequestModel,
    i: number,
    archivo: FileItem
  ) {
    console.log("Pagare Cliente Enviado", pagare);
    this._SolcitudService.cargandSolicitud = true;
    this._SolcitudService.agregaPagareClienteSolicitud(pagare).subscribe(
      resp => {
        // console.log(resp);

        console.log("Pagare cliente guardado");
        this.toaster.success("Pagare Cliente", "Documentos: Guardado");
        this._SolcitudService.cargandSolicitud = false;
        
        this.listArchivosSubidos.push({ index: i, file: archivo });

        this.guardarDocs(i + 1);
      },
      err => {
        this._SolcitudService.cargandSolicitud = false;
        console.log(err);
        this.guardarDocs(i + 1);

        this.toaster.error(err.message, "Documentos: Pagare Cliente");
      }
    );
  }

  setDocumentoGeneral(documento: DocumentoRequestModel, i: number,archivo:FileItem) {
    console.log("Documento Enviado", documento);
    this._SolcitudService.cargandSolicitud = true;
    this._SolcitudService.agregaDocumentoSolicitud(documento).subscribe(
      resp => {
        // console.log(resp);
        if (resp.idDocumento > 1) {
          console.log("documento guardado");
          this.toaster.success(documento.name, "Documento agregado");
          this._SolcitudService.cargandSolicitud = false;
          this.listArchivosSubidos.push({ index: i, file: archivo });

          this.guardarDocs(i + 1);
        }
      },
      err => {
        this._SolcitudService.cargandSolicitud = false;
        console.log(err);
        this.guardarDocs(i + 1);

        this.toaster.error(documento.name, "Documento no agregado");
      }
    );
  }

  validarDocumentos() {
    let pasa: boolean = true;
    this.listArchivos.forEach(x => {
      if (x.idTipoDocumento > 0) {
        switch (x.idTipoDocumento) {
          case 1:
            if (x.idTipoComprobanteDomicilio > 0) {
              if (x.idDomicilio > 0) {
              } else {
                pasa = false;
                this.toaster.error(
                  "algunos comprobantes de domicilio no tienen Domicilio",
                  "No se puede subir el archivo"
                );
              }
            } else {
              pasa = false;
              this.toaster.error(
                "algunos comprobantes de domicilio no tienen tipo de comprobante",
                "No se puede subir el archivo"
              );
            }

            break;
          case 2:
            if (x.idTipoComprobantePropiedad > 1) {
              if (x.idDomicilio > 0) {
              } else {
                pasa = false;
                this.toaster.error(
                  "algunos comprobantes de Propiedad no tienen Domicilio",
                  "No se puede subir el archivo"
                );
              }
            } else {
              pasa = false;
              this.toaster.error(
                "algunos comprobantes de Propiedad no tienen tipo de comprobante",
                "No se puede subir el archivo"
              );
            }
            break;
          case 3:
            if (x.idTipoComprobanteIngreso > 1) {
              if (x.idEmpleo > 0) {
              } else {
                pasa = false;
                this.toaster.error(
                  "algunos comprobantes de Ingreso no tienen Empleo",
                  "No se puede subir el archivo"
                );
              }
            } else {
              pasa = false;
              this.toaster.error(
                "algunos comprobantes de Ingreso no tienen tipo de comprobante",
                "No se puede subir el archivo"
              );
            }
            break;
          case 5:
            if (x.idDomicilio > 0) {
            } else {
              pasa = false;
              this.toaster.error(
                "No seleccionaste domicilio en fotografia exterior",
                "No se puede subir el archivo"
              );
            }
            break;
          case 6:
            if (x.idDomicilio > 0) {
            } else {
              pasa = false;
              this.toaster.error(
                "No seleccionaste domicilio en fotografia interior",
                "No se puede subir el archivo"
              );
            }
            break;
          case 7:
            if (
              !isNullOrUndefined(
                this._SolcitudService.getDatosContacto(3).contacto
              )
            ) {
            } else {
              pasa = false;
              this.toaster.error(
                "No existe el aval",
                "No se puede subir el archivo"
              );
            }
            break;
        }
      } else {
        pasa = false;
        this.toaster.error(
          "algunos documentos no tienen tipo de documento",
          "No se puede subir el archivo"
        );
      }
    });
  }
}
