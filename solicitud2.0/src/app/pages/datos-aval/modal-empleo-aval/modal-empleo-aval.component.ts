import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";
import { SolcitudService } from "src/app/services/solcitud.service";
import { FileItem } from "src/app/models/file-item";
import { EmpleosModel } from "src/app/models/Empleos.model";
import { OtrosIngresosModel } from "src/app/models/OtrosIngresos.model";
import { EmpleoModel } from "../../../models/Empleo.model";
import { DatosEmpleoModel } from "src/app/models/DatosEmpleo.model";
import { EmpleoRequestModel } from "src/app/models/models-request/EmpleoRequest.model";
import { AfterViewChecked } from '@angular/core';

@Component({
  selector: "app-modal-empleo-aval",
  templateUrl: "./modal-empleo-aval.component.html",
  styleUrls: ["./modal-empleo-aval.component.css"]
})
export class ModalEmpleoAvalComponent implements OnInit, AfterViewChecked {
  @Input() openModal: boolean;
  @Output() dataEmpleoAval: EventEmitter<{ open: boolean; data?: any }>;

  @ViewChild("selectTipoEmpleo") selectTipoEmpleo: ElementRef;
  @ViewChild("selectTipoIngreso") selectTipoIngreso: ElementRef;
  @ViewChild("edtEmpresa") edtEmperesa: ElementRef;
  @ViewChild("edtEmpleo") edtEmpleo: ElementRef;
  @ViewChild("edtPuesto") edtPuesto: ElementRef;
  @ViewChild("edtIngreso") edtIngreso: ElementRef;
  @ViewChild("edtTelefono") edtTelefono: ElementRef;
  @ViewChild("edtAnos") edtAnos: ElementRef;
  @ViewChild("edtMeses") edtMeses: ElementRef;

  listEmpleosAval: DatosEmpleoModel[] = [];

  openModalEmpleoAval: boolean = false;

  estaSobreDrop: boolean = false;

  empresa: boolean = true;
  empleo: boolean = true;
  puesto: boolean = true;
  telefono: boolean = true;
  tipoEmpleo: boolean = true;
  tipoIngreso: boolean = false;

  listEmpleos: EmpleosModel[] = [];
  listOtrosIngresos: OtrosIngresosModel[] = [];

  listArchivos: FileItem[] = [];

  constructor(private _solicitudService: SolcitudService) {
    this.dataEmpleoAval = new EventEmitter();
  }

  ngOnInit() {
    this.listEmpleosAval = this._solicitudService.getDatosContacto(
      3
    ).infoEmpleosContacto;
    // this.listEmpleosAval[].domicilioEmpleo
    this.getEmpleos();
    this.getOtrosIngresos();
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
   /*  this.listEmpleosAval = this._solicitudService.solicitudCompleta.infoCliente
                          .contactosCliente
                          .filter(x=> x.contacto.idTipoContacto==3)
                          .fil */
  }

  closeModal() {
    this.openModal = false;
    this.dataEmpleoAval.emit({ open: false });
  }

  getEmpleos() {
    this._solicitudService.getEmpleos().subscribe(respuesta => {
      console.log(respuesta);
      this.listEmpleos = respuesta;
    });
  }

  getOtrosIngresos() {
    this._solicitudService.getOtrosIngresos().subscribe(respuesta => {
      console.log(respuesta);
      this.listOtrosIngresos = respuesta;
    });
  }
  onSelectTipoEmpleo(idTipoEmpleo: string) {
    console.log(idTipoEmpleo);

    switch (parseInt(idTipoEmpleo)) {
      case 1:
        console.log("Formal");
        this.empresa = true;
        this.empleo = true;
        this.puesto = true;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        break;
      case 2:
        console.log("Informal");
        this.empresa = true;
        this.empleo = true;
        this.puesto = true;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        break;
      case 3:
        console.log("Negocio propio");
        this.empresa = true;
        this.empleo = true;
        this.puesto = true;
        this.telefono = true;
        this.tipoEmpleo = true;
        this.tipoIngreso = false;
        break;
      case 4:
        console.log("Otros ingresos");
        this.empresa = false;
        this.empleo = false;
        this.puesto = false;
        this.telefono = false;
        this.tipoEmpleo = true;
        this.tipoIngreso = true;
        break;
    }
  }

  onClickOtroEmpleo() {
    this.empresa = true;
    this.empleo = true;
    this.puesto = true;
    this.telefono = true;
    this.tipoEmpleo = true;
    this.tipoIngreso = false;
  }

  setEmpleoAval() {
    let empleoAval: EmpleoRequestModel = new EmpleoRequestModel();

    empleoAval.agente = SolcitudService.agente;
    empleoAval.idOrigen = -1;
    empleoAval.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    empleoAval.idTipoEmpleo=-1;
    /* empleoAval.idContacto = */ 
  }
}
