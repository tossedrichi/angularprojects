import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { SolcitudService } from "../../services/solcitud.service";
import { EdoCivilModel } from "src/app/models/EdoCivil.model";
import { FileItem } from "src/app/models/file-item";
import { FormGroup, FormControl } from "@angular/forms";
import { ConyugeModel } from "../../models/Conyuge.model";
import { generarRFC, INEFix } from "src/app/tools";
import { DatosContactoModel } from "src/app/models/DatosContactoModel";
import { ContactoModel } from "src/app/models/Contacto.model";
import { DatosContactoRequestModel } from "src/app/models/models-request/DatosContactoRequest.model";
import { ToastrService } from "ngx-toastr";
import { TelefonoRequestModel } from "src/app/models/models-request/TelefonoRequest.model";
import { INEModel } from "src/app/models/INE.model";
import { INERequestModel } from "src/app/models/models-request/INEReques.model";
import { isUndefined, isNullOrUndefined } from "util";
import { DataInputModalGuardarModel } from 'src/app/components/modal-guardar/dataInputModa.model';
import { EmpleoRequestModel } from 'src/app/models/models-request/EmpleoRequest.model';

@Component({
  selector: "app-datos-aval",
  templateUrl: "./datos-aval.component.html",
  styleUrls: ["./datos-aval.component.css"]
})
export class DatosAvalComponent implements OnInit {
  //formularioDatosAval:FormGroup;
  @ViewChild("edtNombre") edtNombre: ElementRef;
  @ViewChild("edtApellidoP") edtApellidoP: ElementRef;
  @ViewChild("edtApellidoM") edtApellidoM: ElementRef;
  @ViewChild("edtFechaNacimiento") edtFechaNacimiento: ElementRef;
  @ViewChild("edtTelefonoFijo") edtTelefonoFIjo: ElementRef;
  @ViewChild("edtTelefonoCelular") edtTelefonoCel: ElementRef;
  @ViewChild("selectEdoCivil") selectEdoCivil: ElementRef;
  @ViewChild("edtIngreso") edtIngreso: ElementRef;
  @ViewChild("edtRfc") edtRfc: ElementRef;
  openModalGuardar:boolean= false;
  dataInput: DataInputModalGuardarModel;
  formularioDatosAval:FormGroup;
  openModalEmpleoAval: boolean = false;
  empleo: boolean = false;
  esEdicion: boolean = false;
  aval: DatosContactoModel;
  openModalEliminarEmpelo:boolean=false;
  listTiposEdoCivil: EdoCivilModel[] = [];
  estaSobreDrop: boolean = false;
  generoF: boolean = false;
  generoM: boolean = false;
  listArchivos: FileItem[] = [];
  listArchivos2: FileItem[] = [];
  ineFrente: FileItem;
  ineReverso: FileItem;
  image: string;

  listContactos: DatosContactoModel[];

  guardoDatos:boolean= false;
  guardoDatosExitoso:boolean= false;

  guardoIne:boolean= false;
  guardoIneExitoso:boolean= false;

  guardoCel:boolean= false;
  guardoCelExitoso:boolean= false;

  guardoFijo:boolean= false;
  guardoFijoExitoso:boolean= false;

  constructor(
    public _solicitudService: SolcitudService,
    private toaster: ToastrService
  ) {}

  cuentaEmplo(req:boolean){
    if(req){
      if(this.aval.infoEmpleosContacto.length==0){
        this.setEmpleoCliente()
      }
    }else{
      if(this.aval.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length>0){
        console.log("Borrar empleos Preguntando");
        this.dataInput = new DataInputModalGuardarModel(
          "Se van a eliminar los empleos que ya has agregado",
          "¿Deseas continuar?",
          []
          
        );
        this.openModalEliminarEmpelo = true;

      }else if(this.aval.infoEmpleosContacto.length>0 &&
        this.aval.infoEmpleosContacto.filter(x=>x.empleo.empleo.localeCompare("NA")!=0).length==0){
          console.log("Borrar empleo NA");
          this.setEliminarEmpleo(0);
        }
    }
  }

  responseDataEliminar(event) {
    console.log(event);
    this.openModalEliminarEmpelo = false;

    if (event.save) {
      this.setEliminarEmpleo(0);
     

     
      //this.guardarTelefonos();
    }
  }
  ngOnInit() {
    this.getTiposEstadoCivil();

    this.aval = this.getDatos();
    console.log("aval", this.aval);

    if (isNullOrUndefined(this.aval.contacto)) {
      console.log("nuevo");

      this.esEdicion = false;
      this.aval = new DatosContactoModel();
      this.aval.contacto= new ContactoModel();
      this.aval.contacto.nombres = "";
      this.aval.contacto.primerApellido = "";
      this.aval.contacto.segundoApellido = ""; 
      this.aval.contacto.rfc="";
      this.aval.contacto.idContacto=0;
      
    } else {
      console.log("edicion");

      console.log("aval", this.aval);
      this.esEdicion = true;
      
    }
    this.autoSelect();
    this.formularioDatosAval = new FormGroup({
      fechaNacimiento: new FormControl(this.getFechaNacimiento()),
      rfc: new FormControl(this.aval.contacto.rfc),

      estadoCivil: new FormControl(this.aval.contacto.idTipoEstadoCivil),
      genero: new FormControl(this.aval.contacto.genero),
      telefonoCelular: new FormControl(this.getTelefonoMovil()),
      telefonoFijo: new FormControl(this.getTelefonoMovil()),
      correo: new FormControl(this.aval.contacto.correo),
      cuentaEmpleo: new FormControl(this.tieneEmpleo()),
      requiereAval: new FormControl()//,
      //empleo: new FormControl(this._solicitudServices.reqDatosEmpleoCliente)
    });
  }
  tieneEmpleo():boolean{
    return this.aval.infoEmpleosContacto.length>0;
   }
  getTelefonoCel():String{
    let cel:String="";
    this.aval.telefonosContacto.forEach(x=>{
      if(x.estadoActual.includes("Activo")&& x.idTipoTelefono==2){
        cel=x.numeroTelefonico;
      }
    })
    
    return cel;
  }


  getTiposEstadoCivil() {
    this._solicitudService.getEstadoCivil().subscribe(response => {
      this.listTiposEdoCivil = response;
      if (this.aval.contacto != undefined && this.aval.contacto != null) {
        if (this.aval.contacto.idTipoEstadoCivil >= 1) {
          this.selectEdoCivil.nativeElement.value = this.aval.contacto.idTipoEstadoCivil.toString();
        }
      }
    });
  }

  reqDatConyuge(selec: number) {
    console.log(selec);

    if (selec == 1 || selec == 3) {
      this._solicitudService.reqDatosConyugeAval = true;
    } else {
      this._solicitudService.reqDatosConyugeAval = false;
    }
  }

  respDataModalEmpleoAval(event) {
    console.log(event);
    this.openModalEmpleoAval = false;
    if (event.isCancel) {
      this.openModalEmpleoAval = event.open;
    }
  }

  requiereEmpleo(req: boolean) {
    this.empleo = req;
  }

  selectImage(evento: any, isFrente) {
    console.log(evento.target.files);
    const archivoTemporal = evento.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(archivoTemporal);
    reader.onload = event => {
      console.log(event);
      let base64 = (<FileReader>event.target).result;
      let ine: INEModel = new INEModel();
      if (isFrente) {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamFrente = file.base64;
        //this._solicitudService.solicitudCompleta.infoCliente.ine = ine;
        this.ineFrente = file;
      } else {
        let file = new FileItem(archivoTemporal, base64.toString());
        ine.fileNameFrente = file.nombreArchivo;
        ine.streamReverso = file.base64;
       // this._solicitudService.solicitudCompleta.infoCliente.ine = ine;
        this.ineReverso = file;
      }
    };
  }

  autoSelect() {
    try {
      if (this.aval.contacto.genero.includes("M")) {
        this.generoM = true;
        this.generoF = false;
      } else if (this.aval.contacto.genero.includes("F")) {
        this.generoF = true;
        this.generoM = false;
      }
    } catch (error) {
      
    }
  }



  getFechaNacimiento(): string {
    if(isUndefined(this.aval.contacto.fechaNacimiento)){
      return "1900-01-01"
    }else{
      return this.aval.contacto.fechaNacimiento.split(" ")[0]
    }
    
    
  }

  generaRfc() {
    this.aval.contacto.rfc = generarRFC(
      this.edtNombre.nativeElement.value,
      this.edtApellidoP.nativeElement.value,
      this.edtApellidoM.nativeElement.value,
      this.edtFechaNacimiento.nativeElement.value
    );

    this.edtRfc.nativeElement.value = this.aval.contacto.rfc;
  }

  renderIne(isFrente: boolean) {
    //console.log("renderINE");

    if (this.aval.infoINE != null) {
      let ine: INEModel = this.aval.infoINE;
      if (isFrente) {
        if (ine.streamFrente != null && this.ineFrente == null) {
          // console.log("Existe Archivo Viejo");
          this.ineFrente = new FileItem(new File([], ""), "");

          this.ineFrente.nombreArchivo = ine.fileNameFrente;
          this.ineFrente.base64 = INEFix + ine.streamFrente;
          this.ineFrente.idDocumento = ine.idCredencial;
          return INEFix + ine.streamFrente;
        } else if (this.ineFrente != null) {
          // console.log("Existe Archivo Nuevo");
          return this.ineFrente.base64;
        } else {
          // console.log("No Existe Archivo");
          this.image = "assets/fren.png";
          return "assets/fren.png";
        }
      } else {
        if (ine.streamReverso != null && this.ineReverso == null) {
          this.ineReverso = new FileItem(new File([], ""), "");
          this.ineReverso.nombreArchivo = ine.fileNameReverso;
          this.ineReverso.base64 = INEFix + ine.streamReverso;
          this.ineReverso.idDocumento = ine.idCredencial;
          return INEFix + ine.streamReverso;
        } else if (this.ineReverso != null) {
          return this.ineReverso.base64;
        } else {
          return "assets/rever.png";
        }
      }
    } else if (isFrente) {
      //console.log("es frente");
      // console.log(this.ineFrente);

      if (this.ineFrente != null) {
        // console.log("Existe Archivo Nuevo");
        return this.ineFrente.base64;
      } else {
        //console.log("asset");

        return "assets/fren.png";
      }
    } else if (!isFrente) {
      if (this.ineReverso != null) {
        return this.ineReverso.base64;
      } else {
        return "assets/rever.png";
      }
    }
  }

  setContactoAval() {
    console.log("nuevo");

    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();
    this._solicitudService.cargandSolicitud=true;
    contacto.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    contacto.idTipoContacto = 3;
    contacto.nombres = this.edtNombre.nativeElement.value;
    contacto.primerApellido = this.edtApellidoP.nativeElement.value;
    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;
    contacto.idTipoEstadoCivil = this.selectEdoCivil.nativeElement.value;

    contacto.fechaNacimiento= this.edtFechaNacimiento.nativeElement.value;
    contacto.genero = this.formularioDatosAval.get("genero").value;
    contacto.rfc=this.aval.contacto.rfc;

    contacto.correo = "NA";
    contacto.idRelacionContacto = -1;
    contacto.idTipoRelacionEmpleo = -1;
    contacto.idTipoRelacionContacto = -1;
    contacto.idTipoContactoIngreso = -1;
    contacto.aportaIngresos = false;

    if (this.generoM) {
      contacto.genero = "M";
    } else if (this.generoF) {
      contacto.genero = "F";
    }
    this.guardoDatos=true;
    this._solicitudService.agregaContactoClientSolicitud(contacto).subscribe(
      resp => {
        this._solicitudService.cargandSolicitud=false;
        console.log(resp);
        this.aval.contacto.idContacto = resp.idContacto;

        this.guardoDatosExitoso=true;
        if (!isUndefined(this.ineFrente) ||!isUndefined(this.ineReverso)) {
          if (isUndefined(this.ineFrente.idDocumento) ||isUndefined(this.ineReverso.idDocumento)) {
            this.guardarINE();
          }else{
            //guarda telefonos
            this.setCelAval(resp.idContacto);
          }
        }else{
          this.setCelAval(resp.idContacto);
          //guarda telefonos
        }

   
      },
      err => {
        this.guardoDatosExitoso=false;
        this._solicitudService.cargandSolicitud=false;
        console.log(err);
 
      }
    );
  }

  setEditarContactoAval() {

    console.log("edicion");

 

    let contacto: DatosContactoRequestModel = new DatosContactoRequestModel();

    contacto.idContacto = this.aval.contacto.idContacto;

    contacto.idSolicitud = this._solicitudService.solicitud.idSolicitud;

    contacto.idTipoContacto = 3;

    contacto.nombres = this.edtNombre.nativeElement.value;

    contacto.primerApellido = this.edtApellidoP.nativeElement.value;

    contacto.segundoApellido = this.edtApellidoM.nativeElement.value;

    contacto.idTipoEstadoCivil = this.selectEdoCivil.nativeElement.value;

    contacto.fechaNacimiento = this.edtFechaNacimiento.nativeElement.value;

    contacto.rfc = this.edtRfc.nativeElement.value;

    contacto.correo = "NA";

    contacto.idRelacionContacto = -1;

    contacto.idTipoRelacionEmpleo = -1;

    contacto.idTipoRelacionContacto = -1;

    contacto.idTipoContactoIngreso = -1;

    contacto.aportaIngresos = false;

    if (this.generoM) {

      contacto.genero = "M";

    } else if (this.generoF) {

      contacto.genero = "F";

    }
    this.guardoDatos=true;
    this._solicitudService.cargandSolicitud=true;

    this._solicitudService.actualizaContactoClientSolicitud(contacto).subscribe(

      resp => {

        console.log(resp);

        this._solicitudService.cargandSolicitud=false;
        this.guardoDatosExitoso=true;
     

        if (!isUndefined(this.ineFrente) ||!isUndefined(this.ineReverso)) {
          if (isUndefined(this.ineFrente.idDocumento) ||isUndefined(this.ineReverso.idDocumento)) {
            this.guardarINE();
          }else{
            //guarda telefonos
            this.setCelAval(resp.idContacto);
          }
        }else{
          this.setCelAval(resp.idContacto);
          //guarda telefonos
        }

      },

      err => {
        this._solicitudService.cargandSolicitud=false;
        console.log(err);
        this.guardoDatosExitoso=false;
        

      }

    );

  }

  onSaveClick() {
    if ((!isUndefined(this.ineReverso) && isUndefined(this.ineFrente))||isUndefined(this.ineReverso) && !isUndefined(this.ineFrente)) {
      this.toaster.error("Falta completar llenado de la INE")
    }else{
    this.dataInput = new DataInputModalGuardarModel(
      "Guardar Cambios",
      "¿Desea guardar los cambios del  del aval?",
      []
    );
    this.openModalGuardar = true;
    }
  }

  responseDataGuardar(event) {
    console.log("es edicion", this.esEdicion);
    this.openModalGuardar = false;
    if(event.save){
      if (this.aval.contacto.idContacto > 0) {
        this.setEditarContactoAval();
      } else {
        this.setContactoAval();
      }
    }


  }

  setTelefonoAval(idContacto) {
  
    let telefonoFijo: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoFijo.idContacto = idContacto;
    telefonoFijo.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    telefonoFijo.idSucursal = this._solicitudService.solicitud.idSucursal;
    telefonoFijo.idTipoTelefono = 1;
    telefonoFijo.telefono = this.edtTelefonoFIjo.nativeElement.value;

 
    telefonoFijo.idTipoTelefono = 1;
    
    if (this.getTelefonoFijo().localeCompare(telefonoFijo.telefono) != 0) {
      this._solicitudService.cargandSolicitud=true;
      this.guardoFijo=true;
      this._solicitudService
      .agregaTelefonoContactoClienteSolicitud(telefonoFijo)
      .subscribe(
        resp => {
          this.guardoFijoExitoso=true;
          this._solicitudService.cargandSolicitud=false;
          console.log(resp);
        
          this.cargaSolicitud();
          
        },
        err => {
          this.guardoFijoExitoso=false;
          this._solicitudService.cargandSolicitud=false;
         
          console.log(err);
        }
      );
    }else{
      //cargar solicitud
      this.cargaSolicitud();
    }
  
    
  }

  setCelAval(idContacto) {


    let telefonoCel: TelefonoRequestModel = new TelefonoRequestModel();
    telefonoCel.idContacto = idContacto;
    telefonoCel.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    telefonoCel.idSucursal = this._solicitudService.solicitud.idSucursal;
    telefonoCel.idTipoTelefono = 2;
   
    telefonoCel.telefono = this.edtTelefonoCel.nativeElement.value;
 
    if (this.getTelefonoMovil().localeCompare(telefonoCel.telefono) != 0) {
      this._solicitudService.cargandSolicitud=true;
      this.guardoCel=true;
      this._solicitudService
      .agregaTelefonoContactoClienteSolicitud(telefonoCel)
      .subscribe(
        resp => {
          this.guardoCelExitoso=true;
          this._solicitudService.cargandSolicitud=false
          console.log(resp);
        
          this.setTelefonoAval(this.aval.contacto.idContacto)
        },
        err => {
          this.guardoCelExitoso=false
          this._solicitudService.cargandSolicitud=false;
        
          console.log(err);
        }
      );
    }else{
      // carga fijo
      this.setTelefonoAval(this.aval.contacto.idContacto)
    }

  }

  cargaSolicitud(){
    this._solicitudService.cargandSolicitud=true;
    this._solicitudService
    .getSolicitudCompleta(
      this._solicitudService.solicitud.idSolicitud
    )
    .subscribe(
      resp => {
        this.aval = this.getDatos();
        if(this.guardoDatos){
          if(this.guardoDatosExitoso){
            this.toaster.success("Datos guardados", "Datos Aval");
          }else{
            this.toaster.error("Error", "Error: Datos Aval");
          }
        }
  
        if(this.guardoIne){
          if(this.guardoIneExitoso){
            this.toaster.success("INE guardada correctamente", "Datos Aval");
          }else{
            this.toaster.error("Error", "Error: Ine Aval");
          }
        }
  
        if(this.guardoCel){
          if(this.guardoCelExitoso){
            this.toaster.success("Teléfono móvil Guardado", "Datos Aval");
          }else{
            this.toaster.error("Error", "Error al guardar telefono");
          }
        }
  
        if(this.guardoFijo){
          if(this.guardoFijoExitoso){
            this.toaster.success("Teléfono fijo Guardado", "Datos Aval");
          }else{
            this.toaster.error("Error", "Error al guardar telefono");
          }
        }
  
        this.guardoDatos= false;
        this.guardoDatosExitoso= false;
      
        this.guardoIne= false;
        this.guardoIneExitoso= false;
      
        this.guardoCel= false;
        this.guardoCelExitoso= false;
      
        this.guardoFijo= false;
        this.guardoFijoExitoso= false;

        this._solicitudService.cargandSolicitud=false;
        console.log(resp);
        this.aval= this.getDatos()
        this._solicitudService.solicitudCompleta.infoCliente.contactosCliente =
          resp.infoCliente.contactosCliente;
      },
      err => {
        this._solicitudService.cargandSolicitud=false;
        this.toaster.error(
          err.message,
          "Error al traer datos: Datos Aval"
        );
      }
    );
  }

  getDatos(): DatosContactoModel {
    return this._solicitudService.getDatosContacto(3);
  }
  

  guardarINE() {
      this._solicitudService.cargandSolicitud=true;
      let ineAnverso;
      if (this.ineFrente.base64.includes("base64")) {
        ineAnverso = this.ineFrente.base64.split("base64,")[1];
      } else {
        ineAnverso = this.ineFrente.base64;
      }

      let ineReverso;
      if (this.ineReverso.base64.includes("base64")) {
        ineReverso = this.ineReverso.base64.split("base64,")[1];
      } else {
        ineReverso = this.ineReverso.base64;
      }

      let ine: INERequestModel = new INERequestModel();

      ine.idSolicitud = this._solicitudService.solicitudCompleta.idSolicitud;
      ine.streamAnverso = ineAnverso;
      ine.fileNameAnverso = this.ineFrente.nombreArchivo;
      ine.streamReverso = ineReverso;
      ine.fileNameReverso = this.ineReverso.nombreArchivo;
      ine.idContacto = this.aval.contacto.idContacto;
      this.guardoIne=true;
      console.log("INE ENVIADA", ine);
      this._solicitudService.agregaCredencialContactoSolicitud(ine).subscribe(
        resp => {
          this._solicitudService.cargandSolicitud=false;
          this.setCelAval(resp.idContacto);
          console.log("Guardado INE", resp);
          this.guardoIneExitoso=true;
         
          //this.setTelefonoAval(resp.idContacto);
          this.ineFrente.idDocumento = resp.idCredencial;
          this.ineReverso.idDocumento = resp.idCredencial;
        },
        err => {
          this._solicitudService.cargandSolicitud=false;
          console.log(err);
          this.guardoIneExitoso=false;
          
        }
      );
    
  }

  getTelefonoMovil() {
    let num = "";
    this.aval.telefonosContacto.forEach(x => {
      if (x.idTipoTelefono == 2) {
        num = x.numeroTelefonico;
      }
    });
    return num;
  }
  getTelefonoFijo() {
    let num = "";
    this.aval.telefonosContacto.forEach(x => {
      if (x.idTipoTelefono == 1) {
        num = x.numeroTelefonico;
      }
    });
    return num;
  }

  setEmpleoCliente() {
    let empleoCl: EmpleoRequestModel = new EmpleoRequestModel();
    
    empleoCl.idSolicitud = this._solicitudService.solicitud.idSolicitud;
    empleoCl.agente = this._solicitudService.solicitud.idAgente;
    empleoCl.idOrigen = -1;
    //  empleoCl.idEmpleo = this.edtEmpleo.nativeElement.value;
    empleoCl.idTipoEmpleo = 1;
    empleoCl.empleo = "NA";
    empleoCl.estuvoDesempleado = false;
    empleoCl.ingresoMensual = 0;
    empleoCl.nombreEmpresa = "NA";
    empleoCl.puesto = "NA";
    empleoCl.antiguedadEmpleoAnterior = 0;
    empleoCl.idContactoEmpleo = -1;;
    //console.log("Select Tipo Cingreso", this.selecTipoIngreso);

  
      empleoCl.idTipoIngreso = -1;
    
    empleoCl.idContacto = this.aval.contacto.idContacto;
    empleoCl.antiguedad = 0;
    //if(empleoCl.idTipoEmpleo>0 &&
    //   empleoCl.nombreEmpresa.localeCompare("")!=0 &&
    //   empleoCl.puesto.localeCompare("")!=0 &&
    //   empleoCl.ingresoMensual>0
    //   ){
      this._solicitudService.cargandSolicitud=true;
      this._solicitudService
        .agregaEmpleoContactoClienteSolicitud(empleoCl)
        .subscribe(
        resp => {
          console.log(resp);
          console.log(
            "Empleo guardado correctamente",
            "Datos Empleo Cliente"
          );
          this._solicitudService.cargandSolicitud=false;
          this.cargaSolicitud();
        },
        err => {
          this._solicitudService.cargandSolicitud=false;
          console.log(err);
          this.toaster.error(err.message, "Datos empleo: Error al guardar");
        }
      );
     
   // }else{
   //   this.toaster.error("Error al guardar", "Llena los campos obligatorios ");
   // }
   
  }

  setEliminarEmpleo(index:number) {
    if(index<this.aval.infoEmpleosContacto.length){
      this._solicitudService.cargandSolicitud=true;
      this._solicitudService.setEmliminarEmpleoSolcitud(
        this.aval.infoEmpleosContacto[index].empleo.idEmpleo,
          -1,
          this.aval.contacto.idContacto
        )
        .subscribe(
          resp => {
            this._solicitudService.cargandSolicitud=false;
            console.log(resp);
         
            console.log("Empleo Elimniado", "Eliminar Empleo");
            this.setEliminarEmpleo(index+1)
          },
          err => {
            this._solicitudService.cargandSolicitud=false;
            console.log(err);
            this.toaster.error(err.message, "Error: Eliminar Empleo");
          }
        );
    }else{
      this.cargaSolicitud()
    }

  }
  

}
