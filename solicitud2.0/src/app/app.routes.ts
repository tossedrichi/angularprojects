import { RouterModule, Routes } from '@angular/router';
import { DatosClienteComponent } from './pages/datos-cliente/datos-cliente.component';
import { DatosConyugeComponent } from './pages/datos-conyuge/datos-conyuge.component';
import { DomicilioClienteComponent } from './pages/domicilio-cliente/domicilio-cliente.component';
import { DatosEmpleoClienteComponent } from './pages/datos-empleo-cliente/datos-empleo-cliente.component';
import { DatosEmpleoConyugeComponent } from './pages/datos-empleo-conyuge/datos-empleo-conyuge.component';
import { DatosAvalComponent } from './pages/datos-aval/datos-aval.component';
import { DocumentosComponent } from './pages/documentos/documentos.component';
import { DatosConyugeAvalComponent } from './pages/datos-conyuge-aval/datos-conyuge-aval.component';
import { LoginComponent } from './pages/login/login.component';
import { SolicitudRedigitalizacionComponent } from './pages/solicitud-redigitalizacion.component';
import { AgregaPedidoComponent } from './pages/agrega-pedido/agrega-pedido.component';
import { ReferenciasComponent } from './pages/referencias/referencias.component';
import { IngresosFamiliaresComponent } from './pages/ingresos-familiares/ingresos-familiares.component';
import { DomiclioAvalComponent } from './pages/domiclio-aval/domiclio-aval.component';
import { FinalizarSolicitudComponent } from './pages/finalizar-solicitud/finalizar-solicitud.component';
import {DatosEmpleoAvalComponent} from './pages/datos-empleo-aval/datos-empleo-aval.component';
 

const APP_ROUTES: Routes = [

    { path: 'login', component: LoginComponent },

    {
        path: 'SolicitudRedigitalizacion',
        component: SolicitudRedigitalizacionComponent,
        children: [
            { path: 'datos-cliente', component: DatosClienteComponent },
            { path: 'datos-conyuge-cliente', component: DatosConyugeComponent },
            { path: 'domicilio-cliente', component: DomicilioClienteComponent },
            { path: 'datos-empleo-cliente', component: DatosEmpleoClienteComponent },
            { path: 'datos-empleo-conyuge-cliente', component: DatosEmpleoConyugeComponent },
            { path: 'datos-aval', component: DatosAvalComponent },
            { path: 'datos-conyuge-aval', component: DatosConyugeAvalComponent },
            { path: 'documentos', component: DocumentosComponent },
            { path: 'agrega-pedido', component: AgregaPedidoComponent },
            { path: 'referencias', component: ReferenciasComponent },
            { path: 'ingresos-familiares', component: IngresosFamiliaresComponent },
            {path: 'domicilio-aval', component: DomiclioAvalComponent},
            {path: 'finalizar-solicitud', component: FinalizarSolicitudComponent},
            {path: 'datos-empleo-aval', component: DatosEmpleoAvalComponent},


            { path: '**', component: DatosClienteComponent },

        ]
    },
    { path: '**', component: LoginComponent } 


];


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
