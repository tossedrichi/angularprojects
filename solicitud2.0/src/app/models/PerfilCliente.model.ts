import { SolicitudModel } from '../models/Solicitud.model';
import { ClienteModel } from './Cliente.model';
export class PerfilClienteModel{
    infoCliente:ClienteModel;
    solicitudesActuales:SolicitudModel[];
    historialCrediticio:SolicitudModel[];

}