import { ContactoModel } from "./Contacto.model";
import { DomicilioModel } from "./Domicilio.Model";
import { DomicilioClienteModel } from "./DomicilioClienteModel";
import { TelefonoModel } from "./TelefonoModel";
import { ConyugeModel } from './Conyuge.model';
import { EmpleoModel } from './Empleo.model';
import { DatosEmpleoModel } from './DatosEmpleo.model';
import { INEModel } from './INE.model';
export class DatosContactoModel {
  contacto: ContactoModel;
  conyugeContacto: ConyugeModel;
  domicilioContacto: DomicilioClienteModel;
  infoEmpleosContacto:DatosEmpleoModel [] = new Array<DatosEmpleoModel>();
  infoINE: INEModel;
  telefonosContacto: TelefonoModel[] = new Array<TelefonoModel>();

  constructor(){
    
  }
}


