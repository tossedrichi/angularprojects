import { HorarioVisitaModel } from './HorarioVisita.model';
import { InfoClienteModel } from './InfoCliente.model';
import { PedidoLigadoModel } from './PedidoLigado.model';

export class SolicitudCompletaModel{
    agente: number;
    agenteAsignacion: number;
    comentariosVentas: string;
    dependientes: number;
    estadoActual: string;
    estadoActualAsignacion: string;
    fechaActualizacion: string;
    fechaActualizacionAsignacion: string;
    fechaCierre: string;
    fechaRegistro: string;
    fechaRegistroAsignacion: string;
    horarioVisita:HorarioVisitaModel;
    idInvestigacion: number;
    idOrigen: number;
    idOrigenAsignacion: number;
    idSolicitud: number;
    infoCliente: InfoClienteModel= new InfoClienteModel();
    ingresoFamiliar: number
    //investigacionParcial: []
    pagareSolicitud: []
    personasDomicilio:number;
    pedidosAdjuntos:PedidoLigadoModel[];
    requiereAval: boolean;
    //seguimientosCredito: []
    tipoInvestigacion: string;
}