import { ClienteModel } from "src/app/models/Cliente.model";
import { INEModel } from "./INE.model";
import { TelefonoModel } from "./Telefono.model";
import { ConyugeModel } from "./Conyuge.model";
import { ContactoModel } from "./Contacto.model";
import { DomicilioClienteModel } from "./DomicilioClienteModel";
import { DatosContactoModel } from "./DatosContactoModel";
import { DatosEmpleoModel } from "./DatosEmpleo.model";
export class InfoClienteModel {
  cliente: ClienteModel;
  contactosCliente: DatosContactoModel[];
  ine: INEModel = new INEModel();
  telefonosCliente: TelefonoModel[] = new Array();
  conyugeCliente: ConyugeModel = new ConyugeModel();
  domicilioCliente: DomicilioClienteModel = new DomicilioClienteModel();
  empleosCliente: DatosEmpleoModel[] = new Array<DatosEmpleoModel>();
}
