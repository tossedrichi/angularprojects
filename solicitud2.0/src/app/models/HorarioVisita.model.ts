export class HorarioVisitaModel{
    agente:number;
    comentariosInvestigador: string;
    comentariosVisita:string;
    estadoVisita: string;
    fechaFinal: string;
    fechaInicial: string;
    idCliente: number;
    idContacto:number; 
    idHorarioVisita:number;
    idInvestigacion: number;
    idOrigen: number;
    idSeguimiento: number;
    idSolicitud: number;
    idTipoVisita: number;
}