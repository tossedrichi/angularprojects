export class ContactoModel{
    agente: number ;
    aportaIngresos: false
    correo: string;
    estadoActual: string;
    fechaNacimiento: string;
    fechaRegistro: string;
    genero: string;
    idContacto:number;
    idConyuge:number;
    idOrigen: number;
    idTipoContacto: number;
    idTipoContactoIngreso: number;
    idTipoEstadoCivil: number;
    idTipoRelacionContacto: number;
    idTipoRelacionEmpleo: number;
    importeIngresos: number;
    nombres:string;
    primerApellido: string;
    rfc: string;
    segundoApellido:string;
}