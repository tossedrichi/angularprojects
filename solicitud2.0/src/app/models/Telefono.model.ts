export class TelefonoModel{
    agente: number;
    estadoActual: string;
    fechaActualizacion: string;
    fechaRegistro: string;
    idOrigen: number;
    idTelefono: number;
    idTipoTelefono: number;
    numeroTelefonico:string;
}