export class DocumentoEmpleo {
    antiguedad: number
    estadoActual: string
    fechaRegistro: string
    fileName: string
    idDocumento: number
    idPeriodicidad: number
    idTipoComprobanteDomicilio: number
    idTipoComprobanteIngreso: number
    idTipoComprobantePropiedad: number
    idTipoDocumento: number
    stream: string
    uiddocumento: string

    constructor(){}
}