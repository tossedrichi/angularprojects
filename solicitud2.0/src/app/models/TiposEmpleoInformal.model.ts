export class TiposEmpleoInformalModel {
  id_tipo_empleo_informal: number
  tipo_empleo_informal: string
  valida_informacion: boolean
  estado_actual: string
}
