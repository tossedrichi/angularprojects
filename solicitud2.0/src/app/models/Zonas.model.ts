export class ZonasModel{
    
    agente: number;
    idOrigen: number;
    idSubadministrador: number;
    idZona: number;
    idZonaSubadmin: number;
    nombreZona:string;
  
}