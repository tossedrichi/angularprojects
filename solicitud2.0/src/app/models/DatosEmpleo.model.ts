import { DocumentoEmpleo } from "./DocumentoEmpleoModel";
import { ContactoModel } from "./Contacto.model";
import { DomicilioModel } from "./Domicilio.Model";
import { EmpleoModel } from "./Empleo.model";
import { TelefonoModel } from "./Telefono.model";
import { InfoEmpleosContactoModel } from './InfoEmpleosContacto.model';

export class DatosEmpleoModel {
  contactoEmpleo: ContactoModel;
  documentosEmpleo: DocumentoEmpleo[] = new Array();
  domicilioEmpleo: DomicilioModel;
  empleo: EmpleoModel;
 
  telefonosEmpleo: TelefonoModel[] = new Array();
}
