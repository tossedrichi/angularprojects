export class FileItem {

  public archivo: File;
  public nombreArchivo: string;
  public url: string;
  public estaSubiendo: boolean;
  public progreso: number;
  public base64:string;
  public idTipoDocumento:number

  public idDocumento:number
  public idTipoComprobanteDomicilio: number
  public idTipoComprobanteIngreso: number
  public idTipoComprobantePropiedad: number
  public idContacto:number;
  public idDomicilio:number;
  public idEmpleo:number;
  
  constructor(archivo: File,base64:string) {
    this.archivo = archivo;
    this.nombreArchivo = archivo.name;
    this.estaSubiendo = false;
    this.progreso = 0;
    this.base64=base64;

  }
}
