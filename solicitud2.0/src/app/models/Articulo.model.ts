export class ArticuloModel{
    cantidad: Number;
    estadoMercanciaLu: string;
    idArticulo: number;
    malEstado: boolean;
    nombreArticulo: string;
    precio: number;

    getPrecio:string="$" + new Intl.NumberFormat().format(this.precio);
    
}