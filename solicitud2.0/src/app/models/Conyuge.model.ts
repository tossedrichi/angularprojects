import { ContactoModel } from './Contacto.model';
import { INEModel } from './INE.model';
import { TelefonoModel } from './Telefono.model';
import { EmpleosModel } from './Empleos.model';
import { InfoClienteModel } from './InfoCliente.model';
import { InfoEmpleosContactoModel } from './InfoEmpleosContacto.model';
import { DatosEmpleoModel } from './DatosEmpleo.model';
export class ConyugeModel{
    contacto:ContactoModel =new ContactoModel();
    // conyugeContacto: null
    // domicilioContacto: null
    // infoEmpleosContacto: [{…}]
     infoINE:INEModel= new INEModel();
     telefonosContacto: TelefonoModel[]= new Array();
     infoEmpleosContacto:DatosEmpleoModel[] = new Array<DatosEmpleoModel>();
}