export class DomicilioClienteRequestModel {

    idSolicitud: number;
    idSucursal: number;
    idCiudad: number;
    colonia: string;
    codigoPostal: string;
    calle: string;
    numeroExterior: string;
    numeroInterior: string;
    codigoSepomex: number;
    referencias: string;
    geolocalizacion: string;
    idTipoDomicilio: number;
    idTipoRelacionCasaFamiliar: number;
    tiempoResidencia: number;
    idTipoCondicionDomicilio: number;
    importeRenta: number;
    habitantesDomicilio: number;
    propieterioDomicilio: number;
    fechaRegistro: string;
    idOrigen: number;
    agente: number;
    idDomicilio: number;
    idRelacionDomicilio: number;
    idEmpleo: number;
	idContacto: number;

    constructor(){}
}