export class EmpleoRequestModel{
	idCliente:number;
    idContacto:number;
	idSolicitud:number;
	idTipoEmpleo:number;
	antiguedad:number;
	empleo:string;
	puesto:string;
	idContactoEmpleo:number;
	ingresoMensual:number;
	nombreEmpresa:string;
	idTipoIngreso:number;
	estuvoDesempleado:boolean;
	periodoDesempleo:number;
	antiguedadEmpleoAnterior:number;
	idOrigen:number;
	agente:number;

	idEmpleo:number;
	idRelacionEmpleo:number;
}