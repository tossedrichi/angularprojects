export class DocumentoRequestModel{
    idSolicitud:number;
	idCliente:number;
	idContacto:number;
	idEmpleo:number;
	idDomicilio:number;
	idTipoDocumento:number;
	idTipoComprobanteDomicilio:number;
	idTipoComprobantePropiedad:number;
	idTipoComprobanteIngreso:number;
	idContactoIngreso:number;
	antiguedad:number;
	idPeriodicidad:number;
	streamFile:string;
	name:string;
	agente:number;
	idOrigen:number;

	idRelacionDocumento:number;
	idDocumento:number;
	idStream:string;
	respuesta:string;
}