export class ClienteModelRequest {
  idCliente: number;
  idSucursal: number;
  rfc: string;
  fechaNacimiento: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  genero: string;
  idTipoEstadoCivil: number;
  idConyuge: number;
  correo: string;
  idOrigen: number;
  agente: number;

  constructor(){

  }
}
