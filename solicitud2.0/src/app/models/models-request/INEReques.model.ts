export class INERequestModel{
    idSolicitud:number;
    streamAnverso:string;
    fileNameAnverso:string;
    fileNameReverso:string;
    streamReverso:string;
    idOrigen:number;
    agente:number;
    idCredencial:number;
    idStreamAnverso:number;
    idStreamReverso:number;
    idContacto:number;

    constructor(){}
} 