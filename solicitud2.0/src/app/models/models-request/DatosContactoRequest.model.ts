export class DatosContactoRequestModel{
    idSolicitud:number;
	idRelacionContacto:number;
	idTipoContacto:number;
	idTipoRelacionContacto:number;
	idTipoRelacionEmpleo:number;
	idTipoContactoIngreso:number;
	aportaIngresos:boolean;
	importeIngresos:number;
    idContacto:number;
    rfc:string;
	fechaNacimiento:string;
	nombres:string;
	primerApellido:string;
	segundoApellido:string;
	genero:string;
	idTipoEstadoCivil:number;
	correo:string;
	idOrigen:number;
	agente:number;

}