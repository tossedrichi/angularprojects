export class NuevaSolicitudModel {
    agente: number;
    idOrigen: number;
    idCliente: number;
    idSolicitud: number;
    idSucursal: number;
    fechaCierre: string;
    fechaRegistro: string;
    ingresoFamiliar: number;
    comentariosVentas: string;
    fechaActualizacion: string;
    numeroDependientes: number;
    numeroPersonasDomicilio: number;

    constructor() {

    }
}

