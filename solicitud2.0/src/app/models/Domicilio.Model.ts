export class DomicilioModel {
    agente: number
    calle: string
    codigoPostal: string
    codigoSepomex: number
    colonia: string
    estadoActual: string
    fechaRegistro: string
    geolocalizacion: string
    habitantesDomicilio: number
    idCiudad: number
    idDomicilio: number
    idTipoCondicionDomicilio: number
    idTipoDomicilio: number
    idTipoRelacionCasaFamiliar: number
    idZonaCredito: number
    importeRenta: number
    nombreCiudad: string
    numeroExterior: string
    numeroInterior: string
    propietarioDomicilio: number
    referencias: string
    tiempoResidencia: number

    constructor(){}
}
