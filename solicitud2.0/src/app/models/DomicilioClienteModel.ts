import { DomicilioModel } from './Domicilio.Model';
import { DocumetnosDomicilioModel } from './DocumentosDomicilioModel';
import { ContactoModel } from './Contacto.model';

export class DomicilioClienteModel {

    contactoDomicilioFamiliar:ContactoModel;
    documentosDomicilio: DocumetnosDomicilioModel[] = new Array<DocumetnosDomicilioModel>();
    domicilio: DomicilioModel = new DomicilioModel();

}