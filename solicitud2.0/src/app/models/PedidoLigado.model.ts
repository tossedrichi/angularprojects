export class PedidoLigadoModel{
   // articulosPedido: [{…}]
   // controlFacturacion: 20
   // garantias: 0
   // importePedido: 16349
   // instalaciones: 0
    nombreControl: string;
    numeroVenta: string;
    pedido: number;
    registro: string;
}