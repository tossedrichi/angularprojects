import { ArticuloModel } from './Articulo.model';

export class PedidoModel{
    idPedido:number;
    venta:number;
    articulos:ArticuloModel[];
}