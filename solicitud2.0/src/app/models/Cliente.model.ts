export class ClienteModel {
    idCliente: number;
    rfc: string;
    nacimiento: string;
    nombres: string;
    primerApellido: string;
    segundoApellido: string;
    genero: string; 
    requiereAval: boolean;
    esPasivo: boolean;
    lineaCredito: string;
    pagoRequerido: number;
    importeLineaCredito: number;
    tipoCliente:number;
    idTipoEstadoCivil: number;
    estadoCivil:string;
    idConyuge: number;
    correo: string;
    idOrigen: number;
    agente: number;
    sucursal:number
  

    constructor() {
     
    }
  }
  