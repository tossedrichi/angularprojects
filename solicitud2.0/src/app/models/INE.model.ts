export class INEModel{
    agente:number;
    estadoActual: string;
    fechaRegistro: string;
    fileNameFrente:string;
    fileNameReverso: string;
    idCliente:number;
    idContacto: number;
    idCredencial: number;
    idOrigen: number;
    streamFrente: string;
    streamReverso: string;
    uiddocumentoFrente: string;
    uiddocumentoReverso:string;
}