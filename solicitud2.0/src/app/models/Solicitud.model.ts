export class SolicitudModel{
    avanceCliente: string;
    capturista: string;
    dependientes: number;
    enReparto: boolean;
    esIncobrable: boolean
    idAgente:number;
    idInvestigacion: number;
    idMotivo: number;
    idSolicitud: number;
    idSucursal: number;
    ingresoFamiliar: number;
    motivoCancelacion: string;
    registro:string;
    statusSolicitud: string;
    sucursal: string;
}