export class TiposComprobantePropiedadModel{
    id_tipo_comprobante_propiedad: number
    tipo_comprobante_propiedad: string
    valida_informacion: boolean
    estado_actual: string
}
