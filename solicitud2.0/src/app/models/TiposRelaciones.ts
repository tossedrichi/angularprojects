export class TiposRelacionesModel {
    id_tipo_relacion_contacto: number;
    tipo_relacion_contacto: string;
    valida_informacion: boolean;
    estado_actual: string;
}