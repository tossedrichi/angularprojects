export class EmpleoModel {
    agente: number
    antiguedad: number
    antiguedadEmpleoAnterior: number
    empleo: string
    estadoActual: string
    estuvoDesempleado: boolean
    fechaRegistro: string
    idContactoEmpleo: number
    idEmpleo: number
    idOrigen: number
    idTipoEmpleo: number
    idTipoIngreso: number
    idTipoEmpleoInformal: number
    ingresoMensual: number
    nombreEmpresa: string
    otrosIngresos: number
    periodoDesempleo: number
    puesto: string
    tipoIngreso: string

    constructor() { }
}