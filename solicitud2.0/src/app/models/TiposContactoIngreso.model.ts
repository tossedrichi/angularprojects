export class TipoContactoIngresosModel{
    id_tipo_contacto_ingreso: number
    tipo_contacto_ingreso: string
    valida_informacion: boolean
    estado_actual: string
}