export class TiposDocumentosModel {
  id_tipo_documento: number;
  tipo_documento: string;
  valida_informacion: boolean;
  estado_actual: string;
}
