import { EmpleoModel } from './Empleo.model';
import { TelefonoEmpleoModel } from './TelefonoEmpleo.model';
export class InfoEmpleosContactoModel {
    contactoEmpleo
    documentosEmpleo
    domicilioEmpleo
    empleo:EmpleoModel = new EmpleoModel()
    telefonosEmpleo:TelefonoEmpleoModel[] = new Array<TelefonoEmpleoModel>();

    constructor(){}

}