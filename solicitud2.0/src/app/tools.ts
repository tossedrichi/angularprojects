import RfcFacil from "rfc-facil";

/*import * as piexif from 'piexifjs'

 function fixName(name: string): string {
  let nameArr = name.split(",");
  name = `${nameArr[0]} ${nameArr[1]}`;
  nameArr = name.split("(");
  nameArr = nameArr[0].split("(");
  name = nameArr[0].toString().trim();
  return name;
}


function getOrinetation(src): string {
  try {
    let exifObjc = piexif.load(src);
    console.log(exifObjc);

    let th = exifObjc['0th'];
    let orientacion = th['274'];

    console.log(orientacion);
    return orientacion
  } catch{
    return undefined;
  }

}

function resetOrientation(srcBase64, srcOrientation, callback) {
  var img = new Image();

  img.onload = function () {
    var width = img.width,
      height = img.height,
      canvas = document.createElement('canvas'),
      ctx = canvas.getContext("2d");

    // set proper canvas dimensions before transform & export
    if (4 < srcOrientation && srcOrientation < 9) {
      canvas.width = height;
      canvas.height = width;
    } else {
      canvas.width = width;
      canvas.height = height;
    }

    // transform context before drawing image
    switch (srcOrientation) {
      case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
      case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
      case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
      case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
      case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
      case 7: ctx.transform(0, -1, -1, 0, height, width); break;
      case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
      default: break;
    }

    // draw image
    ctx.drawImage(img, 0, 0);

    // export base64
    callback(canvas.toDataURL());
  };

  img.src = srcBase64;
};
 */

const INEFix = "data:image/jpeg;base64,";

function generarRFC(
  nombre: string,
  apellidoP: string,
  apellidoM: string,
  fechaNacimiento: string
): string {
  console.log(fechaNacimiento);
  let ano: number = parseInt(fechaNacimiento.split("-")[0]);
  let mes: number = parseInt(fechaNacimiento.split("-")[1]);
  let dia: number = parseInt(fechaNacimiento.split("-")[2]);

  let rfc: string = RfcFacil.forNaturalPerson({
    name: nombre,
    firstLastName: apellidoP,
    secondLastName: apellidoM,
    day: dia,
    month: mes,
    year: ano
  });
  console.log("RFC generado: ", rfc);
  return rfc;
}

function getAnos(tiempo: number): number {
  //let tiempo = this.domicilio.tiempoResidencia;

  let anos: number = tiempo / 12;
  console.log("Años", anos);

  return Math.trunc(anos);
}

function getMeses(tiempo: number): number {
  //let tiempo = this.domicilio.tiempoResidencia;

  let meses = tiempo % 12;
  return Math.trunc(meses);
}

function calcularTiempoMeses(anos: any, meses: any): number {
  if (isNaN(anos)) {
    anos = 0;
  }

  if (isNaN(meses)) {
    meses = 0;
  }

  let tiempo: number = anos * 12 + meses;

  return tiempo;
}

function renderImage(base64: string): string {
  if (base64.includes("base64")) {
    return base64;
  } else {
    return INEFix + base64;
  }
}

function fixToSendImage(base64: string): string {
  if (base64.includes("base64")) {
    return base64.split("base64,")[1];
  } else {
    return base64;
  }
}

function getFechaActual(): string {
  let date: Date = new Date();
  let fechaActual: string =
    date.getFullYear().toString() +
    "-" +
    (date.getMonth() + 1).toString() +
    "-" +
    date.getDate().toString();

  return fechaActual;
}

function toastWarning() {
  `<div class="alert alert-warning alert-dismissible fade show" style="max-width: 20%" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>`;
}

export {
  //fixName,
  //getOrinetation,
  //resetOrientation,
  toastWarning,
  generarRFC,
  INEFix,
  getAnos,
  getMeses,
  calcularTiempoMeses,
  renderImage,
  getFechaActual,
  fixToSendImage
};
