import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { APP_ROUTING } from "./app.routes";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { SideBarComponent } from "./components/side-bar/side-bar.component";
import { MaterialModule } from "./material.module";
import { DatosClienteComponent } from "./pages/datos-cliente/datos-cliente.component";
import { DatosConyugeComponent } from "./pages/datos-conyuge/datos-conyuge.component";
import { DomicilioClienteComponent } from "./pages/domicilio-cliente/domicilio-cliente.component";
import { DatosEmpleoClienteComponent } from "./pages/datos-empleo-cliente/datos-empleo-cliente.component";
import { DatosEmpleoConyugeComponent } from "./pages/datos-empleo-conyuge/datos-empleo-conyuge.component";
import { DatosAvalComponent } from "./pages/datos-aval/datos-aval.component";
import { DatosConyugeAvalComponent } from "./pages/datos-conyuge-aval/datos-conyuge-aval.component";
import { DocumentosComponent } from "./pages/documentos/documentos.component";
import { ModalDomicilioEmpleoComponent } from "./pages/datos-empleo-cliente/modal-domicilio-empleo/modal-domicilio-empleo.component";
import { HttpClientModule } from "@angular/common/http";
import { ModalCasaRentaComponent } from "./pages/domicilio-cliente/modal-casa-renta/modal-casa-renta.component";
import { ModalCasaFamiliarComponent } from "./pages/domicilio-cliente/modal-casa-familiar/modal-casa-familiar.component";
import { ModalDomicilioEmpleoConyugeComponent } from "./pages/datos-empleo-conyuge/modal-domicilio-empleo-conyuge/modal-domicilio-empleo-conyuge.component";
import { NgDropFilesDirective } from "./directives/ng-drop-files.directive";
import { LoginComponent } from "./pages/login/login.component";
import { SolicitudRedigitalizacionComponent } from "./pages/solicitud-redigitalizacion.component";
import { BusquedaClienteComponent } from "./pages/busqueda-cliente/busqueda-cliente.component";
import { ModalEmpleoAvalComponent } from "./pages/datos-aval/modal-empleo-aval/modal-empleo-aval.component";
import { ModalEmpleoConyugeAvalComponent } from "./pages/datos-conyuge-aval/modal-empleo-conyuge-aval/modal-empleo-conyuge-aval.component";
import { ModalPrewComponent } from "./components/modal-prew/modal-prew.component";
import { ModalDocumentosDomicilioComponent } from "./pages/domicilio-cliente/modal-documentos-domicilio/modal-documentos-domicilio.component";
import { ModalGuardarComponent } from "./components/modal-guardar/modal-guardar.component";
import { AgregaPedidoComponent } from "./pages/agrega-pedido/agrega-pedido.component";
import { ReferenciasComponent } from "./pages/referencias/referencias.component";
import { IngresosFamiliaresComponent } from "./pages/ingresos-familiares/ingresos-familiares.component";
import { DomiclioAvalComponent } from "./pages/domiclio-aval/domiclio-aval.component";
import { ModalPedidoComponent } from "./pages/agrega-pedido/modal-pedido/modal-pedido.component";
import { ModalDocumentosEmpleoComponent } from "./pages/datos-empleo-cliente/modal-documentos-empleo/modal-documentos-empleo.component";
import { ModalDocumentosEmpleoConyugeComponent } from "./pages/datos-empleo-conyuge/modal-documentos-empleo-conyuge/modal-documentos-empleo-conyuge.component";
import { NgDropSingleDirective } from "./directives/ng-drop-single-file.directive";

//TOASTER
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SpinnerComponent } from "./components/spinner/spinner.component";
import { ModalContactoEmpleoComponent } from "./pages/datos-empleo-cliente/modal-contacto-empleo/modal-contacto-empleo.component";
import { ModalContactoEmpleoConyugeComponent } from "./pages/datos-empleo-conyuge/modal-contacto-empleo-conyuge/modal-contacto-empleo-conyuge.component";
import { FinalizarSolicitudComponent } from "./pages/finalizar-solicitud/finalizar-solicitud.component";

import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { ModalDocumentosAvalComponent } from "./pages/domiclio-aval/modal-documentos-aval/modal-documentos.component-aval";
import { ModalDomicilioRefComponent } from './pages/referencias/modal-domicilio-ref/modal-domicilio-ref.component';
import { DatosEmpleoAvalComponent } from './pages/datos-empleo-aval/datos-empleo-aval.component';

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    DatosClienteComponent,
    DatosConyugeComponent,
    DomicilioClienteComponent,
    DatosEmpleoClienteComponent,
    DatosEmpleoConyugeComponent,
    DatosAvalComponent,
    DatosConyugeAvalComponent,
    DocumentosComponent,
    ModalDomicilioEmpleoComponent,
    ModalCasaRentaComponent,
    ModalCasaFamiliarComponent,
    ModalDomicilioEmpleoConyugeComponent,
    NgDropFilesDirective,
    NgDropSingleDirective,
    LoginComponent,
    SolicitudRedigitalizacionComponent,
    BusquedaClienteComponent,
    ModalEmpleoAvalComponent,
    ModalEmpleoConyugeAvalComponent,
    ModalPrewComponent,
    ModalDocumentosEmpleoComponent,
    ModalDocumentosDomicilioComponent,
    ModalGuardarComponent,
    AgregaPedidoComponent,
    ReferenciasComponent,
    IngresosFamiliaresComponent,
    DomiclioAvalComponent,
    ModalPedidoComponent,
    ModalDocumentosEmpleoConyugeComponent,
    SpinnerComponent,
    ModalContactoEmpleoComponent,
    ModalContactoEmpleoConyugeComponent,
    FinalizarSolicitudComponent,
    ModalDocumentosAvalComponent,
    ModalDomicilioRefComponent,
    DatosEmpleoAvalComponent
  ],

  imports: [
    BrowserModule,
    MaterialModule,
    APP_ROUTING,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AutocompleteLibModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      tapToDismiss: true,
      closeButton: true,
      //progressBar: true,
      //progressAnimation:'decreasing',
      positionClass: "toast-top-right"
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
