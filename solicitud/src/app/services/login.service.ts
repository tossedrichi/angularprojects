import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LoginModel } from '../models/login.model';

const urlBaseLogin ="http://microstest.gruporoga.com:2021/rogaservices/security/token/login";

const headers = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    
  })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) {  }

  setLogin(usser: string, password:string){
    let login:LoginModel = new LoginModel(usser,password);

    return this.http.post(urlBaseLogin,login,headers)
    .pipe(map(response=>{
      return <any>response;
    }))
  }
}
