import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { BusquedaComponent } from "./components/busqueda/busqueda.component";
import { AppRouting } from "./app.routes";
import { BusquedaItemComponent } from "./components/busqueda/busqueda-item/busqueda-item.component";
import { SolicitudComponent } from "./components/solicitud/solicitud.component";
import { CapturaComponent } from "./components/captura/captura.component";
import { DomicilioComponent } from "./components/domicilio/domicilio.component";
import { EmpleoComponent } from "./components/empleo/empleo.component";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { DocumentosComponent } from "./components/documentos/documentos.component";
import { HttpClientModule } from "@angular/common/http";
import { InformacionclienteComponent } from "./components/informacioncliente/informacioncliente.component";
import { AgregarComponent } from "./components/agregar/agregar.component";
import { DomicilioempleoComponent } from "./components/domicilioempleo/domicilioempleo.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { ReactiveFormsModule } from "@angular/forms";
/* import { MatDatepickerModule } from "@angular/material/datepicker";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatNativeDateModule } from "@angular/material/core";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material"; */

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BusquedaComponent,
    BusquedaItemComponent,
    SolicitudComponent,
    CapturaComponent,
    DomicilioComponent,
    EmpleoComponent,
    ContactoComponent,
    DocumentosComponent,
    InformacionclienteComponent,
    AgregarComponent,
    DomicilioempleoComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    HttpClientModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
/*     MatDatepickerModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule */
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
