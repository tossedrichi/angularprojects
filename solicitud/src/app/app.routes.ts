import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { BusquedaComponent } from './components/busqueda/busqueda.component';
import {SolicitudComponent} from './components/solicitud/solicitud.component';
import { from } from 'rxjs';
import { CapturaComponent } from './components/captura/captura.component';
import { DomicilioComponent } from './components/domicilio/domicilio.component';
import { EmpleoComponent } from './components/empleo/empleo.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { InformacionclienteComponent} from './components/informacioncliente/informacioncliente.component'
import { AgregarComponent } from './components/agregar/agregar.component';
import { DomicilioempleoComponent } from './components/domicilioempleo/domicilioempleo.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'busqueda', component: BusquedaComponent},
    { path: 'solicitud', component: SolicitudComponent},
    {path: 'captura', component: CapturaComponent},
    {path: 'domicilio', component: DomicilioComponent},
    {path: 'empleo', component: EmpleoComponent},
    {path: 'contacto', component: ContactoComponent},
    {path: 'informacioncliente/:fecha', component: InformacionclienteComponent},
    {path: 'agregar', component: AgregarComponent},
    {path: 'domicilioempleo', component: DomicilioempleoComponent},

    {path:"**", pathMatch: "full", redirectTo:"login"}
];

export const AppRouting = RouterModule.forRoot(routes, {useHash: true});
