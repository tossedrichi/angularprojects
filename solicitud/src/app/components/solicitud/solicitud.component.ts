import { Component, OnInit } from '@angular/core';
import {SolicitudService} from '../../services/solicitud.service'
import { from } from 'rxjs';
import { EdoCivilModel } from 'src/app/models/EdoCivil.model';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';
import { TiposContactoModel } from 'src/app/models/TiposContacto.model';
@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.css']
})
export class SolicitudComponent implements OnInit {
  mostrarTipoDocumento: boolean = false;
  mostrarGuardarDocumentos: boolean = false;
  mostrarGuardarSolicitud: boolean = false;
  mostrarTipoContacto: boolean = false;

  listEstadoCivil:EdoCivilModel[]=[];
  listTiposDocumentos:TiposDocumentosModel[]=[];
  listTiposContacto: TiposContactoModel[]=[];

  constructor(private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.getEstadoCivil()
    this.getTiposDocumentos()
    this.getTiposContacto()
  }

  getEstadoCivil(){
      this._solicitudServices.getEstadoCivil().subscribe(respuesta =>{
        console.log(respuesta);
        this.listEstadoCivil=respuesta;
        
      })
   }

   getTiposDocumentos(){
    this._solicitudServices.getTiposDocumentos().subscribe(respuesta =>{
      console.log(respuesta);
      this.listTiposDocumentos=respuesta;
      
    })
 }

 getTiposContacto(){
  this._solicitudServices.getTiposContacto().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposContacto=respuesta;
    
  })
}


}
