import { Component, OnInit } from '@angular/core';
import { EdoCivilModel } from 'src/app/models/EdoCivil.model';
import { SolicitudService } from 'src/app/services/solicitud.service';
import { TiposClienteModel } from 'src/app/models/TiposCliente.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router,ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-informacioncliente',
  templateUrl: './informacioncliente.component.html',
  styleUrls: ['./informacioncliente.component.css']
})
export class InformacionclienteComponent implements OnInit {
  
  
  fechcaRegistro:string;
  estadoCivil:EdoCivilModel;
  mostrarConyuge: boolean = false; 
  
  listEstadoCivil:EdoCivilModel[]=[];
  listTiposCliente: TiposClienteModel[]=[];
  formularioCliente : FormGroup;

  constructor(private route: Router,private activteRoute:ActivatedRoute,  private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.getEstadoCivil()
    this.getTiposCliente()
    this.inicializarFormulario()

    this.fechcaRegistro=this.activteRoute.snapshot.paramMap.get("fecha");
    console.log("fecha",this.fechcaRegistro);
    

  }
  inicializarFormulario(){
    this.formularioCliente = new FormGroup({
      textNombreCliente: new FormControl("",Validators.required),
      textApellidoPaterno: new FormControl("",Validators.required),
      textApellidoMaterno: new FormControl("",Validators.required),
      textFechaNacimiento: new FormControl("",Validators.required),
      textRFC: new FormControl("",Validators.required),
      textTelefonoCasa: new FormControl("",Validators.required),
      textTelefonoCelular: new FormControl("",Validators.required),
      selectEdoCivil: new FormControl(),
      radioGenero: new FormControl()
    })
  }
  selectEstadoCivil(value) {
    console.log(value);
    
    switch (parseInt(value)) {

      case 1:      
      this.mostrarConyuge = true;
      break;
      case 2:      
      this.mostrarConyuge = false;

      break;
      case 3:      
      this.mostrarConyuge = false;
      break;

      case 4:      
      this.mostrarConyuge = false;
      break;

    
    }

  }

  getEstadoCivil(){
    this._solicitudServices.getEstadoCivil().subscribe(respuesta =>{
      console.log(respuesta);
      this.listEstadoCivil=respuesta;
    })
 }

 getTiposCliente(){
  this._solicitudServices.getTiposCliente().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposCliente=respuesta;
    
  })
}
enviarInfoCliente(){
  this.formularioCliente.valid
  console.log(this.formularioCliente.valid)
  console.log(this.formularioCliente)
  console.log(this.estadoCivil);

  if(this.formularioCliente.valid  ){
    //llamar microservicio para enviar
    this.route.navigate(["/agregar"])
  }
}

}
