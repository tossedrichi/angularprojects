import { Component, OnInit } from '@angular/core';
import { EstadosModel } from 'src/app/models/Estados.model';
import { SolicitudService } from 'src/app/services/solicitud.service';
import { CiudadesModel } from 'src/app/models/Ciudades.model';
import { ColoniasModel } from 'src/app/models/Colonias.model';
import { TiposDomiciliosModel } from 'src/app/models/TiposDomicilio.model';
import { RelacionCasaFamiliarModel } from 'src/app/models/RelacionCasaFamiliar.model';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';
import { CondicionesModel } from 'src/app/models/CondicionesDomicilio.model';
import { TiposComprobanteDomicilioModel } from 'src/app/models/TiposComprobanteDomicilio.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {Router} from '@angular/router';


@Component({
  selector: 'app-domicilio',
  templateUrl: './domicilio.component.html',
  styleUrls: ['./domicilio.component.css']
})
export class DomicilioComponent implements OnInit {
mostrarGuardar: boolean = false;
mostrarCasaFamiliar: boolean = false;
mostrarGuardarCasa: boolean = false;
mostrarMonto: boolean = false;
mostrarTipoDocumento: boolean = false;
mostrarGuardarDocumentos: boolean = false;


listEstados:EstadosModel[]=[];
listCiudades: CiudadesModel[]=[];
listColonias: ColoniasModel[]=[];
listTiposDomicilio: TiposDomiciliosModel[]=[];
listTipoRelacionesCasaFam: RelacionCasaFamiliarModel[]=[];
listTiposDocumentos:TiposDocumentosModel[]=[];
listCondiciones:CondicionesModel[]=[];
listComprobanteDomicilio: TiposComprobanteDomicilioModel[]=[];


idEstadoSeleccionado: number
idCiudadSeleccionado : number
cp:number;

formularioDomicilioCliente : FormGroup;



myControl = new FormControl();
options: string[] = ['One', 'Two', 'Three'];
filteredOptions: Observable<string[]>;

constructor(private route: Router, private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.getEstados()
   // this.getCiudades(this.idEstadoSeleccionado)
this.getTiposDomicilios()
this.getTiposRelacionesCasaFam()
this.getTiposDocumentos()
this.getCondiciones()
this.getComprobanteDomicilio()
this.inicializarFormulario()


this.filteredOptions = this.myControl.valueChanges.pipe(
  startWith(''),
  map(value => this._filter(value))
);


  }

  inicializarFormulario(){
    this.formularioDomicilioCliente = new FormGroup({
      textCalle: new FormControl("",Validators.required),
      textNumero: new FormControl("",Validators.required),
      textAños: new FormControl("",Validators.required),
      textMeses: new FormControl("",Validators.required),
      textPersonas: new FormControl("",Validators.required),
      textReferencias: new FormControl("",Validators.required),

    })
  }
  showCp(colonia:ColoniasModel){
    
    this.cp=colonia.codigo_postal;    
  }
  private _filter(value: string): any {
    const filterValue = value.toLowerCase();

    return this.listColonias.filter(colonia => colonia.nombre.toLowerCase().indexOf(filterValue) === 0);
  }

  selectTipoDomiclio(value) {
    console.log(value);
    
    switch (parseInt(value)) {

      case 1:      
      this.mostrarMonto = false;
      break;
      case 2:      
      this.mostrarMonto = false;
      this.mostrarCasaFamiliar = true;
      break;
      case 3:      
      this.mostrarMonto = true;
      break;

      case 4:      
      this.mostrarMonto = false;
      break;

    
    }

  }



  getEstados(){
    this._solicitudServices.getEstados().subscribe(respuesta =>{
      console.log(respuesta);
      this.listEstados=respuesta;
      this.listCiudades=[]
      this.listColonias=[]
      
    })
 }

 getCiudades(estado){
  this._solicitudServices.getCiudades(estado).subscribe(respuesta =>{
    console.log(respuesta);
    this.listCiudades=respuesta;
    this.listColonias=[]
    
  })
}

getColonias(ciudad){
  this._solicitudServices.getColonias(ciudad).subscribe(respuesta =>{
    console.log(respuesta);
    this.listColonias=respuesta;
    
  })
}

changeCP(cp){
  this.cp=cp;

}

getTiposDomicilios(){
  this._solicitudServices.getTiposDomicilios().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposDomicilio=respuesta;
    
  })
}
getTiposRelacionesCasaFam(){
  this._solicitudServices.getTiposRelacionesCasaFam().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTipoRelacionesCasaFam=respuesta;
    
  })
}
getTiposDocumentos(){
  this._solicitudServices.getTiposDocumentos().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposDocumentos=respuesta;
    
  })
}
getCondiciones(){
  this._solicitudServices.getCondiciones().subscribe(respuesta =>{
    console.log(respuesta);
    this.listCondiciones=respuesta;
    
  })
  
}
getComprobanteDomicilio(){
  this._solicitudServices.getTiposComprobanteDomiclio().subscribe(respuesta =>{
    console.log(respuesta);
    this.listComprobanteDomicilio=respuesta;
    
  })


}

validarFormulario(){
  console.log(this.formularioDomicilioCliente.valid)

  if(this.formularioDomicilioCliente.valid){
    //llamar microservicio para enviar
    this.mostrarGuardar = true;
 }
}

enviarDomicilioCliente(){
  this.formularioDomicilioCliente.valid
  console.log(this.formularioDomicilioCliente.valid)

 
}
}
