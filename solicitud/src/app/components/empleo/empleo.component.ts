import { Component, OnInit } from '@angular/core';
import { SolicitudService } from 'src/app/services/solicitud.service';
import { EmpleosModel } from 'src/app/models/Empleos.model';
import { TiposComprobanteIngresosModel } from 'src/app/models/TiposComprobanteIngresos.model';
import { map, startWith } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-empleo',
  templateUrl: './empleo.component.html',
  styleUrls: ['./empleo.component.css']
})
export class EmpleoComponent implements OnInit {
  mostrarGuardar: boolean = false;
  mostrarTipoDocumento: boolean = false;
  mostrarGuardarDocumentos: boolean = false;

  listEmpleos: EmpleosModel[]=[];
  listComprobanteIngresos: TiposComprobanteIngresosModel[]=[];
 
  formularioEmpleoCliente : FormGroup;



myControl = new FormControl();
options: string[] = ['One', 'Two', 'Three'];
filteredOptions: Observable<string[]>;

  constructor(private route: Router,private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.inicializarFormulario()
    this.getEmpleos()
    this.getComprobanteIngresos()


    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

  }


  inicializarFormulario(){
    this.formularioEmpleoCliente = new FormGroup({
      textEmpresa: new FormControl("",Validators.required),
      textEmpleo: new FormControl("",Validators.required),
      textPuesto: new FormControl("",Validators.required),
      textAños: new FormControl("",Validators.required),
      textMeses: new FormControl("",Validators.required),
      textIngreso: new FormControl("",Validators.required),
      textTelefono: new FormControl("",Validators.required),

    })
  }

  private _filter(value: string): any {
    const filterValue = value.toLowerCase();

    return this;
  }

  getEmpleos(){
    this._solicitudServices.getEmpleos().subscribe(respuesta =>{
      console.log(respuesta);
      this.listEmpleos=respuesta;
      
    })



 }
 getComprobanteIngresos(){
  this._solicitudServices.getComprobanteIngresos().subscribe(respuesta =>{
    console.log(respuesta);
    this.listComprobanteIngresos=respuesta;
    
  })
 }

 validarFormulario(){
  console.log(this.formularioEmpleoCliente.valid)

  if(this.formularioEmpleoCliente.valid){
    //llamar microservicio para enviar
    this.mostrarGuardar = true;
 }
}

enviarEmpleoCliente(){
  this.formularioEmpleoCliente.valid
  console.log(this.formularioEmpleoCliente.valid)

 
}
}
