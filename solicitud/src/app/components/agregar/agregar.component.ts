import { Component, OnInit } from '@angular/core';
import { EdoCivilModel } from 'src/app/models/EdoCivil.model';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';
import { TiposContactoModel } from 'src/app/models/TiposContacto.model';
import { SolicitudService } from 'src/app/services/solicitud.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  mostrarTipoDocumento: boolean = false;
  mostrarGuardarDocumentos: boolean = false;
  mostrarGuardarSolicitud: boolean = false;
  mostrarTipoContacto: boolean = false;

  listEstadoCivil:EdoCivilModel[]=[];
  listTiposDocumentos:TiposDocumentosModel[]=[];
  listTiposContacto: TiposContactoModel[]=[];

  constructor(private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.getEstadoCivil()
    this.getTiposDocumentos()
    this.getTiposContacto()
  }

  getEstadoCivil(){
      this._solicitudServices.getEstadoCivil().subscribe(respuesta =>{
        console.log(respuesta);
        this.listEstadoCivil=respuesta;
        
      })
   }

   getTiposDocumentos(){
    this._solicitudServices.getTiposDocumentos().subscribe(respuesta =>{
      console.log(respuesta);
      this.listTiposDocumentos=respuesta;
      
    })
 }

 getTiposContacto(){
  this._solicitudServices.getTiposContacto().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposContacto=respuesta;
    
  })
}


}

