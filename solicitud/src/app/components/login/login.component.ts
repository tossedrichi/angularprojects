import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { toast } from 'materialize-css';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _loginService: LoginService ,private route:Router) { }

  ngOnInit() {
  }
  login(user:string, password:string){
    this._loginService.setLogin(user,password).subscribe(
      response=>{

        if(response.message== "Autenticado"){
          this.route.navigate(['busqueda']);

        }else{
          toast({html:'Usuario no autenticado',classes:'red rounded'})
        } 
        console.log(response);
      }
    )
  }

}
