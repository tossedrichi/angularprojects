import { Component, OnInit } from '@angular/core';
import { EstadosModel } from 'src/app/models/Estados.model';
import { SolicitudService } from 'src/app/services/solicitud.service';
import { CiudadesModel } from 'src/app/models/Ciudades.model';
import { ColoniasModel } from 'src/app/models/Colonias.model';
import { TiposDomiciliosModel } from 'src/app/models/TiposDomicilio.model';
import { RelacionCasaFamiliarModel } from 'src/app/models/RelacionCasaFamiliar.model';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';
import { CondicionesModel } from 'src/app/models/CondicionesDomicilio.model';
import { TiposComprobanteDomicilioModel } from 'src/app/models/TiposComprobanteDomicilio.model';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';


@Component({
  selector: 'app-domicilioempleo',
  templateUrl: './domicilioempleo.component.html',
  styleUrls: ['./domicilioempleo.component.css']
})
export class DomicilioempleoComponent implements OnInit {
  mostrarGuardar: boolean = false;
  mostrarGuardarDocumentos: boolean = false;
  
  
  listEstados:EstadosModel[]=[];
  listCiudades: CiudadesModel[]=[];
  listColonias: ColoniasModel[]=[];
 
  listTiposDocumentos:TiposDocumentosModel[]=[];

  
  
  idEstadoSeleccionado: number
  idCiudadSeleccionado : number
  cp:number;
  
  myControl = new FormControl();
options: string[] = ['One', 'Two', 'Three'];
filteredOptions: Observable<string[]>;

  constructor(private _solicitudServices: SolicitudService) { }
  
    ngOnInit() {
      this.getEstados()
      this.getTiposDocumentos()
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      
      
        }
        showCp(colonia:ColoniasModel){
          
          this.cp=colonia.codigo_postal;    
        }
        private _filter(value: string): any {
          const filterValue = value.toLowerCase();
      
          return this.listColonias.filter(colonia => colonia.nombre.toLowerCase().indexOf(filterValue) === 0);
        }
  
    getEstados(){
      this._solicitudServices.getEstados().subscribe(respuesta =>{
        console.log(respuesta);
        this.listEstados=respuesta;
        this.listCiudades=[]
        this.listColonias=[]
        
      })
   }
  
   getCiudades(estado){
    this._solicitudServices.getCiudades(estado).subscribe(respuesta =>{
      console.log(respuesta);
      this.listCiudades=respuesta;
      this.listColonias=[]
      
    })
  }
  
  getColonias(ciudad){
    this._solicitudServices.getColonias(ciudad).subscribe(respuesta =>{
      console.log(respuesta);
      this.listColonias=respuesta;
      
    })
  }
  
  changeCP(cp){
    this.cp=cp;
  
  }
  
  getTiposDocumentos(){
    this._solicitudServices.getTiposDocumentos().subscribe(respuesta =>{
      console.log(respuesta);
      this.listTiposDocumentos=respuesta;
      
    })
  
  
  }
  
  
  }