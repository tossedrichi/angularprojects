import { Component, OnInit } from '@angular/core';
import { EdoCivilModel } from 'src/app/models/EdoCivil.model';
import { SolicitudService } from 'src/app/services/solicitud.service';
import { TiposDocumentosModel } from 'src/app/models/TiposDocumentos.model';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  mostrarTipoDocumento: boolean = false;


  listEstadoCivil:EdoCivilModel[]=[];
  listTiposDocumentos:TiposDocumentosModel[]=[];


  constructor(private _solicitudServices: SolicitudService) { }

  ngOnInit() {
    this.getEstadoCivil()
    this.getTiposDocumentos()

  }
  getEstadoCivil(){
    this._solicitudServices.getEstadoCivil().subscribe(respuesta =>{
      console.log(respuesta);
      this.listEstadoCivil=respuesta;
      
    })
 }


 getTiposDocumentos(){
  this._solicitudServices.getTiposDocumentos().subscribe(respuesta =>{
    console.log(respuesta);
    this.listTiposDocumentos=respuesta;
    
  })
}
}
