import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {
  mostrarCabecera: boolean = false;

  constructor(private route:Router) { }
   fecha:string="";
  ngOnInit() {
  }

  obtenerFecha(fecha:string){

    if(fecha){
      console.log(fecha);
      this.fecha=fecha;
    }
    
  }
  

  iniciarRegistro(){
    if(this.fecha!=""){
      this.route.navigate(["/informacioncliente/",this.fecha]);

    }
  }

}
