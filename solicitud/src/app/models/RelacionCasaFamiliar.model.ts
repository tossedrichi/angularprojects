export class RelacionCasaFamiliarModel{
    id_tipo_relacion_casa_familiar: number
    tipo_relacion_casa_familiar: string
    valida_informacion: boolean
    estado_actual: string
}