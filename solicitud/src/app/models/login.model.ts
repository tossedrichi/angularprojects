export class LoginModel {
  userId: string;
  userPass: string;
  userProfile: string;
  macAddress: string;
  appId: string;

  constructor(userId, userPass) {
    this.userId = userId;
    this.userPass = userPass;
    this.userProfile = "1";
    this.macAddress = "00-14-22-01-23-45";
    this.appId = "1";
  }
}
